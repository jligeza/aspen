const ParcelProxyServer = require('parcel-proxy-server')

const server = new ParcelProxyServer({
  entryPoint: './index.html',
  parcelOptions: {
  },
  proxies: {
    '/api': {
      target: 'http://localhost:4000/'
    }
  }
})

// the underlying parcel bundler is exposed on the server
// and can be used if needed
server.bundler.on('buildEnd', () => {
  console.log('Build completed!')
})

// start up the server
const port = 8088
server.listen(port, () => {
  console.log('Parcel proxy server has started, port', port)
})
