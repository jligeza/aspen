Aspen - visual programming platform
===================================

To run Aspen, you need to have Elm 0.19 installed globally (npm install -g elm).

###### Development:
- `npm install`
- `npm start`

After that, a web browser will be opened @ `localhost:8080`.
The `elm-live` will reload the app if any changes to the code are made.

The backend is expected to be running @ `localhost:4000`.

##### Build
- `npm install`
- `npm run build`

The files will be placed in the `./dist` catalog.
