module Setters exposing
    ( setConstants
    , setConstantsCreator
    , setConstantsCreatorTree
    , setEventGenerators
    , setLinkEditor
    , setLinkEditorTree
    , setLinkEditorTrees
    , setLinks
    , setNameTags
    , setNewNode
    , setNewNodeData
    , setNewNodeTree
    , setTemplates
    , setTree
    , setTypeCreation
    , setTypes
    , setTypesSelection
    , setUnsavedLinks
    )

import List.Extra as EList


setTree fn m =
    { m | tree = fn m.tree }


setLinkEditorTree idx fn m =
    setLinkEditorTrees (\trees -> EList.updateAt idx fn trees) m


setLinkEditorTrees fn m =
    setLinkEditor (Maybe.map (\a -> { a | trees = fn a.trees })) m


setLinkEditor fn m =
    { m | linkEditor = fn m.linkEditor }


setLinks fn m =
    { m | links = fn m.links }


setConstants fn m =
    { m | constants = fn m.constants }


setConstantsCreatorTree fn m =
    setConstantsCreator (\a -> { a | tree = fn a.tree }) m


setConstantsCreator fn m =
    { m | constantsCreator = fn m.constantsCreator }


setTypes fn m =
    { m | types = fn m.types }


setTypesSelection fn m =
    setTypes (\t -> { t | selected = fn t.selected }) m


setNameTags fn m =
    { m | nameTags = fn m.nameTags }


setTypeCreation fn m =
    { m | typeCreation = fn m.typeCreation }


setEventGenerators fn m =
    { m | eventGenerators = fn m.eventGenerators }


setTemplates fn m =
    { m | templates = fn m.templates }


setNewNodeTree fn m =
    setNewNode (Maybe.map (\a -> { a | tree = fn a.tree })) m


setNewNode fn m =
    { m | newNode = fn m.newNode }


setNewNodeData fn m =
    setNewNodeTree (\a -> { a | data = fn a.data }) m


setUnsavedLinks fn m =
    { m | unsavedLinks = fn m.unsavedLinks }
