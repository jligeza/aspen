module Colors exposing (Colors, init)

import Color as C exposing (Color, hsla, rgb, toHsl, toRgb)


type alias Colors =
    { text : String
    , primary : String
    , primaryD1 : String
    , primaryD2 : String
    , primaryD3 : String
    , primaryL1 : String
    , primaryL2 : String
    , primaryL3 : String
    , secondary : String
    , secondaryD1 : String
    , secondaryD2 : String
    , secondaryD3 : String
    , secondaryL1 : String
    , secondaryL2 : String
    , secondaryL3 : String
    , schemaBool : String
    , schemaFloat : String
    , schemaFunction : String
    , schemaInt : String
    , schemaList : String
    , schemaRef : String
    , schemaString : String
    , schemaUnion : String
    , schemaJson : String
    , error : String
    , warning : String
    }


init : Colors
init =
    { text = txt1 |> toCss
    , primary = primary |> toCss
    , primaryD1 = primary |> darken 0.1 |> toCss
    , primaryD2 = primary |> darken 0.2 |> toCss
    , primaryD3 = primary |> darken 0.3 |> toCss
    , primaryL1 = primary |> lighten 0.1 |> toCss
    , primaryL2 = primary |> lighten 0.2 |> toCss
    , primaryL3 = primary |> lighten 0.3 |> toCss
    , secondary = secondary |> toCss
    , secondaryD1 = secondary |> darken 0.1 |> toCss
    , secondaryD2 = secondary |> darken 0.2 |> toCss
    , secondaryD3 = secondary |> darken 0.3 |> toCss
    , secondaryL1 = secondary |> lighten 0.1 |> toCss
    , secondaryL2 = secondary |> lighten 0.2 |> toCss
    , secondaryL3 = secondary |> lighten 0.3 |> toCss
    , schemaBool = C.yellow |> toCss
    , schemaFloat = C.yellow |> toCss
    , schemaFunction = C.blue |> lighten 0.3 |> toCss
    , schemaInt = C.yellow |> toCss
    , schemaList = C.green |> toCss
    , schemaRef = C.purple |> lighten 0.3 |> toCss
    , schemaString = C.yellow |> toCss
    , schemaUnion = C.white |> toCss
    , schemaJson = C.lightBrown |> toCss
    , error = rgb 255 0 0 |> toCss
    , warning = C.yellow |> toCss
    }


primary =
    rgb 50 63 80


secondary =
    C.green


txt1 =
    rgb 237 103 78



-- HELPERS


darken : Float -> Color -> Color
darken offset cl =
    let
        { hue, saturation, lightness, alpha } =
            toHsl cl
    in
    hsla hue saturation (clamp 0 1 (lightness - offset)) alpha


lighten : Float -> Color -> Color
lighten offset cl =
    darken -offset cl


cssColorString : String -> List String -> String
cssColorString kind values =
    kind ++ "(" ++ String.join ", " values ++ ")"


colorToCssRgb : Color -> String
colorToCssRgb cl =
    let
        c =
            toRgb cl
    in
    cssColorString "rgb"
        [ String.fromInt c.red
        , String.fromInt c.green
        , String.fromInt c.blue
        ]


toCss : Color -> String
toCss =
    colorToCssRgb
