module TypeCreator exposing
    ( addGeneric
    , addRef
    , cannotAddGeneric
    , cannotAddNameTag
    , cannotExpandType
    , cannotRewordType
    , createTypeButtons
    , editNameTag
    , expandType
    , rewordType
    , setDefaultRewordTxt
    , typeCreationBaseTypesButtons
    , typeCreator
    )

import ActionButtons.Definitions exposing (..)
import DbTypes exposing (IdRefSchema, Schema(..), UnionMember(..), UnionSchema)
import Dict
import FetchedUtils
import GenericTypes exposing (BaseType(..), KeyVal(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Keyboard exposing (Key(..))
import List.Extra as EList
import Maybe exposing (andThen, withDefault)
import Schema.Manipulation
import Schema.Tree
import Schema.Utils as SU
import Schema.Validation
import Set
import Setters exposing (setTypeCreation)
import Tree.Main
import Tree.Types as TT
import TypeCreatorUtils exposing (getTypeLabel, isRewordableSelected)
import Types exposing (..)
import Utils


cannotAddNewField : TypeCreation -> (String -> Bool) -> Bool
cannotAddNewField tc fn =
    let
        newKey =
            String.trim tc.expandText
    in
    if String.isEmpty newKey then
        True

    else
        fn newKey


cannotExpandType : TypeCreation -> TypesCollection -> Bool
cannotExpandType tc types =
    case tc.schema of
        Nothing ->
            True

        Just baseSchema ->
            case SU.getFocusedSchema tc.selected baseSchema of
                Nothing ->
                    True

                Just schema ->
                    case schema of
                        SRecord rSchema ->
                            cannotAddNewField
                                tc
                                (\newKey -> Dict.member newKey rSchema)

                        SUnion members ->
                            cannotAddNewField
                                tc
                                (\newKey ->
                                    let
                                        matchingMember =
                                            EList.find
                                                (SU.unionIsMemberMatching newKey)
                                                members
                                    in
                                    if matchingMember == Nothing then
                                        False

                                    else
                                        True
                                )

                        SIdRef ref ->
                            case EList.find (Utils.idVerMatching ref) types of
                                Nothing ->
                                    True

                                Just refedType ->
                                    let
                                        generics =
                                            SU.findGenerics refedType.schema
                                    in
                                    if Set.isEmpty generics then
                                        True

                                    else
                                        let
                                            curGenerics =
                                                Dict.keys ref.generics
                                        in
                                        if List.isEmpty curGenerics then
                                            False

                                        else
                                            List.all
                                                (\a -> Set.member a generics)
                                                curGenerics

                        _ ->
                            True


cannotAddGeneric : TypeCreation -> Bool
cannotAddGeneric tc =
    let
        label =
            String.trim tc.genericText
    in
    case String.uncons label of
        Nothing ->
            True

        Just ( firstChar, rest ) ->
            Char.toUpper firstChar == firstChar


cannotRewordType : TypeCreation -> Bool
cannotRewordType tc =
    let
        newKey =
            String.trim tc.rewordText
    in
    if String.isEmpty newKey then
        True

    else
        case EList.unconsLast tc.selected of
            Nothing ->
                not <| isRewordableSelected tc

            Just ( lastStep, initPath ) ->
                case tc.schema of
                    Nothing ->
                        True

                    Just baseSchema ->
                        case SU.getFocusedSchema initPath baseSchema of
                            Nothing ->
                                True

                            Just prevSchema ->
                                case prevSchema of
                                    SRecord attrs ->
                                        Dict.member newKey attrs

                                    _ ->
                                        not <| isRewordableSelected tc


expandType : Model -> ( Model, Cmd Msg )
expandType m =
    if cannotExpandType m.typeCreation (Result.withDefault [] m.types.items) then
        ( m, Cmd.none )

    else
        let
            tc =
                m.typeCreation

            newKey =
                String.trim tc.expandText

            newSchema =
                case tc.schema of
                    Nothing ->
                        Nothing

                    Just baseSchema ->
                        Just <|
                            Schema.Manipulation.addKey
                                newKey
                                tc.selected
                                baseSchema
        in
        ( setTypeCreation
            (\t ->
                { t
                    | schema = newSchema
                    , expandText = ""
                }
            )
            m
        , Cmd.none
        )


rewordFocused : TypeCreation -> String -> Maybe Schema
rewordFocused tc newKey =
    case tc.schema of
        Nothing ->
            Nothing

        Just baseSchema ->
            case SU.getFocusedSchema tc.selected baseSchema of
                Nothing ->
                    Nothing

                Just focused ->
                    case focused of
                        SGeneric genericTxt ->
                            Just <|
                                Schema.Manipulation.reword
                                    tc.selected
                                    genericTxt
                                    newKey
                                    baseSchema

                        SUnion members ->
                            case EList.last tc.selected of
                                Nothing ->
                                    Nothing

                                Just lastStep ->
                                    case lastStep of
                                        TT.StepKey uId _ ->
                                            Just <|
                                                Schema.Manipulation.reword
                                                    tc.selected
                                                    uId
                                                    newKey
                                                    baseSchema

                                        _ ->
                                            Nothing

                        _ ->
                            Nothing


rewordType : Model -> ( Model, Cmd Msg )
rewordType m =
    if cannotRewordType m.typeCreation then
        ( m, Cmd.none )

    else
        let
            tc =
                m.typeCreation

            newKey =
                String.trim tc.rewordText

            ( newSchema, newSelected ) =
                let
                    old =
                        ( tc.schema, tc.selected )

                    rewordFocused_ pack =
                        case pack of
                            Just ( lastStep, initPath ) ->
                                case lastStep of
                                    TT.StepKey _ kv ->
                                        ( rewordFocused tc newKey
                                        , case kv of
                                            Key ->
                                                initPath ++ [ TT.StepKey newKey kv ]

                                            Val ->
                                                tc.selected
                                        )

                                    _ ->
                                        ( rewordFocused tc newKey
                                        , tc.selected
                                        )

                            Nothing ->
                                ( rewordFocused tc newKey
                                , tc.selected
                                )
                in
                case tc.schema of
                    Nothing ->
                        old

                    Just baseSchema ->
                        case EList.unconsLast tc.selected of
                            Nothing ->
                                rewordFocused_ Nothing

                            Just ( lastStep, initPath ) ->
                                case lastStep of
                                    TT.StepKey oldKey kv ->
                                        case kv of
                                            Key ->
                                                ( Just <|
                                                    Schema.Manipulation.reword
                                                        initPath
                                                        oldKey
                                                        newKey
                                                        baseSchema
                                                , initPath ++ [ TT.StepKey newKey kv ]
                                                )

                                            Val ->
                                                rewordFocused_ <|
                                                    Just ( lastStep, initPath )

                                    _ ->
                                        rewordFocused_ <|
                                            Just ( lastStep, initPath )
        in
        ( setTypeCreation
            (\t ->
                { t
                    | schema = newSchema
                    , rewordText = ""
                    , selected = newSelected
                }
            )
            m
        , Cmd.none
        )


addGeneric : Model -> ( Model, Cmd Msg )
addGeneric m =
    if cannotAddGeneric m.typeCreation then
        ( m, Cmd.none )

    else
        let
            tc =
                m.typeCreation

            label =
                String.trim tc.genericText

            newSchema =
                case tc.schema of
                    Nothing ->
                        Just <| SGeneric tc.genericText

                    Just baseSchema ->
                        Just <|
                            Schema.Manipulation.updateSchema
                                tc.selected
                                (\schema -> SGeneric tc.genericText)
                                baseSchema
        in
        ( setTypeCreation
            (\t ->
                { t
                    | schema = newSchema
                    , genericText = ""
                }
            )
            m
        , Cmd.none
        )


addRef : Model -> Model
addRef m =
    case m.typeCreation.schema of
        Nothing ->
            case FetchedUtils.findSelectedType m of
                Nothing ->
                    m

                Just s ->
                    setTypeCreation
                        (\t ->
                            { t
                                | schema =
                                    Just <|
                                        SIdRef <|
                                            IdRefSchema s.id s.version Dict.empty
                            }
                        )
                        m

        Just baseSchema ->
            case FetchedUtils.findSelectedType m of
                Nothing ->
                    m

                Just s ->
                    setTypeCreation
                        (\t ->
                            { t
                                | schema =
                                    Just <|
                                        Schema.Manipulation.updateSchema
                                            m.typeCreation.selected
                                            (\_ ->
                                                SIdRef <|
                                                    IdRefSchema s.id s.version Dict.empty
                                            )
                                            baseSchema
                            }
                        )
                        m


cannotAddNameTag : TypeCreation -> Bool
cannotAddNameTag tc =
    EList.last tc.selected
        |> andThen
            (\lastStep ->
                case lastStep of
                    TT.StepIdx _ ->
                        Just True

                    TT.StepKey _ kv ->
                        Just <| kv == Val
            )
        |> withDefault False


editNameTag : Model -> Model
editNameTag m =
    let
        tc =
            m.typeCreation

        name =
            String.trim tc.nameTagText

        comparePaths =
            .schemaPath >> (==) tc.selected
    in
    if String.isEmpty name then
        setTypeCreation
            (\t -> { t | nameTags = Utils.removeOnce comparePaths tc.nameTags })
            m

    else
        let
            newNameTag =
                NewNameTag name tc.selected
        in
        case EList.find comparePaths tc.nameTags of
            Nothing ->
                setTypeCreation
                    (\t ->
                        { t
                            | nameTags = newNameTag :: t.nameTags
                        }
                    )
                    m

            Just duplicate ->
                setTypeCreation
                    (\t ->
                        { t
                            | nameTags =
                                Utils.updateIfOnce
                                    comparePaths
                                    (always newNameTag)
                                    tc.nameTags
                        }
                    )
                    m


setDefaultRewordTxt : TypeCreation -> TypeCreation
setDefaultRewordTxt t =
    { t
        | rewordText =
            case EList.last t.selected of
                Nothing ->
                    getTypeLabel t |> Maybe.withDefault ""

                Just lastStep ->
                    case lastStep of
                        TT.StepKey key kv ->
                            case kv of
                                Key ->
                                    key

                                Val ->
                                    getTypeLabel t |> Maybe.withDefault ""

                        _ ->
                            getTypeLabel t |> Maybe.withDefault ""
    }


typeCreator : Model -> Html Msg
typeCreator m =
    let
        baseView content =
            div
                [ style "width" "100%"
                , style "height" "100%"
                , style "overflow" "auto"
                , style "display" "flex"
                , style "align-items" "center"
                , style "justify-content" "center"
                ]
                [ content ]
    in
    case m.typeCreation.schema of
        Nothing ->
            baseView <| text "Nothingness..."

        Just schema ->
            let
                opts =
                    TT.defaultOpts
            in
            Tree.Main.view
                { selection = Just m.typeCreation.selected
                , colors = m.layout.colors
                }
                { opts
                    | onItemSelect = Just SelectTypeCreationSchema
                }
                (Schema.Tree.typeToTreeItem
                    m.layout.colors
                    (Result.withDefault Dict.empty m.nameTags)
                    []
                    schema
                )
                |> baseView


canExpandUnion : Model -> UnionSchema -> Bool
canExpandUnion m members =
    case EList.last m.typeCreation.selected of
        Nothing ->
            False

        Just lastStep ->
            case lastStep of
                TT.StepKey uId kc ->
                    case kc of
                        Val ->
                            False

                        Key ->
                            case EList.find (SU.unionMatchUid uId) members of
                                Nothing ->
                                    True

                                Just uMember ->
                                    case uMember of
                                        ShortUnionMember _ ->
                                            False

                                        _ ->
                                            True

                _ ->
                    False


isExpandDisabled : Model -> Bool
isExpandDisabled m =
    case m.typeCreation.schema of
        Nothing ->
            True

        Just baseSchema ->
            case
                SU.getFocusedSchema
                    m.typeCreation.selected
                    baseSchema
            of
                Nothing ->
                    case EList.unconsLast m.typeCreation.selected of
                        Just ( TT.StepKey k kv, initPath ) ->
                            SU.getFocusedSchema initPath baseSchema
                                |> andThen
                                    (\s ->
                                        case s of
                                            SUnion members ->
                                                EList.find
                                                    (SU.unionIsMemberMatching k)
                                                    members
                                                    |> andThen
                                                        (\member ->
                                                            case member of
                                                                ShortUnionMember _ ->
                                                                    Just False

                                                                FullUnionMember _ ->
                                                                    Just True
                                                        )

                                            _ ->
                                                Just True
                                    )
                                |> withDefault False

                        _ ->
                            True

                Just schema ->
                    case schema of
                        SRecord _ ->
                            False

                        SUnion uSchema ->
                            canExpandUnion m uSchema

                        SIdRef _ ->
                            cannotExpandType
                                m.typeCreation
                                (Result.withDefault [] m.types.items)

                        _ ->
                            True


createTypeButtons : Model -> List ActionButton
createTypeButtons m =
    let
        setBtn key =
            case key of
                Character "a" ->
                    ActionButton
                        { key = key, text = "expand" }
                        { defaultOptions
                            | action = Just <| CreateType CTAExpand
                            , isDisabled = isExpandDisabled m
                        }

                Character "f" ->
                    ActionButton
                        { key = key, text = "delete" }
                        { defaultOptions
                            | action = Just <| CreateType CTADelete
                            , isDisabled =
                                m.typeCreation.schema == Nothing
                        }

                Character "x" ->
                    ActionButton
                        { key = key, text = "reword" }
                        { defaultOptions
                            | action = Just <| CreateType CTAReword
                            , isDisabled =
                                let
                                    tc =
                                        m.typeCreation
                                in
                                case tc.schema of
                                    Nothing ->
                                        True

                                    Just schema ->
                                        case SU.getFocusedSchema tc.selected schema of
                                            Just (SGeneric _) ->
                                                False

                                            _ ->
                                                case EList.last tc.selected of
                                                    Nothing ->
                                                        not <| isRewordableSelected tc

                                                    Just step ->
                                                        case step of
                                                            TT.StepKey _ kv ->
                                                                kv
                                                                    == Val
                                                                    && (not <| isRewordableSelected tc)

                                                            _ ->
                                                                not <| isRewordableSelected tc
                        }

                Character "z" ->
                    ActionButton
                        { key = key, text = "replace" }
                        { defaultOptions
                            | action = Just <| CreateType CTAReplace
                            , isDisabled =
                                case EList.last m.typeCreation.selected of
                                    Nothing ->
                                        False

                                    Just step ->
                                        case step of
                                            TT.StepKey _ kv ->
                                                kv == Key

                                            _ ->
                                                False
                        }

                Character "q" ->
                    ActionButton
                        { key = key, text = "save" }
                        { defaultOptions
                            | action = Just <| CreateType CTASave
                            , isDisabled =
                                let
                                    hasErrors =
                                        case m.typeCreation.schema of
                                            Nothing ->
                                                False

                                            Just schema ->
                                                not <| List.isEmpty (Schema.Validation.validate schema)
                                in
                                m.typeCreation.schema
                                    == Nothing
                                    || hasErrors
                                    || List.member ARType m.loading
                        }

                Character "w" ->
                    ActionButton
                        { key = key, text = "name tag" }
                        { defaultOptions
                            | action = Just <| CreateType CTANameTag
                            , bgColor =
                                let
                                    tc =
                                        m.typeCreation
                                in
                                case
                                    EList.find
                                        (.schemaPath >> (==) tc.selected)
                                        tc.nameTags
                                of
                                    Nothing ->
                                        defaultOptions.bgColor

                                    Just _ ->
                                        SelectedBg
                            , isDisabled =
                                m.typeCreation.schema
                                    == Nothing
                                    || cannotAddNameTag m.typeCreation
                        }

                Character "r" ->
                    ActionButton
                        { key = key, text = "discard" }
                        { defaultOptions
                            | action = Just <| CreateType CTADiscard
                            , isDisabled = m.typeCreation.schema == Nothing
                        }

                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys


typeCreationBaseTypesButtons : Model -> List ActionButton
typeCreationBaseTypesButtons m =
    let
        setBtn key =
            case key of
                Character "q" ->
                    ActionButton
                        { key = key, text = "record" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTRecord
                        }

                Character "a" ->
                    ActionButton
                        { key = key, text = "list" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTList
                        }

                Character "z" ->
                    ActionButton
                        { key = key, text = "ref" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTRef
                        }

                Character "w" ->
                    ActionButton
                        { key = key, text = "union" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTUnion
                        }

                Character "s" ->
                    ActionButton
                        { key = key, text = "string" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTString
                        }

                Character "x" ->
                    ActionButton
                        { key = key, text = "generic" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTGeneric
                        }

                Character "e" ->
                    ActionButton
                        { key = key, text = "int" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTInt
                        }

                Character "d" ->
                    ActionButton
                        { key = key, text = "float" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTFloat
                        }

                Character "c" ->
                    ActionButton
                        { key = key, text = "bool" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTBool
                        }

                Character "r" ->
                    ActionButton
                        { key = key, text = "func" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTFunction
                        }

                Character "f" ->
                    ActionButton
                        { key = key, text = "json" }
                        { defaultOptions
                            | action =
                                Just <| CreateType <| CTAInsert BTJson
                        }

                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys
