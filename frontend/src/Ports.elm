port module Ports exposing
    ( blur
    , focus
    , focused
    , scrollIntoViewById
    )

import Types exposing (..)


port scrollIntoViewById : String -> Cmd msg


port blur : () -> Cmd msg


port focus : String -> Cmd msg


port focused : (Bool -> msg) -> Sub msg
