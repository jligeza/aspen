module Decoders exposing
    ( decodeActiveNodes
    , decodeBasicDbEntries
    , decodeBasicDbEntry
    , decodeConstants
    , decodeInitializedEventGenerators
    , decodeLinks
    , decodeNameTag
    , decodeNameTags
    , decodeNewConstant
    , decodeNewEventGenerator
    , decodeNewLink
    , decodeNewNode
    , decodeNode
    , decodeNodes
    , decodeSingleNameTag
    , decodeTemplates
    , decodeTypeSave
    )

import CommonData.Main exposing (CommonData(..))
import DbTypes exposing (..)
import Dict exposing (Dict)
import GenericTypes exposing (KeyVal(..), SimpleRef)
import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (..)
import Tree.Types as TT
import Types exposing (ActiveNodes, Constants, Nodes)
import Utils


type alias SomeError =
    { id : String }


type alias QueryResult a =
    { ok : Bool
    , val : a
    }


type TypeSaveResultVal
    = TSError String
    | TSSuccess BasicDbEntry


decodeQueryResult : Decoder a -> Decoder (QueryResult a)
decodeQueryResult valDecoder =
    succeed QueryResult
        |> required "ok" bool
        |> required "val" valDecoder


decodeSchema : Decoder Schema
decodeSchema =
    oneOf
        [ string |> andThen decodePrimitive
        , decodeUnion |> andThen (SUnion >> succeed)
        , lazy (\_ -> decodeIdRef) |> andThen (SIdRef >> succeed)
        , decodeFunction |> andThen (SFunction >> succeed)
        , decodeList |> andThen (SList >> succeed)
        , lazy (\_ -> dict decodeSchema) |> andThen (SRecord >> succeed)
        ]


stringToPrimitive : String -> Schema
stringToPrimitive name =
    case name of
        "String" ->
            SString

        "Bool" ->
            SBool

        "Int" ->
            SInt

        "Float" ->
            SFloat

        "Json" ->
            SJson

        _ ->
            case String.uncons name of
                Nothing ->
                    SErr name

                Just ( x, xs ) ->
                    let
                        first =
                            String.fromChar x
                    in
                    if String.toLower first == first then
                        SGeneric name

                    else
                        SErr name


decodePrimitive : String -> Decoder Schema
decodePrimitive =
    stringToPrimitive >> succeed


decodeFunction : Decoder FunctionSchema
decodeFunction =
    succeed FunctionSchema
        |> required "$input" (lazy (\_ -> decodeSchema))
        |> required "$output" (lazy (\_ -> decodeSchema))


decodeFullUnionMember : Decoder FullUnionMemberSchema
decodeFullUnionMember =
    succeed FullUnionMemberSchema
        |> required "$choice" string
        |> optional "$content" (decodeSchema |> andThen (Just >> succeed)) Nothing


decodeUnionMember : Decoder UnionMember
decodeUnionMember =
    oneOf
        [ string |> andThen (ShortUnionMember >> succeed)
        , lazy (\_ -> decodeFullUnionMember) |> andThen (FullUnionMember >> succeed)
        ]


decodeUnion : Decoder UnionSchema
decodeUnion =
    list decodeUnionMember


decodeGenerics : Decoder (Dict.Dict String Schema)
decodeGenerics =
    dict decodeSchema


decodeIdRef : Decoder IdRefSchema
decodeIdRef =
    succeed IdRefSchema
        |> required "$id" string
        |> required "$version" int
        |> optional "$generics" (decodeGenerics |> andThen succeed) Dict.empty


decodeList : Decoder ListSchema
decodeList =
    succeed ListSchema
        |> required "$valsType" (lazy (\_ -> decodeSchema))


decodeBasicDbEntry : Decoder BasicDbEntry
decodeBasicDbEntry =
    succeed BasicDbEntry
        |> required "id" string
        |> required "version" int
        |> required "schema" decodeSchema
        |> required "createdAt" string
        |> required "deletedAt" (nullable string)


decodeBasicDbEntries : Decoder (List BasicDbEntry)
decodeBasicDbEntries =
    list decodeBasicDbEntry


decodeSchemaPath : Decoder TT.Path
decodeSchemaPath =
    let
        decodePath maybePrevStrStep strStep nextStrSteps =
            let
                continue decodedStep =
                    case nextStrSteps of
                        [] ->
                            Ok [ decodedStep ]

                        x :: xs ->
                            decodePath (Just strStep) x xs
                                |> Result.andThen ((::) decodedStep >> Ok)

                keyOrVal () =
                    case nextStrSteps of
                        [] ->
                            Key

                        _ ->
                            Val
            in
            if String.startsWith "$" strStep then
                case strStep of
                    "$content" ->
                        case maybePrevStrStep of
                            Nothing ->
                                Err "unknown uId"

                            Just prevStrStep ->
                                if String.startsWith "$" prevStrStep then
                                    Err "bad uId: dollar"

                                else
                                    continue <| TT.StepKey prevStrStep (keyOrVal ())

                    "$valsType" ->
                        continue <| TT.StepKey "of" Val

                    "$input" ->
                        continue <| TT.StepKey "in" Val

                    "$output" ->
                        continue <| TT.StepKey "out" Val

                    "$generics" ->
                        case nextStrSteps of
                            [] ->
                                Err "need more steps"

                            nextStrStep :: moreStrSteps ->
                                let
                                    decodedStep =
                                        TT.StepKey nextStrStep (keyOrVal ())
                                in
                                case moreStrSteps of
                                    [] ->
                                        Ok [ decodedStep ]

                                    x :: xs ->
                                        decodePath (Just nextStrStep) x xs
                                            |> Result.andThen ((::) decodedStep >> Ok)

                    unknown ->
                        Err <| "unknown special key: " ++ unknown

            else
                continue <| TT.StepKey strStep (keyOrVal ())
    in
    list string
        |> andThen
            (\strPath ->
                case strPath of
                    [] ->
                        succeed []

                    strStep :: strSteps ->
                        case decodePath Nothing strStep strSteps of
                            Ok path ->
                                succeed path

                            Err err ->
                                fail err
            )


decodeNameTag : Decoder NameTag
decodeNameTag =
    succeed NameTag
        |> required "id" string
        |> required "version" int
        |> required "name" string
        |> required "targetId" string
        |> required "targetVersion" int
        |> required "schemaPath" decodeSchemaPath
        |> required "createdAt" string
        |> optional "deletedAt" (nullable string) Nothing


decodeNameTags : Decoder NameTags
decodeNameTags =
    dict (list decodeNameTag)


decodeSingleNameTag : Decoder NameTag
decodeSingleNameTag =
    decodeQueryResult decodeNameTag
        |> andThen (.val >> succeed)


decodeTypeSaveValSuccess : Decoder (QueryResult TypeSaveResultVal)
decodeTypeSaveValSuccess =
    decodeQueryResult (decodeBasicDbEntry |> andThen (TSSuccess >> succeed))


decodeTypeSaveError : Decoder TypeSaveResultVal
decodeTypeSaveError =
    succeed SomeError
        |> required "id" string
        |> andThen (.id >> TSError >> succeed)


decodeTypeSaveValError : Decoder (QueryResult TypeSaveResultVal)
decodeTypeSaveValError =
    decodeQueryResult decodeTypeSaveError


decodeTypeSave : Decoder (Result String BasicDbEntry)
decodeTypeSave =
    field "ok" bool
        |> andThen
            (\flag ->
                if flag then
                    decodeTypeSaveValSuccess

                else
                    decodeTypeSaveValError
            )
        |> andThen
            (\a ->
                case a.val of
                    TSError txt ->
                        Err txt |> succeed

                    TSSuccess entry ->
                        Ok entry |> succeed
            )


decodeSimpleRef : Decoder SimpleRef
decodeSimpleRef =
    succeed SimpleRef
        |> required "$id" string
        |> required "$version" int


decodeEventGeneratorBase : Decoder EventGeneratorBase
decodeEventGeneratorBase =
    succeed EventGeneratorBase
        |> required "id" string
        |> required "version" int
        |> required "config" decodeCommonData
        |> required "template" decodeSimpleRef
        |> required "createdAt" string
        |> optional "deletedAt" (nullable string) Nothing


decodeMaybeIntializedEventGenerator : Decoder EventGenerator
decodeMaybeIntializedEventGenerator =
    decodeEventGeneratorBase
        |> andThen
            (\base ->
                field "error" (nullable string)
                    |> andThen (succeed << EventGenerator base)
            )


decodeInitializedEventGenerators : Decoder (List EventGenerator)
decodeInitializedEventGenerators =
    list decodeMaybeIntializedEventGenerator


decodeTemplateEventGeneratorSchema : Decoder TSEventGenerator
decodeTemplateEventGeneratorSchema =
    succeed TSEventGenerator
        |> required "$config" decodeSchema
        |> required "$defaults" decodeCommonData
        |> required "$emits" decodeSchema
        |> required "$callback" decodeSchema


decodeTemplateBase : Decoder TemplateBase
decodeTemplateBase =
    succeed TemplateBase
        |> required "id" string
        |> required "name" string
        |> required "version" int
        |> required "createdAt" string
        |> optional "deletedAt" (nullable string) Nothing


decodeTemplateEventGenerator : Decoder TemplateSchema
decodeTemplateEventGenerator =
    field "schema" decodeTemplateEventGeneratorSchema
        |> andThen (succeed << TemplateEventGenerator)


decodeTemplateFunction : Decoder TemplateSchema
decodeTemplateFunction =
    field "schema" decodeFunction
        |> andThen (succeed << TemplateFunction)


decodeTemplate : Decoder Template
decodeTemplate =
    decodeTemplateBase
        |> andThen
            (\base ->
                field "type" string
                    |> andThen
                        (\type_ ->
                            case type_ of
                                "EventGenerator" ->
                                    decodeTemplateEventGenerator
                                        |> andThen (succeed << Template base)

                                "Function" ->
                                    decodeTemplateFunction
                                        |> andThen (succeed << Template base)

                                "Special" ->
                                    succeed <| Template base TemplateSpecial

                                a ->
                                    fail <| "unhandled template type: " ++ a
                        )
            )


pairsToIntDict : List ( String, a ) -> Result String (Dict Int a)
pairsToIntDict list_ =
    let
        job list__ dict_ =
            case list__ of
                [] ->
                    Ok dict_

                ( key, val ) :: xs ->
                    case String.toInt key of
                        Just intKey ->
                            job xs (Dict.insert intKey val dict_)

                        Nothing ->
                            Err key
    in
    job list_ Dict.empty


processTemplates : List ( String, List ( String, Template ) ) -> Result String Templates
processTemplates basePairs =
    let
        job pairs newDict =
            case pairs of
                [] ->
                    Ok <| Dict.empty

                ( id, versions ) :: moreBasePairs ->
                    case pairsToIntDict versions of
                        Ok intVersions ->
                            let
                                here =
                                    Dict.insert id intVersions newDict

                                more =
                                    job moreBasePairs newDict
                            in
                            case more of
                                Err err ->
                                    Err err

                                Ok evenMore ->
                                    Ok <| Dict.union here evenMore

                        Err err ->
                            Err err
    in
    job basePairs Dict.empty


decodeTemplates : Decoder Templates
decodeTemplates =
    keyValuePairs (keyValuePairs decodeTemplate)
        |> andThen
            (\basePairs ->
                case processTemplates basePairs of
                    Ok ok ->
                        succeed ok

                    Err err ->
                        fail err
            )


decodeCommonDataUnion : Decoder CommonData
decodeCommonDataUnion =
    field "$choice" string
        |> andThen
            (\choice ->
                maybe (field "$content" decodeCommonData)
                    |> andThen
                        (\maybeContent ->
                            succeed <| CUnion (Just ( choice, maybeContent ))
                        )
            )


decodeCommonDataVerboseType : Decoder CommonData
decodeCommonDataVerboseType =
    field "$type" string
        |> andThen
            (\strType ->
                case strType of
                    "UnionMember" ->
                        decodeCommonDataUnion

                    _ ->
                        fail <| "unknown verbose type: " ++ strType
            )


decodeCommonData : Decoder CommonData
decodeCommonData =
    oneOf
        [ string |> andThen (CString >> succeed)
        , bool |> andThen (Just >> CBool >> succeed)
        , int |> andThen (Just >> CInt >> succeed)
        , float |> andThen (String.fromFloat >> Just >> CFloat >> succeed)
        , decodeSimpleRef |> andThen (Just >> CRef >> succeed)
        , lazy (\_ -> list decodeCommonData) |> andThen (CList >> succeed)
        , decodeCommonDataVerboseType
        , lazy (\_ -> dict decodeCommonData) |> andThen (CRecord >> succeed)
        , nullable string |> andThen (\_ -> Just ( "Nothing", Nothing ) |> CUnion |> succeed)
        ]


decodeConstant : Decoder Constant
decodeConstant =
    succeed Constant
        |> required "id" string
        |> required "version" int
        |> required "value" decodeCommonData
        |> required "createdAt" string
        |> optional "deletedAt" (nullable string) Nothing


decodeConstants : Decoder Constants
decodeConstants =
    list decodeConstant
        |> andThen (Utils.toDictByIdVer >> succeed)


decodeDataPath : Decoder TT.Path
decodeDataPath =
    let
        decodePath strStep nextStrSteps =
            let
                continue decodedStep =
                    case nextStrSteps of
                        [] ->
                            Ok [ decodedStep ]

                        x :: xs ->
                            decodePath x xs
                                |> Result.andThen ((::) decodedStep >> Ok)

                keyOrVal () =
                    case nextStrSteps of
                        [] ->
                            Key

                        _ ->
                            Val
            in
            if String.startsWith "$" strStep then
                case strStep of
                    "$content" ->
                        continue <| TT.StepKey strStep Val

                    other ->
                        case
                            String.uncons other
                                |> Maybe.andThen (Tuple.second >> Just)
                                |> Maybe.andThen String.toInt
                                |> Maybe.andThen (TT.StepIdx >> Just)
                        of
                            Nothing ->
                                Err <| "unknown special key: " ++ strStep

                            Just listStep ->
                                continue listStep

            else
                continue <| TT.StepKey strStep (keyOrVal ())
    in
    list string
        |> andThen
            (\strPath ->
                case strPath of
                    [] ->
                        succeed []

                    strStep :: strSteps ->
                        case decodePath strStep strSteps of
                            Ok path ->
                                succeed path

                            Err err ->
                                fail err
            )


decodeJointFrom : Decoder JointFrom
decodeJointFrom =
    oneOf
        [ decodeSimpleRef |> andThen (JointFromRef >> succeed)
        , decodeDataPath |> andThen (JointFromPath >> succeed)
        ]


decodeJoints : Decoder (List Joint)
decodeJoints =
    list
        (index 0 decodeJointFrom
            |> andThen
                (\from ->
                    index 1 decodeDataPath
                        |> andThen
                            (\pathTo ->
                                case from of
                                    JointFromRef ref ->
                                        succeed <| ConstantJoint ref pathTo

                                    JointFromPath pathFrom ->
                                        succeed <| RewireJoint pathFrom pathTo
                            )
                )
        )


decodeLink : Decoder Link
decodeLink =
    succeed Link
        |> required "id" string
        |> required "version" int
        |> hardcoded Nothing
        |> required "from" string
        |> required "to" (nullable string)
        |> required "joints" decodeJoints
        |> required "createdAt" string
        |> optional "deletedAt" (nullable string) Nothing


decodeLinks : Decoder (List Link)
decodeLinks =
    list decodeLink


decodeNewLink : Decoder Link
decodeNewLink =
    decodeQueryResult decodeLink
        |> andThen (.val >> succeed)


decodeNode : Decoder Node
decodeNode =
    succeed Node
        |> required "id" string
        |> required "version" int
        |> required "targetId" string
        |> required "targetVersion" int
        |> required "createdAt" string
        |> optional "deletedAt" (nullable string) Nothing


decodeNewNode : Decoder Node
decodeNewNode =
    decodeQueryResult decodeNode
        |> andThen (.val >> succeed)


decodeNewConstant : Decoder Constant
decodeNewConstant =
    decodeQueryResult decodeConstant
        |> andThen (.val >> succeed)


decodeNodes : Decoder Nodes
decodeNodes =
    list decodeNode
        |> andThen (Utils.toDictByIdVer >> succeed)


decodeNewEventGenerator : Decoder NewEventGenerator
decodeNewEventGenerator =
    decodeQueryResult
        (succeed NewEventGenerator
            |> required "id" string
            |> required "version" int
            |> required "createdAt" string
            |> required "$config" decodeCommonData
        )
        |> andThen (.val >> succeed)


decodeActiveNodes : Decoder ActiveNodes
decodeActiveNodes =
    succeed ActiveNodes
        |> required "eventGenerators" decodeInitializedEventGenerators
        |> required "nodes" decodeNodes
        |> required "links" decodeLinks
