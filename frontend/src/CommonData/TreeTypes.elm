module CommonData.TreeTypes exposing (Ctx, Opts, TreeModel, TreeMsg(..))

import Colors exposing (Colors)
import CommonData.Main exposing (CommonData)
import DbTypes exposing (Constant, NameTags, Schema(..))
import GenericTypes exposing (CollectionIdVer)
import Tree.Types exposing (Path)


type alias Opts msg =
    { nameTags : NameTags
    , updateMsg : Maybe (TreeMsg -> msg)
    , editable : Bool
    , selectionId : Maybe String
    , constants : CollectionIdVer Constant
    }


type alias TreeModel a =
    { a
        | data : CommonData
        , selection : Maybe Path
        , schema : Maybe Schema
    }


type alias Ctx a msg =
    { o : Opts msg
    , m : TreeModel a
    , data : CommonData
    , path : Path
    , colors : Colors
    }


type TreeMsg
    = DataSelected Path
    | InputChanged Path String
