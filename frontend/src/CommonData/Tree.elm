module CommonData.Tree exposing (dataToTreeItem, defaultOpts, tree, update)

import Colors exposing (Colors)
import CommonData.Main exposing (..)
import CommonData.TreeTypes exposing (..)
import CommonData.Utils as CDU
import DbTypes exposing (Schema(..), UnionMember(..))
import Dict
import FetchedUtils
import FormControls exposing (myInput)
import FormUtils
import GenericTypes exposing (KeyVal(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Maybe exposing (..)
import Schema.Utils as SU
import Tree.Main
import Tree.Types as TT
import Utils


defaultItemOpts =
    TT.defaultItemOpts


defaultOpts : Opts msg
defaultOpts =
    { nameTags = Dict.empty
    , editable = False
    , updateMsg = Nothing
    , selectionId = Nothing
    , constants = Dict.empty
    }


setOptions fn a =
    { a | o = fn a.o }


update : TreeModel a -> TreeMsg -> TreeModel a
update m msg =
    case msg of
        DataSelected path ->
            { m | selection = Just path }

        InputChanged path txt ->
            { m
                | data =
                    onInputTransform m.schema path m.data txt
                        |> (case m.schema of
                                Nothing ->
                                    identity

                                Just schema ->
                                    CDU.expandData schema
                           )
            }


onInputTransform : Maybe Schema -> Path -> CommonData -> String -> CommonData
onInputTransform maybeSchema path val txt =
    updateByPath
        path
        (\val_ ->
            case val_ of
                CJson _ ->
                    if txt == "" then
                        CJson Nothing

                    else
                        CJson <| Just txt

                CInt maybeInt ->
                    CInt <|
                        FormUtils.handleIntInput
                            maybeInt
                            txt

                CFloat _ ->
                    if txt == "" then
                        CFloat Nothing

                    else
                        FormUtils.formatFloatString txt
                            |> Just
                            |> CFloat

                CString _ ->
                    CString txt

                CUnion payload ->
                    if txt == "" then
                        CUnion Nothing

                    else
                        case payload of
                            Just ( key, content ) ->
                                maybeSchema
                                    |> andThen (\schema -> CDU.expandUnionWithKey schema path txt)
                                    |> withDefault (CUnion (Just ( txt, content )))

                            Nothing ->
                                CUnion (Just ( txt, Nothing ))

                CBool _ ->
                    if txt == "True" then
                        CBool (Just True)

                    else if txt == "False" then
                        CBool (Just False)

                    else
                        CBool Nothing

                _ ->
                    val_
        )
        val


withSelection : Ctx a msg -> List (Attribute msg) -> List (Attribute msg)
withSelection ctx attrs =
    attrs
        ++ (case ctx.m.selection of
                Nothing ->
                    []

                Just selection ->
                    if ctx.path == selection then
                        [ style "color" ctx.colors.primaryD3
                        ]

                    else
                        [ style "color " "red" ]
           )
        |> (case ctx.o.updateMsg of
                Nothing ->
                    identity

                Just cb ->
                    (::) (Utils.onClickStopPropagation <| cb <| DataSelected ctx.path)
                        >> (if ctx.o.editable then
                                (::) (style "cursor" "pointer")

                            else
                                identity
                           )
           )


simpleInput : Ctx a msg -> Html msg
simpleInput ctx =
    let
        matchingWidth =
            case ctx.data of
                CJson (Just txt) ->
                    String.length txt

                CJson Nothing ->
                    4

                CInt (Just int) ->
                    int
                        |> String.fromInt
                        |> String.length

                CInt Nothing ->
                    3

                CFloat (Just float) ->
                    if String.isEmpty float then
                        5

                    else
                        String.length float

                CFloat Nothing ->
                    5

                CString string ->
                    if String.isEmpty string then
                        6

                    else
                        String.length string

                CUnion (Just ( key, _ )) ->
                    String.length key

                CUnion Nothing ->
                    5

                _ ->
                    0

        decoration : Html msg -> Html msg
        decoration =
            let
                mark =
                    case ctx.data of
                        CString str ->
                            if String.isEmpty str then
                                ""

                            else
                                "\""

                        _ ->
                            ""
            in
            \a -> span [] [ text mark, a, text mark ]
    in
    myInput
        ctx.colors
        ([ style "text-overflow" "ellipsis"
         , style "font-family" "monospace"
         , style "max-width" "50vw"
         , style "width" <|
            "calc(0.5rem + 0.5rem * "
                ++ (if matchingWidth == 0 then
                        "2"

                    else
                        String.fromInt <| matchingWidth - 1
                   )
                ++ " + 1px)"
         , style "border-bottom" "0"
         , disabled <| not ctx.o.editable
         , value
            (case ctx.data of
                CJson (Just txt) ->
                    txt

                CInt maybeInt ->
                    case maybeInt of
                        Nothing ->
                            ""

                        Just num ->
                            String.fromInt num

                CFloat maybeFloat ->
                    case maybeFloat of
                        Nothing ->
                            ""

                        Just num ->
                            num

                CString string ->
                    string

                CUnion (Just ( key, _ )) ->
                    key

                _ ->
                    ""
            )
         ]
            |> (case ctx.o.updateMsg of
                    Nothing ->
                        identity

                    Just cb ->
                        (::) (onInput (cb << InputChanged ctx.path))
               )
            |> (case ctx.data of
                    CUnion _ ->
                        (::) <| style "color" ctx.colors.schemaUnion

                    CInt _ ->
                        (::) <| style "color" ctx.colors.schemaInt

                    CFloat _ ->
                        (::) <| style "color" ctx.colors.schemaFloat

                    CString _ ->
                        (::) <| style "color" ctx.colors.schemaString

                    CJson _ ->
                        (::) <| style "color" ctx.colors.schemaJson

                    _ ->
                        identity
               )
            |> (case ctx.data of
                    CUnion Nothing ->
                        (::) <| placeholder "Union"

                    CInt Nothing ->
                        (::) <| placeholder "Int"

                    CFloat Nothing ->
                        (::) <| placeholder "Float"

                    CFloat (Just "") ->
                        (::) <| placeholder "Float"

                    CString "" ->
                        (::) <| placeholder "String"

                    CJson Nothing ->
                        (::) <| placeholder "Json"

                    _ ->
                        identity
               )
            |> withSelection ctx
        )
        |> decoration


isSelected : Ctx a msg -> Bool
isSelected ctx =
    case ctx.m.selection of
        Nothing ->
            False

        Just s ->
            s == ctx.path


baseSelect : Ctx a msg -> List (Html msg) -> Html msg
baseSelect ctx =
    select
        ([ style "border" "none"
         , style "outline" "none"
         ]
            |> (case ctx.o.updateMsg of
                    Just cb ->
                        (::) (onInput (cb << InputChanged ctx.path))

                    Nothing ->
                        identity
               )
            |> (if isSelected ctx then
                    (++)
                        [ style "color" ctx.colors.primaryD3
                        , style "background-color" ctx.colors.text
                        ]

                else
                    (++)
                        [ style "color"
                            (case ctx.data of
                                CUnion _ ->
                                    ctx.colors.schemaUnion

                                _ ->
                                    ctx.colors.text
                            )
                        , style "background-color" ctx.colors.primary
                        ]
               )
            |> (case ctx.data of
                    CUnion (Just ( key, _ )) ->
                        (::) (value key)

                    CBool (Just flag) ->
                        (::)
                            (value
                                (if flag then
                                    "True"

                                 else
                                    "False"
                                )
                            )

                    _ ->
                        identity
               )
        )


baseSelectOption : String -> String -> Html msg
baseSelectOption key choice =
    option [ value key, selected <| choice == key ] [ text key ]


boolPicker : Ctx a msg -> Maybe Bool -> Html msg
boolPicker ctx maybeBool =
    let
        choice =
            case maybeBool of
                Nothing ->
                    ""

                Just bool ->
                    if bool then
                        "True"

                    else
                        "False"
    in
    baseSelect ctx <|
        [ baseSelectOption "" choice
        , baseSelectOption "True" choice
        , baseSelectOption "False" choice
        ]


unionPicker : Ctx a msg -> Schema -> Maybe ( String, x ) -> Html msg
unionPicker ctx schema_ maybeChoice =
    case SU.getFocusedSchema ctx.path schema_ of
        Nothing ->
            simpleInput ctx

        Just (SUnion members) ->
            let
                choice =
                    case maybeChoice of
                        Nothing ->
                            ""

                        Just ( key, _ ) ->
                            key
            in
            baseSelect ctx <|
                (List.map
                    (\member ->
                        baseSelectOption (SU.unionGetUid member) choice
                    )
                    members
                    |> (::) (baseSelectOption "" choice)
                )

        Just _ ->
            simpleInput ctx


unionContentDict : Ctx a msg -> Maybe ( String, Maybe CommonData ) -> Maybe (TT.Content msg)
unionContentDict ctx unionPayload =
    case unionPayload of
        Just ( key, Just content_ ) ->
            let
                newPath kv =
                    ctx.path ++ [ TT.StepKey key kv ]
            in
            [ { key =
                    { item =
                        { r =
                            { path = newPath Key
                            , key = key
                            }
                        , o =
                            { defaultItemOpts
                                | color = ctx.colors.schemaUnion
                                , content = Nothing
                                , format = \_ -> text ""
                            }
                        }
                    , selectable = False
                    }
              , val =
                    { item = dataToTreeItem { ctx | data = content_, path = newPath Val }
                    , selectable = True
                    }
                        |> Just
              }
            ]
                |> TT.ContentDict
                |> Just

        _ ->
            Nothing


dataToTreeItem : Ctx a msg -> TT.Item msg
dataToTreeItem ctx =
    case ctx.data of
        CInt maybeInt ->
            { r = { path = ctx.path, key = "Int" }
            , o =
                { defaultItemOpts
                    | color = ctx.colors.schemaInt
                    , content = Nothing
                    , format = \_ -> simpleInput ctx
                }
            }

        CFloat maybeString ->
            { r = { path = ctx.path, key = "Float" }
            , o =
                { defaultItemOpts
                    | color = ctx.colors.schemaFloat
                    , content = Nothing
                    , format = \_ -> simpleInput ctx
                }
            }

        CString string ->
            { r = { path = ctx.path, key = "String" }
            , o =
                { defaultItemOpts
                    | color = ctx.colors.schemaString
                    , content = Nothing
                    , format = \_ -> simpleInput ctx
                }
            }

        CBool maybeBool ->
            { r = { path = ctx.path, key = "Bool" }
            , o =
                { defaultItemOpts
                    | color = ctx.colors.schemaBool
                    , content = Nothing
                    , format = \_ -> boolPicker ctx maybeBool
                }
            }

        CRef maybeRef ->
            { r = { path = ctx.path, key = "Ref" }
            , o =
                { defaultItemOpts
                    | color = ctx.colors.schemaRef
                    , content =
                        maybeRef
                            |> andThen
                                (\ref ->
                                    ctx.o.constants
                                        |> Dict.get ref.id
                                        |> andThen (Dict.get ref.version)
                                )
                            |> Maybe.map
                                (\const ->
                                    let
                                        key =
                                            "preview"
                                    in
                                    [ { key =
                                            { item =
                                                { r =
                                                    { path = ctx.path ++ [ TT.StepKey key Key ]
                                                    , key = key
                                                    }
                                                , o =
                                                    { defaultItemOpts
                                                        | color = ctx.colors.schemaRef
                                                        , content = Nothing
                                                        , format = \a -> text ""
                                                    }
                                                }
                                            , selectable = False
                                            }
                                      , val =
                                            { item =
                                                dataToTreeItem
                                                    ({ ctx
                                                        | data = const.value
                                                        , path =
                                                            ctx.path
                                                                ++ [ TT.StepKey key Val ]
                                                     }
                                                        |> setOptions (\a -> { a | editable = False })
                                                    )
                                            , selectable = False
                                            }
                                                |> Just
                                      }
                                    ]
                                        |> TT.ContentDict
                                )
                    , format =
                        \_ ->
                            (case maybeRef of
                                Nothing ->
                                    "(empty ref)"

                                Just ref ->
                                    case FetchedUtils.getNameTagName ctx.o.nameTags [] ref of
                                        Nothing ->
                                            "Ref (" ++ String.left 5 ref.id ++ "...)"

                                        Just tag ->
                                            tag
                            )
                                |> text
                }
            }

        CJson maybeString ->
            { r = { path = ctx.path, key = "Json" }
            , o =
                { defaultItemOpts
                    | color = ctx.colors.schemaJson
                    , content = Nothing
                    , format = \_ -> simpleInput ctx
                }
            }

        CList items ->
            { r = { path = ctx.path, key = "List" }
            , o =
                { defaultItemOpts
                    | color = ctx.colors.schemaList
                    , format = text
                    , content =
                        items
                            |> List.indexedMap
                                (\idx data_ ->
                                    dataToTreeItem
                                        { ctx
                                            | data = data_
                                            , path = ctx.path ++ [ TT.StepIdx idx ]
                                        }
                                )
                            |> TT.ContentList
                            |> Just
                }
            }

        CUnion maybeChoice ->
            { r = { path = ctx.path, key = "UnionMember" }
            , o =
                { defaultItemOpts
                    | color = ctx.colors.schemaUnion
                    , content = unionContentDict ctx maybeChoice
                    , hintContent = True
                    , format =
                        \_ ->
                            case ctx.m.schema of
                                Nothing ->
                                    simpleInput ctx

                                Just schema ->
                                    unionPicker ctx schema maybeChoice
                }
            }

        CRecord attrs ->
            { r = { path = ctx.path, key = "Record" }
            , o =
                { defaultItemOpts
                    | color = ctx.colors.text
                    , format = text
                    , content =
                        attrs
                            |> Dict.toList
                            |> List.map
                                (\( k, v ) ->
                                    { key =
                                        { item =
                                            { r =
                                                { path = ctx.path ++ [ TT.StepKey k Key ]
                                                , key = k
                                                }
                                            , o =
                                                { defaultItemOpts
                                                    | color = ctx.colors.text
                                                    , content = Nothing
                                                    , format = \a -> text <| a ++ ":"
                                                }
                                            }
                                        , selectable = True
                                        }
                                    , val =
                                        { item =
                                            dataToTreeItem
                                                { ctx
                                                    | data = v
                                                    , path = ctx.path ++ [ TT.StepKey k Val ]
                                                }
                                        , selectable = True
                                        }
                                            |> Just
                                    }
                                )
                            |> TT.ContentDict
                            |> Just
                }
            }


tree : Colors -> TreeModel a -> Opts msg -> Html msg
tree colors m o =
    let
        treeOpts =
            TT.defaultOpts

        ctx =
            { m = m
            , o = o
            , path = []
            , data = m.data
            , colors = colors
            }
    in
    Tree.Main.view
        { colors = colors
        , selection = m.selection
        }
        { treeOpts
            | onItemSelect = o.updateMsg |> Maybe.map (\msg -> msg << DataSelected)
            , selectionId = o.selectionId
            , contentDictAttrs =
                Just <|
                    \path row ->
                        case find Dict.empty path m.data of
                            Just (CUnion _) ->
                                \a ->
                                    a
                                        ++ [ style "display" "inline"
                                           , style "margin-left" "0px"
                                           ]

                            _ ->
                                identity
        }
        (dataToTreeItem ctx)
