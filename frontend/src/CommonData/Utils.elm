module CommonData.Utils exposing
    ( FocusSelectionButtonOpts
    , addKey
    , baseTypeToCommonData
    , canDelete
    , expand
    , expandData
    , expandUnionWithKey
    , focusSelectionButton
    , focusSelectionButtonDefaults
    , getEmptyPaths
    , getListContentTypeBySiblings
    , getSiblings
    , isExpandable
    , isExpandableWithKey
    , isExpandable_
    , isFocusable
    , schemaToCommonData
    )

import ActionButtons.Definitions as ABD
import CommonData.Main exposing (..)
import DbTypes exposing (Schema(..), UnionMember(..))
import Dict
import GenericTypes exposing (BaseType(..), KeyVal(..))
import Keyboard
import List.Extra as EList
import Maybe exposing (andThen, withDefault)
import Schema.Utils as SU
import Tree.Types exposing (Path, Step(..))
import Types exposing (Model)



-- TODO functions that use both Schema and CommonData should have their own module.


expandUnionWithKey : Schema -> Path -> String -> Maybe CommonData
expandUnionWithKey schema path key =
    SU.getFocusedSchema path schema
        |> andThen
            (\schema_ ->
                case schema_ of
                    SUnion members ->
                        members
                            |> SU.unionGetMatchingMember key
                            |> andThen SU.unionGetContentSchema
                            |> andThen
                                (\contentSchema ->
                                    case schemaToCommonData contentSchema of
                                        Err _ ->
                                            Nothing

                                        Ok content ->
                                            CUnion
                                                (Just
                                                    ( key
                                                    , case
                                                        SU.getFocusedSchema
                                                            (path ++ [ StepKey key Val ])
                                                            schema_
                                                      of
                                                        Nothing ->
                                                            expandData schema_ content
                                                                |> Just

                                                        Just schema__ ->
                                                            expandData schema__ content
                                                                |> Just
                                                    )
                                                )
                                                |> Just
                                )

                    _ ->
                        Nothing
            )


expandData_ : Schema -> Path -> CommonData -> CommonData
expandData_ schema path val =
    case val of
        CUnion payload ->
            case payload of
                Just ( key, Just _ ) ->
                    val

                Nothing ->
                    val

                Just ( key, Nothing ) ->
                    expandUnionWithKey schema path key
                        |> withDefault val

        _ ->
            val


expandData : Schema -> CommonData -> CommonData
expandData schema val =
    map (expandData_ schema) val


getEmptyPaths : CommonData -> List Path
getEmptyPaths val =
    val
        |> toList
        |> List.foldl
            (\data acc ->
                case data.val of
                    CUnion Nothing ->
                        data.path :: acc

                    CUnion (Just ( key, _ )) ->
                        let
                            trimmed =
                                String.trim key
                        in
                        if String.isEmpty trimmed || String.startsWith "$" trimmed then
                            data.path :: acc

                        else
                            acc

                    CInt Nothing ->
                        data.path :: acc

                    CFloat _ ->
                        data.path :: acc

                    CBool Nothing ->
                        data.path :: acc

                    CRef Nothing ->
                        data.path :: acc

                    CJson Nothing ->
                        data.path :: acc

                    _ ->
                        acc
            )
            []


isExpandable_ : Db a -> Path -> CommonData -> Bool
isExpandable_ db path val =
    find db path val
        |> Maybe.map isExpandable
        |> withDefault False


isExpandable : CommonData -> Bool
isExpandable val =
    case val of
        CRecord _ ->
            True

        CList _ ->
            True

        CUnion (Just ( _, Nothing )) ->
            True

        _ ->
            False


isExpandableWithKey : String -> CommonData -> Bool
isExpandableWithKey key_ val =
    let
        key =
            String.trim key_
    in
    if String.isEmpty key || String.startsWith "$" key then
        False

    else
        case val of
            CRecord attrs ->
                Dict.member key attrs |> not

            CList _ ->
                True

            CUnion (Just ( _, Nothing )) ->
                True

            _ ->
                False


addKey : String -> CommonData -> CommonData
addKey key val =
    case val of
        CRecord attrs ->
            CRecord <| Dict.insert key (CString "") attrs

        _ ->
            val


tryExpandListByContent : CommonData -> CommonData
tryExpandListByContent val =
    case val of
        CList (item :: items) ->
            CList <| empty item :: item :: items

        _ ->
            val


expand : List Step -> Schema -> CommonData -> CommonData
expand path schema data =
    updateByPath
        path
        (\foundVal ->
            case SU.getFocusedSchema path schema of
                Nothing ->
                    tryExpandListByContent foundVal

                Just matchingSchema ->
                    case matchingSchema of
                        SList lSchema ->
                            case schemaToCommonData lSchema.vType of
                                Err err ->
                                    foundVal

                                Ok newData ->
                                    case foundVal of
                                        CList items ->
                                            CList <| newData :: items

                                        _ ->
                                            foundVal

                        SUnion members ->
                            case foundVal of
                                CUnion (Just ( key, Nothing )) ->
                                    EList.find (SU.unionMatchUid key) members
                                        |> andThen SU.unionGetContentSchema
                                        |> Maybe.map
                                            (\s ->
                                                case schemaToCommonData s of
                                                    Ok s_ ->
                                                        CUnion <| Just ( key, Just s_ )

                                                    Err _ ->
                                                        foundVal
                                            )
                                        |> withDefault foundVal

                                a ->
                                    foundVal

                        _ ->
                            tryExpandListByContent foundVal
        )
        data


schemaToCommonData : Schema -> Result String CommonData
schemaToCommonData schema =
    case schema of
        SBool ->
            Ok <| CBool Nothing

        SErr e ->
            Err e

        SFloat ->
            Ok <| CFloat Nothing

        SFunction _ ->
            Ok <| CRef Nothing

        SGeneric _ ->
            Err "TODO SGeneric -> CGeneric"

        SIdRef _ ->
            Ok <| CRef Nothing

        SInt ->
            Ok <| CInt Nothing

        SList _ ->
            Ok <| CList []

        SRecord attrs ->
            let
                resultAttrs =
                    Dict.foldl
                        (\key val acc ->
                            case acc of
                                Err e ->
                                    Err e

                                Ok newAttrs ->
                                    case schemaToCommonData val of
                                        Err e_ ->
                                            Err e_

                                        Ok const ->
                                            Ok <| Dict.insert key const newAttrs
                        )
                        (Ok Dict.empty)
                        attrs
            in
            case resultAttrs of
                Err e ->
                    Err e

                Ok newAttrs ->
                    Ok <| CRecord newAttrs

        SString ->
            Ok <| CString ""

        SUnion _ ->
            Ok <| CUnion Nothing

        SJson ->
            Ok <| CJson Nothing


baseTypeToCommonData : BaseType -> Maybe CommonData
baseTypeToCommonData bt =
    case bt of
        BTBool ->
            Just <| CBool Nothing

        BTFloat ->
            Just <| CFloat Nothing

        BTFunction ->
            Nothing

        BTGeneric ->
            Nothing

        BTInt ->
            Just <| CInt Nothing

        BTList ->
            Just <| CList []

        BTRecord ->
            Just <| CRecord Dict.empty

        BTRef ->
            Just <| CRef Nothing

        BTString ->
            Just <| CString ""

        BTUnion ->
            Just <| CUnion Nothing

        BTJson ->
            Just <| CJson Nothing


canDelete : Db a -> Path -> CommonData -> Bool
canDelete db path data =
    EList.init path
        |> andThen (\initPath -> find db initPath data)
        |> andThen
            (\data_ ->
                case data_ of
                    CList _ ->
                        Just True

                    _ ->
                        Just False
            )
        |> withDefault False


getSiblings : Path -> CommonData -> List CommonData
getSiblings path val =
    let
        len =
            List.length path
    in
    case EList.last path of
        Nothing ->
            val |> toList |> List.map .val

        Just lastStep ->
            val
                |> toList
                |> List.filter
                    (.path
                        >> (\path_ ->
                                if List.length path_ == len then
                                    case EList.last path_ of
                                        Nothing ->
                                            True

                                        Just lastStep_ ->
                                            case lastStep_ of
                                                StepIdx _ ->
                                                    True

                                                StepKey _ _ ->
                                                    lastStep_ == lastStep

                                else
                                    False
                           )
                    )
                |> List.map .val


getListContentTypeBySiblings : Path -> CommonData -> Maybe CommonData
getListContentTypeBySiblings path val =
    getSiblings path val
        |> List.filter
            (\a ->
                case a of
                    CList items ->
                        not <| List.isEmpty items

                    _ ->
                        False
            )
        |> List.head
        |> andThen
            (\first ->
                case first of
                    CList items ->
                        case items of
                            [] ->
                                Nothing

                            x :: xs ->
                                Just x

                    _ ->
                        Nothing
            )


isFocusable : Maybe Step -> CommonData -> Bool
isFocusable lastStep val =
    let
        checkVal () =
            case val of
                CInt _ ->
                    False

                CFloat _ ->
                    False

                CString _ ->
                    False

                CUnion _ ->
                    False

                CBool _ ->
                    False

                CJson _ ->
                    False

                _ ->
                    True
    in
    case lastStep of
        Just (StepKey _ kv) ->
            kv == GenericTypes.Key || checkVal ()

        _ ->
            checkVal ()


type alias FocusSelectionButtonOpts =
    { path : Maybe Path
    , val : Maybe CommonData
    , focusId : Maybe String
    }


focusSelectionButtonDefaults =
    { path = Nothing
    , val = Nothing
    , focusId = Nothing
    }


focusSelectionButton : Model -> Keyboard.Key -> FocusSelectionButtonOpts -> ABD.ActionButton
focusSelectionButton m key o =
    let
        actionButtonDefaults =
            ABD.defaultOptions
    in
    ABD.ActionButton
        { key = key, text = "focus" }
        { actionButtonDefaults
            | action = Just <| ABD.FocusSelection o.focusId
            , bgColor =
                if m.focusedInput then
                    ABD.SelectedBg

                else
                    ABD.NormalBg
            , isDisabled =
                case o.val of
                    Nothing ->
                        True

                    Just val_ ->
                        case o.path of
                            Nothing ->
                                False

                            Just p ->
                                find m.constants.items p val_
                                    |> Maybe.map (isFocusable <| EList.last p)
                                    |> withDefault False
        }
