module CommonData.Main exposing
    ( CommonData(..)
    , Db
    , Path
    , Step
    , delete
    , empty
    , find
    , map
    , reset
    , toList
    , toString
    , tryMigrateOldVal
    , updateByPath
    )

import Dict exposing (Dict)
import GenericTypes exposing (CollectionIdVer, KeyVal(..), SimpleRef)
import List.Extra as EList
import Maybe exposing (andThen, withDefault)
import Tree.Types as TT


type alias Db a =
    CollectionIdVer { a | value : CommonData }


type alias Step =
    TT.Step


type alias Path =
    TT.Path


type CommonData
    = CRecord (Dict String CommonData)
    | CInt (Maybe Int)
    | CFloat (Maybe String)
    | CString String
    | CBool (Maybe Bool)
    | CRef (Maybe SimpleRef)
    | CList (List CommonData)
    | CUnion (Maybe ( String, Maybe CommonData ))
    | CJson (Maybe String)


tryMigrateOldVal : CommonData -> CommonData -> Result CommonData CommonData
tryMigrateOldVal newVal oldVal =
    case oldVal of
        CString str ->
            case newVal of
                CFloat _ ->
                    Ok <| CFloat <| Just str

                CInt _ ->
                    Ok <| CInt <| String.toInt str

                _ ->
                    Err newVal

        CInt (Just int) ->
            case newVal of
                CFloat _ ->
                    int
                        |> String.fromInt
                        |> Just
                        |> CFloat
                        |> Ok

                CString _ ->
                    int
                        |> String.fromInt
                        |> CString
                        |> Ok

                _ ->
                    Err newVal

        CFloat (Just float) ->
            case newVal of
                CInt _ ->
                    float
                        |> String.toFloat
                        |> andThen (truncate >> Just)
                        |> CInt
                        |> Ok

                CString _ ->
                    Ok <| CString float

                _ ->
                    Err newVal

        _ ->
            Err newVal


{-| !!! Warning !!!

The updater callback can be called multiple times, depending on
the intermediate lists that need to be synchronized.

-}
updateByPath : Path -> (CommonData -> CommonData) -> CommonData -> CommonData
updateByPath path_ updater val_ =
    let
        updater_ val sync =
            let
                updated =
                    updater val
            in
            if sync && reset val == reset updated then
                val

            else
                updated

        simpleCheck path val sync =
            if path_ == path then
                updater_ val sync

            else
                val

        job path val sync =
            case val of
                CInt maybeInt ->
                    simpleCheck path val sync

                CFloat maybeFloat ->
                    simpleCheck path val sync

                CString string ->
                    simpleCheck path val sync

                CBool maybeBool ->
                    simpleCheck path val sync

                CRef maybeRef ->
                    simpleCheck path val sync

                CJson maybeJson ->
                    simpleCheck path val sync

                CUnion Nothing ->
                    simpleCheck path val sync

                CUnion (Just ( key, Nothing )) ->
                    simpleCheck path val sync

                CUnion (Just ( key, Just content )) ->
                    if path_ == path then
                        updater_ val sync

                    else
                        let
                            newPath =
                                path ++ [ TT.StepKey key Val ]
                        in
                        CUnion
                            (Just
                                ( key
                                , Just
                                    (if path_ == newPath then
                                        updater_ content sync

                                     else
                                        job newPath content sync
                                    )
                                )
                            )

                CList items ->
                    if path_ == path then
                        updater_ val sync

                    else
                        items
                            |> List.indexedMap
                                (\idx item ->
                                    let
                                        ( sync_, newStep ) =
                                            EList.getAt (List.length path) path_
                                                |> andThen
                                                    (\lastStep ->
                                                        case lastStep of
                                                            TT.StepIdx idx_ ->
                                                                Just
                                                                    ( if sync then
                                                                        sync

                                                                      else
                                                                        idx /= idx_
                                                                    , TT.StepIdx idx_
                                                                    )

                                                            TT.StepKey _ _ ->
                                                                Nothing
                                                    )
                                                |> withDefault ( sync, TT.StepIdx idx )

                                        newPath =
                                            path ++ [ newStep ]
                                    in
                                    if path_ == newPath then
                                        updater_ item sync_

                                    else
                                        job newPath item sync_
                                )
                            |> CList

                CRecord attrs ->
                    if path_ == path then
                        updater_ val sync

                    else
                        Dict.map
                            (\key item ->
                                let
                                    pathVal =
                                        path ++ [ TT.StepKey key Val ]
                                in
                                if path_ == pathVal || path_ == (path ++ [ TT.StepKey key Key ]) then
                                    updater_ item sync

                                else
                                    job pathVal item sync
                            )
                            attrs
                            |> CRecord
    in
    job [] val_ False


empty : CommonData -> CommonData
empty val =
    case val of
        CRecord attrs ->
            CRecord Dict.empty

        CInt _ ->
            CInt Nothing

        CFloat _ ->
            CFloat Nothing

        CString _ ->
            CString ""

        CBool _ ->
            CBool Nothing

        CRef _ ->
            CRef Nothing

        CList _ ->
            CList []

        CUnion _ ->
            CUnion Nothing

        CJson _ ->
            CJson Nothing


reset : CommonData -> CommonData
reset val =
    case val of
        CRecord attrs ->
            CRecord <| Dict.map (\k v -> empty v) attrs

        CInt _ ->
            CInt Nothing

        CFloat _ ->
            CFloat Nothing

        CString _ ->
            CString ""

        CBool _ ->
            CBool Nothing

        CRef _ ->
            CRef Nothing

        CList _ ->
            CList []

        CUnion _ ->
            CUnion Nothing

        CJson _ ->
            CJson Nothing


type alias DeletionResult =
    { val : Maybe CommonData
    , path : TT.Path
    }


delete : Db a -> Path -> CommonData -> DeletionResult
delete db path val =
    EList.unconsLast path
        |> Maybe.map
            (\( lastStep, initPath ) ->
                find db initPath val
                    |> Maybe.map
                        (\val_ ->
                            case val_ of
                                CInt _ ->
                                    DeletionResult Nothing []

                                CFloat _ ->
                                    DeletionResult Nothing []

                                CString _ ->
                                    DeletionResult Nothing []

                                CBool _ ->
                                    DeletionResult Nothing []

                                CJson _ ->
                                    DeletionResult Nothing []

                                CRef _ ->
                                    DeletionResult Nothing []

                                CRecord attrs ->
                                    DeletionResult
                                        (updateByPath
                                            initPath
                                            (\a ->
                                                case lastStep of
                                                    TT.StepKey k kv ->
                                                        case a of
                                                            CRecord attrs_ ->
                                                                CRecord <| Dict.remove k attrs_

                                                            _ ->
                                                                a

                                                    _ ->
                                                        a
                                            )
                                            val
                                            |> Just
                                        )
                                        (case lastStep of
                                            TT.StepIdx _ ->
                                                initPath

                                            TT.StepKey key kv ->
                                                let
                                                    list =
                                                        Dict.toList attrs
                                                in
                                                list
                                                    |> EList.findIndex (Tuple.first >> (==) key)
                                                    |> Maybe.map
                                                        (\idx ->
                                                            if idx == 0 then
                                                                initPath

                                                            else
                                                                case list of
                                                                    [] ->
                                                                        initPath

                                                                    ( k, v ) :: xs ->
                                                                        initPath ++ [ TT.StepKey k kv ]
                                                        )
                                                    |> withDefault initPath
                                        )

                                CList items ->
                                    let
                                        newVal =
                                            updateByPath
                                                initPath
                                                (\a ->
                                                    case lastStep of
                                                        TT.StepIdx idx ->
                                                            case a of
                                                                CList items_ ->
                                                                    CList <| EList.removeAt idx items

                                                                _ ->
                                                                    a

                                                        _ ->
                                                            a
                                                )
                                                val
                                    in
                                    DeletionResult
                                        (Just newVal)
                                        (case lastStep of
                                            TT.StepKey _ _ ->
                                                initPath

                                            TT.StepIdx idx ->
                                                case find db path newVal of
                                                    Nothing ->
                                                        if idx <= 0 then
                                                            initPath

                                                        else
                                                            initPath ++ [ TT.StepIdx <| idx - 1 ]

                                                    Just _ ->
                                                        path
                                        )

                                CUnion (Just ( key, Just content )) ->
                                    DeletionResult
                                        (updateByPath
                                            initPath
                                            (\a -> CUnion <| Just ( key, Nothing ))
                                            val
                                            |> Just
                                        )
                                        initPath

                                CUnion _ ->
                                    DeletionResult Nothing []
                        )
                    |> withDefault (DeletionResult Nothing [])
            )
        |> withDefault (DeletionResult Nothing [])


find : Db a -> List Step -> CommonData -> Maybe CommonData
find consts path val =
    case path of
        [] ->
            Just val

        step :: moreSteps ->
            case val of
                CInt _ ->
                    Nothing

                CFloat _ ->
                    Nothing

                CString _ ->
                    Nothing

                CBool _ ->
                    Nothing

                CJson _ ->
                    Nothing

                CRef payload ->
                    payload
                        |> andThen
                            (\ref ->
                                consts
                                    |> Dict.get ref.id
                                    |> andThen (Dict.get ref.version)
                                    |> andThen (.value >> find consts moreSteps)
                            )

                CRecord attrs ->
                    case step of
                        TT.StepIdx _ ->
                            Nothing

                        TT.StepKey key _ ->
                            attrs
                                |> Dict.get key
                                |> andThen (find consts moreSteps)

                CList items ->
                    case step of
                        TT.StepKey _ _ ->
                            Nothing

                        TT.StepIdx idx ->
                            EList.getAt idx items
                                |> andThen (find consts moreSteps)

                CUnion (Just ( key, Just content )) ->
                    find consts moreSteps content

                CUnion _ ->
                    Nothing


map : (Path -> CommonData -> CommonData) -> CommonData -> CommonData
map fn val_ =
    let
        job path val =
            case val of
                CInt _ ->
                    fn path val

                CFloat _ ->
                    fn path val

                CString _ ->
                    fn path val

                CBool _ ->
                    fn path val

                CRef _ ->
                    fn path val

                CUnion _ ->
                    fn path val

                CJson _ ->
                    fn path val

                CList items ->
                    items
                        |> List.indexedMap
                            (\idx v ->
                                let
                                    newPath =
                                        path ++ [ TT.StepIdx idx ]
                                in
                                fn newPath v |> job newPath
                            )
                        |> CList
                        |> fn path

                CRecord attrs ->
                    attrs
                        |> Dict.map
                            (\k v ->
                                let
                                    newPath =
                                        path ++ [ TT.StepKey k Val ]
                                in
                                fn newPath v |> job newPath
                            )
                        |> CRecord
                        |> fn path
    in
    job [] val_


toString : CommonData -> String
toString val =
    case val of
        CInt _ ->
            "Int"

        CFloat _ ->
            "Float"

        CString _ ->
            "String"

        CBool _ ->
            "Bool"

        CRef _ ->
            "Ref"

        CUnion _ ->
            "Union"

        CList _ ->
            "List"

        CRecord _ ->
            "Record"

        CJson _ ->
            "Json"


type alias PathVal =
    { path : Path
    , val : CommonData
    }


toList : CommonData -> List PathVal
toList val_ =
    let
        job : Path -> List PathVal -> CommonData -> List PathVal
        job path pathVal val =
            case val of
                CInt _ ->
                    { path = path, val = val } :: pathVal

                CFloat _ ->
                    { path = path, val = val } :: pathVal

                CString _ ->
                    { path = path, val = val } :: pathVal

                CBool _ ->
                    { path = path, val = val } :: pathVal

                CRef _ ->
                    { path = path, val = val } :: pathVal

                CJson _ ->
                    { path = path, val = val } :: pathVal

                CUnion (Just ( key, Just content )) ->
                    { path = path, val = val }
                        :: job (path ++ [ TT.StepKey key Val ]) pathVal content
                        ++ pathVal

                CUnion _ ->
                    { path = path, val = val } :: pathVal

                CList items ->
                    { path = path, val = val }
                        :: (items
                                |> List.indexedMap
                                    (\idx item ->
                                        job (path ++ [ TT.StepIdx idx ]) pathVal item
                                    )
                                |> List.concat
                           )
                        ++ pathVal

                CRecord attrs ->
                    { path = path, val = val }
                        :: (attrs
                                |> Dict.toList
                                |> List.map
                                    (\( k, v ) ->
                                        job (path ++ [ TT.StepKey k Val ]) pathVal v
                                    )
                                |> List.concat
                           )
                        ++ pathVal
    in
    job [] [] val_
