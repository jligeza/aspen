module Tree.Keyboard exposing (getNewPos)

import GenericTypes exposing (KeyVal(..))
import Keyboard as K
import List.Extra as EList
import Maybe exposing (andThen, withDefault)
import Tree.Types exposing (..)
import Tree.Utils exposing (findSelected, toList)
import Utils


goDown : List K.Key -> Path -> List (Item msg) -> Path
goDown pressedKeys curPath list =
    list
        |> EList.findIndex (.r >> .path >> (==) curPath)
        |> andThen (\curIdx -> EList.getAt (curIdx + 1) list)
        |> andThen (.r >> .path >> Just)
        |> withDefault curPath


getNewPos : List K.Key -> Path -> Item msg -> Path
getNewPos pressedKeys curPath item =
    case EList.find Utils.isArrowKey pressedKeys of
        Nothing ->
            curPath

        Just arrowKey ->
            let
                list =
                    toList item
            in
            case arrowKey of
                K.ArrowRight ->
                    case EList.unconsLast curPath of
                        Nothing ->
                            goDown pressedKeys curPath list

                        Just ( lastStep, initPath ) ->
                            let
                                tryJump () =
                                    let
                                        len =
                                            List.length curPath
                                    in
                                    case findSelected curPath item of
                                        Nothing ->
                                            curPath

                                        Just item__ ->
                                            if item__.o.hintContent then
                                                goDown pressedKeys curPath list

                                            else
                                                case
                                                    EList.find
                                                        (\item_ ->
                                                            List.length item_.r.path
                                                                == len
                                                                && Utils.listStartsWith
                                                                    initPath
                                                                    item_.r.path
                                                        )
                                                        (List.reverse list)
                                                of
                                                    Nothing ->
                                                        goDown pressedKeys curPath list

                                                    Just item_ ->
                                                        if item_.r.path == curPath then
                                                            goDown pressedKeys curPath list

                                                        else
                                                            item_.r.path
                            in
                            case lastStep of
                                StepIdx _ ->
                                    tryJump ()

                                StepKey key kv ->
                                    case kv of
                                        Key ->
                                            goDown pressedKeys curPath list

                                        Val ->
                                            tryJump ()

                K.ArrowLeft ->
                    case EList.unconsLast curPath of
                        Nothing ->
                            curPath

                        Just ( lastStep, initPath ) ->
                            case lastStep of
                                StepIdx _ ->
                                    initPath

                                StepKey key kv ->
                                    case kv of
                                        Key ->
                                            initPath

                                        Val ->
                                            list
                                                |> EList.find
                                                    (.r >> .path >> (==) (initPath ++ [ StepKey key Key ]))
                                                |> andThen (.r >> .path >> Just)
                                                |> withDefault initPath

                K.ArrowDown ->
                    goDown pressedKeys curPath list

                K.ArrowUp ->
                    list
                        |> EList.findIndex (.r >> .path >> (==) curPath)
                        |> andThen (\curIdx -> EList.getAt (curIdx - 1) list)
                        |> andThen (.r >> .path >> Just)
                        |> withDefault curPath

                _ ->
                    curPath
