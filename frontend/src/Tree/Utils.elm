module Tree.Utils exposing
    ( findParent
    , findSelected
    , prettyPath
    , spacedPrefix
    , toList
    )

import GenericTypes exposing (KeyVal(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import List.Extra as EList
import Maybe exposing (andThen)
import Tree.Types exposing (..)


spacedPrefix : String -> String -> Html msg
spacedPrefix prefix txt =
    span []
        [ span [ style "margin-right" "3px" ] [ text prefix ]
        , text txt
        ]


findSelected : Path -> Item msg -> Maybe (Item msg)
findSelected path item =
    case path of
        [] ->
            Just item

        step :: steps ->
            item.o.content
                |> andThen
                    (\content ->
                        case content of
                            ContentList rows ->
                                case step of
                                    StepKey _ _ ->
                                        Nothing

                                    StepIdx idx ->
                                        EList.getAt idx rows
                                            |> andThen (findSelected steps)

                            ContentDict items ->
                                case step of
                                    StepIdx _ ->
                                        Nothing

                                    StepKey key kv ->
                                        case kv of
                                            Key ->
                                                Nothing

                                            Val ->
                                                EList.find
                                                    (.key >> .item >> .r >> .key >> (==) key)
                                                    items
                                                    |> andThen (.val >> andThen (.item >> findSelected steps))
                    )


findParent : Path -> Item msg -> Maybe (Item msg)
findParent path item =
    EList.init path
        |> andThen (\initPath -> findSelected initPath item)


toList : Item msg -> List (Item msg)
toList item_ =
    let
        job item list =
            case item.o.content of
                Nothing ->
                    item :: list

                Just content ->
                    case content of
                        ContentDict rows ->
                            List.foldl
                                (\row acc ->
                                    acc
                                        |> (if row.key.selectable then
                                                (++) (toList row.key.item |> List.reverse)

                                            else
                                                identity
                                           )
                                        |> (case row.val of
                                                Nothing ->
                                                    identity

                                                Just val ->
                                                    (++) (toList val.item |> List.reverse)
                                           )
                                )
                                (item :: list)
                                rows

                        ContentList rows ->
                            List.foldl
                                (\a acc -> (toList a |> List.reverse) ++ acc)
                                (item :: list)
                                rows
    in
    job item_ [] |> List.reverse


prettyPath : Path -> String
prettyPath path =
    let
        stepToStr step =
            case step of
                StepIdx idx ->
                    String.fromInt idx

                StepKey key kv ->
                    case kv of
                        Key ->
                            "🔑 " ++ key

                        Val ->
                            key
    in
    List.foldl (\step acc -> acc ++ stepToStr step ++ " 🡒 ") "" path
        |> String.dropRight 4
