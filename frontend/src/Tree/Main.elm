module Tree.Main exposing (view)

import Definitions
import GenericTypes exposing (KeyVal(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Maybe exposing (..)
import Tree.Types exposing (..)
import Utils


isSelected : Ctx msg -> Path -> Bool
isSelected ctx path =
    case ctx.r.selection of
        Nothing ->
            False

        Just p ->
            path == p


setOptions fn ctx =
    { ctx | o = fn ctx.o }


selectionAttrs : Ctx msg -> String -> Path -> List (Attribute msg)
selectionAttrs ctx normalColor path =
    if isSelected ctx path then
        [ style "background" ctx.r.colors.text
        , style "color" ctx.r.colors.primaryD3
        , style "border-radius" "3px"
        , id (ctx.o.selectionId |> withDefault Definitions.selectionId)
        ]

    else
        [ style "color" normalColor ]


clickable : Path -> (Path -> msg) -> List (Attribute msg)
clickable path cb =
    [ path
        |> cb
        |> Utils.onClickStopPropagation
    , style "cursor" "pointer"
    ]


itemView : Ctx msg -> Item msg -> Html msg
itemView ctx item =
    case item.o.content of
        Nothing ->
            span []
                [ span
                    (selectionAttrs ctx item.o.color item.r.path
                        |> (case ctx.o.onItemSelect of
                                Just cb ->
                                    (++) (clickable item.r.path cb)

                                Nothing ->
                                    identity
                           )
                    )
                    [ item.o.format item.r.key ]
                ]

        Just content ->
            span []
                (span
                    (selectionAttrs ctx item.o.color item.r.path
                        |> (case ctx.o.onItemSelect of
                                Just cb ->
                                    \attrs ->
                                        clickable item.r.path cb ++ attrs

                                Nothing ->
                                    identity
                           )
                    )
                    [ item.o.format item.r.key ]
                    :: contentView ctx item.r.path content
                )


contentView : Ctx msg -> Path -> Content msg -> List (Html msg)
contentView ctx path content =
    case content of
        ContentList items ->
            List.map
                (\item ->
                    div
                        [ style "margin-left" "10px" ]
                        [ span
                            [ style "margin-right" "5px"
                            , style "color" ctx.r.colors.schemaList
                            ]
                            [ text "•" ]
                        , itemView ctx item
                        ]
                )
                items

        ContentDict items ->
            List.map
                (\row ->
                    let
                        keyPath =
                            path ++ [ StepKey row.key.item.r.key Key ]
                    in
                    div
                        ([ style "margin-left" "10px" ]
                            |> (case ctx.o.contentDictAttrs of
                                    Nothing ->
                                        identity

                                    Just proxy ->
                                        proxy path row
                               )
                        )
                        ([ span
                            (style "margin-right" "5px"
                                :: selectionAttrs ctx row.key.item.o.color keyPath
                                |> (case ( ctx.o.onItemSelect, row.key.selectable ) of
                                        ( Just cb, True ) ->
                                            (++) (clickable keyPath cb)

                                        _ ->
                                            identity
                                   )
                            )
                            [ row.key.item.o.format row.key.item.r.key
                            ]
                         ]
                            |> (\a ->
                                    case row.val of
                                        Nothing ->
                                            a

                                        Just val ->
                                            let
                                                view_ =
                                                    [ itemView
                                                        (if val.selectable then
                                                            ctx

                                                         else
                                                            setOptions
                                                                (\o ->
                                                                    { o
                                                                        | onItemSelect = Nothing
                                                                        , inactive = True
                                                                    }
                                                                )
                                                                ctx
                                                        )
                                                        val.item
                                                    ]
                                            in
                                            a
                                                ++ (if val.selectable || ctx.o.inactive then
                                                        view_

                                                    else
                                                        [ div [ style "opacity" "0.7" ] view_ ]
                                                   )
                               )
                        )
                )
                items


view : Reqs -> Opts msg -> Item msg -> Html msg
view reqs opts item =
    div
        [ style "padding" "3px" ]
        [ itemView
            { r = reqs
            , o = opts
            }
            item
        ]
