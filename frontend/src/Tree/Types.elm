module Tree.Types exposing
    ( Content(..)
    , Ctx
    , Item
    , ItemOpts
    , ItemReqs
    , ItemRow
    , Msg(..)
    , Opts
    , Path
    , Reqs
    , Step(..)
    , defaultItemOpts
    , defaultOpts
    )

import Colors exposing (Colors)
import GenericTypes exposing (KeyVal)
import Html exposing (Attribute, Html)
import Html.Attributes exposing (..)


type alias Reqs =
    { selection : Maybe Path
    , colors : Colors
    }


type alias Opts msg =
    { onItemSelect : Maybe (Path -> msg)
    , contentDictAttrs : Maybe (Path -> ItemRow msg -> List (Attribute msg) -> List (Attribute msg))
    , selectionId : Maybe String
    , inactive : Bool
    }


defaultOpts : Opts msg
defaultOpts =
    { onItemSelect = Nothing
    , contentDictAttrs = Nothing
    , selectionId = Nothing
    , inactive = False
    }


type alias Ctx msg =
    { r : Reqs
    , o : Opts msg
    }


type alias ItemRow msg =
    { key :
        { item : Item msg
        , selectable : Bool
        }
    , val :
        Maybe
            { item : Item msg
            , selectable : Bool
            }
    }


type alias ItemReqs =
    { path : Path
    , key : String
    }


type alias ItemOpts msg =
    { color : String
    , content : Maybe (Content msg)
    , format : String -> Html msg
    , hintContent : Bool -- for keyboard navigation
    }


defaultItemOpts =
    { color = ""
    , content = Nothing
    , format = Html.text
    , hintContent = False
    }


type alias Item msg =
    { r : ItemReqs
    , o : ItemOpts msg
    }


type Content msg
    = ContentDict (List (ItemRow msg))
    | ContentList (List (Item msg))


type Step
    = StepKey String KeyVal
    | StepIdx Int


type alias Path =
    List Step


type Msg
    = ItemSelected Path
