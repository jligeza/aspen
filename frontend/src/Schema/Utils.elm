module Schema.Utils exposing
    ( baseTypeToSchema
    , emptySchema
    , findGenerics
    , getFocusedSchema
    , idxToKey
    , isOneLiner
    , treeFind
    , unionGetContentSchema
    , unionGetMatchingMember
    , unionGetPrevStep
    , unionGetUid
    , unionIsMemberMatching
    , unionMatchUid
    )

import DbTypes exposing (..)
import Dict
import GenericTypes exposing (BaseType(..), KeyVal(..))
import List.Extra as EList
import Maybe exposing (andThen)
import Set exposing (Set)
import Tree.Types as TT


emptySchema =
    SErr "..."


unionGetUid : UnionMember -> String
unionGetUid choice =
    case choice of
        ShortUnionMember a ->
            a

        FullUnionMember a ->
            a.uId


unionGetMatchingMember : String -> List UnionMember -> Maybe UnionMember
unionGetMatchingMember uId members =
    EList.find (unionGetUid >> (==) uId) members


unionMatchUid : String -> UnionMember -> Bool
unionMatchUid uId unionMember =
    case unionMember of
        ShortUnionMember a ->
            a == uId

        FullUnionMember a ->
            a.uId == uId


unionGetContentSchema : UnionMember -> Maybe Schema
unionGetContentSchema member =
    case member of
        FullUnionMember fSchema ->
            fSchema.vType

        _ ->
            Nothing


getFocusedSchema : TT.Path -> Schema -> Maybe Schema
getFocusedSchema path schema =
    case path of
        [] ->
            Just schema

        step :: moreSteps ->
            case schema of
                SBool ->
                    Nothing

                SErr _ ->
                    Nothing

                SFloat ->
                    Nothing

                SInt ->
                    Nothing

                SString ->
                    Nothing

                SJson ->
                    Nothing

                SGeneric _ ->
                    Nothing

                SIdRef rSchema ->
                    case step of
                        TT.StepIdx _ ->
                            Nothing

                        TT.StepKey key kv ->
                            Dict.get key rSchema.generics
                                |> andThen (getFocusedSchema moreSteps)

                SList lSchema ->
                    getFocusedSchema moreSteps lSchema.vType

                SRecord attrs ->
                    case step of
                        TT.StepIdx _ ->
                            Nothing

                        TT.StepKey key kv ->
                            Dict.get key attrs
                                |> andThen (getFocusedSchema moreSteps)

                SUnion members ->
                    case step of
                        TT.StepIdx _ ->
                            Nothing

                        TT.StepKey key kv ->
                            EList.find (unionMatchUid key) members
                                |> andThen
                                    (\member ->
                                        case member of
                                            ShortUnionMember _ ->
                                                Just schema

                                            FullUnionMember fuSchema ->
                                                fuSchema.vType |> andThen (getFocusedSchema moreSteps)
                                    )

                SFunction fSchema ->
                    case step of
                        TT.StepIdx _ ->
                            Nothing

                        TT.StepKey key kv ->
                            if String.startsWith "i" key then
                                getFocusedSchema moreSteps fSchema.input

                            else
                                getFocusedSchema moreSteps fSchema.output


unionIsMemberMatching : String -> UnionMember -> Bool
unionIsMemberMatching uId member =
    case member of
        ShortUnionMember uId_ ->
            uId_ == uId

        FullUnionMember a ->
            a.uId == uId


isOneLiner : Schema -> Bool
isOneLiner schema =
    case schema of
        SBool ->
            True

        SErr _ ->
            True

        SFloat ->
            True

        SFunction _ ->
            False

        SGeneric _ ->
            True

        SIdRef _ ->
            False

        SInt ->
            True

        SList _ ->
            False

        SRecord _ ->
            False

        SString ->
            True

        SUnion _ ->
            False

        SJson ->
            True


unionGetPrevStep :
    TT.Path
    -> Schema
    -> TT.Path
    -> KeyVal
    -> String
    -> TT.Path
unionGetPrevStep path schema initPath uStep uId =
    case getFocusedSchema path schema of
        Just schema_ ->
            case schema_ of
                SUnion members ->
                    case EList.findIndex (unionGetUid >> (==) uId) members of
                        Just idx ->
                            if idx == 0 then
                                initPath

                            else
                                case EList.getAt (idx - 1) members of
                                    Just prevMember ->
                                        initPath
                                            ++ [ TT.StepKey (unionGetUid prevMember) uStep ]

                                    Nothing ->
                                        path

                        Nothing ->
                            path

                _ ->
                    path

        Nothing ->
            path


findGenerics : Schema -> Set String
findGenerics schema =
    case schema of
        SBool ->
            Set.empty

        SErr _ ->
            Set.empty

        SFloat ->
            Set.empty

        SJson ->
            Set.empty

        SFunction fSchema ->
            Set.union (findGenerics fSchema.input) (findGenerics fSchema.output)

        SGeneric a ->
            Set.singleton a

        SIdRef rSchema ->
            case Dict.values rSchema.generics of
                [] ->
                    Set.empty

                vals ->
                    List.foldl
                        (\s acc ->
                            case s of
                                SGeneric txt ->
                                    Set.insert txt acc

                                _ ->
                                    acc
                        )
                        Set.empty
                        vals

        SInt ->
            Set.empty

        SList lSchema ->
            findGenerics lSchema.vType

        SRecord attrs ->
            Dict.values attrs
                |> List.foldl
                    (\a acc -> Set.union acc <| findGenerics a)
                    Set.empty

        SString ->
            Set.empty

        SUnion members ->
            List.foldl
                (\member acc ->
                    case member of
                        ShortUnionMember _ ->
                            acc

                        FullUnionMember fumSchema ->
                            case fumSchema.vType of
                                Nothing ->
                                    acc

                                Just s ->
                                    Set.union (findGenerics s) acc
                )
                Set.empty
                members


idxToKey : Schema -> Int -> Maybe String
idxToKey schema idx =
    case schema of
        SRecord attrs ->
            Dict.keys attrs |> EList.getAt idx

        SUnion choices ->
            EList.getAt idx choices
                |> andThen
                    (\choice ->
                        case choice of
                            ShortUnionMember a ->
                                Just a

                            FullUnionMember a ->
                                Just a.uId
                    )

        SIdRef ref ->
            Dict.keys ref.generics |> EList.getAt idx

        _ ->
            Nothing


baseTypeToSchema : BaseType -> Schema
baseTypeToSchema baseType =
    case baseType of
        BTJson ->
            SJson

        BTBool ->
            SBool

        BTFloat ->
            SFloat

        BTFunction ->
            SFunction
                { input = emptySchema
                , output = emptySchema
                }

        BTGeneric ->
            emptySchema

        BTInt ->
            SInt

        BTList ->
            SList { vType = emptySchema }

        BTRecord ->
            SRecord Dict.empty

        BTRef ->
            SErr "unknown ref"

        BTString ->
            SString

        BTUnion ->
            SUnion []


treeFind : TT.Path -> Schema -> Maybe Schema
treeFind path schema =
    case path of
        [] ->
            Just schema

        step :: steps ->
            case schema of
                SBool ->
                    Nothing

                SErr _ ->
                    Nothing

                SFloat ->
                    Nothing

                SJson ->
                    Nothing

                SGeneric a ->
                    Nothing

                SInt ->
                    Nothing

                SString ->
                    Nothing

                SFunction fSchema ->
                    case step of
                        TT.StepKey key kv ->
                            case kv of
                                Key ->
                                    Nothing

                                Val ->
                                    if String.startsWith "i" key then
                                        treeFind steps fSchema.input

                                    else
                                        treeFind steps fSchema.output

                        TT.StepIdx idx ->
                            Nothing

                SIdRef rSchema ->
                    case step of
                        TT.StepKey key kv ->
                            case kv of
                                Key ->
                                    Nothing

                                Val ->
                                    Dict.get key rSchema.generics
                                        |> andThen (treeFind steps)

                        TT.StepIdx _ ->
                            Nothing

                SList lSchema ->
                    case step of
                        TT.StepKey key kv ->
                            case kv of
                                Key ->
                                    Nothing

                                Val ->
                                    treeFind steps lSchema.vType

                        TT.StepIdx _ ->
                            Nothing

                SRecord attrs ->
                    case step of
                        TT.StepKey key kv ->
                            case kv of
                                Key ->
                                    Nothing

                                Val ->
                                    Dict.get key attrs
                                        |> andThen (treeFind steps)

                        TT.StepIdx _ ->
                            Nothing

                SUnion members ->
                    case step of
                        TT.StepKey key kv ->
                            case kv of
                                Key ->
                                    Nothing

                                Val ->
                                    EList.find (unionIsMemberMatching key) members
                                        |> andThen
                                            (\member ->
                                                case member of
                                                    ShortUnionMember _ ->
                                                        Nothing

                                                    FullUnionMember a ->
                                                        a.vType |> andThen (treeFind steps)
                                            )

                        TT.StepIdx _ ->
                            Nothing
