module Schema.Manipulation exposing
    ( addKey
    , delete
    , reword
    , unionShortToFull
    , updateSchema
    )

import DbTypes exposing (..)
import Dict
import GenericTypes exposing (KeyVal(..))
import List.Extra as EList
import Schema.Utils exposing (..)
import Tree.Types as TT
import Utils


updateUnionMemberSchema :
    TT.Path
    -> String
    -> (Schema -> Schema)
    -> UnionMember
    -> UnionMember
updateUnionMemberSchema path uId updater member =
    if not <| unionIsMemberMatching uId member then
        member

    else
        case member of
            ShortUnionMember _ ->
                member

            FullUnionMember mSchema ->
                case mSchema.vType of
                    Nothing ->
                        member

                    Just a ->
                        FullUnionMember
                            { uId = uId
                            , vType = Just <| updateSchema path updater a
                            }


updateSchema : TT.Path -> (Schema -> Schema) -> Schema -> Schema
updateSchema path updater schema =
    case path of
        [] ->
            updater schema

        step :: steps ->
            case schema of
                SJson ->
                    schema

                SBool ->
                    schema

                SErr _ ->
                    schema

                SFloat ->
                    schema

                SInt ->
                    schema

                SString ->
                    schema

                SGeneric _ ->
                    schema

                SIdRef ref ->
                    case step of
                        TT.StepIdx _ ->
                            schema

                        TT.StepKey key kv ->
                            case kv of
                                Key ->
                                    updateSchema steps updater schema

                                Val ->
                                    { ref
                                        | generics =
                                            Dict.update
                                                key
                                                (Maybe.andThen
                                                    (\a ->
                                                        Just <|
                                                            updateSchema
                                                                steps
                                                                updater
                                                                a
                                                    )
                                                )
                                                ref.generics
                                    }
                                        |> SIdRef

                SFunction fSchema ->
                    case step of
                        TT.StepIdx _ ->
                            schema

                        TT.StepKey key kv ->
                            if String.startsWith "i" key then
                                SFunction
                                    { input = updateSchema steps updater fSchema.input
                                    , output = fSchema.output
                                    }

                            else
                                SFunction
                                    { input = fSchema.input
                                    , output = updateSchema steps updater fSchema.output
                                    }

                SList lSchema ->
                    SList { vType = updateSchema steps updater lSchema.vType }

                SRecord attrs ->
                    case step of
                        TT.StepIdx _ ->
                            schema

                        TT.StepKey key kv ->
                            Dict.update
                                key
                                (Maybe.andThen
                                    (\a -> Just <| updateSchema steps updater a)
                                )
                                attrs
                                |> SRecord

                SUnion members ->
                    case step of
                        TT.StepIdx _ ->
                            schema

                        TT.StepKey key kv ->
                            case kv of
                                Key ->
                                    updateSchema steps updater schema

                                Val ->
                                    SUnion <|
                                        List.map
                                            (updateUnionMemberSchema
                                                steps
                                                key
                                                updater
                                            )
                                            members


addKey : String -> TT.Path -> Schema -> Schema
addKey key path baseSchema =
    updateSchema
        path
        (\schema ->
            case schema of
                SRecord attrs ->
                    SRecord <|
                        Dict.insert key emptySchema attrs

                SUnion members ->
                    SUnion <|
                        List.sortBy unionGetUid <|
                            members
                                ++ [ ShortUnionMember key ]

                _ ->
                    schema
        )
        baseSchema


type alias DeleteResult =
    { schema : Maybe Schema
    , path : TT.Path
    }


deleteRecordAttr : TT.Path -> TT.Path -> String -> Schema -> DeleteResult
deleteRecordAttr path initPath key baseSchema =
    let
        canBeFullyDeleted =
            case EList.last path of
                Nothing ->
                    False

                Just lastStep ->
                    case lastStep of
                        TT.StepKey _ kv ->
                            if kv == Key then
                                True

                            else
                                case getFocusedSchema path baseSchema of
                                    Nothing ->
                                        False

                                    Just schema ->
                                        case schema of
                                            SErr _ ->
                                                True

                                            _ ->
                                                False

                        _ ->
                            False

        newPath =
            if not canBeFullyDeleted then
                path

            else
                case getFocusedSchema initPath baseSchema of
                    Nothing ->
                        path

                    Just schema ->
                        case schema of
                            SRecord attrs ->
                                case Utils.getPosOfKeyInDict key attrs of
                                    Nothing ->
                                        path

                                    Just idx ->
                                        if idx == 0 then
                                            initPath

                                        else
                                            case
                                                EList.getAt
                                                    (idx - 1)
                                                    (Dict.keys attrs)
                                            of
                                                Nothing ->
                                                    path

                                                Just prevKey ->
                                                    initPath
                                                        ++ [ TT.StepKey prevKey Val ]

                            _ ->
                                path

        newRecord schema_ =
            case schema_ of
                SRecord attrs ->
                    if canBeFullyDeleted then
                        SRecord <| Dict.remove key attrs

                    else
                        SRecord <| Dict.insert key emptySchema attrs

                _ ->
                    schema_
    in
    { schema = Just <| updateSchema initPath newRecord baseSchema
    , path = newPath
    }


deleteRefItem :
    TT.Path
    -> TT.Path
    -> Schema
    -> KeyVal
    -> String
    -> DeleteResult
deleteRefItem path initPath schema kv key =
    let
        newSchema =
            updateSchema
                initPath
                (\schema_ ->
                    case schema_ of
                        SIdRef ref ->
                            SIdRef <| { ref | generics = Dict.remove key ref.generics }

                        _ ->
                            schema_
                )
                schema

        newPath =
            case getFocusedSchema initPath schema of
                Just (SIdRef ref) ->
                    case Dict.keys ref.generics |> EList.findIndex ((==) key) of
                        Nothing ->
                            path

                        Just idx ->
                            if idx == 0 then
                                initPath

                            else
                                case idxToKey (SIdRef ref) (idx - 1) of
                                    Nothing ->
                                        path

                                    Just key_ ->
                                        initPath ++ [ TT.StepKey key_ kv ]

                _ ->
                    path
    in
    { schema = Just newSchema
    , path = newPath
    }


deleteUnionItem :
    TT.Path
    -> TT.Path
    -> Schema
    -> KeyVal
    -> String
    -> DeleteResult
deleteUnionItem path initPath schema uStep uId =
    let
        newSchema =
            case uStep of
                Val ->
                    updateSchema
                        initPath
                        (\schema_ ->
                            case schema_ of
                                SUnion members ->
                                    SUnion <|
                                        EList.updateIf
                                            (unionGetUid >> (==) uId)
                                            (always <| ShortUnionMember uId)
                                            members

                                _ ->
                                    schema_
                        )
                        schema

                Key ->
                    updateSchema
                        path
                        (\schema_ ->
                            case schema_ of
                                SUnion members ->
                                    SUnion <|
                                        Utils.removeBy
                                            (unionGetUid >> (==) uId)
                                            members

                                _ ->
                                    schema_
                        )
                        schema

        newPath =
            case uStep of
                Val ->
                    initPath ++ [ TT.StepKey uId Key ]

                Key ->
                    case getFocusedSchema path newSchema of
                        Just focused ->
                            case focused of
                                SUnion focusedMembers ->
                                    case
                                        EList.find
                                            (unionGetUid >> (==) uId)
                                            focusedMembers
                                    of
                                        Just _ ->
                                            initPath

                                        Nothing ->
                                            unionGetPrevStep
                                                path
                                                schema
                                                initPath
                                                uStep
                                                uId

                                _ ->
                                    path

                        Nothing ->
                            path
    in
    { schema = Just newSchema
    , path = newPath
    }


delete : TT.Path -> Schema -> DeleteResult
delete path schema =
    case EList.unconsLast path of
        Nothing ->
            { schema = Nothing
            , path = path
            }

        Just ( lastStep, initPath ) ->
            let
                noChange () =
                    { schema = Just schema, path = path }
            in
            case getFocusedSchema initPath schema of
                Nothing ->
                    noChange ()

                Just schema_ ->
                    case schema_ of
                        SJson ->
                            noChange ()

                        SBool ->
                            noChange ()

                        SErr _ ->
                            noChange ()

                        SFloat ->
                            noChange ()

                        SInt ->
                            noChange ()

                        SString ->
                            noChange ()

                        SGeneric _ ->
                            noChange ()

                        SIdRef ref ->
                            case lastStep of
                                TT.StepIdx _ ->
                                    noChange ()

                                TT.StepKey key kv ->
                                    deleteRefItem path initPath schema kv key

                        SFunction fSchema ->
                            { schema = Just <| updateSchema path (\_ -> emptySchema) schema
                            , path = path
                            }

                        SList lSchema ->
                            { schema = Just <| updateSchema path (\_ -> emptySchema) schema
                            , path = path
                            }

                        SRecord attrs ->
                            case lastStep of
                                TT.StepIdx _ ->
                                    noChange ()

                                TT.StepKey key kv ->
                                    deleteRecordAttr path initPath key schema

                        SUnion members ->
                            case lastStep of
                                TT.StepIdx _ ->
                                    noChange ()

                                TT.StepKey key kv ->
                                    deleteUnionItem path initPath schema kv key


reword : TT.Path -> String -> String -> Schema -> Schema
reword path oldKey newKey baseSchema =
    updateSchema
        path
        (\schema ->
            case schema of
                SRecord attrs ->
                    case Dict.get oldKey attrs of
                        Nothing ->
                            schema

                        Just val ->
                            Dict.remove oldKey attrs
                                |> Dict.insert newKey val
                                |> SRecord

                SGeneric _ ->
                    SGeneric newKey

                SUnion members ->
                    members
                        |> List.map
                            (\member ->
                                case member of
                                    ShortUnionMember uId ->
                                        if uId == oldKey then
                                            ShortUnionMember newKey

                                        else
                                            member

                                    FullUnionMember fuSchema ->
                                        if fuSchema.uId == oldKey then
                                            FullUnionMember
                                                { uId = newKey
                                                , vType = fuSchema.vType
                                                }

                                        else
                                            member
                            )
                        |> List.sortBy unionGetUid
                        |> SUnion

                _ ->
                    schema
        )
        baseSchema


unionShortToFull : TT.Path -> Schema -> Maybe Schema
unionShortToFull path schema =
    case EList.last path of
        Nothing ->
            Nothing

        Just lastStep ->
            case lastStep of
                TT.StepKey uId uStep ->
                    Just <|
                        updateSchema
                            path
                            (\s ->
                                case s of
                                    SUnion members ->
                                        SUnion <|
                                            Utils.updateIfOnce
                                                (unionIsMemberMatching uId)
                                                (\_ ->
                                                    FullUnionMember <|
                                                        FullUnionMemberSchema
                                                            uId
                                                            (Just <| emptySchema)
                                                )
                                                members

                                    _ ->
                                        s
                            )
                            schema

                _ ->
                    Nothing
