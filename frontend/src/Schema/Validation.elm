module Schema.Validation exposing
    ( Error
    , Errors
    , Reason(..)
    , validate
    )

import DbTypes exposing (..)
import Dict
import GenericTypes exposing (KeyVal(..))
import Tree.Types as TT exposing (Path)


type Reason
    = EmptyRecord
    | UnionBelowTwoMembers
    | ErrorMemberPresent


type alias Error =
    { path : Path
    , reason : Reason
    }


type alias Errors =
    List Error


validateUnionSchema : UnionSchema -> Path -> Errors -> Errors
validateUnionSchema uSchema path errors =
    case uSchema of
        [] ->
            errors ++ [ { path = path, reason = UnionBelowTwoMembers } ]

        [ _ ] ->
            errors ++ [ { path = path, reason = UnionBelowTwoMembers } ]

        _ ->
            List.map
                (\member ->
                    case member of
                        ShortUnionMember _ ->
                            errors

                        FullUnionMember mSchema ->
                            case mSchema.vType of
                                Nothing ->
                                    errors

                                Just nestedSchema ->
                                    validate_
                                        nestedSchema
                                        (path ++ [ TT.StepKey mSchema.uId Val ])
                                        errors
                )
                uSchema
                |> List.concat


validateRecord : RecordSchema -> Path -> Errors -> Errors
validateRecord schema path errors =
    case Dict.toList schema of
        [] ->
            errors ++ [ { path = path, reason = EmptyRecord } ]

        pairs ->
            List.map
                (\( k, v ) -> validate_ v (path ++ [ TT.StepKey k Val ]) errors)
                pairs
                |> List.concat


validate_ : Schema -> Path -> Errors -> Errors
validate_ schema path errors =
    case schema of
        SJson ->
            errors

        SBool ->
            errors

        SErr _ ->
            errors ++ [ { path = path, reason = ErrorMemberPresent } ]

        SFloat ->
            errors

        SFunction fSchema ->
            validate_ fSchema.input (path ++ [ TT.StepKey "in" Val ]) errors
                ++ validate_ fSchema.output (path ++ [ TT.StepKey "out" Val ]) errors

        SGeneric _ ->
            errors

        SIdRef _ ->
            errors

        SInt ->
            errors

        SList lSchema ->
            validate_ lSchema.vType (path ++ [ TT.StepKey "of" Val ]) errors

        SRecord rSchema ->
            validateRecord rSchema path errors

        SString ->
            errors

        SUnion uSchema ->
            validateUnionSchema uSchema path errors


validate : Schema -> Errors
validate schema =
    validate_ schema [] []
