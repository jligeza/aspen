module Schema.Tree exposing (typeToTreeItem)

import Colors exposing (Colors)
import DbTypes exposing (..)
import Dict
import FetchedUtils
import GenericTypes exposing (KeyVal(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Tree.Types as TT
import Tree.Utils as TU


defaultItemOpts =
    TT.defaultItemOpts


typeToTreeItem : Colors -> NameTags -> TT.Path -> Schema -> TT.Item msg
typeToTreeItem colors nameTags path schema =
    case schema of
        SString ->
            { r = { path = path, key = "String" }
            , o = { defaultItemOpts | color = colors.schemaString }
            }

        SBool ->
            { r = { path = path, key = "Bool" }
            , o = { defaultItemOpts | color = colors.schemaBool }
            }

        SInt ->
            { r = { path = path, key = "Int" }
            , o = { defaultItemOpts | color = colors.schemaInt }
            }

        SFloat ->
            { r = { path = path, key = "Float" }
            , o = { defaultItemOpts | color = colors.schemaFloat }
            }

        SJson ->
            { r = { path = path, key = "Json" }
            , o = { defaultItemOpts | color = colors.schemaJson }
            }

        SGeneric txt ->
            { r = { path = path, key = txt }
            , o = { defaultItemOpts | color = colors.text }
            }

        SErr a ->
            { r = { path = path, key = a }
            , o = { defaultItemOpts | color = colors.error }
            }

        SIdRef payload ->
            { r =
                { path = path
                , key =
                    case FetchedUtils.getNameTagName nameTags [] payload of
                        Nothing ->
                            "Ref"

                        Just tag ->
                            tag
                }
            , o =
                { defaultItemOpts
                    | color = colors.schemaRef
                    , content =
                        (Dict.toList payload.generics
                            |> List.map
                                (\( k, v ) ->
                                    { key =
                                        { item =
                                            { r =
                                                { path = path ++ [ TT.StepKey k Key ]
                                                , key = k
                                                }
                                            , o =
                                                { defaultItemOpts
                                                    | color = colors.schemaRef
                                                    , format = \a -> text <| a ++ ":"
                                                }
                                            }
                                        , selectable = False
                                        }
                                    , val =
                                        { item =
                                            typeToTreeItem
                                                colors
                                                nameTags
                                                (path ++ [ TT.StepKey k Val ])
                                                v
                                        , selectable = True
                                        }
                                            |> Just
                                    }
                                )
                        )
                            |> TT.ContentDict
                            |> Just
                }
            }

        SUnion members ->
            { r =
                { path = path
                , key = "Union"
                }
            , o =
                { defaultItemOpts
                    | color = colors.schemaUnion
                    , content =
                        List.indexedMap
                            (\idx member ->
                                case member of
                                    ShortUnionMember key ->
                                        { key =
                                            { item =
                                                { r =
                                                    { path = path ++ [ TT.StepKey key Key ]
                                                    , key = key
                                                    }
                                                , o =
                                                    { defaultItemOpts
                                                        | color = colors.schemaUnion
                                                        , format = TU.spacedPrefix "|"
                                                    }
                                                }
                                            , selectable = True
                                            }
                                        , val = Nothing
                                        }

                                    FullUnionMember a ->
                                        { key =
                                            { item =
                                                { r =
                                                    { path = path ++ [ TT.StepKey a.uId Key ]
                                                    , key = a.uId
                                                    }
                                                , o =
                                                    { defaultItemOpts
                                                        | color = colors.schemaUnion
                                                        , format = TU.spacedPrefix "|"
                                                    }
                                                }
                                            , selectable = True
                                            }
                                        , val =
                                            case a.vType of
                                                Nothing ->
                                                    Nothing

                                                Just schema_ ->
                                                    { item =
                                                        typeToTreeItem
                                                            colors
                                                            nameTags
                                                            (path ++ [ TT.StepKey a.uId Val ])
                                                            schema_
                                                    , selectable = True
                                                    }
                                                        |> Just
                                        }
                            )
                            members
                            |> TT.ContentDict
                            |> Just
                }
            }

        SFunction payload ->
            let
                color =
                    colors.schemaFunction
            in
            { r = { path = path, key = "Function" }
            , o =
                { defaultItemOpts
                    | color = color
                    , content =
                        [ { key =
                                { item =
                                    { r =
                                        { path = path ++ [ TT.StepKey "in" Key ]
                                        , key = "in"
                                        }
                                    , o =
                                        { defaultItemOpts
                                            | color = color
                                            , format = \a -> text <| a ++ ":"
                                        }
                                    }
                                , selectable = False
                                }
                          , val =
                                { item =
                                    typeToTreeItem
                                        colors
                                        nameTags
                                        (path ++ [ TT.StepKey "in" Val ])
                                        payload.input
                                , selectable = True
                                }
                                    |> Just
                          }
                        , { key =
                                { item =
                                    { r =
                                        { path = path ++ [ TT.StepKey "out" Key ]
                                        , key = "out"
                                        }
                                    , o =
                                        { defaultItemOpts
                                            | color = color
                                            , format = \a -> text <| a ++ ":"
                                        }
                                    }
                                , selectable = False
                                }
                          , val =
                                { item =
                                    typeToTreeItem
                                        colors
                                        nameTags
                                        (path ++ [ TT.StepKey "out" Val ])
                                        payload.output
                                , selectable = True
                                }
                                    |> Just
                          }
                        ]
                            |> TT.ContentDict
                            |> Just
                }
            }

        SList payload ->
            let
                color =
                    colors.schemaList
            in
            { r = { path = path, key = "List" }
            , o =
                { defaultItemOpts
                    | color = color
                    , content =
                        [ { key =
                                { item =
                                    { r =
                                        { path = path ++ [ TT.StepKey "of" Key ]
                                        , key = "of"
                                        }
                                    , o = { defaultItemOpts | color = color }
                                    }
                                , selectable = False
                                }
                          , val =
                                { item =
                                    typeToTreeItem
                                        colors
                                        nameTags
                                        (path ++ [ TT.StepKey "of" Val ])
                                        payload.vType
                                , selectable = True
                                }
                                    |> Just
                          }
                        ]
                            |> TT.ContentDict
                            |> Just
                }
            }

        SRecord attrs ->
            { r = { path = path, key = "Record" }
            , o =
                { defaultItemOpts
                    | color = colors.text
                    , content =
                        (Dict.toList attrs
                            |> List.indexedMap
                                (\idx ( k, v ) ->
                                    { key =
                                        { item =
                                            { r =
                                                { path = path ++ [ TT.StepKey k Key ]
                                                , key = k
                                                }
                                            , o =
                                                { defaultItemOpts
                                                    | color = colors.text
                                                    , format = \a -> text <| a ++ ":"
                                                }
                                            }
                                        , selectable = True
                                        }
                                    , val =
                                        { item =
                                            typeToTreeItem
                                                colors
                                                nameTags
                                                (path ++ [ TT.StepKey k Val ])
                                                v
                                        , selectable = True
                                        }
                                            |> Just
                                    }
                                )
                        )
                            |> TT.ContentDict
                            |> Just
                }
            }
