module DbTypesUtils exposing (flatTemplates)

import DbTypes exposing (..)
import Dict


flatTemplates : Templates -> List Template
flatTemplates templates =
    Dict.values templates
        |> List.map Dict.values
        |> List.concat
