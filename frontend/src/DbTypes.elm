module DbTypes exposing
    ( BasicDbEntry
    , Constant
    , EventGenerator
    , EventGeneratorBase
    , FullUnionMemberSchema
    , FunctionSchema
    , IdRefSchema
    , Joint(..)
    , JointFrom(..)
    , Link
    , ListSchema
    , NameTag
    , NameTags
    , NewEventGenerator
    , Node
    , RecordSchema
    , Schema(..)
    , ShortUnionMemberSchema
    , TSEventGenerator
    , Template
    , TemplateBase
    , TemplateSchema(..)
    , Templates
    , UnionMember(..)
    , UnionSchema
    )

import CommonData.Main exposing (CommonData)
import Dict exposing (Dict)
import GenericTypes exposing (SimpleRef)
import Tree.Types exposing (Path)


type alias ListSchema =
    { vType : Schema
    }


type alias FullUnionMemberSchema =
    { uId : String
    , vType : Maybe Schema
    }


type alias ShortUnionMemberSchema =
    String


type UnionMember
    = ShortUnionMember ShortUnionMemberSchema
    | FullUnionMember FullUnionMemberSchema


type alias UnionSchema =
    List UnionMember


type alias FunctionSchema =
    { input : Schema
    , output : Schema
    }


type alias IdRefSchema =
    { id : String
    , version : Int
    , generics : Dict String Schema
    }


type alias RecordSchema =
    Dict String Schema


type Schema
    = SBool
    | SErr String
    | SFloat
    | SFunction FunctionSchema
    | SGeneric String
    | SIdRef IdRefSchema
    | SInt
    | SList ListSchema
    | SRecord RecordSchema
    | SString
    | SUnion UnionSchema
    | SJson


type alias BasicDbEntry =
    { id : String
    , version : Int
    , schema : Schema
    , createdAt : String
    , deletedAt : Maybe String
    }


type alias NameTag =
    { id : String
    , version : Int
    , name : String
    , targetId : String
    , targetVersion : Int
    , schemaPath : Path
    , createdAt : String
    , deletedAt : Maybe String
    }


type alias NameTags =
    Dict String (List NameTag)


type alias EventGeneratorBase =
    { id : String
    , version : Int
    , config : CommonData
    , template : SimpleRef
    , createdAt : String
    , deletedAt : Maybe String
    }


type alias EventGenerator =
    { base : EventGeneratorBase
    , error : Maybe String
    }


type alias TSEventGenerator =
    { config : Schema
    , defaults : CommonData
    , emits : Schema
    , callback : Schema
    }


type alias TemplateBase =
    { id : String
    , name : String
    , version : Int
    , createdAt : String
    , deletedAt : Maybe String
    }


type TemplateSchema
    = TemplateEventGenerator TSEventGenerator
    | TemplateFunction FunctionSchema
    | TemplateSpecial


type alias Template =
    { base : TemplateBase
    , schema : TemplateSchema
    }


type alias Templates =
    Dict String (Dict Int Template)


type alias Constant =
    { id : String
    , version : Int
    , value : CommonData
    , createdAt : String
    , deletedAt : Maybe String
    }


type JointFrom
    = JointFromRef SimpleRef
    | JointFromPath Path


type Joint
    = ConstantJoint SimpleRef Path
    | RewireJoint Path Path


type alias Link =
    { id : String
    , version : Int
    , error : Maybe String
    , from : String
    , to : Maybe String
    , joints : List Joint
    , createdAt : String
    , deletedAt : Maybe String
    }


type alias Node =
    { id : String
    , version : Int
    , targetId : String
    , targetVersion : Int
    , createdAt : String
    , deletedAt : Maybe String
    }


type alias NewEventGenerator =
    { id : String
    , version : Int
    , createdAt : String
    , config : CommonData
    }
