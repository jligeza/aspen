module Encoders exposing
    ( encodeCommonDataPath
    , encodeSchema
    , listActiveNodes
    , listConstants
    , listEventGenerators
    , listLinks
    , listNameTags
    , listNodes
    , listTemplates
    , listTypes
    , saveConstant
    , saveLink
    , saveNameTag
    , saveNewNode
    , saveType
    )

import CommonData.Main as CD
import DbTypes exposing (..)
import Json.Encode exposing (..)
import Schema.Utils as SU
import Tree.Types as TT
import Types exposing (NewNameTag, NewNodePayload(..))


listActiveNodes : Value
listActiveNodes =
    object
        [ ( "taskType", string "listActiveNodes" )
        , ( "payload", object [] )
        ]


listLinks : Value
listLinks =
    object
        [ ( "taskType", string "listLinks" )
        , ( "payload", object [] )
        ]


listNodes : Value
listNodes =
    object
        [ ( "taskType", string "listNodes" )
        , ( "payload", object [] )
        ]


listConstants : Value
listConstants =
    object
        [ ( "taskType", string "listConsts" )
        , ( "payload", object [] )
        ]


listTemplates : Value
listTemplates =
    object
        [ ( "taskType", string "listTemplates" )
        , ( "payload", object [] )
        ]


listEventGenerators : Value
listEventGenerators =
    object
        [ ( "taskType", string "listInitializedEventGenerators" )
        , ( "payload", object [] )
        ]


listTypes : Value
listTypes =
    object
        [ ( "taskType", string "listTypes" )
        , ( "payload", object [] )
        ]


listNameTags : Value
listNameTags =
    object
        [ ( "taskType", string "listNameTags" )
        , ( "payload", object [] )
        ]


saveType : Schema -> Value
saveType schema =
    object
        [ ( "taskType", string "saveType" )
        , ( "payload", object [ ( "schema", encodeSchema schema ) ] )
        ]


saveNewNode : NewNodePayload -> Value
saveNewNode payload =
    case payload of
        NNPCommon p ->
            object
                [ ( "taskType", string "saveNode" )
                , ( "payload"
                  , object
                        [ ( "targetId", string p.targetId )
                        , ( "targetVersion", int p.targetVersion )
                        ]
                  )
                ]

        NNPEventGenerator p ->
            object
                [ ( "taskType", string "saveEventGenerator" )
                , ( "payload"
                  , object
                        [ ( "$config", encodeCommonData p.config )
                        , ( "templateId", string p.templateId )
                        , ( "version", int p.templateVersion )
                        ]
                  )
                ]


saveConstant : CD.CommonData -> Value
saveConstant const =
    object
        [ ( "taskType", string "saveConst" )
        , ( "payload", object [ ( "value", encodeCommonData const ) ] )
        ]


encodeCommonData : CD.CommonData -> Value
encodeCommonData val =
    case val of
        CD.CRecord attrs ->
            dict identity encodeCommonData attrs

        CD.CInt maybeInt ->
            case maybeInt of
                Nothing ->
                    null

                Just num ->
                    int num

        CD.CFloat maybeFloat ->
            case maybeFloat of
                Nothing ->
                    null

                Just num ->
                    case String.toFloat num of
                        Nothing ->
                            null

                        Just a ->
                            float a

        CD.CString str ->
            string str

        CD.CJson payload ->
            case payload of
                Nothing ->
                    null

                Just str ->
                    object
                        [ ( "$type", string "Json" )
                        , ( "$val", string str )
                        ]

        CD.CBool maybeBool ->
            case maybeBool of
                Nothing ->
                    null

                Just bool_ ->
                    bool bool_

        CD.CRef maybeRef ->
            case maybeRef of
                Nothing ->
                    null

                Just ref ->
                    object
                        [ ( "$id", string ref.id )
                        , ( "$version", int ref.version )
                        ]

        CD.CList items ->
            list encodeCommonData items

        CD.CUnion maybeChoice ->
            case maybeChoice of
                Nothing ->
                    null

                Just ( key, Nothing ) ->
                    object
                        [ ( "$type", string "UnionMember" )
                        , ( "$choice", string key )
                        ]

                Just ( key, Just content ) ->
                    object
                        [ ( "$type", string "UnionMember" )
                        , ( "$choice", string key )
                        , ( "$content", encodeCommonData content )
                        ]


encodeSchemaPath : TT.Path -> Schema -> Value
encodeSchemaPath path schema_ =
    List.foldl
        (\step acc ->
            case step of
                TT.StepIdx _ ->
                    acc

                TT.StepKey key kv ->
                    case SU.getFocusedSchema [ step ] acc.schema of
                        Nothing ->
                            acc

                        Just s ->
                            case acc.schema of
                                SBool ->
                                    acc

                                SErr _ ->
                                    acc

                                SFloat ->
                                    acc

                                SGeneric _ ->
                                    acc

                                SInt ->
                                    acc

                                SString ->
                                    acc

                                SJson ->
                                    acc

                                SRecord attrs ->
                                    { acc | schema = s, path = string key :: acc.path }

                                SFunction fSchema ->
                                    { acc
                                        | schema = s
                                        , path =
                                            if String.startsWith "i" key then
                                                string "$input" :: acc.path

                                            else
                                                string "$output" :: acc.path
                                    }

                                SIdRef iSchema ->
                                    { acc | schema = s, path = string "$generics" :: acc.path }

                                SList lSchema ->
                                    { acc | schema = s, path = string "$valsType" :: acc.path }

                                SUnion memebers ->
                                    { acc | schema = s, path = string key :: acc.path }
        )
        { schema = schema_
        , path = []
        }
        path
        |> .path
        |> List.reverse
        |> list identity


saveNameTag : Schema -> String -> Maybe Int -> NewNameTag -> Value
saveNameTag schema targetId maybeTargetVersion tag =
    object
        [ ( "taskType", string "saveNameTag" )
        , ( "payload"
          , object
                ([ ( "targetId", string targetId )
                 , ( "name", string tag.name )
                 , ( "schemaPath", encodeSchemaPath tag.schemaPath schema )
                 ]
                    |> (case maybeTargetVersion of
                            Nothing ->
                                identity

                            Just ver ->
                                (::) <| ( "targetVersion", int ver )
                       )
                )
          )
        ]


saveLink : Link -> Value
saveLink link =
    object
        [ ( "taskType", string "saveLink" )
        , ( "payload", encodeLink link )
        ]


encodeLink : Link -> Value
encodeLink link =
    object
        [ ( "from", string link.from )
        , ( "to"
          , case link.to of
                Nothing ->
                    null

                Just to ->
                    string to
          )
        , ( "joints", list encodeLinkJoint link.joints )
        ]


encodeLinkJoint : Joint -> Value
encodeLinkJoint joint =
    case joint of
        ConstantJoint ref pathTo ->
            tuple2Encoder encodeRef encodePathSimple ( ref, pathTo )

        RewireJoint pathFrom pathTo ->
            tuple2Encoder encodePathSimple encodePathSimple ( pathFrom, pathTo )


encodePathSimple : TT.Path -> Value
encodePathSimple path =
    path
        |> List.map
            (\step ->
                case step of
                    TT.StepIdx idx ->
                        int idx

                    TT.StepKey key _ ->
                        string key
            )
        |> List.reverse
        |> list identity


encodeCommonDataPath : CD.Db a -> CD.Path -> CD.CommonData -> Value
encodeCommonDataPath db path data_ =
    List.foldl
        (\step acc ->
            case CD.find db [ step ] acc.data of
                Nothing ->
                    acc

                Just data ->
                    let
                        addStep a =
                            { acc | data = data, path = a :: acc.path }
                    in
                    case step of
                        TT.StepIdx idx ->
                            addStep <| int idx

                        TT.StepKey key kv ->
                            case data of
                                CD.CList _ ->
                                    addStep <| string "$content"

                                _ ->
                                    addStep <| string key
        )
        { data = data_
        , path = []
        }
        path
        |> .path
        |> List.reverse
        |> list identity


tuple2Encoder : (a -> Value) -> (b -> Value) -> ( a, b ) -> Value
tuple2Encoder enc1 enc2 ( val1, val2 ) =
    list identity [ enc1 val1, enc2 val2 ]


encodeRef : { a | id : String, version : Int } -> Value
encodeRef ref =
    object
        [ ( "$id", string ref.id )
        , ( "$version", int ref.version )
        ]


encodeSchema : Schema -> Value
encodeSchema schema =
    case schema of
        SBool ->
            string "Bool"

        SErr txt ->
            string "!error"

        SFloat ->
            string "Float"

        SJson ->
            string "Json"

        SFunction fSchema ->
            object
                [ ( "$input", encodeSchema fSchema.input )
                , ( "$output", encodeSchema fSchema.output )
                ]

        SGeneric name ->
            string name

        SIdRef ref ->
            object
                [ ( "$id", string ref.id )
                , ( "$version", int ref.version )
                , ( "$generics", dict identity encodeSchema ref.generics )
                ]

        SInt ->
            string "Int"

        SList lSchema ->
            object [ ( "$valsType", encodeSchema lSchema.vType ) ]

        SRecord attrs ->
            dict identity encodeSchema attrs

        SString ->
            string "String"

        SUnion members ->
            list
                (\member ->
                    case member of
                        ShortUnionMember uId ->
                            string uId

                        FullUnionMember a ->
                            let
                                content =
                                    case a.vType of
                                        Nothing ->
                                            null

                                        Just contentSchema ->
                                            encodeSchema contentSchema
                            in
                            object
                                [ ( "$type", string "UnionMember" )
                                , ( "$choice", string a.uId )
                                , ( "$content", content )
                                ]
                )
                members
