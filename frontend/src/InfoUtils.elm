module InfoUtils exposing (newTextView)

import Colors exposing (Colors)
import FormControls exposing (labelInput)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Types exposing (..)


newTextView : Colors -> String -> (String -> Msg) -> Html Msg
newTextView colors val update =
    div
        []
        [ labelInput
            colors
            "New text"
            [ value val
            , onInput update
            ]
        ]
