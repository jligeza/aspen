module Templates exposing
    ( templatesButtons
    , templatesList
    )

import ActionButtons.Definitions exposing (..)
import Colors exposing (Colors)
import CommonData.Tree
import DbTypes exposing (..)
import DbTypesUtils exposing (flatTemplates)
import Dict
import Grid.Extra
import Grid.Main
import Grid.Types
import Html exposing (..)
import Html.Attributes exposing (..)
import Keyboard exposing (Key(..))
import Schema.Tree as ST
import Schema.Utils as SU
import Tree.Main
import Tree.Types
import Types exposing (..)


eventGeneratorTile : Colors -> NameTags -> TSEventGenerator -> Html Msg
eventGeneratorTile colors nameTags evgn =
    let
        label txt =
            i
                [ style "font-weight" "bold" ]
                [ text txt ]

        contentWrapper content =
            div
                [ style "margin" "0px 0px 15px 10px" ]
                [ content ]

        schemaWrapper schema =
            div
                [ style "margin" "0px 0px 15px 10px" ]
                [ Tree.Main.view
                    { colors = colors, selection = Nothing }
                    Tree.Types.defaultOpts
                    (ST.typeToTreeItem
                        colors
                        nameTags
                        []
                        schema
                    )
                ]
    in
    div
        []
        [ label "config:"
        , schemaWrapper evgn.config
        , label "defaults:"
        , contentWrapper <|
            CommonData.Tree.tree
                colors
                { data = evgn.defaults, selection = Nothing, schema = Nothing }
                CommonData.Tree.defaultOpts
        , label "emits:"
        , schemaWrapper evgn.emits
        , label "callback:"
        , schemaWrapper evgn.callback
        ]


tile : Colors -> NameTags -> Grid.Types.Coord -> Template -> Html Msg
tile colors nameTags coord item =
    div
        [ style "margin-left" "3px"
        ]
        [ case item.schema of
            TemplateFunction schema ->
                Tree.Main.view
                    { selection = Nothing
                    , colors = colors
                    }
                    Tree.Types.defaultOpts
                    (ST.typeToTreeItem
                        colors
                        nameTags
                        []
                        (SFunction schema)
                    )

            TemplateEventGenerator evgn ->
                eventGeneratorTile colors nameTags evgn

            TemplateSpecial ->
                case item.base.name of
                    "return" ->
                        text "Return a value to the event emitter, like a response to http."

                    "set" ->
                        text "Sets a value at path in the envelope."

                    _ ->
                        text "unknown special template"
        ]


templatesList : Model -> Html Msg
templatesList m =
    Grid.Main.grid
        { colors = m.layout.colors
        , width = m.layout.boardSize.width
        , height = m.layout.boardSize.height
        , page = 0
        , coord = ( -1, 0 )
        , tileContent =
            \coord idx item ->
                Grid.Extra.contentWithTitle
                    m.layout.colors
                    False
                    [ text item.base.name ]
                    [ tile
                        m.layout.colors
                        (Result.withDefault Dict.empty m.nameTags)
                        coord
                        item
                    ]
        }
        Grid.Main.defaultOpts
        (flatTemplates m.templates.items)


templatesButtons : Model -> List ActionButton
templatesButtons m =
    let
        setBtn key =
            case key of
                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys
