module CreateNode exposing
    ( createHttp
    , createSet
    )

import ActionButtons.Utils as ABU
import CommonData.Main exposing (CommonData(..))
import CommonData.Utils exposing (schemaToCommonData)
import DbTypes exposing (..)
import Dict
import FetchedUtils as FU
import Ids
import Link.Main
import Types exposing (..)


init : Model -> String -> ( Model, Cmd Msg )
init m id =
    case FU.getLatestTemplate m.templates.items id of
        Just template ->
            ( { m
                | newNode =
                    Just <|
                        { template =
                            { id = template.base.id
                            , version = template.base.version
                            }
                        , saveError = Nothing
                        , tree =
                            { data =
                                case template.schema of
                                    TemplateEventGenerator t ->
                                        case schemaToCommonData t.config of
                                            Err e ->
                                                CString <| "TODO error " ++ e

                                            Ok data ->
                                                data

                                    TemplateFunction io ->
                                        CString "TODO handle func template"

                                    TemplateSpecial ->
                                        CString "special"
                            , selection = Just []
                            , schema = templateSchemaToSchema template.schema
                            }
                        }
              }
            , Cmd.none
            )

        Nothing ->
            ( m, Cmd.none )


templateSchemaToSchema : TemplateSchema -> Maybe Schema
templateSchemaToSchema tSchema =
    case tSchema of
        TemplateEventGenerator schema ->
            Just schema.config

        TemplateFunction schema ->
            Just <| SFunction schema

        TemplateSpecial ->
            Nothing


createHttp : Model -> ( Model, Cmd Msg )
createHttp m =
    init m Ids.ids.templateHttp


createSet : Model -> ( Model, Cmd Msg )
createSet m =
    let
        id =
            String.fromInt m.counter

        newModelBase =
            { m
                | counter = m.counter + 1
                , unsavedNodes =
                    Dict.insert
                        id
                        { id = id
                        , version = 1
                        , targetId = Ids.ids.templateSet
                        , targetVersion = 1
                        , createdAt = ""
                        , deletedAt = Nothing
                        }
                        m.unsavedNodes
            }
    in
    case m.boardSelection of
        Nothing ->
            ( m, Cmd.none )

        Just ref ->
            ( Link.Main.connectLink
                (ABU.popStack
                    { newModelBase
                        | boardSelection =
                            Just { id = id, version = 1 }
                    }
                )
                id
                ref.id
            , Cmd.none
            )
