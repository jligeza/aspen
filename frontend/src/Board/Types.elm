module Board.Types exposing (BoardItem(..))

import DbTypes exposing (EventGenerator, Link, Node)


type BoardItem
    = BIEventGenerator EventGenerator
    | BINode Node
    | BIUnsavedNode Node
    | BILink Link
    | BIUnsavedLink Link
