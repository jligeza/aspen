module Board.Items exposing
    ( TreeItem(..)
    , getRootNode
    )

import DbTypes as DT
import Dict exposing (Dict)
import List.Extra as EList


type Link
    = Link
        { base : DT.Link
        , from : Node
        , to : Maybe Node
        }


type alias Node =
    { id : String
    , version : Int
    , input : Maybe Link
    , output : Maybe Link
    }


type TreeItem
    = LinkItem Link
    | NodeItem Node


type alias Ctx =
    { eventGenerators : List DT.EventGenerator
    , links : Dict String DT.Link
    , linksByFrom : Dict String (List DT.Link)
    , linksByTo : Dict String DT.Link
    , nodes : Dict String (Dict Int DT.Node)
    }


type BadTreeResult
    = NotFound
    | WrongMatch String
    | MissingVersion


findEventGenerator : String -> Maybe Int -> List DT.EventGenerator -> Maybe DT.EventGenerator
findEventGenerator id maybeVer items =
    case maybeVer of
        Nothing ->
            List.filter (\a -> a.base.id == id) items |> EList.last

        Just ver ->
            EList.find (\a -> a.base.id == id && a.base.version == ver) items


getLink : Ctx -> DT.Link -> Result BadTreeResult TreeItem
getLink ctx link =
    case getItemTree ctx link.from Nothing of
        Err e ->
            Err e

        Ok fromItem ->
            case fromItem of
                LinkItem fromLink ->
                    Err <| WrongMatch "expected fromNode, got link"

                NodeItem fromNode ->
                    case getItemTree ctx (Maybe.withDefault "" link.to) Nothing of
                        Err e ->
                            case e of
                                NotFound ->
                                    Ok <|
                                        LinkItem <|
                                            Link
                                                { base = link
                                                , from = fromNode
                                                , to = Nothing
                                                }

                                WrongMatch a ->
                                    Err <| WrongMatch a

                                MissingVersion ->
                                    Err MissingVersion

                        Ok toItem ->
                            case toItem of
                                LinkItem toLink ->
                                    Err <| WrongMatch "expected toNode, got link"

                                NodeItem toNode ->
                                    Ok <|
                                        LinkItem <|
                                            Link
                                                { base = link
                                                , from = fromNode
                                                , to = Just toNode
                                                }


getNode : Ctx -> Maybe DT.Link -> Maybe DT.Link -> String -> Int -> Result BadTreeResult Node
getNode ctx maybeInput maybeOutput id ver =
    let
        getLink_ link =
            case getLink ctx link of
                Err e ->
                    Err e

                Ok item ->
                    case item of
                        NodeItem a ->
                            Err <| WrongMatch "expected link"

                        LinkItem a ->
                            Ok a
    in
    case maybeInput of
        Nothing ->
            case maybeOutput of
                Nothing ->
                    Ok <| Node id ver Nothing Nothing

                Just output ->
                    case getLink_ output of
                        Err e ->
                            Err e

                        Ok outputLink ->
                            Ok <| Node id ver Nothing (Just outputLink)

        Just input ->
            case getLink_ input of
                Err e ->
                    Err e

                Ok inputLink ->
                    case maybeOutput of
                        Nothing ->
                            Ok <| Node id ver (Just inputLink) Nothing

                        Just output ->
                            case getLink_ output of
                                Err e ->
                                    Err e

                                Ok outputLink ->
                                    Ok <| Node id ver (Just inputLink) (Just outputLink)


getNode_ : Ctx -> Maybe DT.Link -> Maybe DT.Link -> String -> Int -> Result BadTreeResult TreeItem
getNode_ ctx maybeInput maybeOutput id ver =
    case getNode ctx maybeInput maybeOutput id ver of
        Err e ->
            Err e

        Ok a ->
            Ok <| NodeItem a


getRootNode : Ctx -> String -> Maybe Int -> Result BadTreeResult Node
getRootNode ctx refId maybeRefVer =
    case getItemTree ctx refId maybeRefVer of
        Err e ->
            Err e

        Ok item ->
            case item of
                NodeItem node ->
                    Ok node

                LinkItem link ->
                    Err <| WrongMatch "expected root node"


getItemTree : Ctx -> String -> Maybe Int -> Result BadTreeResult TreeItem
getItemTree ctx refId maybeRefVer =
    case Dict.get refId ctx.links of
        Just link ->
            getLink ctx link

        Nothing ->
            case Dict.get refId ctx.nodes of
                Just nodesByVer ->
                    case maybeRefVer of
                        Nothing ->
                            Err MissingVersion

                        Just ver ->
                            case Dict.get ver nodesByVer of
                                Nothing ->
                                    Err <| NotFound

                                Just node ->
                                    case Dict.get node.id ctx.linksByFrom of
                                        Nothing ->
                                            case Dict.get node.id ctx.linksByTo of
                                                Nothing ->
                                                    Ok <|
                                                        NodeItem <|
                                                            { id = node.id
                                                            , version = node.version
                                                            , input = Nothing
                                                            , output = Nothing
                                                            }

                                                Just output ->
                                                    getNode_
                                                        ctx
                                                        Nothing
                                                        (Just output)
                                                        node.id
                                                        node.version

                                        Just input ->
                                            case Dict.get node.id ctx.linksByTo of
                                                Nothing ->
                                                    Ok <|
                                                        NodeItem <|
                                                            { id = node.id
                                                            , version = node.version
                                                            , input = Nothing
                                                            , output = Nothing
                                                            }

                                                Just output ->
                                                    getNode_
                                                        ctx
                                                        (List.head input)
                                                        (Just output)
                                                        node.id
                                                        node.version

                Nothing ->
                    case findEventGenerator refId maybeRefVer ctx.eventGenerators of
                        Just evgn ->
                            case Dict.get evgn.base.id ctx.linksByFrom of
                                Nothing ->
                                    Ok <|
                                        NodeItem <|
                                            { id = evgn.base.id
                                            , version = evgn.base.version
                                            , input = Nothing
                                            , output = Nothing
                                            }

                                Just output ->
                                    getNode_
                                        ctx
                                        Nothing
                                        (List.head output)
                                        evgn.base.id
                                        evgn.base.version

                        Nothing ->
                            Err <| NotFound
