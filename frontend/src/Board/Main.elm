module Board.Main exposing (view)

import DbTypes exposing (..)
import Dict
import FetchedUtils
import GenericTypes exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Icons exposing (icons)
import List.Extra as EList
import Maybe exposing (andThen)
import Svg as S
import Svg.Attributes as SA exposing (stroke, strokeWidth, x1, x2, y1, y2)
import Types exposing (..)
import Utils


styles =
    { nodeWidth = "2rem"
    , nodeBorder = "2px"
    }


isSelected : Maybe SimpleRef -> IdVer x -> Bool
isSelected selection ref =
    case selection of
        Nothing ->
            False

        Just a ->
            Utils.idVerMatching a ref


clickable : IdVer a -> List (Attribute Msg) -> List (Attribute Msg)
clickable ref curStyle =
    style "cursor" "pointer"
        :: (Utils.onClickStopPropagation <|
                BoardSelect <|
                    Just (SimpleRef ref.id ref.version)
           )
        :: curStyle


node : Model -> Bool -> IdVer a -> Html Msg
node m error ref =
    let
        c =
            m.layout.colors

        selected =
            isSelected m.boardSelection ref

        isUnsaved =
            Dict.member ref.id m.unsavedNodes

        border =
            if selected then
                styles.nodeBorder ++ " solid " ++ c.text

            else if error then
                styles.nodeBorder ++ " solid " ++ c.error

            else
                styles.nodeBorder ++ " solid " ++ c.primaryD3

        baseAttrs =
            clickable ref
                [ style "display" "inline-block"
                , style "text-align" "center"
                , style "width" styles.nodeWidth
                , style "height" styles.nodeWidth
                , style "border-radius" "50%"
                , style "border" border
                , style "transition" "box-shadow 0.5s"
                ]
    in
    div
        (baseAttrs
            |> (if selected then
                    (::) (style "box-shadow" <| "0px 0px 10px 2px " ++ c.text)

                else
                    identity
               )
        )
        (if isUnsaved then
            [ span
                [ style "color" m.layout.colors.text
                , style "display" "flex"
                , style "flex-direction" "column"
                , style "justify-content" "center"
                , style "height" "100%"
                ]
                [ text icons.unsaved ]
            ]

         else
            []
        )


eventGenerator : Model -> EventGenerator -> Html Msg
eventGenerator m evgn =
    let
        desc err evgnBase =
            span
                [ style "position" "absolute"
                , style "top" "-1.1rem"
                , style "left" "0"
                , style "width" "100%"
                , style "text-align" "center"
                ]
                [ FetchedUtils.getEventGeneratorName
                    m.templates.items
                    (Result.withDefault Dict.empty m.nameTags)
                    evgn
                    |> Maybe.withDefault "http?"
                    |> text
                ]

        node_ nodeStyle a =
            node m nodeStyle a

        hasError =
            evgn.error /= Nothing
    in
    div
        [ style "display" "inline-block"
        , style "position" "relative"
        ]
        [ desc hasError evgn.base
        , node_ hasError evgn.base
        ]


nodeWithDesc : Model -> Maybe String -> IdVer a -> Html Msg
nodeWithDesc m maybeError idVer =
    let
        desc =
            span
                [ style "position" "absolute"
                , style "top" "-1.1rem"
                , style "left" "0"
                , style "width" "100%"
                , style "text-align" "center"
                , style "white-space" "nowrap"
                ]
                [ text
                    (FetchedUtils.getName
                        m
                        (SimpleRef idVer.id idVer.version)
                        |> Maybe.withDefault "?"
                    )
                ]
    in
    div
        [ style "display" "inline-block"
        , style "position" "relative"
        ]
        ([ desc
         , node m False idVer
         ]
            |> (case maybeFollowingLinkView m maybeError idVer of
                    Nothing ->
                        identity

                    Just a ->
                        (::) a
               )
        )


linkView : Model -> Maybe String -> Link -> Html Msg
linkView m maybeError link =
    let
        isUnsaved =
            Dict.member link.id m.unsavedLinks.items

        maybeNextNode =
            case
                link.to
                    |> andThen (\a -> Dict.get a m.nodes)
                    |> andThen (Dict.values >> EList.last)
            of
                Just a ->
                    Just a

                Nothing ->
                    link.to
                        |> andThen (\a -> Dict.get a m.unsavedNodes)

        selected =
            isSelected m.boardSelection link

        color =
            if selected then
                m.layout.colors.text

            else if maybeError /= Nothing then
                m.layout.colors.error

            else
                m.layout.colors.primaryD3

        shadowable =
            if selected then
                (::) <|
                    style "filter" <|
                        "drop-shadow(0 0 5px "
                            ++ m.layout.colors.text
                            ++ ")"

            else
                identity
    in
    div
        ([ style "position" "absolute"
         , style "left" <|
            "calc("
                ++ styles.nodeWidth
                ++ " + (2 * "
                ++ styles.nodeBorder
                ++ "))"
         , style "height" <|
            "calc("
                ++ styles.nodeWidth
                ++ " + "
                ++ styles.nodeBorder
                ++ ")"
         , style "display" "flex"
         , style "flex-direction" "row-reverse"
         ]
            |> clickable link
        )
        ([ div
            [ style "position" "relative"
            , style "width" <| "calc(" ++ styles.nodeWidth ++ " * 3)"
            ]
            ([ S.svg
                ([ SA.width "100%"
                 , SA.height "calc(1px + 100%)"
                 , style "position" "absolute"
                 , style "left" "0"
                 ]
                    |> shadowable
                )
                [ S.line
                    [ x1 "0"
                    , y1 "52%"
                    , x2 "100%"
                    , y2 "52%"
                    , stroke color
                    , strokeWidth styles.nodeBorder
                    ]
                    []
                ]
             ]
                |> (if List.isEmpty link.joints then
                        (::)
                            (div
                                [ style "position" "absolute"
                                , style "width" "100%"
                                , style "display" "flex"
                                , style "justify-content" "flex-end"
                                ]
                                [ span
                                    [ style "color" color
                                    , style "font-size" "200%"
                                    , style "margin-top" "2px"
                                    , style "margin-right" "-3px"
                                    ]
                                    [ text "(" ]
                                ]
                            )

                    else
                        identity
                   )
                |> (if isUnsaved then
                        (::)
                            (div
                                [ style "position" "absolute"
                                , style "width" "100%"
                                , style "display" "flex"
                                , style "justify-content" "center"
                                ]
                                [ span [] [ text Icons.icons.unsaved ]
                                ]
                            )

                    else
                        identity
                   )
            )
         ]
            |> (case maybeNextNode of
                    Nothing ->
                        identity

                    Just nextNode ->
                        (::) <| nodeWithDesc m maybeError nextNode
               )
        )


maybeFollowingLinkView : Model -> Maybe String -> IdVer a -> Maybe (Html Msg)
maybeFollowingLinkView m maybeError item =
    let
        getFirstLink links =
            case links of
                [] ->
                    Nothing

                link :: more ->
                    Just <| linkView m maybeError link
    in
    case Dict.get item.id m.links.byFrom of
        Nothing ->
            case Dict.get item.id m.unsavedLinks.byFrom of
                Nothing ->
                    Nothing

                Just links ->
                    getFirstLink links

        Just links ->
            getFirstLink links


nodesRow : Model -> EventGenerator -> Html Msg
nodesRow m evgn =
    div
        [ style "position" "relative"
        , style "margin-top" "4rem"
        , style "margin-left" "2rem"
        ]
        ([ eventGenerator m evgn
         ]
            |> (case maybeFollowingLinkView m evgn.error evgn.base of
                    Nothing ->
                        identity

                    Just link ->
                        (::) link
               )
        )


nodesRows : Model -> List (Html Msg)
nodesRows m =
    List.map (nodesRow m) m.eventGenerators.items


view : Model -> Html Msg
view m =
    div
        [ style "backgroundColor" m.layout.colors.primary
        , style "overflow" "hidden"
        , onClick BoardClick
        ]
        (nodesRows m)
