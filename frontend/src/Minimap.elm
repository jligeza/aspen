module Minimap exposing (minimapView)

import Html exposing (..)
import Html.Attributes exposing (..)
import Types exposing (Model, Msg(..))


minimapView : Model -> Html Msg
minimapView m =
    let
        colors =
            m.layout.colors
    in
    div
        [ style "backgroundColor" colors.primary
        , style "overflow" "hidden"
        , style "border" ("3px solid " ++ colors.primaryD2)
        ]
        []

