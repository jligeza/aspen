module BigOverlay exposing (bigOverlay)

import ActionButtons.Definitions exposing (..)
import Constants.Main
import DbTypes as DT
import Html exposing (..)
import Html.Attributes exposing (..)
import Link.Editor
import Link.Types
import NewNode.Main
import NewNode.Types
import Templates exposing (templatesList)
import TypeCreator
import Types exposing (..)
import TypesList exposing (typesList)
import Utils exposing (toPx)


typeCreator : Model -> Html Msg
typeCreator m =
    fullSpaceCentered m [ TypeCreator.typeCreator m ]


fullSpaceCentered : Model -> List (Html Msg) -> Html Msg
fullSpaceCentered m =
    div
        [ style "position" "fixed"
        , style "display" "flex"
        , style "flex-direction" "column"
        , style "justify-content" "center"
        , style "align-items" "center"
        , style "width" <| toPx m.layout.boardSize.width
        , style "height" <| toPx m.layout.boardSize.height
        , style "background" "rgba(0,0,0,0.4)"
        , style "overflow" "auto"
        ]


fullSpacePaddingCentered : Model -> List (Html Msg) -> Html Msg
fullSpacePaddingCentered m content =
    div
        [ style "position" "fixed"
        , style "width" <| toPx m.layout.boardSize.width
        , style "height" <| toPx m.layout.boardSize.height
        , style "background" "rgba(0,0,0,0.4)"
        , style "overflow" "auto"
        ]
        [ div
            [ style "padding" "0.5rem"
            , style "box-sizing" "border-box"
            , style "display" "flex"
            , style "flex-direction" "column"
            , style "align-items" "center"
            , style "min-height" "100%"
            , style "justify-content" "center"
            ]
            content
        ]


fullSpace : Model -> List (Html Msg) -> Html Msg
fullSpace m =
    div
        [ style "position" "fixed"
        , style "width" <| toPx m.layout.boardSize.width
        , style "height" <| toPx m.layout.boardSize.height
        , style "left" "0"
        , style "top" "0"
        , style "background" "rgba(0,0,0,0.4)"
        , style "overflow" "auto"
        ]


typesList_ : Model -> Html Msg
typesList_ m =
    fullSpace m [ typesList m ]


templatesList_ : Model -> Html Msg
templatesList_ m =
    fullSpace m [ templatesList m ]


editLinkView : Model -> Link.Types.Editor -> Html Msg
editLinkView m editor =
    fullSpaceCentered m [ Link.Editor.view m editor ]


newNodeView : Model -> NewNode.Types.NewNode -> Html Msg
newNodeView m newNode =
    fullSpaceCentered m [ NewNode.Main.newNodeView m newNode ]


constsBrowser : Model -> Html Msg
constsBrowser m =
    fullSpaceCentered
        m
        [ Constants.Main.browser m ]


constsCreator : Model -> Html Msg
constsCreator m =
    fullSpacePaddingCentered
        m
        [ Constants.Main.creator m ]


bigOverlayByFocus : Model -> Html Msg
bigOverlayByFocus m =
    case m.focus of
        FocusTemplates ->
            templatesList_ m

        FocusCreateType fctFocus ->
            case fctFocus of
                TCFAddRef ->
                    typesList_ m

                _ ->
                    typeCreator m

        FocusHome ->
            div [] []

        FocusTypes ->
            typesList_ m


bigOverlay : Model -> Html Msg
bigOverlay m =
    case m.actionButtons of
        [] ->
            bigOverlayByFocus m

        topBtnSet :: _ ->
            case topBtnSet of
                ABConstsCreator ABCCSelectRef ->
                    constsBrowser m

                ABConstsCreator _ ->
                    constsCreator m

                ABConstsBrowser ->
                    constsBrowser m

                ABEdit subSet ->
                    case subSet of
                        ABELRefs ->
                            constsBrowser m

                        _ ->
                            case m.linkEditor of
                                Nothing ->
                                    div [] []

                                Just editor ->
                                    editLinkView m editor

                ABNewNode ->
                    case m.newNode of
                        Nothing ->
                            bigOverlayByFocus m

                        Just newNode ->
                            newNodeView m newNode

                _ ->
                    bigOverlayByFocus m
