module FetchedUtils exposing
    ( findBoardItemParent
    , findLinkParent
    , findSelectedType
    , getBoardItem
    , getEventGeneratorName
    , getLatestTemplate
    , getName
    , getNameByTemplate
    , getNameTagName
    , getTemplate
    , getTemplateName
    )

import Board.Types exposing (BoardItem(..))
import DbTypes exposing (..)
import Dict
import GenericTypes exposing (SimpleRef)
import Grid.Utils
import List.Extra as EList
import Maybe exposing (andThen)
import Tree.Types exposing (Path)
import Types exposing (..)
import Utils


getTemplateName : NameTags -> Templates -> SimpleRef -> Maybe String
getTemplateName nameTags templates ref =
    case getNameTagName nameTags [] ref of
        Just nameTag ->
            Just nameTag

        Nothing ->
            templates
                |> Dict.get ref.id
                |> Maybe.andThen (Dict.get ref.version)
                |> Maybe.andThen (.base >> .name >> Just)


getName : Model -> SimpleRef -> Maybe String
getName m ref =
    case getNameTagName (Result.withDefault Dict.empty m.nameTags) [] ref of
        Just nameTag ->
            Just nameTag

        Nothing ->
            case
                getBoardItem m ref
            of
                Nothing ->
                    Nothing

                Just item ->
                    case item of
                        BIEventGenerator evgn ->
                            Dict.get evgn.base.template.id m.templates.items
                                |> Maybe.andThen (Dict.get evgn.base.template.version)
                                |> Maybe.andThen (.base >> .name >> Just)

                        BINode node ->
                            Dict.get node.targetId m.templates.items
                                |> Maybe.andThen (Dict.get node.targetVersion)
                                |> Maybe.andThen (.base >> .name >> Just)

                        BILink link ->
                            Just <| "link"

                        BIUnsavedLink link ->
                            Just "unsaved link"

                        BIUnsavedNode node ->
                            Dict.get node.targetId m.templates.items
                                |> Maybe.andThen (Dict.get node.targetVersion)
                                |> Maybe.andThen (.base >> .name >> Just)


getBoardItem : Model -> SimpleRef -> Maybe BoardItem
getBoardItem m ref =
    case EList.find (.base >> Utils.idVerMatching ref) m.eventGenerators.items of
        Just evgn ->
            Just <| BIEventGenerator evgn

        Nothing ->
            case
                m.nodes
                    |> Dict.get ref.id
                    |> Maybe.andThen (Dict.get ref.version)
            of
                Just node ->
                    Just <| BINode node

                Nothing ->
                    case Dict.get ref.id m.links.items of
                        Just link ->
                            Just <| BILink link

                        Nothing ->
                            case Dict.get ref.id m.unsavedLinks.items of
                                Just uLink ->
                                    Just <| BIUnsavedLink uLink

                                Nothing ->
                                    Dict.get ref.id m.unsavedNodes
                                        |> andThen (BIUnsavedNode >> Just)


getEventGeneratorName : Templates -> NameTags -> EventGenerator -> Maybe String
getEventGeneratorName templates nameTags evgn =
    case getNameTagName nameTags [] evgn.base of
        Nothing ->
            getNameByTemplate templates evgn.base.template

        Just tag ->
            Just tag


getNameByTemplate : Templates -> SimpleRef -> Maybe String
getNameByTemplate templates ref =
    templates
        |> Dict.get ref.id
        |> Maybe.andThen (Dict.get ref.version)
        |> Maybe.andThen (Just << .name << .base)


pathCheck : NameTag -> Path -> Bool
pathCheck nameTag path =
    nameTag.schemaPath == path


matchVersionToNameTagEntry : Path -> Int -> List NameTag -> Maybe NameTag
matchVersionToNameTagEntry path version tagEntires =
    case EList.find (\a -> pathCheck a path && a.targetVersion == version) tagEntires of
        Just matchingEntry ->
            Just matchingEntry

        Nothing ->
            List.reverse tagEntires
                |> EList.find (\a -> pathCheck a path)


getNameTagName :
    NameTags
    -> Path
    -> { a | id : String, version : Int }
    -> Maybe String
getNameTagName nameTags path ref =
    Dict.get ref.id nameTags
        |> Maybe.andThen (matchVersionToNameTagEntry path ref.version)
        |> Maybe.andThen (.name >> Just)


getLatestTemplate : Templates -> String -> Maybe Template
getLatestTemplate templates id =
    Dict.get id templates
        |> Maybe.andThen
            (\byVer ->
                EList.last <| Dict.values byVer
            )


getTemplate : { a | templates : { b | items : Templates } } -> SimpleRef -> Maybe Template
getTemplate m ref =
    m.templates.items
        |> Dict.get ref.id
        |> Maybe.andThen (Dict.get ref.version)


findLinkParent : Model -> { a | from : String } -> Maybe SimpleRef
findLinkParent m link =
    case Dict.get link.from m.nodes of
        Just byVer ->
            case Dict.values byVer |> EList.last of
                Nothing ->
                    Nothing

                Just node ->
                    Just { id = node.id, version = node.version }

        Nothing ->
            case
                EList.find
                    (.base >> .id >> (==) link.from)
                    m.eventGenerators.items
            of
                Nothing ->
                    Nothing

                Just evgn ->
                    Just
                        { id = evgn.base.id
                        , version = evgn.base.version
                        }


findBoardItemParent : Model -> String -> Maybe SimpleRef
findBoardItemParent m id =
    case Dict.get id m.unsavedLinks.items of
        Just link ->
            findLinkParent m link

        Nothing ->
            case Dict.get id m.unsavedLinks.byTo of
                Nothing ->
                    Nothing

                Just link_ ->
                    Just { id = link_.id, version = link_.version }


findSelectedType : Model -> Maybe BasicDbEntry
findSelectedType m =
    m.types.selected
        |> Maybe.andThen
            (\selection ->
                case m.types.items of
                    Err err ->
                        Nothing

                    Ok types ->
                        Grid.Utils.findItem_
                            m.types.index
                            selection.coord
                            m.layout.boardSize.width
                            m.layout.boardSize.height
                            types
            )
