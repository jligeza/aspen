module Utils exposing
    ( addOpacity
    , findByCoord
    , findIdVerCoord
    , getPosOfKeyInDict
    , idVerMatching
    , idxToCoord
    , initLast
    , isArrowKey
    , isOk
    , listStartsWith
    , memberBy
    , msgToCmd
    , onClickStopPropagation
    , prettyDate
    , prettyDate_
    , removeBy
    , removeOnce
    , setItemIdVer
    , setItemsIdVer
    , timeDistance
    , timeDistance_
    , toDictById
    , toDictByIdFrom
    , toDictByIdVer
    , toPx
    , toPxF
    , updateIfOnce
    , updateLast
    )

import Date
import Dict exposing (Dict)
import GenericTypes exposing (..)
import Html
import Html.Events exposing (stopPropagationOn)
import Iso8601
import Json.Decode as JD
import Keyboard as K
import List.Extra as EList
import Task
import Time
import Time.Distance


msgToCmd : a -> Cmd a
msgToCmd msg =
    Task.perform (\_ -> msg) <| Task.succeed ()


toPx : Int -> String
toPx int =
    String.fromInt int ++ "px"


toPxF : Float -> String
toPxF float =
    String.fromInt (floor float) ++ "px"


prettyDate : Time.Zone -> Time.Posix -> String
prettyDate zone posix =
    let
        fi =
            String.fromInt >> String.padLeft 2 '0'

        hour =
            Time.toHour zone posix |> fi

        minutes =
            Time.toMinute zone posix |> fi

        seconds =
            Time.toSecond zone posix |> fi

        date =
            Date.fromPosix zone posix |> Date.format "dd.MM.yyyy"
    in
    date ++ " " ++ String.join ":" [ hour, minutes, seconds ]


prettyDate_ : Time.Zone -> String -> String
prettyDate_ zone uglyTime =
    case Iso8601.toTime uglyTime of
        Ok a ->
            prettyDate zone a

        Err _ ->
            uglyTime


timeDistance : Time.Posix -> Time.Posix -> String
timeDistance myTime itemTime =
    Time.Distance.inWords myTime itemTime
        |> String.replace "about " "~"


timeDistance_ : Time.Posix -> String -> String
timeDistance_ myTime uglyTime =
    case Iso8601.toTime uglyTime of
        Ok a ->
            if Time.posixToMillis myTime <= Time.posixToMillis a then
                "few seconds"

            else
                timeDistance myTime a

        Err _ ->
            uglyTime


idVerMatching a b =
    a.id == b.id && a.version == b.version


removeBy : (a -> Bool) -> List a -> List a
removeBy fn list =
    case list of
        [] ->
            []

        x :: xs ->
            if fn x then
                xs

            else
                x :: removeBy fn xs


memberBy : (a -> Bool) -> List a -> Bool
memberBy fn list =
    case list of
        [] ->
            False

        x :: xs ->
            if fn x then
                True

            else
                memberBy fn xs


isArrowKey : K.Key -> Bool
isArrowKey key =
    List.member key [ K.ArrowUp, K.ArrowDown, K.ArrowLeft, K.ArrowRight ]


type alias IdVersion_ a =
    { a
        | id : String
        , version : Int
    }


findIdVerCoord : List (List (IdVersion_ a)) -> IdVersion_ b -> Maybe ( Int, Int )
findIdVerCoord types idVer =
    let
        y =
            EList.findIndex
                (\row ->
                    case EList.find (idVerMatching idVer) row of
                        Nothing ->
                            False

                        Just _ ->
                            True
                )
                types

        x =
            y
                |> Maybe.andThen
                    (\y_ ->
                        case EList.getAt y_ types of
                            Nothing ->
                                Nothing

                            Just row ->
                                EList.findIndex (idVerMatching idVer) row
                    )
    in
    case ( x, y ) of
        ( Just x_, Just y_ ) ->
            Just ( x_, y_ )

        _ ->
            Nothing


findByCoord : List (List a) -> ( Int, Int ) -> Maybe a
findByCoord rows ( x, y ) =
    EList.getAt y rows |> Maybe.andThen (\a -> EList.getAt x a)


idxToCoord : Int -> Int -> ( Int, Int )
idxToCoord maxX idx =
    let
        y =
            idx // maxX

        x =
            idx - y * maxX
    in
    ( x, y )


onClickStopPropagation : a -> Html.Attribute a
onClickStopPropagation msg =
    stopPropagationOn "click" <| JD.succeed ( msg, True )


getPosOfKeyInDict : String -> Dict.Dict String a -> Maybe Int
getPosOfKeyInDict key dict =
    Dict.keys dict
        |> EList.findIndex (\a -> a == key)


initLast : List a -> Maybe a
initLast list =
    EList.init list |> Maybe.andThen EList.last


updateIfOnce : (a -> Bool) -> (a -> a) -> List a -> List a
updateIfOnce checker updater list =
    case list of
        [] ->
            []

        x :: xs ->
            if checker x then
                updater x :: xs

            else
                x :: updateIfOnce checker updater xs


removeOnce : (a -> Bool) -> List a -> List a
removeOnce checker list =
    case list of
        [] ->
            []

        x :: xs ->
            if checker x then
                xs

            else
                x :: removeOnce checker xs


updateLast : (a -> a) -> List a -> List a
updateLast updater list =
    case list of
        [] ->
            []

        x :: xs ->
            case xs of
                [] ->
                    [ updater x ]

                _ ->
                    x :: updateLast updater xs


isOk : Result a b -> Bool
isOk a =
    case a of
        Ok _ ->
            True

        Err _ ->
            False


toDictById : List (WithId a) -> Dict String (WithId a)
toDictById items =
    List.foldl
        (\item acc -> Dict.insert item.id item acc)
        Dict.empty
        items


toDictByIdFrom :
    List (LinkLike a)
    ->
        { items : Dict String (LinkLike a)
        , byFrom : Dict String (List (LinkLike a))
        , byTo : Dict String (LinkLike a)
        }
toDictByIdFrom items =
    List.foldl
        (\item acc ->
            { acc
                | items = Dict.insert item.id item acc.items
                , byFrom =
                    Dict.update item.from
                        (\maybeLinks ->
                            case maybeLinks of
                                Nothing ->
                                    Just [ item ]

                                Just links ->
                                    Just <| item :: links
                        )
                        acc.byFrom
                , byTo =
                    case item.to of
                        Nothing ->
                            acc.byTo

                        Just to ->
                            Dict.insert to item acc.byTo
            }
        )
        { items = Dict.empty, byFrom = Dict.empty, byTo = Dict.empty }
        items


toDictByIdVer : List (IdVer a) -> Dict String (Dict Int (IdVer a))
toDictByIdVer items =
    List.foldl
        (\item acc ->
            Dict.update
                item.id
                (\maybeByVer ->
                    case maybeByVer of
                        Nothing ->
                            Just <| Dict.singleton item.version item

                        Just byVer ->
                            Just <| Dict.insert item.version item byVer
                )
                acc
        )
        Dict.empty
        items


addOpacity : String -> String -> String
addOpacity opacity str =
    if String.startsWith "rgb(" str then
        "rgba("
            ++ String.dropLeft 4 str
            |> String.replace ")" (", " ++ opacity ++ ")")

    else
        str


setItemIdVer : IdVer a -> CollectionIdVer a -> CollectionIdVer a
setItemIdVer item col =
    Dict.update
        item.id
        (\a ->
            case a of
                Nothing ->
                    Dict.singleton item.version item
                        |> Just

                Just old ->
                    Dict.insert item.version item old
                        |> Just
        )
        col


setItemsIdVer : List (IdVer a) -> CollectionIdVer a -> CollectionIdVer a
setItemsIdVer items col =
    List.foldl setItemIdVer col items


listStartsWith : List x -> List x -> Bool
listStartsWith a b =
    case ( a, b ) of
        ( [], [] ) ->
            True

        ( ai :: ais, bi :: bis ) ->
            ai == bi && listStartsWith ais bis

        ( [], _ :: _ ) ->
            True

        ( ai :: ais, [] ) ->
            False
