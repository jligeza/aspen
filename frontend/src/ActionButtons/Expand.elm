module ActionButtons.Expand exposing (expand)

import ActionButtons.Definitions exposing (..)
import ActionButtons.Misc exposing (focusInfoPanel)
import ActionButtons.Utils as ABU
import DbTypes exposing (..)
import Dict
import GenericTypes exposing (KeyVal(..))
import List.Extra as EList
import Schema.Manipulation exposing (unionShortToFull, updateSchema)
import Schema.Utils as SU
import Set
import Setters exposing (setTypeCreation)
import Tree.Types as TT
import Types exposing (..)
import Utils


expandUnionMember : Model -> Schema -> String -> ( Model, Cmd Msg )
expandUnionMember m schema uId =
    case unionShortToFull m.typeCreation.selected schema of
        Nothing ->
            ( m, Cmd.none )

        Just newSchema ->
            ( setTypeCreation
                (\t ->
                    { t
                        | schema = Just newSchema
                        , selected =
                            Utils.updateLast
                                (always <| TT.StepKey uId Val)
                                m.typeCreation.selected
                    }
                )
                m
            , Cmd.none
            )


showNameInput : Model -> ( Model, Cmd Msg )
showNameInput m =
    ( ABU.pushBtnSet ABExpandType m, focusInfoPanel )


expandRef : Model -> Schema -> Schema -> ( Model, Cmd Msg )
expandRef m baseSchema schema =
    case schema of
        SIdRef ref ->
            let
                types =
                    Result.withDefault [] m.types.items
            in
            case EList.find (Utils.idVerMatching ref) types of
                Nothing ->
                    ( m, Cmd.none )

                Just refedType ->
                    case SU.findGenerics refedType.schema |> Set.toList of
                        [] ->
                            ( m, Cmd.none )

                        genericsList ->
                            let
                                genericsDict =
                                    List.map (\a -> ( a, SErr "..." )) genericsList
                                        |> Dict.fromList

                                newSchema =
                                    updateSchema
                                        m.typeCreation.selected
                                        (\toUpdate ->
                                            case toUpdate of
                                                SIdRef a ->
                                                    SIdRef <|
                                                        { ref
                                                            | generics =
                                                                Dict.union
                                                                    a.generics
                                                                    genericsDict
                                                        }

                                                _ ->
                                                    toUpdate
                                        )
                                        baseSchema
                            in
                            ( setTypeCreation (\t -> { t | schema = Just newSchema }) m
                            , Cmd.none
                            )

        _ ->
            ( m, Cmd.none )


expand : Model -> ( Model, Cmd Msg )
expand m =
    let
        tc =
            m.typeCreation
    in
    case tc.schema of
        Nothing ->
            ( m, Cmd.none )

        Just schema ->
            case SU.getFocusedSchema tc.selected schema of
                Nothing ->
                    ( m, Cmd.none )

                Just focused ->
                    case focused of
                        SIdRef _ ->
                            expandRef m schema focused

                        SRecord _ ->
                            showNameInput m

                        SUnion uSchema ->
                            case EList.last tc.selected of
                                Nothing ->
                                    showNameInput m

                                Just lastStep ->
                                    case lastStep of
                                        TT.StepKey uId uStep ->
                                            case uStep of
                                                Val ->
                                                    showNameInput m

                                                Key ->
                                                    expandUnionMember m schema uId

                                        _ ->
                                            showNameInput m

                        _ ->
                            ( m, Cmd.none )
