module ActionButtons.Generic exposing (confirmationButtons)

import ActionButtons.Constants
import ActionButtons.Definitions exposing (..)
import ActionButtons.Utils as ABU
import Keyboard exposing (Key(..))
import Link.Editor
import TypeCreator
import Types exposing (Model)


confirmationButtons : Model -> List ActionButton
confirmationButtons m =
    let
        setBtn key =
            case key of
                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                Character "a" ->
                    ActionButton
                        { key = key, text = "confirm" }
                        { defaultOptions
                            | action = Just Confirm
                            , isDisabled =
                                case ABU.getTopButtonSet m.actionButtons of
                                    ABEdit ABELExpand ->
                                        Link.Editor.canExpandSelected_ m |> not

                                    ABConstsCreator ABCCExpand ->
                                        ActionButtons.Constants.expandConfirmDisabled m

                                    ABConstsCreator ABCCReword ->
                                        ActionButtons.Constants.rewordConfirmDisabled m

                                    ABExpandType ->
                                        TypeCreator.cannotExpandType
                                            m.typeCreation
                                            (Result.withDefault [] m.types.items)

                                    ABRewordType ->
                                        TypeCreator.cannotRewordType m.typeCreation

                                    ABGeneric ->
                                        TypeCreator.cannotAddGeneric m.typeCreation

                                    ABNameTag ->
                                        TypeCreator.cannotAddNameTag m.typeCreation

                                    ABRef ->
                                        m.types.selected == Nothing

                                    _ ->
                                        False
                        }

                Character "q" ->
                    ActionButton
                        { key = key, text = "focus form" }
                        { defaultOptions
                            | action = Just FocusForm
                            , bgColor =
                                if m.focusedInput then
                                    SelectedBg

                                else
                                    NormalBg
                        }

                Character "r" ->
                    ActionButton
                        { key = key, text = "clear" }
                        { defaultOptions
                            | action = Just ClearForm
                            , isDisabled =
                                case ABU.getTopButtonSet m.actionButtons of
                                    ABExpandType ->
                                        String.isEmpty m.typeCreation.expandText

                                    ABNameTag ->
                                        String.isEmpty m.typeCreation.nameTagText

                                    _ ->
                                        False
                        }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys
