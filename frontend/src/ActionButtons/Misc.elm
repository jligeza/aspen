module ActionButtons.Misc exposing (focusInfoPanel, focusSelection)

import Definitions
import Ports


focusInfoPanel : Cmd msg
focusInfoPanel =
    Ports.focus "#info-panel input"


focusSelection : Maybe String -> Cmd msg
focusSelection selector =
    Ports.focus <| "#" ++ (selector |> Maybe.withDefault Definitions.selectionId)
