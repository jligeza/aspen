module ActionButtons.NewNode exposing
    ( newNodeFormMainButtons
    , newNodeMainButtons
    , newSetButtons
    , update
    )

import ActionButtons.Definitions exposing (..)
import ActionButtons.Utils as ABU
import CommonData.Main as CD exposing (CommonData(..))
import CommonData.Utils as CDU
import DbTypes exposing (..)
import Dict
import GenericTypes exposing (KeyVal(..))
import Keyboard exposing (Key(..))
import List.Extra as EList
import Setters
import Tree.Types as TT
import Types exposing (..)
import Utils


save : Model -> ( Model, Cmd Msg )
save m =
    case m.newNode of
        Nothing ->
            ( m, Cmd.none )

        Just newNode ->
            ( m
            , NNPEventGenerator
                { templateId = newNode.template.id
                , templateVersion = newNode.template.version
                , config = newNode.tree.data
                }
                |> CreateNewNode
                |> AjaxSend
                |> Ajax
                |> Utils.msgToCmd
            )


mainAction : Model -> Model
mainAction m =
    ABU.pushBtnSet ABNewNode m


discard : Model -> Model
discard m =
    { m | newNode = Nothing }


deletePart : Model -> Model
deletePart m =
    let
        setNewDataAndSelection newNode initPath idx =
            let
                newData =
                    CD.updateByPath
                        initPath
                        (\a ->
                            case a of
                                CList items ->
                                    CList <| EList.removeAt idx items

                                _ ->
                                    a
                        )
                        newNode.data
            in
            Setters.setTree
                (\tree ->
                    { tree
                        | data = newData
                        , selection =
                            case newNode.selection of
                                Nothing ->
                                    newNode.selection

                                Just selection ->
                                    case
                                        CD.find
                                            Dict.empty
                                            selection
                                            newData
                                    of
                                        Just _ ->
                                            newNode.selection

                                        Nothing ->
                                            if idx > 0 then
                                                List.indexedMap
                                                    (\i s ->
                                                        if i == idx then
                                                            TT.StepIdx <| idx - 1

                                                        else
                                                            s
                                                    )
                                                    selection
                                                    |> Just

                                            else
                                                case EList.last initPath of
                                                    Nothing ->
                                                        Just initPath

                                                    Just step ->
                                                        case step of
                                                            TT.StepKey key kv ->
                                                                case EList.init initPath of
                                                                    Nothing ->
                                                                        Just []

                                                                    Just a ->
                                                                        a
                                                                            ++ [ TT.StepKey key Key ]
                                                                            |> Just

                                                            _ ->
                                                                Just initPath
                    }
                )
                newNode

        setNewNode newNode =
            case EList.last (newNode.tree.selection |> Maybe.withDefault []) of
                Nothing ->
                    newNode

                Just (TT.StepIdx idx) ->
                    --     case EList.init (newNode.tree.selection |> Maybe.withDefault []) of
                    --         Nothing ->
                    --             newNode
                    --         Just initPath ->
                    --             setNewDataAndSelection newNode initPath idx
                    -- _ ->
                    newNode

                _ ->
                    newNode
    in
    Setters.setNewNode (Maybe.map setNewNode) m


expand : Model -> Model
expand m =
    case m.newNode of
        Nothing ->
            m

        Just newNode ->
            Setters.setNewNodeTree
                (\tree ->
                    case
                        m.templates.items
                            |> Dict.get newNode.template.id
                            |> Maybe.andThen (Dict.get newNode.template.version)
                    of
                        Nothing ->
                            tree

                        Just template ->
                            case template.schema of
                                TemplateEventGenerator tSchema ->
                                    case newNode.tree.selection of
                                        Nothing ->
                                            tree

                                        Just selection ->
                                            { tree
                                                | data =
                                                    CDU.expand
                                                        selection
                                                        tSchema.config
                                                        tree.data
                                            }

                                TemplateFunction _ ->
                                    tree

                                TemplateSpecial ->
                                    tree
                )
                m


update : Model -> NewNodeAction -> ( Model, Cmd Msg )
update m payload =
    case payload of
        NNDelete ->
            ( deletePart m, Cmd.none )

        NNMain ->
            ( mainAction m, Cmd.none )

        NNDiscard ->
            ( discard m, Cmd.none )

        NNExpand ->
            ( expand m, Cmd.none )

        NNSave ->
            save m


newNodeFormMainButtons : Model -> List ActionButton
newNodeFormMainButtons m =
    let
        setBtn key =
            case key of
                Character "q" ->
                    ActionButton
                        { key = key, text = "save" }
                        { defaultOptions | action = Just <| NewNodeAct NNSave }

                Character "a" ->
                    ActionButton
                        { key = key, text = "expand" }
                        { defaultOptions
                            | action = Just <| NewNodeAct NNExpand
                            , isDisabled =
                                case m.newNode of
                                    Nothing ->
                                        True

                                    Just newNode ->
                                        case newNode.tree.selection of
                                            Nothing ->
                                                True

                                            Just selection ->
                                                case
                                                    CD.find
                                                        m.constants.items
                                                        selection
                                                        newNode.tree.data
                                                of
                                                    Nothing ->
                                                        True

                                                    Just val ->
                                                        case val of
                                                            CList _ ->
                                                                False

                                                            _ ->
                                                                True
                        }

                Character "s" ->
                    let
                        ( path, val ) =
                            case m.newNode of
                                Nothing ->
                                    ( Nothing, Nothing )

                                Just newNode ->
                                    ( newNode.tree.selection, Just newNode.tree.data )

                        o =
                            CDU.focusSelectionButtonDefaults
                    in
                    CDU.focusSelectionButton m key { o | path = path, val = val }

                Character "f" ->
                    ActionButton
                        { key = key, text = "delete" }
                        { defaultOptions
                            | action = Just <| NewNodeAct NNDelete
                            , isDisabled =
                                case m.newNode of
                                    Nothing ->
                                        True

                                    Just newNode ->
                                        case newNode.tree.selection of
                                            Nothing ->
                                                True

                                            Just selection ->
                                                CDU.canDelete
                                                    m.constants.items
                                                    selection
                                                    newNode.tree.data
                                                    |> not
                        }

                Character "r" ->
                    ActionButton
                        { key = key, text = "discard" }
                        { defaultOptions | action = Just <| NewNodeAct NNDiscard }

                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys


newSetButtons : Model -> List ActionButton
newSetButtons m =
    let
        setBtn key =
            case key of
                Character "q" ->
                    ActionButton
                        { key = key, text = "save" }
                        { defaultOptions | action = Just <| NewNodeAct NNSave }

                Character "a" ->
                    ActionButton
                        { key = key, text = "expand" }
                        { defaultOptions | action = Just <| FocusSelection Nothing }

                Character "s" ->
                    let
                        ( path, val ) =
                            case m.newNode of
                                Nothing ->
                                    ( Nothing, Nothing )

                                Just newNode ->
                                    ( newNode.tree.selection, Just newNode.tree.data )

                        o =
                            CDU.focusSelectionButtonDefaults
                    in
                    CDU.focusSelectionButton m key { o | path = path, val = val }

                Character "f" ->
                    ActionButton
                        { key = key, text = "delete" }
                        { defaultOptions | action = Just <| NewNodeAct NNDelete }

                Character "r" ->
                    ActionButton
                        { key = key, text = "discard" }
                        { defaultOptions | action = Just <| NewNodeAct NNDiscard }

                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys


newNodeMainButtons : Model -> List ActionButton
newNodeMainButtons m =
    let
        setBtn key =
            case key of
                Character "r" ->
                    ActionButton
                        { key = key, text = "http" }
                        { defaultOptions | action = Just BuildHttp }

                Character "a" ->
                    ActionButton
                        { key = key, text = "set" }
                        { defaultOptions
                            | action = Just BuildSet
                            , isDisabled =
                                case m.boardSelection of
                                    Nothing ->
                                        True

                                    Just ref ->
                                        not <|
                                            Dict.member ref.id m.unsavedLinks.items
                                                || Dict.member ref.id m.links.items
                        }

                Character "z" ->
                    ActionButton
                        { key = key, text = "browse templ" }
                        { defaultOptions | action = Just BrowseTemplates }

                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys
