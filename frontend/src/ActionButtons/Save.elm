module ActionButtons.Save exposing
    ( isSaveAllDisabled
    , isSaveDisabled
    , save
    , saveAll
    )

import Dict
import Types exposing (..)
import Utils


save : Model -> ( Model, Cmd Msg )
save m =
    case m.boardSelection of
        Nothing ->
            ( m, Cmd.none )

        Just ref ->
            case Dict.get ref.id m.unsavedNodes of
                Just node ->
                    ( m
                    , NNPCommon
                        { targetId = node.targetId
                        , targetVersion = node.targetVersion
                        , tmpId = ref.id
                        }
                        |> CreateNewNode
                        |> AjaxSend
                        |> Ajax
                        |> Utils.msgToCmd
                    )

                Nothing ->
                    case Dict.get ref.id m.unsavedLinks.items of
                        Just link ->
                            ( m
                            , link
                                |> CreateNewLink
                                |> AjaxSend
                                |> Ajax
                                |> Utils.msgToCmd
                            )

                        Nothing ->
                            ( m, Cmd.none )


saveAll : Model -> ( Model, Cmd Msg )
saveAll m =
    ( m, Cmd.none )


isSaveDisabled : Model -> Bool
isSaveDisabled m =
    List.member ARNewNode m.loading
        || (case m.boardSelection of
                Nothing ->
                    True

                Just ref ->
                    case Dict.get ref.id m.unsavedNodes of
                        Just _ ->
                            False

                        Nothing ->
                            case Dict.get ref.id m.unsavedLinks.items of
                                Nothing ->
                                    True

                                Just uLink ->
                                    case uLink.to of
                                        Nothing ->
                                            True

                                        Just toId ->
                                            case Dict.get toId m.nodes of
                                                Nothing ->
                                                    True

                                                Just _ ->
                                                    False
           )


isSaveAllDisabled : Model -> Bool
isSaveAllDisabled m =
    List.member ARNewNode m.loading
        || Dict.isEmpty m.unsavedLinks.items
        || Dict.isEmpty m.unsavedNodes
