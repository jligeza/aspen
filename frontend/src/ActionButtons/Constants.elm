module ActionButtons.Constants exposing
    ( constsBrowserButtons
    , constsCreatorMainButtons
    , constsMenuButtons
    , expandConfirm
    , expandConfirmDisabled
    , expandRecordClearForm
    , mainButtonsList
    , rewordClearForm
    , rewordConfirm
    , rewordConfirmDisabled
    , update
    )

import ActionButtons.Definitions exposing (..)
import ActionButtons.Misc exposing (focusInfoPanel)
import ActionButtons.Utils as ABU
import CommonData.Main as CD
import CommonData.Utils as CDU
import Constants.Main as CM
import Constants.Types as CT
import DataCreator.Buttons
import GenericTypes
import Grid.Extra
import Keyboard exposing (Key(..))
import List.Extra as EList
import Maybe exposing (..)
import Setters
import Types exposing (..)


mainButtonsList : Model -> List ActionButton
mainButtonsList m =
    case m.constantsCreator.tree of
        Nothing ->
            DataCreator.Buttons.dataCreatorBaseTypesButtons m

        Just val ->
            constsCreatorMainButtons m


constsCreatorMainButtons : Model -> List ActionButton
constsCreatorMainButtons m =
    let
        setBtn key =
            case key of
                Character "q" ->
                    ActionButton
                        { key = key, text = "save" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator CCSave
                            , isDisabled = List.member ARNewConstant m.loading
                        }

                Character "a" ->
                    ActionButton
                        { key = key, text = "expand" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator CCExpand
                            , isDisabled =
                                CM.findSelectedCreatorVal m
                                    |> Maybe.map (CDU.isExpandable >> not)
                                    |> withDefault True
                        }

                Character "s" ->
                    let
                        o =
                            CDU.focusSelectionButtonDefaults
                    in
                    CDU.focusSelectionButton
                        m
                        key
                        { o
                            | path = m.constantsCreator.tree |> Maybe.andThen .selection
                            , val = m.constantsCreator.tree |> Maybe.map .data
                        }

                Character "r" ->
                    ActionButton
                        { key = key, text = "discard" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator CCDiscard
                        }

                Character "f" ->
                    ActionButton
                        { key = key, text = "delete" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator CCDelete
                            , isDisabled =
                                case CM.findSelectedCreatorVal m of
                                    Nothing ->
                                        True

                                    _ ->
                                        False
                        }

                Character "z" ->
                    ActionButton
                        { key = key, text = "replace" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator CCReplace
                            , isDisabled =
                                case CM.findSelectedCreatorVal m of
                                    Nothing ->
                                        True

                                    _ ->
                                        False
                        }

                Character "x" ->
                    ActionButton
                        { key = key, text = "reword" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator CCReword
                            , isDisabled =
                                case
                                    ( m.constantsCreator.tree |> Maybe.map .data
                                    , m.constantsCreator.tree
                                        |> Maybe.andThen
                                            (.selection
                                                >> Maybe.withDefault []
                                                >> EList.init
                                            )
                                    )
                                of
                                    ( Just val, Just initPath ) ->
                                        case CD.find m.constants.items initPath val of
                                            Just (CD.CRecord _) ->
                                                False

                                            _ ->
                                                True

                                    _ ->
                                        True
                        }

                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Nothing }
    in
    List.map setBtn availableActionKeys


constsMenuButtons : Model -> List ActionButton
constsMenuButtons m =
    let
        setBtn key =
            case key of
                Character "a" ->
                    ActionButton
                        { key = key, text = "browse" }
                        { defaultOptions | action = Just <| ConstsAct ConstsBrowse }

                Character "d" ->
                    ActionButton
                        { key = key, text = "create" }
                        { defaultOptions | action = Just <| ConstsAct ConstsCreate }

                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys


constsBrowserButtons : Model -> List ActionButton
constsBrowserButtons m =
    let
        setBtn key =
            case key of
                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                Character "q" ->
                    ActionButton
                        { key = key, text = "first page" }
                        { defaultOptions
                            | action = Just <| ConstsAct <| ConstsBrowseNav CBNFirstPage
                            , isDisabled = m.constants.page == 0
                        }

                Character "w" ->
                    ActionButton
                        { key = key, text = "last page" }
                        { defaultOptions
                            | action = Just <| ConstsAct <| ConstsBrowseNav CBNLastPage
                            , isDisabled = m.constants.page == Grid.Extra.getMaxConstantsPage m
                        }

                Character "a" ->
                    ActionButton
                        { key = key, text = "prev page" }
                        { defaultOptions
                            | action = Just <| ConstsAct <| ConstsBrowseNav CBNPrevPage
                            , isDisabled = m.constants.page <= 0
                        }

                Character "s" ->
                    ActionButton
                        { key = key, text = "next page" }
                        { defaultOptions
                            | action = Just <| ConstsAct <| ConstsBrowseNav CBNNextPage
                            , isDisabled = m.constants.page >= Grid.Extra.getMaxConstantsPage m
                        }

                Character "d" ->
                    case ABU.getTopButtonSet m.actionButtons of
                        ABConstsCreator ABCCSelectRef ->
                            ActionButton
                                { key = key, text = "select" }
                                { defaultOptions | action = Just <| ConstsAct <| ConstsBrowseNav CBNFocus }

                        ABEdit ABELRefs ->
                            ActionButton
                                { key = key, text = "select" }
                                { defaultOptions | action = Just <| Edit EditSelectRef }

                        _ ->
                            ActionButton
                                { key = key, text = "" }
                                { defaultOptions | action = Just NoAction }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys


update : Model -> ConstsAction -> ( Model, Cmd Msg )
update m action =
    case action of
        ConstsMain ->
            ( ABU.pushBtnSet ABConstsMenu m, Cmd.none )

        ConstsBrowse ->
            ( ABU.pushBtnSet ABConstsBrowser m, Cmd.none )

        ConstsBrowseNav subAction ->
            case subAction of
                CBNFirstPage ->
                    ( Setters.setConstants
                        (\a -> { a | page = 0, selection = ( 0, 0 ) })
                        m
                    , Cmd.none
                    )

                CBNLastPage ->
                    ( Setters.setConstants
                        (\a ->
                            { a
                                | page = Grid.Extra.getMaxConstantsPage m
                                , selection = ( 0, 0 )
                            }
                        )
                        m
                    , Cmd.none
                    )

                CBNNextPage ->
                    ( Setters.setConstants
                        (\a -> { a | page = a.page + 1, selection = ( 0, 0 ) })
                        m
                    , Cmd.none
                    )

                CBNPrevPage ->
                    ( Setters.setConstants
                        (\a -> { a | page = a.page - 1, selection = ( 0, 0 ) })
                        m
                    , Cmd.none
                    )

                CBNFocus ->
                    case CM.findSelected m of
                        Nothing ->
                            ( m, Cmd.none )

                        Just constant ->
                            let
                                ( newM, cmd ) =
                                    CM.update (CT.FocusConstant constant) m
                            in
                            ( newM |> ABU.popStack |> ABU.popStack, cmd )

        ConstsCreate ->
            ( ABU.pushBtnSet (ABConstsCreator ABCCMain) m, Cmd.none )

        ConstsCreator subAction ->
            case subAction of
                CCDelete ->
                    ( CM.deleteSelected m, Cmd.none )

                CCReplace ->
                    ( ABU.pushBtnSet (ABConstsCreator ABCCReplace) m, Cmd.none )

                CCReword ->
                    ( m
                        |> ABU.pushBtnSet (ABConstsCreator ABCCReword)
                        |> Setters.setConstantsCreator
                            (\a ->
                                { a | rewordText = CM.getRewordText m }
                            )
                    , focusInfoPanel
                    )

                CCExpand ->
                    case CM.findSelectedCreatorVal m of
                        Nothing ->
                            ( m, Cmd.none )

                        Just val ->
                            case val of
                                CD.CRecord _ ->
                                    ( ABU.pushBtnSet (ABConstsCreator ABCCExpand) m
                                    , focusInfoPanel
                                    )

                                CD.CUnion _ ->
                                    ( CM.expandSelected m
                                    , Cmd.none
                                    )

                                CD.CList items ->
                                    case m.constantsCreator.tree of
                                        Nothing ->
                                            ( m, Cmd.none )

                                        Just tree ->
                                            case tree.selection of
                                                Nothing ->
                                                    ( m, Cmd.none )

                                                Just selection ->
                                                    case
                                                        CDU.getListContentTypeBySiblings
                                                            selection
                                                            tree.data
                                                    of
                                                        Nothing ->
                                                            ( ABU.pushBtnSet
                                                                (ABConstsCreator ABCCExpand)
                                                                m
                                                            , Cmd.none
                                                            )

                                                        Just _ ->
                                                            ( CM.expandSelected m, Cmd.none )

                                _ ->
                                    ( m, Cmd.none )

                CCSelectType baseType ->
                    case baseType of
                        GenericTypes.BTRef ->
                            ( ABU.pushBtnSet (ABConstsCreator ABCCSelectRef) m
                            , Cmd.none
                            )

                        _ ->
                            let
                                ( newModel, cmd ) =
                                    CM.update (CT.SelectType baseType) m
                            in
                            ( newModel
                                |> ABU.popStack
                                |> ABU.pushBtnSet (ABConstsCreator ABCCMain)
                            , cmd
                            )

                CCSave ->
                    CM.save m

                CCDiscard ->
                    CM.update CT.Discard m


expandConfirm : Model -> Model
expandConfirm m =
    CM.expandSelected m
        |> ABU.popStack


rewordConfirm : Model -> Model
rewordConfirm m =
    CM.rewordSelected m
        |> ABU.popStack


expandConfirmDisabled : Model -> Bool
expandConfirmDisabled m =
    not <| CM.canExpandSelected m


rewordConfirmDisabled : Model -> Bool
rewordConfirmDisabled m =
    not <| CM.canRewordSelected m


expandRecordClearForm : Model -> Model
expandRecordClearForm m =
    Setters.setConstantsCreator
        (\a -> { a | expandText = "" })
        m


rewordClearForm : Model -> Model
rewordClearForm m =
    Setters.setConstantsCreator
        (\a -> { a | rewordText = "" })
        m
