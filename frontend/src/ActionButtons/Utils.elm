module ActionButtons.Utils exposing
    ( getTopButtonSet
    , popStack
    , popStack_
    , pushBtnSet
    )

import ActionButtons.Definitions exposing (ActionButtonsSet(..), initialSet)
import Types exposing (Model)


popStack_ : Model -> List ActionButtonsSet
popStack_ m =
    case m.actionButtons of
        [] ->
            initialSet

        x :: xs ->
            xs


popStack : Model -> Model
popStack m =
    { m | actionButtons = popStack_ m }


getTopButtonSet : List ActionButtonsSet -> ActionButtonsSet
getTopButtonSet list =
    case list of
        [] ->
            ABHome

        x :: xs ->
            x


pushBtnSet : ActionButtonsSet -> Model -> Model
pushBtnSet set m =
    case m.actionButtons of
        [] ->
            { m | actionButtons = set :: m.actionButtons }

        x :: xs ->
            if x == set then
                m

            else
                { m | actionButtons = set :: m.actionButtons }
