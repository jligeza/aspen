module ActionButtons.Main exposing
    ( findBtnByAction
    , findBtnByAction_
    , findBtnByKey
    , findBtnByKey_
    , onActionButtonPress
    , view
    )

import ActionButtons.Constants
import ActionButtons.Definitions exposing (..)
import ActionButtons.Edit
import ActionButtons.Expand exposing (expand)
import ActionButtons.Generic
import ActionButtons.Home
import ActionButtons.Misc exposing (focusInfoPanel, focusSelection)
import ActionButtons.NewNode
import ActionButtons.Save
import ActionButtons.Utils as ABU
import Colors exposing (Colors)
import CommonData.Main as CD
import Constants.Main
import CreateNode
import DataCreator.Buttons
import DbTypes exposing (..)
import DeletionUtils
import GenericTypes exposing (BaseType(..), KeyVal(..))
import Grid.Extra
import Grid.Main
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Ids
import Keyboard exposing (Key(..))
import Link.Buttons
import Link.Editor
import Link.Main
import List
import List.Extra as EList
import Schema.Manipulation
import Schema.Utils as SU
import Setters exposing (setTypeCreation, setTypes)
import Templates
import Tree.Types
import TypeCreator
import Types exposing (..)
import TypesList
import Utils


findBtnByKey : Model -> ActionButtonsSet -> Key -> Maybe ActionButton
findBtnByKey m set key =
    EList.find (.r >> .key >> (==) key) (getBtnsList m set)


findBtnByKey_ : Model -> Key -> Maybe ActionButton
findBtnByKey_ m key =
    findBtnByKey m (ABU.getTopButtonSet m.actionButtons) key


findBtnByAction : Model -> ActionButtonsSet -> Maybe Action -> Maybe ActionButton
findBtnByAction m set action =
    EList.find (.o >> .action >> (==) action) (getBtnsList m set)


findBtnByAction_ : Model -> Maybe Action -> Maybe ActionButton
findBtnByAction_ m action =
    findBtnByAction m (ABU.getTopButtonSet m.actionButtons) action


getBtnsList : Model -> ActionButtonsSet -> List ActionButton
getBtnsList m set =
    case set of
        ABConstsCreator ABCCReplace ->
            DataCreator.Buttons.dataCreatorBaseTypesButtons m

        ABConstsCreator ABCCReword ->
            ActionButtons.Generic.confirmationButtons m

        ABConstsCreator ABCCMain ->
            ActionButtons.Constants.mainButtonsList m

        ABConstsCreator ABCCSelectType ->
            DataCreator.Buttons.dataCreatorBaseTypesButtons m

        ABConstsCreator ABCCSelectTypeExpandUnion ->
            DataCreator.Buttons.dataCreatorBaseTypesButtons m

        ABConstsCreator ABCCExpand ->
            case Constants.Main.findSelectedCreatorVal m of
                Just (CD.CList []) ->
                    DataCreator.Buttons.dataCreatorBaseTypesButtons m

                _ ->
                    ActionButtons.Generic.confirmationButtons m

        ABConstsCreator ABCCSelectRef ->
            ActionButtons.Constants.constsBrowserButtons m

        ABConstsBrowser ->
            ActionButtons.Constants.constsBrowserButtons m

        ABConstsMenu ->
            ActionButtons.Constants.constsMenuButtons m

        ABHome ->
            ActionButtons.Home.emptySelectionButtons m

        ABCreateType ->
            if m.typeCreation.schema == Nothing then
                TypeCreator.typeCreationBaseTypesButtons m

            else
                TypeCreator.createTypeButtons m

        ABSelectType ->
            TypeCreator.typeCreationBaseTypesButtons m

        ABExpandType ->
            ActionButtons.Generic.confirmationButtons m

        ABEdit subSet ->
            case subSet of
                ABELExpand ->
                    ActionButtons.Generic.confirmationButtons m

                ABELMain ->
                    Link.Buttons.editLinkButtons m

                ABELTypes ->
                    Link.Buttons.editLinkBaseTypesButtons m

                ABELRefs ->
                    ActionButtons.Constants.constsBrowserButtons m

        ABRewordType ->
            ActionButtons.Generic.confirmationButtons m

        ABGeneric ->
            ActionButtons.Generic.confirmationButtons m

        ABNameTag ->
            ActionButtons.Generic.confirmationButtons m

        ABRef ->
            TypesList.typesButtons m

        ABReplaceType ->
            TypeCreator.typeCreationBaseTypesButtons m

        ABTypes ->
            TypesList.typesButtons m

        ABNewNode ->
            case m.newNode of
                Nothing ->
                    ActionButtons.NewNode.newNodeMainButtons m

                Just newNode ->
                    if newNode.template.id == Ids.ids.templateSet then
                        ActionButtons.NewNode.newSetButtons m

                    else
                        ActionButtons.NewNode.newNodeFormMainButtons m

        ABTemplates ->
            Templates.templatesButtons m


fancyButton : Colors -> FancyBgColor -> Bool -> ActionButton -> Html Msg
fancyButton colors fancyBgColor isDisabled_ actionButton =
    let
        bgColor =
            case fancyBgColor of
                NormalBg ->
                    colors.primaryD2

                PressedBg ->
                    colors.primaryL1

                SelectedBg ->
                    colors.primary

        extraAttrs =
            if isDisabled_ then
                [ style "opacity" "0.5"
                ]

            else
                [ onMouseUp <| ActionButtonPress actionButton
                , onMouseDown <| ActionButtonSelect actionButton
                ]
    in
    div
        (extraAttrs
            ++ [ style "display" "flex"
               , style "alignItems" "center"
               , style "justifyContent" "center"
               , style "backgroundColor" bgColor
               , style "color" colors.text
               , style "textAlign" "center"
               , style "cursor" "pointer"
               , style "border" ("1px solid " ++ colors.primaryL1)
               , style "margin" "1px"
               ]
        )
        [ actionButton.r.text |> text ]


isPressed : Model -> ActionButton -> Bool
isPressed m actionButton =
    List.member actionButton.r.key m.pressedKeys


getBtnBgColor : Model -> ActionButton -> FancyBgColor
getBtnBgColor m btn =
    if isPressed m btn then
        PressedBg

    else
        btn.o.bgColor


btnToHtml : Model -> ActionButton -> Html Msg
btnToHtml m btn =
    fancyButton m.layout.colors (getBtnBgColor m btn) btn.o.isDisabled btn


actionButtons : Model -> List (Html Msg)
actionButtons m =
    List.map
        (btnToHtml m)
        (getBtnsList m <| ABU.getTopButtonSet m.actionButtons)


onNewList : Model -> Model
onNewList m =
    ABU.pushBtnSet ABReplaceType m
        |> setTypeCreation
            (\t ->
                { t
                    | selected =
                        t.selected ++ [ Tree.Types.StepKey "of" Val ]
                }
            )


onNewGeneric : Model -> Model
onNewGeneric m =
    ABU.pushBtnSet ABGeneric m


onNewRef : Model -> Model
onNewRef m =
    ABU.pushBtnSet ABRef <| browseTypes m


onInsertType : Model -> BaseType -> ( Model, Cmd Msg )
onInsertType m baseType =
    if m.typeCreation.schema == Nothing then
        case baseType of
            BTList ->
                ( onNewList <| onCreateType m baseType, Cmd.none )

            BTGeneric ->
                ( onNewGeneric m, focusInfoPanel )

            BTRef ->
                ( onNewRef m, Cmd.none )

            _ ->
                ( onCreateType m baseType, Cmd.none )

    else
        let
            default =
                ABU.popStack <| onCreateType m baseType
        in
        case baseType of
            BTList ->
                case baseType of
                    BTList ->
                        ( onNewList default, Cmd.none )

                    _ ->
                        ( default, Cmd.none )

            BTGeneric ->
                ( onNewGeneric m, focusInfoPanel )

            BTRef ->
                ( onNewRef m, Cmd.none )

            _ ->
                ( default, Cmd.none )


onCreateType : Model -> BaseType -> Model
onCreateType m baseType =
    case m.focus of
        FocusCreateType tcFocus ->
            let
                set schema =
                    setTypeCreation
                        (\t -> { t | schema = Just schema })
                        m
            in
            case tcFocus of
                TCFReplace ->
                    case m.typeCreation.schema of
                        Nothing ->
                            m

                        Just schema ->
                            set <|
                                Schema.Manipulation.updateSchema
                                    m.typeCreation.selected
                                    (\_ -> SU.baseTypeToSchema baseType)
                                    schema

                _ ->
                    set <| SU.baseTypeToSchema baseType

        _ ->
            m


browseTypes : Model -> Model
browseTypes m =
    let
        newSelected =
            if m.types.selected /= Nothing then
                m.types.selected

            else
                case m.types.items of
                    Err _ ->
                        Nothing

                    Ok types ->
                        Just
                            { coord = ( 0, 0 )
                            , path = Nothing
                            }
    in
    setTypes
        (\t -> { t | selected = newSelected })
        m


editNameTag : Model -> Model
editNameTag m =
    let
        tc =
            m.typeCreation

        name =
            case EList.find (.schemaPath >> (==) tc.selected) tc.nameTags of
                Nothing ->
                    ""

                Just nameTag ->
                    nameTag.name
    in
    setTypeCreation (\t -> { t | nameTagText = name }) m
        |> ABU.pushBtnSet ABNameTag


popAfterTypeSelection m =
    case EList.getAt 1 m.actionButtons of
        Nothing ->
            ABU.popStack m

        Just secondSet ->
            case secondSet of
                ABReplaceType ->
                    ABU.popStack m |> ABU.popStack

                _ ->
                    ABU.popStack m


onActiveActionButtonPress : Model -> Action -> ( Model, Cmd Msg )
onActiveActionButtonPress m action =
    let
        setTypesByIdx idx =
            setTypes (\t -> { t | index = idx }) m

        ( newModel, cmd ) =
            case action of
                NoAction ->
                    ( m, Cmd.none )

                ConstsAct subAction ->
                    ActionButtons.Constants.update m subAction

                ActionButtons.Definitions.Delete ->
                    case m.boardSelection of
                        Nothing ->
                            ( m, Cmd.none )

                        Just ref ->
                            ( DeletionUtils.deleteUnsavedBoardItem m ref.id
                            , Cmd.none
                            )

                BuildHttp ->
                    CreateNode.createHttp m

                BuildSet ->
                    CreateNode.createSet m

                FetchAll_ ->
                    ( m
                    , Utils.msgToCmd <| Ajax AjaxFetchAll
                    )

                BrowseTypes ->
                    ( ABU.pushBtnSet ABTypes <| browseTypes m, Cmd.none )

                NewNodeAct payload ->
                    ActionButtons.NewNode.update m payload

                BrowseTemplates ->
                    ( ABU.pushBtnSet ABTemplates m, Cmd.none )

                GoCreateType ->
                    ( ABU.pushBtnSet ABCreateType m, Cmd.none )

                FocusForm ->
                    ( m, focusInfoPanel )

                Save ->
                    ActionButtons.Save.save m

                SaveAll ->
                    ActionButtons.Save.saveAll m

                Edit subAction ->
                    ActionButtons.Edit.edit m subAction

                ActionButtons.Definitions.Link ->
                    ( Link.Main.create m, Cmd.none )

                CreateType ctAction ->
                    case ctAction of
                        CTAInsert baseType ->
                            onInsertType m baseType

                        CTAExpand ->
                            expand m

                        CTANameTag ->
                            ( editNameTag m, focusInfoPanel )

                        CTAReword ->
                            ( ABU.pushBtnSet ABRewordType m
                                |> setTypeCreation TypeCreator.setDefaultRewordTxt
                            , focusInfoPanel
                            )

                        CTAReplace ->
                            ( ABU.pushBtnSet ABReplaceType m, Cmd.none )

                        CTADelete ->
                            ( case m.typeCreation.schema of
                                Nothing ->
                                    m

                                Just schema ->
                                    setTypeCreation
                                        (\t ->
                                            let
                                                a =
                                                    Schema.Manipulation.delete
                                                        m.typeCreation.selected
                                                        schema
                                            in
                                            { t
                                                | schema = a.schema
                                                , selected = a.path
                                            }
                                        )
                                        m
                            , Cmd.none
                            )

                        CTASave ->
                            case m.typeCreation.schema of
                                Nothing ->
                                    ( m, Cmd.none )

                                Just schema ->
                                    ( m
                                    , CreateNewType schema m.typeCreation.nameTags
                                        |> AjaxSend
                                        |> Ajax
                                        |> Utils.msgToCmd
                                    )

                        CTADiscard ->
                            ( setTypeCreation
                                (\t ->
                                    { t
                                        | schema = Nothing
                                        , selected = []
                                    }
                                )
                                m
                            , Cmd.none
                            )

                TypesFirstPage ->
                    ( setTypesByIdx 0, Cmd.none )

                TypesLastPage ->
                    let
                        sizes =
                            Grid.Main.getSizesInfo_
                                m.layout.boardSize.width
                                m.layout.boardSize.height
                    in
                    ( setTypesByIdx <| Grid.Extra.getMaxTypesPage m
                    , Cmd.none
                    )

                TypesNextPage ->
                    ( setTypesByIdx <| m.types.index + 1, Cmd.none )

                TypesPrevPage ->
                    if m.types.index > 0 then
                        ( setTypesByIdx <| m.types.index - 1, Cmd.none )

                    else
                        ( setTypesByIdx 0, Cmd.none )

                TypesFocus ->
                    case m.types.selected of
                        Nothing ->
                            ( m, Cmd.none )

                        Just selected ->
                            let
                                setSelectedPath path =
                                    setTypes
                                        (\t ->
                                            { t
                                                | selected =
                                                    Just { selected | path = path }
                                            }
                                        )
                                        m
                            in
                            case selected.path of
                                Just _ ->
                                    ( setSelectedPath Nothing, Cmd.none )

                                Nothing ->
                                    ( setSelectedPath <| Just [], Cmd.none )

                Confirm ->
                    case ABU.getTopButtonSet m.actionButtons of
                        ABEdit ABELExpand ->
                            ( Link.Editor.expandWithText_ m |> ABU.popStack
                            , Cmd.none
                            )

                        ABConstsCreator ABCCExpand ->
                            ( ActionButtons.Constants.expandConfirm m
                            , Cmd.none
                            )

                        ABConstsCreator ABCCReword ->
                            ( ActionButtons.Constants.rewordConfirm m
                            , Cmd.none
                            )

                        ABExpandType ->
                            let
                                ( m_, msg ) =
                                    TypeCreator.expandType m
                            in
                            ( ABU.popStack m_, msg )

                        ABRewordType ->
                            let
                                ( m_, msg ) =
                                    TypeCreator.rewordType m
                            in
                            ( ABU.popStack m_, msg )

                        ABGeneric ->
                            let
                                ( m_, msg ) =
                                    TypeCreator.addGeneric m
                            in
                            ( popAfterTypeSelection m_, msg )

                        ABRef ->
                            ( TypeCreator.addRef m |> popAfterTypeSelection
                            , Cmd.none
                            )

                        ABNameTag ->
                            ( TypeCreator.editNameTag m |> popAfterTypeSelection
                            , Cmd.none
                            )

                        _ ->
                            ( m, Cmd.none )

                ClearForm ->
                    case ABU.getTopButtonSet m.actionButtons of
                        ABEdit ABELExpand ->
                            ( Setters.setLinkEditor (Maybe.map (\e -> { e | expandText = "" })) m
                            , Cmd.none
                            )

                        ABConstsCreator ABCCExpand ->
                            ( ActionButtons.Constants.expandRecordClearForm m
                            , Cmd.none
                            )

                        ABConstsCreator ABCCReword ->
                            ( ActionButtons.Constants.rewordClearForm m
                            , Cmd.none
                            )

                        ABExpandType ->
                            ( setTypeCreation (\t -> { t | expandText = "" }) m
                            , Cmd.none
                            )

                        ABRewordType ->
                            ( setTypeCreation (\t -> { t | rewordText = "" }) m
                            , Cmd.none
                            )

                        ABNameTag ->
                            ( setTypeCreation (\t -> { t | nameTagText = "" }) m
                            , Cmd.none
                            )

                        _ ->
                            ( m, Cmd.none )

                FocusSelection selector ->
                    ( m, focusSelection selector )

                Return ->
                    ( ABU.popStack m, Cmd.none )

        newFocus =
            case ABU.getTopButtonSet newModel.actionButtons of
                ABExpandType ->
                    FocusCreateType TCFExpand

                ABSelectType ->
                    FocusCreateType TCFDefault

                ABCreateType ->
                    FocusCreateType TCFDefault

                ABReplaceType ->
                    FocusCreateType TCFReplace

                ABRewordType ->
                    FocusCreateType TCFReword

                ABGeneric ->
                    FocusCreateType TCFAddGeneric

                ABRef ->
                    FocusCreateType TCFAddRef

                ABNameTag ->
                    FocusCreateType TCFNameTag

                ABTypes ->
                    FocusTypes

                ABTemplates ->
                    FocusTemplates

                _ ->
                    FocusHome
    in
    ( { newModel | focus = newFocus }, cmd )


onActionButtonPress : Model -> ActionButton -> ( Model, Cmd Msg )
onActionButtonPress m btn =
    if btn.o.isDisabled then
        ( m, Cmd.none )

    else
        case btn.o.action of
            Nothing ->
                ( m, Cmd.none )

            Just action ->
                onActiveActionButtonPress m action


view : Model -> Html Msg
view m =
    div
        [ style "display" "grid"
        , style "height" <| String.fromInt (m.layout.bottomBarSize.height - 6) ++ "px"
        , style "gridTemplateColumns" "25% 25% 25% 25%"
        , style "gridTemplateRows" "33% 33% 33%"
        , style "border" ("3px solid " ++ m.layout.colors.primaryD2)
        ]
        (m |> actionButtons)
