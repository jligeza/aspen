module ActionButtons.Home exposing (emptySelectionButtons)

import ActionButtons.Definitions exposing (..)
import ActionButtons.Edit
import ActionButtons.Save
import Dict
import Keyboard exposing (Key(..))
import Link.Main
import Types exposing (Model, ajaxFetchableResources)


emptySelectionButtons : Model -> List ActionButton
emptySelectionButtons m =
    let
        setBtn key =
            case key of
                Character "q" ->
                    ActionButton
                        { key = key, text = "save" }
                        { defaultOptions
                            | action = Just Save
                            , isDisabled = ActionButtons.Save.isSaveDisabled m
                        }

                Character "w" ->
                    ActionButton
                        { key = key, text = "save all" }
                        { defaultOptions
                            | action = Just SaveAll
                            , isDisabled =
                                ActionButtons.Save.isSaveAllDisabled m
                        }

                Character "e" ->
                    ActionButton
                        { key = key, text = "edit" }
                        { defaultOptions
                            | action = Just <| Edit EditMain
                            , isDisabled =
                                case m.boardSelection of
                                    Nothing ->
                                        True

                                    Just ref ->
                                        not <| Dict.member ref.id m.unsavedLinks.items
                        }

                Character "z" ->
                    ActionButton
                        { key = key, text = "new node" }
                        { defaultOptions | action = Just <| NewNodeAct NNMain }

                Character "x" ->
                    ActionButton
                        { key = key, text = "new link" }
                        { defaultOptions
                            | action = Just Link
                            , isDisabled =
                                not <| Link.Main.canCreate m
                        }

                Character "a" ->
                    ActionButton
                        { key = key, text = "browse types" }
                        { defaultOptions | action = Just BrowseTypes }

                Character "s" ->
                    ActionButton
                        { key = key, text = "create type" }
                        { defaultOptions | action = Just GoCreateType }

                Character "r" ->
                    ActionButton
                        { key = key, text = "fetch all" }
                        { defaultOptions
                            | action = Just FetchAll_
                            , isDisabled =
                                List.all
                                    (\a -> List.member a m.loading)
                                    ajaxFetchableResources
                        }

                Character "d" ->
                    ActionButton
                        { key = key, text = "consts" }
                        { defaultOptions | action = Just <| ConstsAct ConstsMain }

                Character "f" ->
                    ActionButton
                        { key = key, text = "delete" }
                        { defaultOptions
                            | action = Just ActionButtons.Definitions.Delete
                            , isDisabled =
                                case m.boardSelection of
                                    Nothing ->
                                        True

                                    Just ref ->
                                        not <|
                                            (Dict.member ref.id m.unsavedLinks.items
                                                || Dict.member ref.id m.unsavedNodes
                                            )
                        }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys
