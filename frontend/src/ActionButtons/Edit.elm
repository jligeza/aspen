module ActionButtons.Edit exposing (edit)

import ActionButtons.Definitions exposing (..)
import ActionButtons.Misc exposing (focusInfoPanel)
import ActionButtons.Utils as ABU
import CommonData.Main as CD
import Constants.Main as CM
import Dict
import GenericTypes exposing (BaseType(..))
import Link.Editor
import Link.Main
import List.Extra as EList
import Maybe exposing (..)
import MyToast
import Setters
import Toasty
import Tree.Types as TT
import Types exposing (..)
import Utils


edit : Model -> EditAction -> ( Model, Cmd Msg )
edit m action =
    case action of
        EditAddJoint ->
            ( Link.Editor.addJoint_ m, Cmd.none )

        EditDelete ->
            ( Link.Editor.delete_ m, Cmd.none )

        EditDiscard ->
            ( ABU.popStack { m | linkEditor = Nothing }
            , Cmd.none
            )

        EditExpand ->
            case Link.Editor.getSelectedData_ m of
                Just (CD.CRecord _) ->
                    ( m
                        |> Setters.setLinkEditor (map (\e -> { e | expandText = "" }))
                        |> ABU.pushBtnSet (ABEdit ABELExpand)
                    , focusInfoPanel
                    )

                Just (CD.CList []) ->
                    ( ABU.pushBtnSet (ABEdit ABELTypes) m
                    , Cmd.none
                    )

                Just (CD.CUnion (Just ( _, Nothing ))) ->
                    ( ABU.pushBtnSet (ABEdit ABELTypes) m
                    , Cmd.none
                    )

                _ ->
                    ( Link.Editor.expand_ m
                    , Cmd.none
                    )

        EditMain ->
            ( ABU.pushBtnSet
                (ABEdit ABELMain)
                (Setters.setLinkEditor
                    (\linkEditor ->
                        let
                            linkFromBoard =
                                case m.boardSelection of
                                    Nothing ->
                                        Nothing

                                    Just ref ->
                                        Dict.get ref.id m.unsavedLinks.items
                        in
                        case linkEditor of
                            Just curEditor ->
                                linkFromBoard
                                    |> Maybe.map
                                        (\link ->
                                            if link.id == curEditor.link.id then
                                                curEditor

                                            else
                                                Link.Editor.init link
                                        )

                            Nothing ->
                                linkFromBoard |> Maybe.map Link.Editor.init
                    )
                    m
                )
            , Cmd.none
            )

        EditReplace ->
            ( ABU.pushBtnSet (ABEdit ABELTypes) m, Cmd.none )

        EditSelectRef ->
            ( CM.findSelected m
                |> map
                    (\const ->
                        Link.Editor.updateSelectedTreeData_
                            (always <| CD.CRef (Just { id = const.id, version = const.version }))
                            m
                    )
                |> withDefault m
                |> ABU.popStack
                |> ABU.popStack
            , Cmd.none
            )

        EditSelectType baseType ->
            ( case baseType of
                BTRef ->
                    ABU.pushBtnSet (ABEdit ABELRefs) m

                _ ->
                    case Link.Editor.getSelectedData_ m of
                        Just (CD.CUnion _) ->
                            Link.Editor.expandByType_ baseType m |> ABU.popStack

                        _ ->
                            Link.Editor.selectType_ baseType m |> ABU.popStack
            , Cmd.none
            )

        EditSave ->
            case m.linkEditor of
                Nothing ->
                    ( m, Cmd.none )

                Just editor ->
                    case Link.Editor.getNonRefConstValues_ m of
                        [] ->
                            ( Link.Main.updateUnsavedLink (Link.Editor.updateLink editor) m
                            , Cmd.none
                            )
                                |> Toasty.addToast
                                    Toasty.config
                                    ToastyMsg
                                    (MyToast.Success "Link updated.")

                        items ->
                            ( m
                            , items
                                |> CreateNewConstantsForJoints
                                |> AjaxSend
                                |> Ajax
                                |> Utils.msgToCmd
                            )
