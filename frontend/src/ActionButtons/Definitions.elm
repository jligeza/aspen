module ActionButtons.Definitions exposing
    ( ABConstsCreatorSet(..)
    , ABEditLinkSet(..)
    , Action(..)
    , ActionButton
    , ActionButtonOpts
    , ActionButtonReqs
    , ActionButtonsSet(..)
    , ConstsAction(..)
    , ConstsBrowseNavAction(..)
    , ConstsCreatorAction(..)
    , CreateTypeAction(..)
    , EditAction(..)
    , FancyBgColor(..)
    , NewNodeAction(..)
    , availableActionKeys
    , defaultOptions
    , initialSet
    )

import GenericTypes exposing (BaseType)
import Keyboard exposing (Key(..))


type alias ActionButtonReqs =
    { key : Key
    , text : String
    }


type alias ActionButtonOpts =
    { action : Maybe Action
    , bgColor : FancyBgColor
    , isDisabled : Bool
    }


defaultOptions : ActionButtonOpts
defaultOptions =
    { action = Nothing
    , bgColor = NormalBg
    , isDisabled = False
    }


type alias ActionButton =
    { r : ActionButtonReqs
    , o : ActionButtonOpts
    }


availableActionKeys : List Key
availableActionKeys =
    [ Character "q"
    , Character "w"
    , Character "e"
    , Character "r"
    , Character "a"
    , Character "s"
    , Character "d"
    , Character "f"
    , Character "z"
    , Character "x"
    , Character "c"
    , Character "v"
    ]


initialSet : List ActionButtonsSet
initialSet =
    [ ABHome ]


type ActionButtonsSet
    = ABCreateType
    | ABConstsMenu
    | ABConstsBrowser
    | ABConstsCreator ABConstsCreatorSet
    | ABEdit ABEditLinkSet
    | ABExpandType
    | ABGeneric
    | ABHome
    | ABNameTag
    | ABNewNode
    | ABRef
    | ABReplaceType
    | ABRewordType
    | ABSelectType
    | ABTypes
    | ABTemplates


type ABConstsCreatorSet
    = ABCCExpand
    | ABCCMain
    | ABCCReplace
    | ABCCReword
    | ABCCSelectRef
    | ABCCSelectType
    | ABCCSelectTypeExpandUnion


type ABEditLinkSet
    = ABELExpand
    | ABELMain
    | ABELRefs
    | ABELTypes


type CreateTypeAction
    = CTADiscard
    | CTADelete
    | CTAExpand
    | CTAInsert BaseType
    | CTANameTag
    | CTAReplace
    | CTAReword
    | CTASave


type NewNodeAction
    = NNDelete
    | NNDiscard
    | NNExpand
    | NNMain
    | NNSave


type EditAction
    = EditAddJoint
    | EditDelete
    | EditDiscard
    | EditExpand
    | EditMain
    | EditReplace
    | EditSave
    | EditSelectRef
    | EditSelectType BaseType


type ConstsBrowseNavAction
    = CBNFirstPage
    | CBNLastPage
    | CBNNextPage
    | CBNPrevPage
    | CBNFocus


type ConstsCreatorAction
    = CCDelete
    | CCDiscard
    | CCExpand
    | CCReplace
    | CCReword
    | CCSave
    | CCSelectType BaseType


type ConstsAction
    = ConstsMain
    | ConstsBrowse
    | ConstsBrowseNav ConstsBrowseNavAction
    | ConstsCreate
    | ConstsCreator ConstsCreatorAction


type Action
    = BrowseTemplates
    | BrowseTypes
    | BuildHttp
    | BuildSet
    | ClearForm
    | ConstsAct ConstsAction
    | Edit EditAction
    | Save
    | SaveAll
    | Confirm
    | CreateType CreateTypeAction
    | Link
    | Delete
    | FetchAll_
    | FocusForm
    | FocusSelection (Maybe String)
    | GoCreateType
    | NewNodeAct NewNodeAction
    | NoAction
    | Return
    | TypesFirstPage
    | TypesFocus
    | TypesLastPage
    | TypesNextPage
    | TypesPrevPage


type FancyBgColor
    = NormalBg
    | PressedBg
    | SelectedBg
