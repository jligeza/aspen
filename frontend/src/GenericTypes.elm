module GenericTypes exposing
    ( BaseType(..)
    , CollectionIdVer
    , IdVer
    , KeyVal(..)
    , LinkLike
    , SimpleRef
    , WithId
    )

import Dict exposing (Dict)


type alias LinkLike a =
    { a
        | id : String
        , from : String
        , to : Maybe String
    }


type alias WithId a =
    { a | id : String }


type alias IdVer a =
    { a
        | id : String
        , version : Int
    }


type alias SimpleRef =
    { id : String
    , version : Int
    }


type KeyVal
    = Key
    | Val


type alias CollectionIdVer a =
    Dict String (Dict Int (IdVer a))


type BaseType
    = BTBool
    | BTFloat
    | BTFunction
    | BTGeneric
    | BTInt
    | BTList
    | BTRecord
    | BTRef
    | BTString
    | BTUnion
    | BTJson
