module TypeCreatorUtils exposing
    ( getTypeLabel
    , isRewordableSelected
    )

import DbTypes exposing (Schema(..))
import GenericTypes exposing (KeyVal(..))
import List.Extra as EList
import Schema.Utils as SU
import Tree.Types as TT
import Types exposing (TypeCreation)


isRewordableSelected : TypeCreation -> Bool
isRewordableSelected tc =
    case tc.schema of
        Nothing ->
            False

        Just schema ->
            case SU.getFocusedSchema tc.selected schema of
                Nothing ->
                    False

                Just schema_ ->
                    case schema_ of
                        SGeneric _ ->
                            True

                        SUnion _ ->
                            case EList.last tc.selected of
                                Nothing ->
                                    False

                                Just lastStep ->
                                    case lastStep of
                                        TT.StepKey _ kv ->
                                            case kv of
                                                Key ->
                                                    True

                                                Val ->
                                                    False

                                        TT.StepIdx _ ->
                                            False

                        _ ->
                            False


getTypeLabel : TypeCreation -> Maybe String
getTypeLabel tc =
    case tc.schema of
        Nothing ->
            Nothing

        Just baseSchema ->
            case SU.getFocusedSchema tc.selected baseSchema of
                Nothing ->
                    Nothing

                Just schema ->
                    case schema of
                        SGeneric txt ->
                            Just txt

                        SUnion _ ->
                            case EList.last tc.selected of
                                Nothing ->
                                    Nothing

                                Just lastStep ->
                                    case lastStep of
                                        TT.StepKey key _ ->
                                            Just key

                                        TT.StepIdx _ ->
                                            Nothing

                        _ ->
                            Nothing
