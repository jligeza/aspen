module MyLoading exposing (myLoading)

import Html exposing (..)
import Html.Attributes exposing (..)
import Loading exposing (LoaderType(..), defaultConfig)
import Types exposing (..)


myLoading : List AjaxResource -> Html Msg
myLoading loading =
    if List.isEmpty loading then
        div [] []

    else
        div
            [ style "display" "flex"
            , style "height" "100%"
            , style "justifyContent" "flex-end"
            , style "pointerEvents" "none"
            , style "position" "fixed"
            , style "right" "5px"
            , style "top" "5px"
            , style "width" "100%"
            ]
            [ Loading.render
                Circle
                defaultConfig
                Loading.On
            ]
