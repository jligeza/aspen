module Info exposing (infoView)

import ActionButtons.Definitions exposing (..)
import Board.Types exposing (BoardItem(..))
import CommonData.Main as CD
import CommonData.Tree
import CommonData.Utils as CDU
import Constants.Main
import DbTypes as DBT
import Dict
import FetchedUtils
import FormControls exposing (labelInput)
import GenericTypes exposing (SimpleRef)
import Grid.Extra
import Grid.Utils
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Keyed as HK
import Html.Lazy exposing (..)
import Icons exposing (icons)
import InfoUtils
import Link.Editor
import List.Extra as EList
import Schema.Validation as SV
import Tree.Types as TT
import Tree.Utils
import Types exposing (..)
import Utils


border m =
    "3px solid " ++ m.layout.colors.primaryD2


row =
    div [ style "padding-bottom" "2px" ]


createdAt : MyTime -> { a | createdAt : String } -> String
createdAt time item =
    if String.isEmpty item.createdAt then
        ""

    else
        Utils.prettyDate_ time.zone item.createdAt
            ++ " ("
            ++ Utils.timeDistance_ time.posix item.createdAt
            ++ " ago)"


createdAtRow : MyTime -> { a | createdAt : String } -> Html Msg
createdAtRow time item =
    if String.isEmpty item.createdAt then
        row [ text <| "Created at: -" ]

    else
        row [ text <| "Created at: " ++ createdAt time item ]


idRow : { a | id : String, version : Int } -> Html Msg
idRow item =
    let
        versionPart =
            if item.version == 1 then
                ""

            else
                " / " ++ String.fromInt item.version
    in
    row [ text <| "ID: " ++ item.id ++ versionPart ]


nameTagRow_ : Model -> GenericTypes.IdVer a -> Html Msg
nameTagRow_ m item =
    nameTagRow (Result.withDefault Dict.empty m.nameTags) item


nameTagRow : DBT.NameTags -> GenericTypes.IdVer a -> Html Msg
nameTagRow nameTags item =
    let
        nameTag =
            FetchedUtils.getNameTagName
                nameTags
                []
                item
                |> Maybe.withDefault ""
    in
    row [ text <| "Name tag: " ++ nameTag ]


selectedTypeView : DBT.BasicDbEntry -> DBT.NameTags -> MyTime -> Html Msg
selectedTypeView type_ nameTags time =
    div
        []
        [ nameTagRow nameTags type_
        , idRow type_
        , createdAtRow time type_
        ]


eventGeneratorsInfo : EventGeneratorsCollection -> Html Msg
eventGeneratorsInfo evs =
    let
        ( startedEvgns, failedEvgns ) =
            List.partition
                (.error >> (==) Nothing)
                evs.items

        failedNum =
            List.length failedEvgns
    in
    row
        [ text <|
            "Event generators: "
                ++ String.fromInt (List.length startedEvgns)
                ++ (if failedNum > 0 then
                        " (plus " ++ String.fromInt failedNum ++ " failed)"

                    else
                        ""
                   )
        ]


homeView :
    RawTypesCollection
    -> NameTagsCollection
    -> Nodes
    -> LinksCollection
    -> ConstantsCollection
    -> EventGeneratorsCollection
    -> Html Msg
homeView types nameTags nodes links consts evgns =
    let
        nameTagsNum =
            case nameTags of
                Err a ->
                    a

                Ok a ->
                    Dict.size a |> String.fromInt

        typesNum =
            case types of
                Err a ->
                    a

                Ok a ->
                    List.length a |> String.fromInt

        countDict =
            .items >> Dict.keys >> List.length >> String.fromInt

        countDict_ =
            Dict.keys >> List.length >> String.fromInt
    in
    div
        []
        [ eventGeneratorsInfo evgns
        , row [ text <| "Types: " ++ typesNum ]
        , row [ text <| "Nodes: " ++ countDict_ nodes ]
        , row [ text <| "Links: " ++ countDict links ]
        , row [ text <| "Name tags: " ++ nameTagsNum ]
        , row [ text <| "Constants: " ++ countDict consts ]
        ]


getTypeContent : Model -> Html Msg
getTypeContent m =
    case m.types.selected of
        Nothing ->
            text "No type selected."

        Just selection ->
            case m.types.items of
                Err err ->
                    text <| "No types available: " ++ err

                Ok types ->
                    case
                        Grid.Utils.findItem_
                            m.types.index
                            selection.coord
                            m.layout.boardSize.width
                            m.layout.boardSize.height
                            types
                    of
                        Nothing ->
                            text "No type selected."

                        Just a ->
                            let
                                nameTags =
                                    Result.withDefault Dict.empty m.nameTags
                            in
                            lazy3 selectedTypeView a nameTags m.time


constantsBrowserTitle : Model -> Html Msg
constantsBrowserTitle m =
    text <|
        "Constants, page "
            ++ String.fromInt (m.constants.page + 1)
            ++ " of "
            ++ String.fromInt (Grid.Extra.getMaxConstantsPage m + 1)


infoTitle : Model -> Html Msg
infoTitle m =
    case m.actionButtons of
        [] ->
            infoTitleByFocus m

        topBtnSet :: more ->
            case topBtnSet of
                ABConstsCreator ABCCReplace ->
                    text "Replace"

                ABConstsCreator ABCCSelectType ->
                    text "Select type"

                ABConstsCreator ABCCExpand ->
                    Constants.Main.creatorExpandTextInfoTitle m

                ABConstsCreator ABCCSelectRef ->
                    constantsBrowserTitle m

                ABConstsCreator ABCCMain ->
                    case m.constantsCreator.tree of
                        Nothing ->
                            text "Select initial type"

                        Just _ ->
                            text "Constants creator"

                ABConstsMenu ->
                    text "Constants"

                ABConstsBrowser ->
                    constantsBrowserTitle m

                ABEdit subSet ->
                    case subSet of
                        ABELExpand ->
                            text "Expand"

                        ABELMain ->
                            case m.linkEditor of
                                Just editor ->
                                    text <| "Edit link: " ++ editor.link.id

                                Nothing ->
                                    text "Edit link: ?"

                        ABELTypes ->
                            text "Select type"

                        ABELRefs ->
                            constantsBrowserTitle m

                ABNewNode ->
                    if m.newNode == Nothing then
                        text "Build"

                    else
                        text <|
                            "New node: "
                                ++ (case m.newNode of
                                        Nothing ->
                                            "?"

                                        Just newNode ->
                                            case
                                                FetchedUtils.getTemplateName
                                                    (Result.withDefault Dict.empty m.nameTags)
                                                    m.templates.items
                                                    newNode.template
                                            of
                                                Nothing ->
                                                    "unknown template"

                                                Just name ->
                                                    name
                                   )

                _ ->
                    infoTitleByFocus m


{-| Legacy.
-}
infoTitleByFocus : Model -> Html Msg
infoTitleByFocus m =
    case m.focus of
        FocusCreateType nestedFocus ->
            case nestedFocus of
                TCFNameTag ->
                    let
                        tc =
                            m.typeCreation
                    in
                    case EList.find (.schemaPath >> (==) tc.selected) tc.nameTags of
                        Nothing ->
                            text "New name tag"

                        Just tag ->
                            text <| "Replace name tag \"" ++ tag.name ++ "\""

                TCFAddGeneric ->
                    text "New generic"

                TCFAddRef ->
                    text "New ref"

                TCFExpand ->
                    text "Expand type"

                TCFDefault ->
                    let
                        tc =
                            m.typeCreation
                    in
                    case EList.find (.schemaPath >> (==) []) tc.nameTags of
                        Nothing ->
                            text "Type creation"

                        Just tag ->
                            text <| "Type creation \"" ++ tag.name ++ "\""

                TCFReplace ->
                    text "Type replacement"

                TCFReword ->
                    text "Reword"

        FocusHome ->
            case m.boardSelection of
                Nothing ->
                    text "Home"

                Just ref ->
                    FetchedUtils.getName m ref
                        |> Maybe.withDefault ref.id
                        |> text

        FocusTypes ->
            let
                sizes =
                    Grid.Utils.getDefaultSizesInfo
                        m.layout.boardSize.width
                        m.layout.boardSize.height
            in
            text <|
                "Types, page "
                    ++ (String.fromInt <| m.types.index + 1)
                    ++ " of "
                    ++ (String.fromInt <| Grid.Extra.getMaxTypesPage m + 1)

        FocusTemplates ->
            text "Templates"


newTextView : Model -> String -> (String -> Msg) -> Html Msg
newTextView m val update =
    div
        []
        [ labelInput
            m.layout.colors
            "New text"
            [ value val
            , onInput update
            ]
        ]


schemaError : SV.Error -> Html Msg
schemaError err =
    case err.reason of
        SV.ErrorMemberPresent ->
            text "Not implemented."

        SV.EmptyRecord ->
            text "Empty record."

        SV.UnionBelowTwoMembers ->
            text "Union below two members."


typeCreationDefaultInfo : Model -> Html Msg
typeCreationDefaultInfo m =
    case m.typeCreation.schema of
        Nothing ->
            text "..."

        Just schema ->
            case SV.validate schema of
                [] ->
                    text "No errors."

                errors ->
                    div
                        []
                        [ text "Errors:"
                        , HK.node
                            "div"
                            []
                            (List.map
                                (\e ->
                                    let
                                        path =
                                            case Tree.Utils.prettyPath e.path of
                                                "" ->
                                                    ""

                                                a ->
                                                    a ++ " | "
                                    in
                                    ( path
                                    , div
                                        []
                                        [ text path
                                        , schemaError e
                                        ]
                                    )
                                )
                                errors
                            )
                        ]


eventGeneratorInfo : Model -> DBT.EventGenerator -> Html Msg
eventGeneratorInfo m evgn =
    let
        baseContent =
            [ idRow evgn.base
            , createdAtRow m.time evgn.base
            , specContent
            ]

        specContent =
            div
                [ style "margin-top" "0.5rem" ]
                [ CommonData.Tree.tree
                    m.layout.colors
                    { data = evgn.base.config, selection = Nothing, schema = Nothing }
                    CommonData.Tree.defaultOpts
                ]
    in
    div
        []
        (baseContent
            |> (case evgn.error of
                    Nothing ->
                        identity

                    Just err ->
                        (::) <|
                            row
                                [ span
                                    [ style "color" m.layout.colors.error ]
                                    [ text <| "Error: " ++ err ]
                                ]
               )
        )


linkPath : List CD.Step -> Html Msg
linkPath path =
    text <| Tree.Utils.prettyPath path


linkJointConst : Model -> SimpleRef -> Html Msg
linkJointConst m ref =
    case
        m.constants.items
            |> Dict.get ref.id
            |> Maybe.andThen (Dict.get ref.version)
    of
        Nothing ->
            text "unknown constant"

        Just const ->
            CommonData.Tree.tree
                m.layout.colors
                { data = const.value, selection = Nothing, schema = Nothing }
                CommonData.Tree.defaultOpts


longArrow =
    div
        [ style "font-size" "200%"
        , style "margin" "0 1rem"
        , style "line-height" "1rem"
        , style "display" "inline-block"
        , style "overflow" "hidden"
        ]
        [ text icons.longArrowRight ]


linkJoint : Model -> DBT.Joint -> Html Msg
linkJoint m joint =
    case joint of
        DBT.ConstantJoint ref toPath ->
            li
                []
                [ div [ style "display" "flex" ]
                    [ linkJointConst m ref
                    , longArrow
                    , linkPath toPath
                    ]
                ]

        DBT.RewireJoint fromPath toPath ->
            li
                []
                [ linkPath fromPath, longArrow, linkPath toPath ]


linkJoints : Model -> DBT.Link -> Html Msg
linkJoints m link =
    div
        []
    <|
        if List.isEmpty link.joints then
            [ row [ text "Default joint." ] ]

        else
            [ row [ text "Joints:" ]
            , ul []
                (List.map (linkJoint m) link.joints)
            ]


specPart part =
    div
        [ style "margin-top" "0.5rem" ]
        part


linkInfo : Model -> DBT.Link -> Html Msg
linkInfo m link =
    div
        []
        ((if Dict.member link.id m.unsavedLinks.items then
            [ row [ text <| "ID: " ++ link.id ]
            , row [ text "Created at: -" ]
            ]

          else
            [ idRow link
            , createdAtRow m.time link
            ]
         )
            ++ [ specPart [ linkJoints m link ] ]
        )


nodeInfoSetJoint : Model -> DBT.Joint -> Html Msg
nodeInfoSetJoint m joint =
    case joint of
        DBT.RewireJoint fromPath toPath ->
            text "TODO handle rewire joint"

        DBT.ConstantJoint ref toPath ->
            case toPath of
                [ TT.StepKey key keyVal ] ->
                    case key of
                        "return" ->
                            case
                                m.constants.items
                                    |> Dict.get ref.id
                                    |> Maybe.andThen (Dict.get ref.version)
                            of
                                Nothing ->
                                    text "const not found"

                                Just const ->
                                    row
                                        [ text "Return: "
                                        , div [ style "margin-left" "0.5rem" ]
                                            [ CommonData.Tree.tree
                                                m.layout.colors
                                                { data = const.value
                                                , selection = Nothing
                                                , schema = Nothing
                                                }
                                                CommonData.Tree.defaultOpts
                                            ]
                                        ]

                        _ ->
                            text "TODO handle more set keys"

                _ ->
                    text "unknown toPath format"


nodeInfoSet : Model -> DBT.Node -> Html Msg
nodeInfoSet m node =
    let
        joints link =
            if List.isEmpty link.joints then
                text "Input: default joint."

            else
                div
                    []
                    [ text "Input:"
                    , ul
                        []
                        [ li [] (List.map (nodeInfoSetJoint m) link.joints) ]
                    ]
    in
    case m.links.byTo |> Dict.get node.id of
        Nothing ->
            case m.unsavedLinks.byTo |> Dict.get node.id of
                Nothing ->
                    text "link not found"

                Just link ->
                    joints link

        Just link ->
            joints link


nodeInfo : Model -> DBT.Node -> Html Msg
nodeInfo m node =
    div
        []
        [ idRow node
        , createdAtRow m.time node
        , specPart
            [ case
                m.templates.items
                    |> Dict.get node.targetId
                    |> Maybe.andThen (Dict.get node.targetVersion)
              of
                Nothing ->
                    text "template not found"

                Just template ->
                    case template.base.name of
                        "set" ->
                            nodeInfoSet m node

                        unknown ->
                            text <| "unknown template: " ++ unknown
            ]
        ]


boardItemInfoContent : Model -> BoardItem -> Html Msg
boardItemInfoContent m item =
    case item of
        BINode node ->
            nodeInfo m node

        BIUnsavedNode node ->
            nodeInfo m node

        BILink link ->
            linkInfo m link

        BIUnsavedLink link ->
            linkInfo m link

        BIEventGenerator evgn ->
            eventGeneratorInfo m evgn


constsBrowserContent : Model -> Html Msg
constsBrowserContent m =
    case Constants.Main.findSelected m of
        Nothing ->
            text "not found"

        Just const ->
            div
                []
                [ nameTagRow_ m const
                , idRow const
                , createdAtRow m.time const
                ]


linkEditorErrors : Model -> Html Msg
linkEditorErrors m =
    case m.linkEditor of
        Nothing ->
            text ""

        Just editor ->
            let
                errors =
                    editor.trees
                        |> List.map (.data >> CDU.getEmptyPaths)
                        |> List.concat
            in
            if List.isEmpty errors then
                text ""

            else
                div
                    [ style "color" m.layout.colors.error ]
                    [ text "Error: some values are empty." ]


infoContent : Model -> Html Msg
infoContent m =
    case m.actionButtons of
        [] ->
            infoContentByFocus m

        topBtnSet :: _ ->
            case topBtnSet of
                ABConstsCreator ABCCSelectRef ->
                    constsBrowserContent m

                ABConstsCreator ABCCExpand ->
                    Constants.Main.creatorExpandTextInfoContent m

                ABConstsCreator ABCCReplace ->
                    text ""

                ABConstsCreator ABCCReword ->
                    Constants.Main.creatorRewordInfoContent m

                ABConstsCreator ABCCSelectType ->
                    text ""

                ABConstsCreator ABCCMain ->
                    Constants.Main.creatorMainInfoContent m

                ABConstsMenu ->
                    text "What to do with constants?"

                ABConstsBrowser ->
                    constsBrowserContent m

                ABNewNode ->
                    if m.newNode == Nothing then
                        text "Select a template."

                    else
                        text "todo new node"

                ABEdit subSet ->
                    case subSet of
                        ABELExpand ->
                            case m.linkEditor of
                                Just editor ->
                                    InfoUtils.newTextView
                                        m.layout.colors
                                        editor.expandText
                                        LinkEditorExpandTextUpdated

                                Nothing ->
                                    text ""

                        ABELRefs ->
                            constsBrowserContent m

                        _ ->
                            case m.linkEditor of
                                Just link ->
                                    div []
                                        [ div [] [ text "Edit link joint." ]
                                        , p [] [ text "For now, only one joint is editable." ]
                                        , linkEditorErrors m
                                        ]

                                Nothing ->
                                    text "no link selected"

                _ ->
                    infoContentByFocus m


{-| Legacy.
-}
infoContentByFocus : Model -> Html Msg
infoContentByFocus m =
    case m.focus of
        FocusTemplates ->
            text "todo templates"

        FocusCreateType nestedFocus ->
            case nestedFocus of
                TCFNameTag ->
                    div
                        []
                        [ labelInput
                            m.layout.colors
                            "Name"
                            [ value m.typeCreation.nameTagText
                            , onInput NameTagTextChange
                            ]
                        ]

                TCFAddGeneric ->
                    div
                        []
                        [ labelInput
                            m.layout.colors
                            "Label"
                            [ value m.typeCreation.genericText
                            , onInput GenericTextChange
                            ]
                        ]

                TCFAddRef ->
                    getTypeContent m

                TCFExpand ->
                    newTextView m m.typeCreation.expandText ExpandTextChange

                TCFDefault ->
                    if m.typeCreation.schema == Nothing then
                        text "Start by selecting some type."

                    else
                        typeCreationDefaultInfo m

                TCFReplace ->
                    text "Select a replacement."

                TCFReword ->
                    newTextView m m.typeCreation.rewordText RewordTextChange

        FocusTypes ->
            getTypeContent m

        FocusHome ->
            case m.boardSelection of
                Nothing ->
                    lazy6
                        homeView
                        m.types.items
                        m.nameTags
                        m.nodes
                        m.links
                        m.constants
                        m.eventGenerators

                Just ref ->
                    case
                        FetchedUtils.getBoardItem m ref
                    of
                        Just item ->
                            boardItemInfoContent m item

                        Nothing ->
                            text "not found"


infoView : Model -> Html Msg
infoView m =
    div
        [ style "border-bottom" (border m)
        , id "info-panel"
        ]
        [ div
            [ style "display" "flex"
            , style "align-items" "center"
            , style "padding" "3px"
            , style "justify-content" "center"
            , style "border-bottom" (border m)
            , style "border-top" (border m)
            ]
            [ infoTitle m ]
        , div
            [ style "padding" "3px"
            , style "max-height" <| Utils.toPx <| m.layout.bottomBarSize.height - 36
            , style "overflow" "auto"
            ]
            [ infoContent m ]
        ]
