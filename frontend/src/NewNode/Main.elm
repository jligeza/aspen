module NewNode.Main exposing (newNodeView)

import CommonData.Main exposing (CommonData(..))
import CommonData.Tree
import Html exposing (..)
import NewNode.Types exposing (..)
import Types exposing (Model)


newNodeView : Model -> NewNode -> Html Types.Msg
newNodeView m newNode =
    let
        o =
            CommonData.Tree.defaultOpts
    in
    CommonData.Tree.tree
        m.layout.colors
        newNode.tree
        { o
            | editable = True
            , updateMsg = Just Types.NewNodeUpdateMsg
        }
