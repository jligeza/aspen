module NewNode.Types exposing (NewNode)

import CommonData.Main exposing (CommonData(..))
import DbTypes exposing (Schema)
import GenericTypes exposing (SimpleRef)
import Tree.Types as TT


type alias NewNode =
    { template : SimpleRef
    , saveError : Maybe String
    , tree :
        { data : CommonData
        , selection : Maybe TT.Path
        , schema : Maybe Schema
        }
    }
