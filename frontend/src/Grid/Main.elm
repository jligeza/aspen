module Grid.Main exposing
    ( defaultOpts
    , getSizesInfo
    , getSizesInfo_
    , grid
    )

import Grid.Types exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Utils exposing (toPx)


defaultOpts =
    { onTileSelect = Nothing
    , prefTileWidth = 200
    , prefTileHeight = 150
    , tileMargin = 1
    }


getBestTileSize : Int -> Int -> Int -> Int
getBestTileSize totalSize prefered tileMargin =
    let
        tilesNum =
            totalSize // prefered

        totalExtraSpace =
            totalSize - tilesNum * prefered

        extraSpacePerTile =
            totalExtraSpace // tilesNum
    in
    prefered + extraSpacePerTile - tileMargin * 2


tile : Ctx a msg -> Int -> Coord -> Int -> Int -> a -> Html msg
tile ctx idx coord width_ height_ item =
    let
        c =
            ctx.reqs.colors

        borderColor =
            if coord == ctx.reqs.coord then
                c.text

            else
                c.primaryL1
    in
    div
        ([ style "width" (toPx width_)
         , style "height" (toPx height_)
         , style "overflow" "auto"
         , style "position" "relative"
         , style "margin" <| toPx ctx.opts.tileMargin
         , style "box-sizing" "border-box"
         , style "border" <| "1px solid " ++ borderColor
         , style "background" c.primaryD2
         ]
            |> (case ctx.opts.onTileSelect of
                    Nothing ->
                        identity

                    Just cb ->
                        (::) (onClick <| cb <| Tile coord item)
               )
        )
        [ ctx.reqs.tileContent coord idx item
        ]


getSizesInfo gridWidth gridHeight prefTileWidth prefTileHeight tileMargin =
    let
        tileWidth =
            getBestTileSize gridWidth prefTileWidth tileMargin

        tileHeight =
            getBestTileSize gridHeight prefTileHeight tileMargin

        maxInRow =
            gridWidth // tileWidth

        maxInCol =
            gridHeight // tileHeight
    in
    { tileWidth = tileWidth
    , tileHeight = tileHeight
    , maxInRow = maxInRow
    , maxInCol = maxInCol
    , maxPerPage = maxInRow * maxInCol
    }


getSizesInfo_ gridWidth gridHeight =
    getSizesInfo gridWidth
        gridHeight
        defaultOpts.prefTileWidth
        defaultOpts.prefTileHeight
        defaultOpts.tileMargin


tiles : Ctx a msg -> List a -> List (Html msg)
tiles ctx items =
    let
        sizes =
            getSizesInfo
                ctx.reqs.width
                ctx.reqs.height
                ctx.opts.prefTileWidth
                ctx.opts.prefTileHeight
                ctx.opts.tileMargin

        visibleItems =
            items
                |> List.drop (ctx.reqs.page * sizes.maxPerPage)
                |> List.take sizes.maxPerPage
    in
    List.indexedMap
        (\idx item ->
            let
                coord =
                    ( modBy sizes.maxInRow idx
                    , idx // sizes.maxInRow
                    )
            in
            tile
                ctx
                idx
                coord
                sizes.tileWidth
                sizes.tileHeight
                item
        )
        visibleItems


grid : Reqs a msg -> Opts a msg -> List a -> Html msg
grid reqs opts items =
    let
        ctx =
            Ctx reqs opts
    in
    div
        [ style "display" "flex"
        , style "justify-content" "center"
        , style "align-items" "center"
        , style "flex-wrap" "wrap"
        , style "width" "100%"
        , style "height" "100%"
        , style "position" "relative"
        ]
        (tiles ctx items)
