module Grid.Utils exposing
    ( findItem
    , findItem_
    , getDefaultSizesInfo
    , getVisibleItems
    )

import Grid.Main
import Grid.Types exposing (..)
import List.Extra as EList


type alias ItemPos =
    { coord : Coord
    , page : Int
    , gridWidth : Int
    , gridHeight : Int
    , prefTileWidth : Int
    , prefTileHeight : Int
    , tileMargin : Int
    }


findItem : ItemPos -> List a -> Maybe a
findItem pos items =
    let
        ( x, y ) =
            pos.coord

        sizes =
            Grid.Main.getSizesInfo
                pos.gridWidth
                pos.gridHeight
                pos.prefTileWidth
                pos.prefTileHeight
                pos.tileMargin

        idx =
            (pos.page * sizes.maxPerPage) + (y * sizes.maxInRow) + x
    in
    EList.getAt idx items


findItem_ : Int -> Coord -> Int -> Int -> List a -> Maybe a
findItem_ page coord gridWidth gridHeight items =
    findItem
        { coord = coord
        , page = page
        , gridWidth = gridWidth
        , gridHeight = gridHeight
        , prefTileWidth = Grid.Main.defaultOpts.prefTileWidth
        , prefTileHeight = Grid.Main.defaultOpts.prefTileHeight
        , tileMargin = Grid.Main.defaultOpts.tileMargin
        }
        items


getDefaultSizesInfo gridWidth gridHeight =
    Grid.Main.getSizesInfo
        gridWidth
        gridHeight
        Grid.Main.defaultOpts.prefTileWidth
        Grid.Main.defaultOpts.prefTileHeight
        Grid.Main.defaultOpts.tileMargin


getVisibleItems : Int -> Int -> Int -> List a -> List a
getVisibleItems gridWidth gridHeight page items =
    let
        sizes =
            Grid.Main.getSizesInfo
                gridWidth
                gridHeight
                Grid.Main.defaultOpts.prefTileWidth
                Grid.Main.defaultOpts.prefTileHeight
                Grid.Main.defaultOpts.tileMargin
    in
    items
        |> List.drop (page * sizes.maxPerPage)
        |> List.take sizes.maxPerPage
