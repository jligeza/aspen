module Grid.Extra exposing
    ( contentWithNameTag
    , contentWithTitle
    , getMaxConstantsPage
    , getMaxTypesPage
    , getVisibleConstants
    , getVisibleConstantsRows
    , getVisibleTypes
    , getVisibleTypesRows
    )

import Colors exposing (Colors)
import DbTypes exposing (BasicDbEntry, Constant, NameTags)
import Dict
import FetchedUtils
import GenericTypes exposing (IdVer)
import Grid.Main
import Grid.Types exposing (..)
import Grid.Utils
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import List.Extra as EList
import Types exposing (Model)


getMaxTypesPage : Model -> Int
getMaxTypesPage m =
    List.length (Result.withDefault [] m.types.items)
        // (Grid.Main.getSizesInfo_
                m.layout.boardSize.width
                m.layout.boardSize.height
           ).maxPerPage


getMaxConstantsPage : Model -> Int
getMaxConstantsPage m =
    (m.constants.items
        |> Dict.values
        |> List.map Dict.values
        |> List.concat
        |> List.length
    )
        // (Grid.Main.getSizesInfo_
                m.layout.boardSize.width
                m.layout.boardSize.height
           ).maxPerPage


getVisibleConstants : Model -> List Constant
getVisibleConstants m =
    Grid.Utils.getVisibleItems
        m.layout.boardSize.width
        m.layout.boardSize.height
        m.constants.page
        (m.constants.items
            |> Dict.values
            |> List.map Dict.values
            |> List.concat
        )


getVisibleConstantsRows : Model -> List (List Constant)
getVisibleConstantsRows m =
    let
        sizes =
            Grid.Main.getSizesInfo_
                m.layout.boardSize.width
                m.layout.boardSize.height
    in
    getVisibleConstants m
        |> EList.greedyGroupsOf sizes.maxInRow


getVisibleTypes : Model -> List BasicDbEntry
getVisibleTypes m =
    Grid.Utils.getVisibleItems
        m.layout.boardSize.width
        m.layout.boardSize.height
        m.types.index
        (Result.withDefault [] m.types.items)


getVisibleTypesRows : Model -> List (List BasicDbEntry)
getVisibleTypesRows m =
    let
        sizes =
            Grid.Main.getSizesInfo_
                m.layout.boardSize.width
                m.layout.boardSize.height
    in
    getVisibleTypes m
        |> EList.greedyGroupsOf sizes.maxInRow


contentWithTitle : Colors -> Bool -> List (Html msg) -> List (Html msg) -> Html msg
contentWithTitle colors selected title_ content =
    let
        txtColor =
            if selected then
                colors.primaryD3

            else
                colors.text

        bgColor =
            if selected then
                colors.text

            else
                colors.primaryD1
    in
    div
        [ style "position" "absolute"
        , style "height" "100%"
        , style "width" "100%"
        ]
        [ div
            [ style "position" "absolute"
            , style "top" "0"
            , style "width" "100%"
            , style "height" "1rem"
            , style "color" txtColor
            , style "background" bgColor
            ]
            [ span
                [ style "margin-left" "3px"
                ]
                title_
            ]
        , div
            [ style "position" "absolute"
            , style "top" "1rem"
            , style "width" "100%"
            , style "height" "calc(100% - 1rem)"
            , style "overflow" "auto"
            , style "box-sizing" "border-box"
            , style "border-top" "2px solid transparent"
            ]
            content
        ]


contentWithNameTag : Colors -> NameTags -> Bool -> IdVer a -> Html msg -> Html msg
contentWithNameTag colors nameTags selected item content =
    case FetchedUtils.getNameTagName nameTags [] item of
        Just name ->
            contentWithTitle
                colors
                selected
                [ text name ]
                [ content ]

        Nothing ->
            contentWithTitle
                colors
                selected
                [ text "" ]
                [ content ]
