module Grid.Types exposing (Coord, Ctx, Opts, Reqs, Tile)

import Colors exposing (Colors)
import Html exposing (Html)


type alias Reqs a msg =
    { colors : Colors
    , width : Int
    , height : Int
    , page : Int
    , coord : ( Int, Int )
    , tileContent : Coord -> Int -> a -> Html msg
    }


type alias Opts a msg =
    { onTileSelect : Maybe (Tile a -> msg)
    , prefTileWidth : Int
    , prefTileHeight : Int
    , tileMargin : Int
    }


type alias Ctx a msg =
    { reqs : Reqs a msg
    , opts : Opts a msg
    }


type alias Tile a =
    { coord : Coord
    , item : a
    }


type alias Coord =
    ( Int, Int )
