module Icons exposing (icons)


icons =
    { wip = "🛠️"
    , warning = "⚠️"
    , longArrowRight = "➞"
    , arrowheadRight = "➤"
    , unsaved = "🞴"
    }
