module Types exposing
    ( ActiveNodes
    , AjaxReceivePayload(..)
    , AjaxReq(..)
    , AjaxResource(..)
    , AjaxSendPayload(..)
    , Constants
    , ConstantsCollection
    , Coord
    , EventGeneratorsCollection
    , Focus(..)
    , GridSchemaSelection
    , Layout
    , Links
    , LinksCollection
    , Model
    , Msg(..)
    , MyTime
    , NNPEventGeneratorPayload
    , NameTagsCollection
    , NewNameTag
    , NewNodePayload(..)
    , Nodes
    , RawTypesCollection
    , RawTypesRows
    , ReceiveNewNodePayload(..)
    , Size
    , TemplatesCollection
    , TypeCreation
    , TypeCreationFocus(..)
    , TypesCollection
    , TypesPack
    , TypesRows
    , UnsavedNodes
    , ajaxFetchableResources
    )

import ActionButtons.Definitions exposing (ActionButton, ActionButtonsSet)
import Browser.Dom exposing (Viewport)
import Browser.Navigation
import Colors exposing (Colors)
import CommonData.Main as CD
import CommonData.TreeTypes
import Constants.Types
import DbTypes
    exposing
        ( BasicDbEntry
        , Constant
        , EventGenerator
        , Link
        , NameTag
        , NameTags
        , NewEventGenerator
        , Node
        , Schema
        , Templates
        )
import Dict exposing (Dict)
import GenericTypes exposing (SimpleRef)
import Grid.Types
import Http
import Keyboard exposing (Key(..))
import Link.Types
import MyToast
import NewNode.Types
import Time
import Toasty
import Tree.Types


type alias Size =
    { width : Int
    , height : Int
    }


type alias MyTime =
    { posix : Time.Posix
    , zone : Time.Zone
    }


type Msg
    = ActionButtonPress ActionButton
    | ActionButtonSelect ActionButton
    | Ajax AjaxReq
    | BackendMessage String
    | BoardClick
    | BoardSelect (Maybe SimpleRef)
    | ConstantsMsg Constants.Types.Msg
    | ExpandTextChange String
    | GenericTextChange String
    | InputBlur
    | InputFocus
    | KeyboardMsg Keyboard.Msg
    | LinkEditorTreeUpdated Int CommonData.TreeTypes.TreeMsg
    | LinkEditorExpandTextUpdated String
    | MainMouseUp
    | NameTagTextChange String
    | NewNodeUpdateMsg CommonData.TreeTypes.TreeMsg
    | NoOp
    | RewordTextChange String
    | SelectType Grid.Types.Coord
    | SelectTypeCreationSchema Tree.Types.Path
    | SelectTypeSchema Tree.Types.Path Grid.Types.Coord
    | SetLayoutByViewport Viewport
    | SetTimePosix Time.Posix
    | SetTimeZone Time.Zone
    | ToastyMsg (Toasty.Msg MyToast.Toast)
    | WindowResize Int Int


type alias Layout =
    { boardSize : Size
    , bottomBarSize : Size
    , colors : Colors
    }


type alias Model =
    { actionButtons : List ActionButtonsSet
    , counter : Int
    , unsavedLinks : LinksCollection
    , unsavedNodes : UnsavedNodes
    , nodes : Nodes
    , links : LinksCollection
    , linkEditor : Maybe Link.Types.Editor
    , newNode : Maybe NewNode.Types.NewNode
    , boardSelection : Maybe SimpleRef
    , browserNavigationKey : Browser.Navigation.Key
    , constants : ConstantsCollection
    , constantsCreator : Constants.Types.Model
    , eventGenerators : EventGeneratorsCollection
    , focus : Focus
    , focusedInput : Bool
    , hostAjaxAddress : String
    , hostWsAddress : String
    , layout : Layout
    , loading : List AjaxResource
    , nameTags : NameTagsCollection
    , pressedKeys : List Key
    , templates : TemplatesCollection
    , time : MyTime
    , toasties : Toasty.Stack MyToast.Toast
    , typeCreation : TypeCreation
    , types : TypesPack
    }


type alias UnsavedNodes =
    Dict String Node


type alias RawTypesCollection =
    Result String TypesCollection


type alias TypesCollection =
    List BasicDbEntry


type alias TemplatesCollection =
    { items : Templates
    , error : Maybe String
    }


type alias ConstantsCollection =
    { items : Constants
    , error : Maybe String
    , page : Int
    , selection : Coord
    }


type alias EventGeneratorsCollection =
    { items : List EventGenerator
    , error : Maybe String
    }


type alias Nodes =
    Dict String (Dict Int Node)


type alias Links =
    Dict String Link


type alias LinksCollection =
    { items : Links
    , byFrom : Dict String (List Link)
    , byTo : Dict String Link
    , error : Maybe String
    }


type alias RawTypesRows =
    Result String TypesRows


type alias TypesRows =
    List TypesCollection


type alias NameTagsCollection =
    Result String NameTags


type alias Constants =
    Dict String (Dict Int Constant)


type alias Coord =
    ( Int, Int )


type alias GridSchemaSelection =
    { coord : Coord
    , path : Maybe Tree.Types.Path
    }


type alias ActiveNodes =
    { eventGenerators : List EventGenerator
    , nodes : Nodes
    , links : List Link
    }


type alias TypesPack =
    { items : RawTypesCollection
    , index : Int
    , selected : Maybe GridSchemaSelection
    }


type alias TypeCreation =
    { expandText : String
    , genericText : String
    , nameTagText : String
    , nameTags : List NewNameTag
    , rewordText : String
    , schema : Maybe Schema
    , selected : Tree.Types.Path
    }


type alias NewNameTag =
    { name : String
    , schemaPath : Tree.Types.Path
    }


type TypeCreationFocus
    = TCFAddGeneric
    | TCFAddRef
    | TCFDefault
    | TCFExpand
    | TCFNameTag
    | TCFReplace
    | TCFReword


type Focus
    = FocusCreateType TypeCreationFocus
    | FocusHome
    | FocusTypes
    | FocusTemplates


type AjaxResource
    = ARActiveNodes
    | AREventGenerators
    | ARConstants
    | ARLinks
    | ARNameTags
    | ARNewConstant
    | ARNewLink
    | ARNewNode
    | ARNodes
    | ARTemplates
    | ARType
    | ARTypes


ajaxFetchableResources =
    [ ARActiveNodes
    , ARConstants
    , ARNameTags
    , ARTemplates
    , ARTypes
    ]


type alias NNPEventGeneratorPayload =
    { templateId : String
    , templateVersion : Int
    , config : CD.CommonData
    }


type alias NNPCommonPayload =
    { targetId : String
    , targetVersion : Int
    , tmpId : String
    }


type NewNodePayload
    = NNPEventGenerator NNPEventGeneratorPayload
    | NNPCommon NNPCommonPayload


type AjaxSendPayload
    = FetchActiveNodes
    | FetchConstants
    | FetchNameTags
    | FetchTemplates
    | FetchTypes
    | CreateNewConstant CD.CommonData
    | CreateNewConstantsForJoints (List ( Int, CD.CommonData ))
    | CreateNewLink Link
    | CreateNewNode NewNodePayload
    | CreateNewType Schema (List NewNameTag)


type ReceiveNewNodePayload
    = RNNPEventGenerator SimpleRef NewEventGenerator
    | RNNPCommon String Node


type AjaxReceivePayload
    = ReceiveActiveNodes (Result Http.Error ActiveNodes)
    | ReceiveConstants (Result Http.Error Constants)
    | ReceiveNameTags (Result Http.Error NameTags)
    | ReceiveTemplates (Result Http.Error Templates)
    | ReceiveTypes (Result Http.Error TypesCollection)
    | ReceiveNewConstant (Result Http.Error Constant)
    | ReceiveNewConstantsForJoints (Result Http.Error (List ( Int, Constant )))
    | ReceiveNewLink (Result Http.Error Link)
    | ReceiveNewNode (Result Http.Error ReceiveNewNodePayload)
    | ReceiveNewType (Result Http.Error (Result String ( BasicDbEntry, List NameTag )))


type AjaxReq
    = AjaxFetchAll
    | AjaxSend AjaxSendPayload
    | AjaxReceive AjaxReceivePayload
