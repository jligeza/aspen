module FormControls exposing
    ( labelInput
    , myInput
    )

import Colors exposing (Colors)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Utils


myInput : Colors -> List (Attribute msg) -> Html msg
myInput c attrs =
    input
        ([ style "background-color" "transparent"
         , style "color" c.text
         , style "border" "none"
         , style "border-bottom" <| "2px solid " ++ Utils.addOpacity "0.6" c.text
         , style "outline" "none"
         ]
            ++ attrs
        )
        []


labelInput : Colors -> String -> List (Attribute msg) -> Html msg
labelInput c labelTxt inputAttrs =
    div
        []
        [ span [ style "margin-right" "3px" ] [ text <| labelTxt ++ ":" ]
        , myInput c inputAttrs
        ]
