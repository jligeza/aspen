module Ajax exposing (onAjax)

import CommonData.Main as CD
import DbTypes exposing (Constant, Link, NewEventGenerator, Node, Schema)
import Decoders as Dec
import Dict
import Encoders as Enc
import GenericTypes exposing (SimpleRef)
import Http exposing (..)
import Json.Decode as JD
import Json.Encode as JE
import Link.Editor
import Link.Main
import List.Extra as EList
import Maybe exposing (andThen)
import MyToast
import Setters as S
import Task
import Toasty
import Types exposing (..)
import Utils


post : String -> (Result Error a -> AjaxReq) -> JE.Value -> JD.Decoder a -> Cmd Msg
post url ajaxMsg encoder decoder =
    Http.post
        { url = url
        , body = Http.jsonBody encoder
        , expect = Http.expectJson (Ajax << ajaxMsg) decoder
        }


postTask : String -> JE.Value -> JD.Decoder a -> Task.Task Error a
postTask url encoder decoder =
    task
        { method = "post"
        , headers = []
        , url = url
        , body = jsonBody encoder
        , resolver = defaultResolver decoder
        , timeout = Nothing
        }


defaultResolver : JD.Decoder a -> Resolver Error a
defaultResolver decoder =
    stringResolver (resolveStringResponse decoder)


resolveStringResponse : JD.Decoder a -> Response String -> Result Error a
resolveStringResponse decoder response =
    case response of
        BadStatus_ meta body ->
            Err <| BadStatus meta.statusCode

        GoodStatus_ meta body ->
            JD.decodeString decoder body
                |> Result.mapError (BadBody << JD.errorToString)

        BadUrl_ url_ ->
            Err <| BadUrl url_

        Timeout_ ->
            Err Timeout

        NetworkError_ ->
            Err NetworkError


fetchActiveNodes : String -> Cmd Msg
fetchActiveNodes url =
    post
        url
        (AjaxReceive << ReceiveActiveNodes)
        Enc.listActiveNodes
        Dec.decodeActiveNodes


fetchTemplates : String -> Cmd Msg
fetchTemplates url =
    post
        url
        (AjaxReceive << ReceiveTemplates)
        Enc.listTemplates
        Dec.decodeTemplates


fetchTypes : String -> Cmd Msg
fetchTypes url =
    post
        url
        (AjaxReceive << ReceiveTypes)
        Enc.listTypes
        Dec.decodeBasicDbEntries


fetchNameTags : String -> Cmd Msg
fetchNameTags url =
    post
        url
        (AjaxReceive << ReceiveNameTags)
        Enc.listNameTags
        Dec.decodeNameTags


fetchConstants : String -> Cmd Msg
fetchConstants url =
    post
        url
        (AjaxReceive << ReceiveConstants)
        Enc.listConstants
        Dec.decodeConstants


sendByResource : String -> AjaxSendPayload -> Cmd Msg
sendByResource url payload =
    case payload of
        FetchActiveNodes ->
            fetchActiveNodes url

        FetchTemplates ->
            fetchTemplates url

        FetchTypes ->
            fetchTypes url

        FetchNameTags ->
            fetchNameTags url

        FetchConstants ->
            fetchConstants url

        CreateNewConstant const ->
            saveConstant url const

        CreateNewConstantsForJoints items ->
            saveConstants url items

        CreateNewLink link ->
            saveLink url link

        CreateNewType schema nameTags ->
            saveType url schema nameTags

        CreateNewNode payload_ ->
            saveNewNode url payload_


saveNewNode : String -> NewNodePayload -> Cmd Msg
saveNewNode url payload =
    case payload of
        NNPCommon p ->
            post
                url
                (AjaxReceive << ReceiveNewNode)
                (Enc.saveNewNode payload)
                (Dec.decodeNewNode
                    |> JD.andThen (JD.succeed << RNNPCommon p.tmpId)
                )

        NNPEventGenerator p ->
            post
                url
                (AjaxReceive << ReceiveNewNode)
                (Enc.saveNewNode payload)
                (Dec.decodeNewEventGenerator
                    |> JD.andThen
                        (JD.succeed
                            << RNNPEventGenerator
                                { id = p.templateId
                                , version = p.templateVersion
                                }
                        )
                )


saveLink : String -> Link -> Cmd Msg
saveLink url link =
    post
        url
        (AjaxReceive << ReceiveNewLink)
        (Enc.saveLink link)
        Dec.decodeNewLink


saveConstant : String -> CD.CommonData -> Cmd Msg
saveConstant url const =
    post
        url
        (AjaxReceive << ReceiveNewConstant)
        (Enc.saveConstant const)
        Dec.decodeNewConstant


saveConstants : String -> List ( Int, CD.CommonData ) -> Cmd Msg
saveConstants url payload =
    payload
        |> List.map
            (\( idx, val ) ->
                postTask
                    url
                    (Enc.saveConstant val)
                    Dec.decodeNewConstant
                    |> Task.map (\const -> ( idx, const ))
            )
        |> Task.sequence
        |> Task.attempt (Ajax << AjaxReceive << ReceiveNewConstantsForJoints)


saveType : String -> Schema -> List NewNameTag -> Cmd Msg
saveType url schema nameTags =
    postTask url (Enc.saveType schema) Dec.decodeTypeSave
        |> Task.andThen
            (\resultNewType ->
                case resultNewType of
                    Err validationError ->
                        Task.succeed <| Err validationError

                    Ok newType ->
                        List.map
                            (\nameTag ->
                                postTask
                                    url
                                    (Enc.saveNameTag
                                        schema
                                        newType.id
                                        (Just newType.version)
                                        nameTag
                                    )
                                    Dec.decodeSingleNameTag
                            )
                            nameTags
                            |> Task.sequence
                            |> Task.andThen
                                (\tags -> Task.succeed <| Ok ( newType, tags ))
            )
        |> Task.attempt (Ajax << AjaxReceive << ReceiveNewType)


setError fn err =
    fn <| Err <| errorToString err


errorToString : Error -> String
errorToString err =
    case err of
        BadBody txt ->
            txt

        BadStatus status ->
            "bad status: " ++ String.fromInt status

        BadUrl _ ->
            "bad url"

        Timeout ->
            "timeout"

        NetworkError ->
            "network error"


unknownAjaxError err =
    MyToast.Error <| "Unknown ajax error. " ++ Debug.toString err


receiveNewEventGenerator :
    Model
    -> SimpleRef
    -> NewEventGenerator
    -> ( Maybe MyToast.Toast, Model )
receiveNewEventGenerator m templateRef newEventGenerator =
    ( Just <| MyToast.Success "New event generator saved"
    , S.setEventGenerators
        (\a ->
            { a
                | items =
                    { base =
                        { id = newEventGenerator.id
                        , version = newEventGenerator.version
                        , createdAt = newEventGenerator.createdAt
                        , deletedAt = Nothing
                        , template = templateRef
                        , config = newEventGenerator.config
                        }
                    , error = Just "not initialized"
                    }
                        :: a.items
            }
        )
        m
    )


receiveActiveNodes : Model -> ActiveNodes -> Model
receiveActiveNodes m payload =
    { m | nodes = payload.nodes }
        |> S.setLinks
            (\a ->
                let
                    processedItems =
                        Utils.toDictByIdFrom payload.links
                in
                { a
                    | items = processedItems.items
                    , byFrom = processedItems.byFrom
                    , byTo = processedItems.byTo
                }
            )
        |> S.setEventGenerators (\a -> { a | items = payload.eventGenerators })


receiveNewNode : Model -> String -> Node -> ( Maybe MyToast.Toast, Model )
receiveNewNode m tmpId node =
    ( Just <| MyToast.Success "New node saved"
    , { m
        | boardSelection =
            m.boardSelection
                |> andThen
                    (\ref ->
                        if ref.id == tmpId then
                            Just { id = node.id, version = node.version }

                        else
                            Just ref
                    )
        , nodes =
            Dict.insert
                node.id
                (Dict.singleton node.version node)
                m.nodes
        , unsavedNodes = Dict.remove tmpId m.unsavedNodes
      }
        |> Link.Main.updateUnsavedLinkTarget tmpId (Just node.id)
    )


receiveNewConstant : Model -> Constant -> ( Maybe MyToast.Toast, Model )
receiveNewConstant m const =
    ( Just <| MyToast.Success "New constant saved"
    , S.setConstants
        (\a -> { a | items = Utils.setItemIdVer const a.items })
        m
    )


toRef : Constant -> CD.CommonData
toRef const =
    CD.CRef <| Just { id = const.id, version = const.version }


receiveNewConstantsForJoints : Model -> List ( Int, Constant ) -> ( Maybe MyToast.Toast, Model )
receiveNewConstantsForJoints m consts =
    ( Just <| MyToast.Success "New constants saved"
    , m
        |> S.setConstants
            (\a -> { a | items = Utils.setItemsIdVer (List.map Tuple.second consts) a.items })
        |> Link.Editor.updateDraftJoints_ (List.map (Tuple.mapSecond toRef) consts)
        |> (\m_ ->
                case m_.linkEditor of
                    Nothing ->
                        m_

                    Just editor ->
                        Link.Main.updateUnsavedLink (Link.Editor.updateLink editor) m_
           )
    )


receiveNewLink : Model -> Link -> ( Maybe MyToast.Toast, Model )
receiveNewLink m link =
    ( Just <| MyToast.Success "New link saved"
    , Link.Main.save
        { m
            | boardSelection =
                m.boardSelection
                    |> andThen
                        (\_ ->
                            Dict.get link.from m.unsavedLinks.byFrom
                        )
                    |> andThen
                        (\_ ->
                            Just { id = link.id, version = link.version }
                        )
        }
        link
    )


ajaxReceive : Model -> AjaxReceivePayload -> ( Maybe MyToast.Toast, Model )
ajaxReceive m rawPayload =
    case rawPayload of
        ReceiveNewConstant payload ->
            case payload of
                Ok a ->
                    receiveNewConstant m a

                Err err ->
                    ( Just <| unknownAjaxError err, m )

        ReceiveNewConstantsForJoints payload ->
            case payload of
                Ok a ->
                    receiveNewConstantsForJoints m a

                Err err ->
                    ( Just <| unknownAjaxError err, m )

        ReceiveNewLink payload ->
            case payload of
                Ok link ->
                    receiveNewLink m link

                Err err ->
                    ( Just <| unknownAjaxError err, m )

        ReceiveActiveNodes payload ->
            case payload of
                Ok activeNodes ->
                    ( Nothing, receiveActiveNodes m activeNodes )

                Err err ->
                    ( Just <| unknownAjaxError err, m )

        ReceiveNewNode payload ->
            case payload of
                Ok (RNNPCommon tmpId node) ->
                    receiveNewNode m tmpId node

                Ok (RNNPEventGenerator templateRef newEventGenerator) ->
                    receiveNewEventGenerator m templateRef newEventGenerator

                Err err ->
                    ( Just <| unknownAjaxError err
                    , S.setNewNode
                        (\a ->
                            case a of
                                Nothing ->
                                    Nothing

                                Just nn ->
                                    Just { nn | saveError = Just <| errorToString err }
                        )
                        m
                    )

        ReceiveConstants payload ->
            case payload of
                Ok consts ->
                    ( Nothing
                    , S.setConstants
                        (\c ->
                            { c
                                | items = consts
                                , error = Nothing
                            }
                        )
                        m
                    )

                Err err ->
                    ( Just <| unknownAjaxError err
                    , S.setConstants
                        (\t -> { t | error = Just <| errorToString err })
                        m
                    )

        ReceiveTemplates payload ->
            case payload of
                Ok templates ->
                    ( Nothing
                    , S.setTemplates
                        (\ev ->
                            { ev
                                | items = templates
                                , error = Nothing
                            }
                        )
                        m
                    )

                Err err ->
                    ( Just <| unknownAjaxError err
                    , S.setTemplates
                        (\t -> { t | error = Just <| errorToString err })
                        m
                    )

        ReceiveTypes payload ->
            case payload of
                Ok types ->
                    ( Nothing, S.setTypes (\t -> { t | items = Ok types }) m )

                Err err ->
                    ( Just <| unknownAjaxError err
                    , setError
                        (\e -> S.setTypes (\t -> { t | items = e }) m)
                        err
                    )

        ReceiveNameTags payload ->
            case payload of
                Ok nameTags ->
                    ( Nothing, { m | nameTags = Ok nameTags } )

                Err err ->
                    ( Just <| unknownAjaxError err
                    , setError (\e -> { m | nameTags = e }) err
                    )

        ReceiveNewType payload ->
            case payload of
                Ok saveResult ->
                    case saveResult of
                        Ok ( newType, nameTags ) ->
                            ( Just <| MyToast.Success "Type saved"
                            , S.setTypes
                                (\t ->
                                    { t
                                        | items =
                                            case t.items of
                                                Ok types ->
                                                    Ok <| newType :: types

                                                Err e ->
                                                    Err e
                                    }
                                )
                                m
                                |> S.setNameTags
                                    (\resultTags ->
                                        case resultTags of
                                            Err _ ->
                                                Ok <| Dict.singleton newType.id nameTags

                                            Ok tags ->
                                                case Dict.get newType.id tags of
                                                    Nothing ->
                                                        Ok <|
                                                            Dict.insert
                                                                newType.id
                                                                nameTags
                                                                tags

                                                    Just curTags ->
                                                        Ok <|
                                                            Dict.insert
                                                                newType.id
                                                                (curTags ++ nameTags)
                                                                tags
                                    )
                            )

                        Err err ->
                            ( Just <| MyToast.Error <| "Type error: " ++ err, m )

                Err err ->
                    ( Just <| unknownAjaxError err, m )


onAjax : Model -> AjaxReq -> ( Model, Cmd Msg )
onAjax m req =
    let
        url =
            m.hostAjaxAddress

        sendByResource_ payload =
            sendByResource url payload
    in
    case req of
        AjaxFetchAll ->
            ( { m | loading = ajaxFetchableResources ++ m.loading }
            , Cmd.batch
                [ sendByResource_ FetchActiveNodes
                , sendByResource_ FetchTypes
                , sendByResource_ FetchNameTags
                , sendByResource_ FetchTemplates
                , sendByResource_ FetchConstants
                ]
            )

        AjaxSend payload ->
            let
                resource =
                    case payload of
                        FetchActiveNodes ->
                            ARActiveNodes

                        FetchConstants ->
                            ARConstants

                        FetchTemplates ->
                            ARTemplates

                        FetchNameTags ->
                            ARTypes

                        FetchTypes ->
                            ARTypes

                        CreateNewConstant _ ->
                            ARNewConstant

                        CreateNewConstantsForJoints _ ->
                            ARNewConstant

                        CreateNewLink _ ->
                            ARNewLink

                        CreateNewType _ _ ->
                            ARType

                        CreateNewNode _ ->
                            ARNewNode
            in
            ( { m | loading = resource :: m.loading }
            , sendByResource_ payload
            )

        AjaxReceive rawPayload ->
            let
                ( maybeToastMsg, newModel ) =
                    ajaxReceive m rawPayload

                resource =
                    case rawPayload of
                        ReceiveActiveNodes _ ->
                            ARActiveNodes

                        ReceiveNewNode _ ->
                            ARNewNode

                        ReceiveConstants _ ->
                            ARConstants

                        ReceiveNewConstant _ ->
                            ARNewConstant

                        ReceiveNewConstantsForJoints _ ->
                            ARNewConstant

                        ReceiveTemplates _ ->
                            ARTemplates

                        ReceiveNameTags _ ->
                            ARNameTags

                        ReceiveTypes _ ->
                            ARTypes

                        ReceiveNewType _ ->
                            ARType

                        ReceiveNewLink _ ->
                            ARNewLink
            in
            ( { newModel | loading = EList.remove resource m.loading }
            , Cmd.none
            )
                |> (case maybeToastMsg of
                        Nothing ->
                            identity

                        Just toastMsg ->
                            Toasty.addToast Toasty.config ToastyMsg toastMsg
                   )
