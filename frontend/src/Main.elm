module Main exposing (main)

import ActionButtons.Definitions
import ActionButtons.Main
import Ajax
import BigOverlay
import Board.Main
import Browser
import Browser.Dom
import Browser.Events
import Browser.Navigation as Nav
import Colors
import CommonData.Tree
import Constants.Main
import DataCreator.Main
import Dict
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onMouseUp)
import Html.Lazy exposing (..)
import Info
import Keyboard as K
import Link.Editor
import Link.Utils
import Maybe exposing (andThen)
import Minimap
import MyKeyboard
import MyLoading exposing (myLoading)
import MyToast
import NewNode.Main
import Ports
import Setters exposing (setTypeCreation, setTypes)
import Task
import Time
import Toasty
import Types exposing (..)
import Url
import Utils


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = \_ -> NoOp
        , onUrlRequest = \_ -> NoOp
        }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        hostAddress =
            getHostAddress url
    in
    ( { actionButtons = ActionButtons.Definitions.initialSet
      , counter = 0
      , unsavedLinks =
            { items = Dict.empty
            , byFrom = Dict.empty
            , byTo = Dict.empty
            , error = Nothing
            }
      , unsavedNodes = Dict.empty
      , nodes = Dict.empty
      , links =
            { items = Dict.empty
            , byFrom = Dict.empty
            , byTo = Dict.empty
            , error = Nothing
            }
      , linkEditor = Nothing
      , newNode = Nothing
      , boardSelection = Nothing
      , browserNavigationKey = key
      , constants =
            { items = Dict.empty
            , error = Nothing
            , page = 0
            , selection = ( 0, 0 )
            }
      , constantsCreator = DataCreator.Main.initModel
      , eventGenerators =
            { items = []
            , error = Nothing
            }
      , focus = FocusHome
      , focusedInput = False
      , hostAjaxAddress = "http://" ++ hostAddress ++ "/api"
      , hostWsAddress = "ws://" ++ hostAddress ++ "/api"
      , layout =
            { boardSize = { width = 0, height = 0 }
            , bottomBarSize = { width = 0, height = 200 }
            , colors = Colors.init
            }
      , loading = []
      , nameTags = Ok Dict.empty
      , pressedKeys = []
      , templates =
            { items = Dict.empty
            , error = Nothing
            }
      , time =
            { posix = Time.millisToPosix 0
            , zone = Time.utc
            }
      , typeCreation =
            { expandText = ""
            , genericText = ""
            , nameTagText = ""
            , nameTags = []
            , rewordText = ""
            , schema = Nothing
            , selected = []
            }
      , types =
            { items = Ok []
            , index = 0
            , selected = Nothing
            }
      , toasties = Toasty.initialState
      }
    , Cmd.batch
        [ Task.perform SetLayoutByViewport Browser.Dom.getViewport
        , Task.perform SetTimeZone Time.here
        , Task.perform SetTimePosix Time.now
        , Utils.msgToCmd <| Ajax AjaxFetchAll
        ]
    )


getHostAddress : Url.Url -> String
getHostAddress url =
    let
        port_ =
            case url.port_ of
                Just p ->
                    ":" ++ String.fromInt p

                Nothing ->
                    ""
    in
    url.host ++ port_


setLayout : (Layout -> Layout) -> Model -> Model
setLayout fn m =
    { m | layout = fn m.layout }


setTime : (MyTime -> MyTime) -> Model -> Model
setTime fn m =
    { m | time = fn m.time }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg m =
    case msg of
        ConstantsMsg subMsg ->
            Constants.Main.update subMsg m

        BackendMessage backendMsg ->
            ( m, Cmd.none )

        BoardSelect maybeRef ->
            let
                selected =
                    case m.boardSelection of
                        Nothing ->
                            maybeRef

                        Just ref ->
                            case maybeRef of
                                Nothing ->
                                    Nothing

                                Just a ->
                                    if a.id == ref.id && a.version == ref.version then
                                        Nothing

                                    else
                                        maybeRef
            in
            ( { m | boardSelection = selected }, Cmd.none )

        BoardClick ->
            ( if m.boardSelection == Nothing then
                m

              else
                { m
                    | boardSelection = Nothing
                    , focus = FocusHome
                }
            , Cmd.none
            )

        InputFocus ->
            ( { m | focusedInput = True }, Cmd.none )

        InputBlur ->
            ( { m | focusedInput = False }, Cmd.none )

        KeyboardMsg keyMsg ->
            MyKeyboard.onKeyboardMsg m keyMsg

        LinkEditorTreeUpdated treeIdx subMsg ->
            ( Link.Editor.updateTree m treeIdx subMsg
            , Cmd.none
            )

        LinkEditorExpandTextUpdated txt ->
            ( Setters.setLinkEditor (Maybe.map (\e -> { e | expandText = txt })) m
            , Cmd.none
            )

        ActionButtonPress actionButton ->
            case actionButton.o.action of
                Nothing ->
                    ( m, Cmd.none )

                Just action ->
                    ActionButtons.Main.onActionButtonPress m actionButton

        ActionButtonSelect actionButton ->
            ( { m | pressedKeys = [ actionButton.r.key ] }, Cmd.none )

        WindowResize width height ->
            let
                boardHeight =
                    height - m.layout.bottomBarSize.height
            in
            ( setLayout (\layout -> { layout | boardSize = Size width boardHeight }) m
            , Cmd.none
            )

        SetTimeZone zone ->
            ( setTime (\time -> { time | zone = zone }) m
            , Cmd.none
            )

        SetTimePosix posix ->
            ( setTime (\time -> { time | posix = posix }) m
            , Cmd.none
            )

        SetLayoutByViewport info ->
            let
                { width, height } =
                    info.viewport

                boardHeight =
                    truncate height - m.layout.bottomBarSize.height

                boardSize =
                    Size (truncate width) boardHeight
            in
            ( setLayout (\layout -> { layout | boardSize = boardSize }) m
            , Cmd.none
            )

        MainMouseUp ->
            ( { m | pressedKeys = [] }
            , Cmd.none
            )

        SelectType coord ->
            let
                selected =
                    Just
                        { coord = coord
                        , path = Nothing
                        }
            in
            ( setTypes (\t -> { t | selected = selected }) m
            , Cmd.none
            )

        SelectTypeSchema path coord ->
            let
                selected =
                    m.types.selected
                        |> Maybe.andThen
                            (\a ->
                                Just
                                    { a
                                        | path = Just path
                                        , coord = coord
                                    }
                            )
            in
            ( setTypes (\t -> { t | selected = selected }) m
            , Cmd.none
            )

        SelectTypeCreationSchema path ->
            ( setTypeCreation (\t -> { t | selected = path }) m, Cmd.none )

        GenericTextChange txt ->
            ( setTypeCreation (\t -> { t | genericText = txt }) m, Cmd.none )

        ExpandTextChange txt ->
            ( setTypeCreation (\t -> { t | expandText = txt }) m, Cmd.none )

        RewordTextChange txt ->
            ( setTypeCreation (\t -> { t | rewordText = txt }) m, Cmd.none )

        NameTagTextChange txt ->
            ( setTypeCreation (\t -> { t | nameTagText = txt }) m, Cmd.none )

        NewNodeUpdateMsg subMsg ->
            ( Setters.setNewNodeTree (\tree -> CommonData.Tree.update tree subMsg) m
            , Cmd.none
            )

        Ajax req ->
            Ajax.onAjax m req

        ToastyMsg subMsg ->
            Toasty.update Toasty.config ToastyMsg subMsg m

        NoOp ->
            ( m, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions m =
    Sub.batch
        [ Sub.map KeyboardMsg K.subscriptions
        , Browser.Events.onResize WindowResize
        , Time.every 60000 SetTimePosix
        , Ports.focused
            (\focused ->
                if focused then
                    InputFocus

                else
                    InputBlur
            )
        ]


bottomBar : Model -> Html Msg
bottomBar m =
    let
        gridTemplateColumns =
            String.fromInt m.layout.bottomBarSize.height ++ "px auto 264px"
    in
    div
        [ style "display" "grid"
        , style "gridTemplateColumns" gridTemplateColumns
        , style "backgroundColor" m.layout.colors.primaryD1
        ]
        [ m |> Minimap.minimapView
        , m |> Info.infoView
        , m |> ActionButtons.Main.view
        ]


view : Model -> Browser.Document Msg
view m =
    let
        gridTemplateRows =
            String.fromInt
                m.layout.boardSize.height
                ++ "px"
                ++ " "
                ++ String.fromInt m.layout.bottomBarSize.height
                ++ "px"
    in
    Browser.Document
        "Aspen"
        [ div
            [ style "display" "grid"
            , style "height" "100vh"
            , style "gridTemplateColumns" "100%"
            , style "gridTemplateRows" gridTemplateRows
            , style "color" m.layout.colors.text
            , style "font-family" "monospace"
            , onMouseUp MainMouseUp
            ]
            [ m |> Board.Main.view
            , m |> bottomBar
            , m |> BigOverlay.bigOverlay
            , m.loading |> lazy myLoading
            , lazy3 MyToast.toastsContainerView m.layout.colors m.toasties ToastyMsg
            ]
        ]
