module MyKeyboard exposing (onKeyboardMsg)

import ActionButtons.Definitions exposing (..)
import ActionButtons.Main
    exposing
        ( findBtnByAction_
        , findBtnByKey_
        , onActionButtonPress
        )
import ActionButtons.Utils
import CommonData.Tree
import DbTypes exposing (..)
import Dict
import GenericTypes exposing (KeyVal(..))
import Grid.Extra
import Grid.Types
import Keyboard as K
import Link.Utils
import List.Extra as EList
import Maybe exposing (andThen, withDefault)
import Ports
import Schema.Tree
import Setters
import Tree.Keyboard
import Tree.Types as TT
import Types exposing (..)
import TypesList
import Utils


moveXByY : K.Key -> Coord -> List (List a) -> List a -> Int
moveXByY arrowKey ( x, y ) visible row =
    let
        rowLength =
            List.length row

        firstRow =
            List.head visible |> Maybe.withDefault []

        firstRowLength =
            List.length firstRow
    in
    case arrowKey of
        K.ArrowUp ->
            let
                lastRow =
                    EList.last visible |> Maybe.withDefault []

                prevRow =
                    EList.getAt (y - 1) visible |> Maybe.withDefault lastRow

                prevRowLength =
                    List.length prevRow
            in
            if rowLength < firstRowLength then
                x + (firstRowLength - rowLength) // 2

            else if prevRowLength < rowLength then
                let
                    result =
                        x - prevRowLength
                in
                if result < 0 then
                    0

                else
                    result

            else
                x

        K.ArrowDown ->
            let
                nextRow =
                    EList.getAt (y + 1) visible |> Maybe.withDefault firstRow

                nextRowLength =
                    List.length nextRow
            in
            if nextRowLength < firstRowLength then
                let
                    result =
                        x - nextRowLength
                in
                if result < 0 then
                    0

                else
                    result

            else if nextRowLength > rowLength then
                x + (nextRowLength - rowLength) // 2

            else
                x

        _ ->
            x


getNewCoord : Coord -> K.Key -> List (List a) -> Coord
getNewCoord ( x, y ) arrowKey visible =
    let
        row =
            EList.getAt y visible |> Maybe.withDefault []

        maxY =
            List.length visible - 1

        maxX =
            List.length row - 1

        decrementX =
            if x - 1 < 0 then
                maxX

            else
                x - 1

        incrementX =
            if x + 1 > maxX then
                0

            else
                x + 1

        decrementY =
            if y - 1 < 0 then
                maxY

            else
                y - 1

        incrementY =
            if y + 1 > maxY then
                0

            else
                y + 1

        moveXByY_ _ =
            moveXByY arrowKey ( x, y ) visible row

        newCoord =
            case arrowKey of
                K.ArrowUp ->
                    ( moveXByY_ (), decrementY )

                K.ArrowDown ->
                    ( moveXByY_ (), incrementY )

                K.ArrowRight ->
                    ( incrementX, y )

                K.ArrowLeft ->
                    ( decrementX, y )

                _ ->
                    ( x, y )

        ( isNewCoordOnBoard, maxIdx ) =
            if maxX < 0 then
                ( False, 0 )

            else
                let
                    ( newX, newY ) =
                        newCoord

                    flatIdx =
                        newY * maxX + newX

                    maxIdx_ =
                        List.foldl (\a acc -> List.length a - 1 + acc) 0 visible
                in
                ( flatIdx <= maxIdx_, maxIdx_ )
    in
    if isNewCoordOnBoard then
        newCoord

    else
        Utils.idxToCoord maxX maxIdx


type Direction
    = Down
    | Left
    | Right
    | Up


getDirection : List K.Key -> Maybe Direction
getDirection keys =
    EList.find Utils.isArrowKey keys
        |> andThen
            (\key ->
                case key of
                    K.ArrowUp ->
                        Just Up

                    K.ArrowDown ->
                        Just Down

                    K.ArrowLeft ->
                        Just Left

                    K.ArrowRight ->
                        Just Right

                    _ ->
                        Nothing
            )


getTypeSelectionOnFocusTypes : Model -> List K.Key -> Maybe GridSchemaSelection
getTypeSelectionOnFocusTypes m pressedKeys =
    let
        visible =
            Grid.Extra.getVisibleTypesRows m

        oldSelected =
            m.types.selected

        firstArrowKey =
            EList.find Utils.isArrowKey pressedKeys
    in
    case m.types.selected of
        Just selected ->
            case firstArrowKey of
                Just firstArrowKey_ ->
                    Just <|
                        if selected.path == Nothing then
                            { path = Nothing
                            , coord =
                                getNewCoord
                                    selected.coord
                                    firstArrowKey_
                                    visible
                            }

                        else
                            let
                                schema =
                                    Utils.findByCoord
                                        visible
                                        selected.coord
                                        |> Maybe.andThen (Just << .schema)
                            in
                            case schema of
                                Nothing ->
                                    selected

                                Just schema_ ->
                                    { path =
                                        case selected.path of
                                            Nothing ->
                                                Just []

                                            Just path ->
                                                TypesList.getSelectedTree m
                                                    |> Maybe.map (Tree.Keyboard.getNewPos pressedKeys path)
                                    , coord = selected.coord
                                    }

                _ ->
                    Just selected

        Nothing ->
            if firstArrowKey /= Nothing then
                oldSelected

            else
                case List.head visible of
                    Nothing ->
                        oldSelected

                    Just firstRow ->
                        case List.head firstRow of
                            Nothing ->
                                oldSelected

                            Just a ->
                                Just
                                    { path = Nothing
                                    , coord = ( 0, 0 )
                                    }


getTypeSelection : Model -> List K.Key -> Maybe (Maybe GridSchemaSelection)
getTypeSelection m pressedKeys =
    case m.types.items of
        Err _ ->
            Nothing

        Ok visible ->
            case m.focus of
                FocusTypes ->
                    Just <| getTypeSelectionOnFocusTypes m pressedKeys

                FocusCreateType TCFAddRef ->
                    Just <| getTypeSelectionOnFocusTypes m pressedKeys

                _ ->
                    Nothing


getConstantsSelection : Model -> List K.Key -> Maybe Grid.Types.Coord
getConstantsSelection m pressedKeys =
    let
        job () =
            let
                maybeFirstArrowKey =
                    EList.find Utils.isArrowKey pressedKeys
            in
            maybeFirstArrowKey
                |> Maybe.map
                    (\firstArrowKey ->
                        getNewCoord
                            m.constants.selection
                            firstArrowKey
                            (Grid.Extra.getVisibleConstantsRows m)
                    )
    in
    case ActionButtons.Utils.getTopButtonSet m.actionButtons of
        ABConstsCreator ABCCSelectRef ->
            job ()

        ABConstsBrowser ->
            job ()

        ABEdit ABELRefs ->
            job ()

        _ ->
            Nothing


getConstantsCreatorSelection : Model -> List K.Key -> Maybe TT.Path
getConstantsCreatorSelection m pressedKeys =
    case ActionButtons.Utils.getTopButtonSet m.actionButtons of
        ABConstsCreator ABCCMain ->
            m.constantsCreator.tree
                |> andThen
                    (\tree ->
                        case tree.selection of
                            Nothing ->
                                Just []

                            Just selection ->
                                Tree.Keyboard.getNewPos
                                    pressedKeys
                                    selection
                                    (CommonData.Tree.dataToTreeItem
                                        { m = tree
                                        , o = CommonData.Tree.defaultOpts
                                        , data = tree.data
                                        , path = []
                                        , colors = m.layout.colors
                                        }
                                    )
                                    |> Just
                    )

        _ ->
            Nothing


getNodeCreatorSelection : Model -> List K.Key -> Maybe TT.Path
getNodeCreatorSelection m pressedKeys =
    case ActionButtons.Utils.getTopButtonSet m.actionButtons of
        ABNewNode ->
            m.newNode
                |> andThen
                    (\newNode ->
                        case newNode.tree.selection of
                            Nothing ->
                                Just []

                            Just selection ->
                                Tree.Keyboard.getNewPos
                                    pressedKeys
                                    selection
                                    (CommonData.Tree.dataToTreeItem
                                        { m = newNode.tree
                                        , o = CommonData.Tree.defaultOpts
                                        , data = newNode.tree.data
                                        , path = []
                                        , colors = m.layout.colors
                                        }
                                    )
                                    |> Just
                    )

        _ ->
            Nothing


isShiftPressed : List K.Key -> Bool
isShiftPressed keys =
    List.member K.Shift keys


moveHorizontally : Int -> Int -> List K.Key -> Int
moveHorizontally curIdx maxIdx keys =
    case getDirection keys of
        Nothing ->
            curIdx

        Just direction ->
            case direction of
                Left ->
                    if curIdx <= 0 then
                        curIdx

                    else
                        curIdx - 1

                Right ->
                    if curIdx >= maxIdx then
                        curIdx

                    else
                        curIdx + 1

                _ ->
                    curIdx


getLinkEditorSelection : Model -> List K.Key -> Maybe TT.Path
getLinkEditorSelection m pressedKeys =
    case ActionButtons.Utils.getTopButtonSet m.actionButtons of
        ABEdit ABELMain ->
            Link.Utils.getSelectedEditorTree m
                |> andThen
                    (\tree ->
                        case tree.selection of
                            Nothing ->
                                Just []

                            Just selection ->
                                Tree.Keyboard.getNewPos
                                    pressedKeys
                                    selection
                                    (CommonData.Tree.dataToTreeItem
                                        { m = tree
                                        , o = CommonData.Tree.defaultOpts
                                        , data = tree.data
                                        , path = []
                                        , colors = m.layout.colors
                                        }
                                    )
                                    |> Just
                    )

        _ ->
            Nothing


getTypeCreationSchemaSelection : Model -> List K.Key -> Maybe TT.Path
getTypeCreationSchemaSelection m pressedKeys =
    case ( m.typeCreation.schema, m.focus ) of
        ( Just schema, FocusCreateType _ ) ->
            Tree.Keyboard.getNewPos
                pressedKeys
                m.typeCreation.selected
                (Schema.Tree.typeToTreeItem
                    m.layout.colors
                    (Result.withDefault Dict.empty m.nameTags)
                    []
                    schema
                )
                |> Just

        _ ->
            Nothing


getTypeSelectionScrollMsg : Model -> Cmd Msg
getTypeSelectionScrollMsg m =
    case m.types.selected of
        Nothing ->
            Cmd.none

        Just selected ->
            case selected.path of
                Nothing ->
                    Cmd.none

                Just path ->
                    Ports.scrollIntoViewById "selected"


getScrollCmd : K.Key -> Model -> Cmd Msg
getScrollCmd key m =
    if Utils.isArrowKey key then
        case m.focus of
            FocusCreateType innerFocus ->
                case innerFocus of
                    TCFAddRef ->
                        getTypeSelectionScrollMsg m

                    _ ->
                        Ports.scrollIntoViewById "selected"

            FocusTypes ->
                getTypeSelectionScrollMsg m

            _ ->
                Cmd.none

    else
        Cmd.none


formEscape pressedKeys =
    List.member K.Escape pressedKeys


formConfirm pressedKeys =
    List.member K.Enter pressedKeys


onKeyboardMsg : Model -> K.Msg -> ( Model, Cmd Msg )
onKeyboardMsg m keyMsg =
    let
        ( pressedKeys, keyChange ) =
            K.updateWithKeyChange K.anyKey keyMsg m.pressedKeys
    in
    if formEscape pressedKeys then
        ( m, Ports.blur () )

    else if formConfirm pressedKeys then
        case findBtnByAction_ m (Just Confirm) of
            Nothing ->
                ( m, Cmd.none )

            Just btn ->
                onActionButtonPress m btn

    else if m.focusedInput then
        ( m, Cmd.none )

    else
        case keyChange of
            Nothing ->
                if EList.find Utils.isArrowKey pressedKeys == Nothing then
                    ( m, Cmd.none )

                else
                    onKeyboardMsg_ m pressedKeys keyChange

            Just keyChange_ ->
                case keyChange_ of
                    K.KeyUp _ ->
                        onKeyboardMsg_ m pressedKeys keyChange

                    K.KeyDown _ ->
                        onKeyboardMsg_ m pressedKeys keyChange


onKeyboardMsg_ : Model -> List K.Key -> Maybe K.KeyChange -> ( Model, Cmd Msg )
onKeyboardMsg_ m pressedKeys keyChange =
    let
        correspondingBtn =
            case keyChange of
                Nothing ->
                    List.head pressedKeys
                        |> andThen (findBtnByKey_ m)

                Just keyChange_ ->
                    case keyChange_ of
                        K.KeyUp key ->
                            findBtnByKey_ m key

                        K.KeyDown key ->
                            findBtnByKey_ m key

        ( newModel, cmd ) =
            case correspondingBtn of
                Just btn ->
                    if btn.o.isDisabled then
                        ( m, Cmd.none )

                    else
                        case keyChange of
                            Nothing ->
                                ( m, Cmd.none )

                            Just keyChange_ ->
                                case keyChange_ of
                                    K.KeyUp _ ->
                                        onActionButtonPress m btn

                                    K.KeyDown _ ->
                                        ( m, Cmd.none )

                Nothing ->
                    ( m, Cmd.none )

        scrollCmd =
            case EList.last pressedKeys of
                Just key ->
                    getScrollCmd key newModel

                Nothing ->
                    Cmd.none

        newModel_ =
            { newModel | pressedKeys = pressedKeys }
                |> (case getTypeSelection newModel pressedKeys of
                        Nothing ->
                            identity

                        Just s ->
                            Setters.setTypes (\t -> { t | selected = s })
                   )
                |> (case getTypeCreationSchemaSelection newModel pressedKeys of
                        Nothing ->
                            identity

                        Just p ->
                            Setters.setTypeCreation (\t -> { t | selected = p })
                   )
                |> (case getConstantsSelection newModel pressedKeys of
                        Nothing ->
                            identity

                        Just s ->
                            Setters.setConstants (\a -> { a | selection = s })
                   )
                |> (case getConstantsCreatorSelection newModel pressedKeys of
                        Nothing ->
                            identity

                        Just s ->
                            Setters.setConstantsCreatorTree
                                (Maybe.map
                                    (\a ->
                                        { a | selection = Just s }
                                    )
                                )
                   )
                |> (case getNodeCreatorSelection newModel pressedKeys of
                        Nothing ->
                            identity

                        Just s ->
                            Setters.setNewNodeTree (\a -> { a | selection = Just s })
                   )
                |> (if isShiftPressed pressedKeys then
                        Setters.setLinkEditor
                            (Maybe.map
                                (\editor ->
                                    { editor
                                        | selectedTreeIdx =
                                            moveHorizontally
                                                editor.selectedTreeIdx
                                                (List.length editor.trees - 1)
                                                pressedKeys
                                    }
                                )
                            )

                    else
                        case getLinkEditorSelection newModel pressedKeys of
                            Nothing ->
                                identity

                            Just s ->
                                Link.Utils.setSelectedEditorTreeSelection (Just s)
                   )
    in
    ( newModel_
    , Cmd.batch [ cmd, scrollCmd ]
    )
