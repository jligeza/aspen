module MyToast exposing
    ( Toast(..)
    , toastsContainerView
    )

import Colors exposing (Colors)
import Html exposing (..)
import Html.Attributes exposing (..)
import Toasty as T
import Toasty.Defaults


type Toast
    = Success String
    | Error String
    | Warning String


toastView : Colors -> Toast -> Html msg
toastView colors toast =
    let
        ( toastText, borderColor ) =
            case toast of
                Success a ->
                    ( a, colors.primaryL3 )

                Warning a ->
                    ( a, colors.warning )

                Error a ->
                    ( a, colors.error )
    in
    div
        [ style "background-color" colors.primaryD1
        , style "padding" "0.5em"
        , style "border" <| "1px solid " ++ borderColor
        ]
        [ text toastText ]


toastsContainerView : Colors -> T.Stack Toast -> (T.Msg Toast -> msg) -> Html msg
toastsContainerView colors toasties msg =
    span
        [ style "position" "fixed"
        , style "width" "100%"
        ]
        [ T.view
            Toasty.Defaults.config
            (toastView colors)
            msg
            toasties
        ]
