module TypesList exposing
    ( getSelectedTree
    , typesButtons
    , typesList
    )

import ActionButtons.Definitions exposing (..)
import ActionButtons.Utils
import DbTypes exposing (..)
import Dict
import Grid.Extra
import Grid.Main
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Lazy exposing (..)
import Keyboard exposing (Key(..))
import List.Extra as EList
import Maybe exposing (andThen)
import Schema.Tree
import Tree.Main as TM
import Tree.Types as TT
import Types exposing (..)


lazyGrid :
    Layout
    -> NameTags
    -> MyTime
    -> TypesPack
    -> Html Msg
lazyGrid layout nameTags time types =
    let
        defaultGridOpts =
            Grid.Main.defaultOpts

        defaultTreeOpts =
            TT.defaultOpts
    in
    Grid.Main.grid
        { colors = layout.colors
        , width = layout.boardSize.width
        , height = layout.boardSize.height
        , page = types.index
        , coord =
            case types.selected of
                Nothing ->
                    ( 0, 0 )

                Just a ->
                    a.coord
        , tileContent =
            \coord idx item ->
                Grid.Extra.contentWithNameTag
                    layout.colors
                    nameTags
                    (case types.selected of
                        Nothing ->
                            False

                        Just selected ->
                            coord == selected.coord
                    )
                    item
                    (TM.view
                        { colors = layout.colors
                        , selection =
                            Maybe.andThen
                                (\s ->
                                    if s.coord == coord then
                                        s.path

                                    else
                                        Nothing
                                )
                                types.selected
                        }
                        { defaultTreeOpts
                            | onItemSelect = Just <| \p -> SelectTypeSchema p coord
                        }
                        (Schema.Tree.typeToTreeItem layout.colors nameTags [] item.schema)
                    )
        }
        { defaultGridOpts | onTileSelect = Just (.coord >> SelectType) }
        (Result.withDefault [] types.items)


getSelectedTree : Model -> Maybe (TT.Item msg)
getSelectedTree m =
    m.types.selected
        |> andThen
            (\selection ->
                let
                    ( x, y ) =
                        selection.coord
                in
                Grid.Extra.getVisibleTypesRows m
                    |> EList.getAt y
                    |> andThen (EList.getAt x)
                    |> Maybe.map .schema
                    |> Maybe.map (Schema.Tree.typeToTreeItem m.layout.colors Dict.empty [])
            )


typesList : Model -> Html Msg
typesList m =
    case m.nameTags of
        Ok nameTags ->
            lazy4 lazyGrid
                m.layout
                nameTags
                m.time
                m.types

        Err err ->
            text err


typesButtons : Model -> List ActionButton
typesButtons m =
    let
        setBtn key =
            case key of
                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                Character "q" ->
                    ActionButton
                        { key = key, text = "first page" }
                        { defaultOptions
                            | action = Just TypesFirstPage
                            , isDisabled = m.types.index == 0
                        }

                Character "w" ->
                    ActionButton
                        { key = key, text = "last page" }
                        { defaultOptions
                            | action = Just TypesLastPage
                            , isDisabled = m.types.index == Grid.Extra.getMaxTypesPage m
                        }

                Character "a" ->
                    ActionButton
                        { key = key, text = "prev page" }
                        { defaultOptions
                            | action = Just TypesPrevPage
                            , isDisabled = m.types.index == 0
                        }

                Character "s" ->
                    ActionButton
                        { key = key, text = "next page" }
                        { defaultOptions
                            | action = Just TypesNextPage
                            , isDisabled =
                                case m.types.items of
                                    Err _ ->
                                        True

                                    Ok items ->
                                        Grid.Extra.getMaxTypesPage m <= m.types.index
                        }

                Character "d" ->
                    case ActionButtons.Utils.getTopButtonSet m.actionButtons of
                        ABRef ->
                            ActionButton
                                { key = key, text = "select" }
                                { defaultOptions | action = Just Confirm }

                        _ ->
                            ActionButton
                                { key = key, text = "focus" }
                                { defaultOptions
                                    | action = Just TypesFocus
                                    , bgColor =
                                        case m.types.selected of
                                            Nothing ->
                                                defaultOptions.bgColor

                                            Just selected ->
                                                case selected.path of
                                                    Nothing ->
                                                        defaultOptions.bgColor

                                                    Just _ ->
                                                        SelectedBg
                                }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys
