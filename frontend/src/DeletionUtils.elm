module DeletionUtils exposing (deleteUnsavedBoardItem)

import Dict
import FetchedUtils
import Link.Main
import Setters
import Types exposing (Model)


deleteUnsavedLink : Model -> String -> Model
deleteUnsavedLink m id =
    Setters.setUnsavedLinks
        (\a ->
            { a
                | items = Dict.remove id a.items
                , byTo =
                    Dict.filter
                        (\k v -> v.id |> (==) id |> not)
                        a.byTo
                , byFrom =
                    Dict.foldl
                        (\k v acc ->
                            let
                                newList =
                                    List.filter
                                        (.id >> (==) id >> not)
                                        v
                            in
                            if List.isEmpty newList then
                                acc

                            else
                                Dict.insert k newList acc
                        )
                        Dict.empty
                        a.byFrom
            }
        )
        { m | boardSelection = FetchedUtils.findBoardItemParent m id }


deleteUnsavedBoardItem : Model -> String -> Model
deleteUnsavedBoardItem m id =
    case Dict.get id m.unsavedLinks.items of
        Just _ ->
            deleteUnsavedLink m id

        Nothing ->
            { m
                | unsavedNodes = Dict.remove id m.unsavedNodes
                , boardSelection = FetchedUtils.findBoardItemParent m id
            }
                |> Link.Main.updateUnsavedLinkTarget id Nothing
