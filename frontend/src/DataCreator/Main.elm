module DataCreator.Main exposing
    ( defaultOpts
    , deleteSelected
    , discard
    , expandInfoView
    , expandSelected
    , initModel
    , rewordInfoView
    , rewordSelected
    , setDataSelected
    , setExpandText
    , update
    , view
    )

import Colors exposing (Colors)
import CommonData.Main as CD
import CommonData.Tree
import CommonData.Utils as CDU
import DataCreator.Types exposing (..)
import Dict
import Dict.Extra as EDict
import Html exposing (..)
import Html.Attributes exposing (..)
import InfoUtils
import List.Extra as EList
import Setters
import Tree.Types as TT
import Types


initModel : Model
initModel =
    { expandText = ""
    , rewordText = ""
    , tree = Nothing
    }


defaultOpts : Opts
defaultOpts =
    { nameTags = Dict.empty
    , constants = Dict.empty
    }


discard : Model -> Model
discard m =
    { m | tree = Nothing }


setData : Model -> CD.Path -> CD.CommonData -> Model
setData m path val =
    { m
        | tree =
            case m.tree of
                Nothing ->
                    Just { data = val, selection = Just [], schema = Nothing }

                Just tree ->
                    Just
                        { tree
                            | data =
                                CD.updateByPath
                                    path
                                    (CD.tryMigrateOldVal val >> Result.withDefault val)
                                    tree.data
                        }
    }


setDataSelected : Model -> CD.CommonData -> Model
setDataSelected m val =
    case m.tree of
        Nothing ->
            { m | tree = Just { data = val, selection = Just [], schema = Nothing } }

        Just tree ->
            case tree.selection of
                Nothing ->
                    m

                Just selection ->
                    setData m selection val


setExpandText : Model -> String -> Model
setExpandText m txt =
    { m | expandText = txt }


update : Model -> Msg -> Model
update m msg =
    case msg of
        DataTreeUpdated subMsg ->
            case m.tree of
                Nothing ->
                    m

                Just tree ->
                    { m | tree = Just <| CommonData.Tree.update tree subMsg }

        ExpandInfoInputChanged txt ->
            { m | expandText = txt }

        RewordInfoInputChanged txt ->
            { m | rewordText = txt }


deleteSelected : CD.Db a -> Model -> Model
deleteSelected db m =
    Setters.setTree
        (Maybe.map
            (\tree ->
                let
                    deletionResult =
                        tree.selection
                            |> Maybe.map (\selection -> CD.delete db selection tree.data)
                in
                case deletionResult of
                    Just pack ->
                        case pack.val of
                            Nothing ->
                                tree

                            Just val ->
                                { tree
                                    | data = val
                                    , selection = Just pack.path
                                }

                    Nothing ->
                        tree
            )
        )
        m


expandSelected : CD.Db a -> Model -> Model
expandSelected consts m =
    Setters.setTree
        (Maybe.map
            (\tree ->
                case tree.selection of
                    Nothing ->
                        tree

                    Just selection ->
                        { tree
                            | data =
                                CD.updateByPath
                                    selection
                                    (\val ->
                                        case val of
                                            CD.CRecord attrs ->
                                                attrs
                                                    |> Dict.insert
                                                        (String.trim m.expandText)
                                                        (CD.CString "")
                                                    |> CD.CRecord

                                            CD.CList (item :: more) ->
                                                CD.reset item
                                                    :: item
                                                    :: more
                                                    |> CD.CList

                                            CD.CList [] ->
                                                case
                                                    CDU.getListContentTypeBySiblings
                                                        selection
                                                        tree.data
                                                of
                                                    Nothing ->
                                                        val

                                                    Just content ->
                                                        CD.CList [ CD.reset content ]

                                            CD.CUnion (Just ( key, Nothing )) ->
                                                CD.CUnion (Just ( key, Just <| CD.CString "" ))

                                            _ ->
                                                val
                                    )
                                    tree.data
                        }
            )
        )
        { m | expandText = "" }


rewordSelected : CD.Db a -> Model -> Model
rewordSelected db m =
    Setters.setTree
        (Maybe.map
            (\tree ->
                case tree.selection of
                    Nothing ->
                        tree

                    Just selection ->
                        case EList.unconsLast selection of
                            Just ( TT.StepKey key kv, initSelection ) ->
                                let
                                    newKey =
                                        m.rewordText |> String.trim
                                in
                                { tree
                                    | selection = Just <| initSelection ++ [ TT.StepKey newKey kv ]
                                    , data =
                                        CD.updateByPath
                                            initSelection
                                            (\val ->
                                                case val of
                                                    CD.CRecord attrs ->
                                                        EDict.mapKeys
                                                            (\k ->
                                                                if k == key then
                                                                    newKey

                                                                else
                                                                    k
                                                            )
                                                            attrs
                                                            |> CD.CRecord

                                                    _ ->
                                                        val
                                            )
                                            tree.data
                                }

                            _ ->
                                tree
            )
        )
        { m | rewordText = "" }


expandInfoView : Colors -> (Msg -> Types.Msg) -> Model -> Html Types.Msg
expandInfoView colors updater m =
    InfoUtils.newTextView
        colors
        m.expandText
        (updater << ExpandInfoInputChanged)


rewordInfoView : Colors -> (Msg -> Types.Msg) -> Model -> Html Types.Msg
rewordInfoView colors updater m =
    InfoUtils.newTextView
        colors
        m.rewordText
        (updater << RewordInfoInputChanged)


view : Reqs msg -> Opts -> Html msg
view r o =
    case r.model.tree of
        Nothing ->
            text "Nothingness..."

        Just tree ->
            let
                treeOpts =
                    CommonData.Tree.defaultOpts
            in
            CommonData.Tree.tree
                r.colors
                tree
                { treeOpts
                    | nameTags = o.nameTags
                    , constants = o.constants
                    , editable = True
                    , updateMsg = Just (r.msg << DataTreeUpdated)
                }
