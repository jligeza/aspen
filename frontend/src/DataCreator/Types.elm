module DataCreator.Types exposing
    ( Model
    , Msg(..)
    , Opts
    , Reqs
    )

import Colors exposing (Colors)
import CommonData.Main as CD
import CommonData.TreeTypes
import DbTypes exposing (Constant, NameTags, Schema)
import GenericTypes


type alias Opts =
    { nameTags : NameTags
    , constants : GenericTypes.CollectionIdVer Constant
    }


type alias Reqs msg =
    { colors : Colors
    , model : Model
    , msg : Msg -> msg
    }


type alias Model =
    { expandText : String
    , rewordText : String
    , tree :
        Maybe
            { data : CD.CommonData
            , selection : Maybe CD.Path
            , schema : Maybe Schema
            }
    }


type Msg
    = DataTreeUpdated CommonData.TreeTypes.TreeMsg
    | ExpandInfoInputChanged String
    | RewordInfoInputChanged String
