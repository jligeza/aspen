module DataCreator.Buttons exposing (dataCreatorBaseTypesButtons)

import ActionButtons.Definitions exposing (..)
import GenericTypes exposing (BaseType(..))
import Keyboard exposing (Key(..))
import Types exposing (Model)


dataCreatorBaseTypesButtons : Model -> List ActionButton
dataCreatorBaseTypesButtons m =
    let
        setBtn key =
            case key of
                Character "q" ->
                    ActionButton
                        { key = key, text = "record" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator <| CCSelectType BTRecord
                        }

                Character "a" ->
                    ActionButton
                        { key = key, text = "list" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator <| CCSelectType BTList
                        }

                Character "z" ->
                    ActionButton
                        { key = key, text = "ref" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator <| CCSelectType BTRef
                        }

                Character "w" ->
                    ActionButton
                        { key = key, text = "union" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator <| CCSelectType BTUnion
                        }

                Character "s" ->
                    ActionButton
                        { key = key, text = "string" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator <| CCSelectType BTString
                        }

                Character "e" ->
                    ActionButton
                        { key = key, text = "int" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator <| CCSelectType BTInt
                        }

                Character "d" ->
                    ActionButton
                        { key = key, text = "float" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator <| CCSelectType BTFloat
                        }

                Character "c" ->
                    ActionButton
                        { key = key, text = "bool" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator <| CCSelectType BTBool
                        }

                Character "r" ->
                    ActionButton
                        { key = key, text = "json" }
                        { defaultOptions
                            | action =
                                Just <| ConstsAct <| ConstsCreator <| CCSelectType BTJson
                        }

                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys
