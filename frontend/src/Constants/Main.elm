module Constants.Main exposing
    ( browser
    , canExpandSelected
    , canRewordSelected
    , creator
    , creatorExpandTextInfoContent
    , creatorExpandTextInfoTitle
    , creatorMainInfoContent
    , creatorRewordInfoContent
    , deleteSelected
    , expandSelected
    , findSelected
    , findSelectedCreatorVal
    , getRewordText
    , rewordSelected
    , save
    , update
    )

import CommonData.Main as CD
import CommonData.Tree
import CommonData.Utils
import Constants.Types as CT
import DataCreator.Main as DC
import DbTypes exposing (Constant)
import Dict
import Grid.Extra
import Grid.Main
import Grid.Types
import Grid.Utils
import Html exposing (..)
import Html.Attributes exposing (..)
import List.Extra as EList
import Maybe exposing (withDefault)
import Setters
import Tree.Types as TT
import Types exposing (..)
import Utils


treeOpts =
    CommonData.Tree.defaultOpts


tile : Model -> Grid.Types.Coord -> Int -> Constant -> Html Msg
tile m coord idx item =
    let
        nameTags =
            Result.withDefault Dict.empty m.nameTags
    in
    Grid.Extra.contentWithNameTag
        m.layout.colors
        nameTags
        (m.constants.selection == coord)
        item
        (CommonData.Tree.tree
            m.layout.colors
            { data = item.value, selection = Nothing, schema = Nothing }
            { treeOpts
                | constants = m.constants.items
                , nameTags = m.nameTags |> Result.withDefault Dict.empty
            }
        )


browser : Model -> Html Msg
browser m =
    let
        defaults =
            Grid.Main.defaultOpts
    in
    Grid.Main.grid
        { colors = m.layout.colors
        , width = m.layout.boardSize.width
        , height = m.layout.boardSize.height
        , page = m.constants.page
        , coord = m.constants.selection
        , tileContent = tile m
        }
        { defaults
            | onTileSelect =
                Just
                    (ConstantsMsg << CT.SelectItem << .coord)
        }
        (getConstsList m)


getConstsList : Model -> List Constant
getConstsList m =
    m.constants.items
        |> Dict.values
        |> List.map Dict.values
        |> List.concat


findSelected : Model -> Maybe Constant
findSelected m =
    Grid.Utils.findItem_
        m.constants.page
        m.constants.selection
        m.layout.boardSize.width
        m.layout.boardSize.height
        (getConstsList m)


findSelectedCreatorVal : Model -> Maybe CD.CommonData
findSelectedCreatorVal m =
    m.constantsCreator.tree
        |> Maybe.andThen
            (\tree ->
                tree.selection
                    |> Maybe.andThen
                        (\selection ->
                            CD.find
                                m.constants.items
                                selection
                                tree.data
                        )
            )


findPrevSelectedCreatorVal : Model -> Maybe CD.CommonData
findPrevSelectedCreatorVal m =
    m.constantsCreator.tree
        |> Maybe.andThen
            (\tree ->
                tree.selection
                    |> Maybe.andThen
                        (\selection ->
                            CD.find
                                m.constants.items
                                (EList.init selection |> withDefault [])
                                tree.data
                        )
            )


update : CT.Msg -> Model -> ( Model, Cmd Msg )
update msg m =
    case msg of
        CT.Expand ->
            ( m, Cmd.none )

        CT.Discard ->
            ( { m
                | constantsCreator =
                    DC.discard m.constantsCreator
              }
            , Cmd.none
            )

        CT.DataCreatorUpdated subMsg ->
            ( { m
                | constantsCreator =
                    DC.update m.constantsCreator subMsg
              }
            , Cmd.none
            )

        CT.SelectType baseType ->
            ( case CommonData.Utils.baseTypeToCommonData baseType of
                Nothing ->
                    m

                Just data ->
                    let
                        cc =
                            m.constantsCreator

                        update_ newData =
                            { m
                                | constantsCreator =
                                    DC.setDataSelected cc newData
                            }
                    in
                    case cc.tree of
                        Nothing ->
                            update_ data

                        Just tree ->
                            case tree.selection of
                                Nothing ->
                                    update_ data

                                Just selection ->
                                    case
                                        CD.find
                                            m.constants.items
                                            selection
                                            tree.data
                                    of
                                        Just (CD.CList []) ->
                                            update_ <| CD.CList [ data ]

                                        _ ->
                                            update_ data
            , Cmd.none
            )

        CT.SelectItem coord ->
            ( Setters.setConstants
                (\a -> { a | selection = coord })
                m
            , Cmd.none
            )

        CT.FocusConstant constant ->
            ( { m
                | constantsCreator =
                    DC.setDataSelected
                        m.constantsCreator
                        (CD.CRef <|
                            Just
                                { id = constant.id
                                , version = constant.version
                                }
                        )
              }
            , Cmd.none
            )


save : Model -> ( Model, Cmd Msg )
save m =
    ( m
    , case m.constantsCreator.tree of
        Nothing ->
            Cmd.none

        Just tree ->
            tree.data
                |> CreateNewConstant
                |> AjaxSend
                |> Ajax
                |> Utils.msgToCmd
    )


expandSelected : Model -> Model
expandSelected m =
    { m
        | constantsCreator =
            DC.expandSelected
                m.constants.items
                m.constantsCreator
    }


rewordSelected : Model -> Model
rewordSelected m =
    { m
        | constantsCreator =
            DC.rewordSelected
                m.constants.items
                m.constantsCreator
    }


deleteSelected : Model -> Model
deleteSelected m =
    { m
        | constantsCreator =
            DC.deleteSelected
                m.constants.items
                m.constantsCreator
    }


canExpandSelected : Model -> Bool
canExpandSelected m =
    let
        txt =
            String.trim m.constantsCreator.expandText
    in
    if String.isEmpty txt then
        False

    else if String.startsWith "$" txt then
        False

    else
        case findSelectedCreatorVal m of
            Nothing ->
                False

            Just val ->
                case val of
                    CD.CRecord attrs ->
                        not <| Dict.member txt attrs

                    _ ->
                        False


canRewordSelected : Model -> Bool
canRewordSelected m =
    let
        txt =
            String.trim m.constantsCreator.rewordText
    in
    if String.isEmpty txt then
        False

    else if String.startsWith "$" txt then
        False

    else
        case findPrevSelectedCreatorVal m of
            Nothing ->
                False

            Just val ->
                case val of
                    CD.CRecord attrs ->
                        not <| Dict.member txt attrs

                    _ ->
                        False


creatorExpandTextInfoContent : Model -> Html Msg
creatorExpandTextInfoContent m =
    case findSelectedCreatorVal m of
        Just (CD.CList _) ->
            text "Select type of list content."

        _ ->
            DC.expandInfoView
                m.layout.colors
                (ConstantsMsg << CT.DataCreatorUpdated)
                m.constantsCreator


creatorRewordInfoContent : Model -> Html Msg
creatorRewordInfoContent m =
    DC.rewordInfoView
        m.layout.colors
        (ConstantsMsg << CT.DataCreatorUpdated)
        m.constantsCreator


creatorExpandTextInfoTitle : Model -> Html Msg
creatorExpandTextInfoTitle m =
    text "Expand"


creatorMainInfoContent : Model -> Html Msg
creatorMainInfoContent m =
    case m.constantsCreator.tree of
        Nothing ->
            text ""

        Just tree ->
            case tree.selection of
                Nothing ->
                    text ""

                Just selection ->
                    case CD.find m.constants.items selection tree.data of
                        Nothing ->
                            text ""

                        Just a ->
                            text <| "Selected type: " ++ CD.toString a


getRewordText : Model -> String
getRewordText m =
    case m.constantsCreator.tree of
        Nothing ->
            ""

        Just tree ->
            case EList.last (tree.selection |> Maybe.withDefault []) of
                Nothing ->
                    ""

                Just lastStep ->
                    case lastStep of
                        TT.StepKey key _ ->
                            key

                        _ ->
                            ""


creator : Model -> Html Msg
creator m =
    let
        o =
            DC.defaultOpts
    in
    DC.view
        { colors = m.layout.colors
        , model = m.constantsCreator
        , msg = ConstantsMsg << CT.DataCreatorUpdated
        }
        { o
            | nameTags = Result.withDefault Dict.empty m.nameTags
            , constants = m.constants.items
        }
