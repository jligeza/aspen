module Constants.Types exposing (Model, Msg(..))

import DataCreator.Types
import DbTypes exposing (Constant)
import GenericTypes exposing (BaseType)
import Grid.Types


type Msg
    = Discard
    | DataCreatorUpdated DataCreator.Types.Msg
    | Expand
    | FocusConstant Constant
    | SelectItem Grid.Types.Coord
    | SelectType BaseType


type alias Model =
    DataCreator.Types.Model
