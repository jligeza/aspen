module FormUtils exposing (formatFloatString, handleIntInput)


maxSafeNum =
    2 ^ 53 - 1


minSafeNum =
    -2 ^ 53


handleIntInput : Maybe Int -> String -> Maybe Int
handleIntInput oldNum str =
    let
        result =
            case str of
                "" ->
                    Nothing

                "-" ->
                    Nothing

                _ ->
                    if String.startsWith "-" str then
                        let
                            hypenCount =
                                String.foldl
                                    (\a acc ->
                                        if a == '-' then
                                            acc + 1

                                        else
                                            acc
                                    )
                                    0
                                    str
                        in
                        (if hypenCount > 1 then
                            String.replace "-" "" str

                         else
                            str
                        )
                            |> String.toInt

                    else if String.contains "-" str then
                        case
                            str
                                |> String.replace "-" ""
                                |> String.cons '-'
                                |> String.toInt
                        of
                            Nothing ->
                                oldNum

                            Just a ->
                                Just a

                    else
                        case String.toInt str of
                            Nothing ->
                                oldNum

                            Just a ->
                                Just a
    in
    case result of
        Nothing ->
            Nothing

        Just num ->
            if num <= minSafeNum then
                oldNum

            else if num >= maxSafeNum then
                oldNum

            else
                result


formatFloatString : String -> String
formatFloatString =
    String.foldl
        (\a acc ->
            if a == '.' then
                if acc.dot then
                    acc

                else
                    { acc | dot = True, txt = acc.txt ++ "." }

            else if a == '-' then
                { acc | hypen = acc.hypen + 1 }

            else if Char.isDigit a then
                { acc | txt = acc.txt ++ String.fromChar a }

            else
                acc
        )
        { hypen = 0
        , dot = False
        , txt = ""
        }
        >> (\a ->
                if a.hypen == 1 then
                    { a | txt = "-" ++ a.txt }

                else
                    a
           )
        >> .txt
