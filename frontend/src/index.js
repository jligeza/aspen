import * as ElmDebugger from 'elm-debug-transformer'

import('./Main.elm')
  .then(({ Elm }) => {
    ElmDebugger.register()

    let app = Elm.Main.init()

    let prevFocused = document.body
    setInterval(() => {
      if (prevFocused !== document.activeElement) {
        app.ports.focused.send(document.activeElement !== document.body)
      }

      prevFocused = document.activeElement
    }, 100)

    app.ports.scrollIntoViewById.subscribe(id => {
      setTimeout(() => {
        let el = document.getElementById(id)

        el && el.scrollIntoView && el.scrollIntoView({ block: 'center' })
      })
    })

    app.ports.blur.subscribe(() => {
      let el = document.activeElement

      el && el.blur()
    })

    app.ports.focus.subscribe(query => {
      setTimeout(() => {
        let el = document.querySelector(query)

        let t = el && el.tagName

        if (el && (t === 'INPUT' || t === 'TEXTAREA' || t === 'SELECT')) {
          el.focus()
        } else {
          let el2 = document.querySelector(`${query} input,${query} textarea,${query} select`)
          el2 && el2.focus()
        }
      }, 10)
    })

    window.addEventListener('keydown', e => {
      // Prevent default scrolling with space and arrow keys.
      if ([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        if (!document.activeElement || document.activeElement === document.body) {
          e.preventDefault()
        }
      }
    }, false)
  })
