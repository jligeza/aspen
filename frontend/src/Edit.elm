module Edit exposing (edit)

import Types exposing (..)


edit : Model -> ( Model, Cmd Msg )
edit m =
    ( m, Cmd.none )
