module Link.Editor exposing
    ( addJoint
    , addJoint_
    , canExpandSelected
    , canExpandSelected_
    , delete
    , delete_
    , expand
    , expandByType
    , expandByType_
    , expandWithText
    , expandWithText_
    , expand_
    , getNonRefConstValues
    , getNonRefConstValues_
    , getSelectedData
    , getSelectedData_
    , getSelectedTree_
    , init
    , replaceSelectedTreeDataByType
    , replaceSelectedTreeDataByType_
    , selectPath
    , selectType
    , selectType_
    , updateDraftJoints
    , updateDraftJoints_
    , updateLink
    , updateSelectedTreeData
    , updateSelectedTreeData_
    , updateTree
    , view
    )

import CommonData.Main exposing (..)
import CommonData.Tree
import CommonData.TreeTypes
import CommonData.Utils as CDU
import DbTypes exposing (..)
import Definitions
import Dict
import GenericTypes exposing (BaseType(..), KeyVal(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Link.Types exposing (..)
import List.Extra as EList
import Maybe exposing (..)
import Setters
import Tree.Types as TT
import Types exposing (..)


treeDefaultOpts =
    CommonData.Tree.defaultOpts


init : Link -> Editor
init link =
    { link = link
    , selectedTreeIdx = 0
    , expandText = ""
    , trees =
        List.map
            (\joint ->
                { data = jointToData joint
                , selection = Just []
                , schema = Just linkSchema
                }
            )
            link.joints
    }


setTrees fn m =
    { m | trees = fn m.trees }


setTree idx fn m =
    setTrees (EList.updateAt idx fn) m


constValuePath : TT.Path
constValuePath =
    [ TT.StepKey "constant" Val, TT.StepKey "value" Val ]


getConstValue : TreeModel -> Maybe CommonData
getConstValue tree =
    find Dict.empty constValuePath tree.data


setConstantValue : (CommonData -> CommonData) -> TreeModel -> TreeModel
setConstantValue updater tree =
    { tree | data = updateByPath constValuePath updater tree.data }


getNonRefConstValues : Editor -> List ( Int, CommonData )
getNonRefConstValues editor =
    EList.indexedFoldl
        (\idx tree acc ->
            case getConstValue tree of
                Nothing ->
                    acc

                Just val ->
                    case val of
                        CRef _ ->
                            acc

                        a ->
                            ( idx, a ) :: acc
        )
        []
        editor.trees


getNonRefConstValues_ : Model -> List ( Int, CommonData )
getNonRefConstValues_ m =
    m.linkEditor
        |> Maybe.map getNonRefConstValues
        |> withDefault []


updateLink : Editor -> Link
updateLink editor =
    let
        link =
            editor.link
    in
    { link
        | joints =
            List.foldl
                (\tree acc ->
                    case commonDataToJoint tree.data of
                        Nothing ->
                            acc

                        Just a ->
                            a :: acc
                )
                []
                editor.trees
    }


updateDraftJoints : List ( Int, CommonData ) -> Editor -> Editor
updateDraftJoints joints editor =
    List.foldl
        (\( idx, val ) -> setTree idx (setConstantValue (always val)))
        editor
        joints


updateDraftJoints_ : List ( Int, CommonData ) -> Model -> Model
updateDraftJoints_ items =
    Setters.setLinkEditor (Maybe.map <| updateDraftJoints items)


commonDataPathToPath : CommonData -> Maybe TT.Path
commonDataPathToPath val =
    case val of
        CUnion (Just ( key, _ )) ->
            Just [ TT.StepKey key Val ]

        _ ->
            Nothing


commonDataToJoint : CommonData -> Maybe Joint
commonDataToJoint val =
    case val of
        CUnion (Just ( "constant", Just (CRecord attrs) )) ->
            case ( Dict.get "to" attrs, Dict.get "value" attrs ) of
                ( Just pathTo, Just (CRef (Just ref)) ) ->
                    commonDataPathToPath pathTo
                        |> Maybe.map
                            (\path ->
                                ConstantJoint
                                    ref
                                    path
                            )

                _ ->
                    Nothing

        CUnion (Just ( "rewire", Just (CRecord attrs) )) ->
            case ( Dict.get "to" attrs, Dict.get "from" attrs ) of
                ( Just pathTo, Just pathFrom ) ->
                    case ( commonDataPathToPath pathTo, commonDataPathToPath pathFrom ) of
                        ( Just pathTo_, Just pathFrom_ ) ->
                            RewireJoint
                                pathFrom_
                                pathTo_
                                |> Just

                        _ ->
                            Nothing

                _ ->
                    Nothing

        _ ->
            Nothing


setSelectedTree : (TreeModel -> TreeModel) -> Editor -> Editor
setSelectedTree fn editor =
    setTree editor.selectedTreeIdx fn editor


canExpandSelected : Db a -> Editor -> Bool
canExpandSelected db editor =
    getSelectedData db editor
        |> Maybe.map (CDU.isExpandableWithKey editor.expandText)
        |> withDefault False


canExpandSelected_ : Model -> Bool
canExpandSelected_ m =
    m.linkEditor
        |> Maybe.map (canExpandSelected m.constants.items)
        |> withDefault False


updateTree : Model -> Int -> CommonData.TreeTypes.TreeMsg -> Model
updateTree m idx msg =
    m
        |> Setters.setLinkEditorTree
            idx
            (\tree -> CommonData.Tree.update tree msg)
        |> Setters.setLinkEditor
            (Maybe.map (\editor -> { editor | selectedTreeIdx = idx }))


selectType : Db a -> BaseType -> Editor -> Editor
selectType db baseType editor =
    case getSelectedData db editor of
        Just (CList []) ->
            case CDU.baseTypeToCommonData baseType of
                Nothing ->
                    editor

                Just val ->
                    updateSelectedTreeData
                        (\val_ ->
                            case val_ of
                                CList _ ->
                                    CList [ val ]

                                _ ->
                                    val_
                        )
                        editor

        _ ->
            replaceSelectedTreeDataByType baseType editor


selectType_ : BaseType -> Model -> Model
selectType_ baseType m =
    Setters.setLinkEditor (Maybe.map (selectType m.constants.items baseType)) m


addJoint : Editor -> Editor
addJoint editor =
    setTrees
        ((::)
            { data = CUnion Nothing
            , selection = Just []
            , schema = Just linkSchema
            }
        )
        editor


replaceSelectedTreeDataByType : BaseType -> Editor -> Editor
replaceSelectedTreeDataByType baseType editor =
    case CDU.baseTypeToCommonData baseType of
        Nothing ->
            editor

        Just val ->
            updateSelectedTreeData (always val) editor


replaceSelectedTreeDataByType_ : BaseType -> Model -> Model
replaceSelectedTreeDataByType_ baseType m =
    Setters.setLinkEditor (Maybe.map (replaceSelectedTreeDataByType baseType)) m


updateSelectedTreeData : (CommonData -> CommonData) -> Editor -> Editor
updateSelectedTreeData updater editor =
    setSelectedTree
        (\tree ->
            case tree.selection of
                Nothing ->
                    tree

                Just selection ->
                    { tree | data = updateByPath selection updater tree.data }
        )
        editor


updateSelectedTreeData_ : (CommonData -> CommonData) -> Model -> Model
updateSelectedTreeData_ updater m =
    Setters.setLinkEditor (Maybe.map (updateSelectedTreeData updater)) m


getSelectedTree : Editor -> Maybe TreeModel
getSelectedTree editor =
    EList.getAt editor.selectedTreeIdx editor.trees


getSelectedData : Db a -> Editor -> Maybe CommonData
getSelectedData db =
    getSelectedTree
        >> andThen
            (\tree ->
                case tree.selection of
                    Nothing ->
                        Nothing

                    Just selection ->
                        find db selection tree.data
            )


getSelectedData_ : Model -> Maybe CommonData
getSelectedData_ m =
    m.linkEditor
        |> andThen (getSelectedData m.constants.items)


getSelectedTree_ : Model -> Maybe TreeModel
getSelectedTree_ m =
    m.linkEditor
        |> andThen getSelectedTree


deleteSelectedJoint : Editor -> Editor
deleteSelectedJoint editor =
    getSelectedTree editor
        |> Maybe.map
            (\tree ->
                let
                    newTrees =
                        EList.removeAt editor.selectedTreeIdx editor.trees
                in
                { editor
                    | trees = newTrees
                    , selectedTreeIdx =
                        if EList.getAt editor.selectedTreeIdx newTrees == Nothing then
                            if editor.selectedTreeIdx < 1 then
                                editor.selectedTreeIdx

                            else
                                editor.selectedTreeIdx - 1

                        else
                            editor.selectedTreeIdx
                }
            )
        |> withDefault editor


delete : Db a -> Editor -> Editor
delete consts editor =
    getSelectedTree editor
        |> Maybe.map
            (\tree ->
                case tree.selection of
                    Just [] ->
                        deleteSelectedJoint editor

                    Nothing ->
                        deleteSelectedJoint editor

                    Just path ->
                        let
                            deletionResult =
                                CommonData.Main.delete consts path tree.data
                        in
                        case deletionResult.val of
                            Nothing ->
                                deleteSelectedJoint editor

                            Just val ->
                                setTree
                                    editor.selectedTreeIdx
                                    (\a -> { a | data = val, selection = Just deletionResult.path })
                                    editor
            )
        |> withDefault editor


delete_ : Model -> Model
delete_ m =
    Setters.setLinkEditor (Maybe.map (delete m.constants.items)) m


addJoint_ : Model -> Model
addJoint_ m =
    Setters.setLinkEditor (Maybe.map addJoint) m


expand_ : Model -> Model
expand_ m =
    Setters.setLinkEditor (Maybe.map expand) m


expandByType : BaseType -> Editor -> Editor
expandByType baseType editor =
    updateSelectedTreeData
        (\val ->
            case val of
                CUnion (Just ( key, _ )) ->
                    case CDU.baseTypeToCommonData baseType of
                        Nothing ->
                            val

                        Just content ->
                            CUnion <| Just ( key, Just content )

                _ ->
                    val
        )
        editor


expandByType_ : BaseType -> Model -> Model
expandByType_ baseType m =
    Setters.setLinkEditor (Maybe.map <| expandByType baseType) m


expand : Editor -> Editor
expand editor =
    EList.getAt editor.selectedTreeIdx editor.trees
        |> andThen .selection
        |> Maybe.map
            (\selection ->
                setTree
                    editor.selectedTreeIdx
                    (\a -> { a | data = CDU.expand selection linkSchema a.data })
                    editor
            )
        |> Maybe.withDefault editor


expandWithText : Editor -> Editor
expandWithText editor =
    setSelectedTree
        (\tree ->
            case tree.selection of
                Nothing ->
                    tree

                Just selection ->
                    { tree
                        | data =
                            updateByPath
                                selection
                                (CDU.addKey <| String.trim editor.expandText)
                                tree.data
                    }
        )
        { editor | expandText = "" }


expandWithText_ : Model -> Model
expandWithText_ m =
    Setters.setLinkEditor (Maybe.map expandWithText) m


dataPath : Schema
dataPath =
    SUnion
        [ ShortUnionMember "default"
        , ShortUnionMember "result"
        , ShortUnionMember "return"
        ]


linkSchema : Schema
linkSchema =
    SUnion <|
        [ Dict.fromList
            [ ( "value", SString )
            , ( "to", dataPath )
            ]
            |> SRecord
            |> Just
            |> FullUnionMemberSchema "constant"
            |> FullUnionMember
        , Dict.fromList
            [ ( "from", dataPath )
            , ( "to", dataPath )
            ]
            |> SRecord
            |> Just
            |> FullUnionMemberSchema "rewire"
            |> FullUnionMember
        ]


selectPath : Model -> TT.Path -> Model
selectPath m path =
    Setters.setLinkEditor
        (Maybe.map
            (\editor ->
                setTree editor.selectedTreeIdx (\a -> { a | selection = Just path }) editor
            )
        )
        m


jointPathToUnion : TT.Path -> CommonData
jointPathToUnion path =
    case path of
        [ TT.StepKey "return" _ ] ->
            CUnion (Just ( "return", Nothing ))

        [ TT.StepKey "default" _ ] ->
            CUnion (Just ( "default", Nothing ))

        [ TT.StepKey "result" _ ] ->
            CUnion (Just ( "result", Nothing ))

        _ ->
            CUnion Nothing


jointToData : Joint -> CommonData
jointToData joint =
    case joint of
        ConstantJoint ref pathTo ->
            CUnion <|
                Just
                    ( "constant"
                    , Just <|
                        CRecord <|
                            Dict.fromList
                                [ ( "value"
                                  , { id = ref.id
                                    , version = ref.version
                                    }
                                        |> Just
                                        |> CRef
                                  )
                                , ( "to", jointPathToUnion pathTo )
                                ]
                    )

        RewireJoint pathFrom pathTo ->
            CUnion <|
                Just
                    ( "rewire"
                    , Just <|
                        CRecord <|
                            Dict.fromList
                                [ ( "from", jointPathToUnion pathFrom )
                                , ( "to", jointPathToUnion pathTo )
                                ]
                    )


view : Model -> Editor -> Html Msg
view m editor =
    if List.isEmpty editor.trees then
        span [] [ text "Default joint" ]

    else
        div
            [ style "display" "flex" ]
            (List.indexedMap
                (\idx tree ->
                    div
                        ([ style "border" "2px solid"
                         , style "margin" "3px"
                         ]
                            |> (if idx /= editor.selectedTreeIdx then
                                    (::) (style "color" "transparent")

                                else
                                    -- toggle to identity for visible border
                                    (::) (style "color" "transparent")
                               )
                        )
                        [ CommonData.Tree.tree
                            m.layout.colors
                            tree
                            { treeDefaultOpts
                                | editable = True
                                , updateMsg = Just <| LinkEditorTreeUpdated idx
                                , constants = m.constants.items
                                , selectionId =
                                    Definitions.selectionId
                                        ++ String.fromInt idx
                                        |> Just
                            }
                        ]
                )
                editor.trees
            )
