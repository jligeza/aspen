module Link.Utils exposing
    ( getSelectedEditorTree
    , setSelectedEditorTree
    , setSelectedEditorTreeSelection
    )

import Link.Types exposing (..)
import List.Extra as EList
import Maybe exposing (..)
import Setters
import Tree.Types exposing (Path)
import Types exposing (..)


getSelectedEditorTree : Model -> Maybe TreeModel
getSelectedEditorTree m =
    m.linkEditor
        |> andThen (\editor -> EList.getAt editor.selectedTreeIdx editor.trees)


setSelectedEditorTreeSelection : Maybe Path -> Model -> Model
setSelectedEditorTreeSelection path m =
    setSelectedEditorTree (\t -> { t | selection = path }) m


setSelectedEditorTree : (TreeModel -> TreeModel) -> Model -> Model
setSelectedEditorTree fn m =
    m.linkEditor
        |> map
            (\editor ->
                Setters.setLinkEditorTree
                    editor.selectedTreeIdx
                    fn
                    m
            )
        |> withDefault m
