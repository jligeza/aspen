module Link.Types exposing (Editor, TreeModel)

import CommonData.Main as CD
import DbTypes as DT
import Tree.Types as TT


type alias TreeModel =
    { data : CD.CommonData
    , selection : Maybe TT.Path
    , schema : Maybe DT.Schema
    }


type alias Editor =
    { link : DT.Link
    , selectedTreeIdx : Int
    , trees : List TreeModel
    , expandText : String
    }
