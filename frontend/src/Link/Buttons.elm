module Link.Buttons exposing (editLinkBaseTypesButtons, editLinkButtons)

import ActionButtons.Definitions exposing (..)
import CommonData.Main as CD
import CommonData.Utils as CDU
import DbTypes exposing (Link)
import Definitions
import GenericTypes exposing (..)
import Keyboard exposing (Key(..))
import Link.Editor exposing (getSelectedTree_)
import List.Extra as EList
import Maybe exposing (..)
import Tree.Types as TT
import Types exposing (..)


isNonSchemaData : Model -> Bool
isNonSchemaData m =
    getSelectedTree_ m
        |> andThen .selection
        |> withDefault []
        |> isNonSchemaData_


isNonSchemaData_ : TT.Path -> Bool
isNonSchemaData_ =
    List.take 2
        >> (\steps ->
                steps
                    == [ TT.StepKey "constant" Val, TT.StepKey "value" Val ]
                    || steps
                    == [ TT.StepKey "constant" Val, TT.StepKey "value" Key ]
           )


isDeletable : Model -> Bool
isDeletable m =
    getSelectedTree_ m
        |> andThen .selection
        |> withDefault []
        |> (\selection ->
                List.length selection
                    > 2
                    && List.take 2 selection
                    == [ TT.StepKey "constant" Val, TT.StepKey "value" Val ]
           )


editLinkButtons : Model -> List ActionButton
editLinkButtons m =
    let
        setBtn key =
            case key of
                Character "q" ->
                    ActionButton
                        { key = key, text = "save" }
                        { defaultOptions
                            | action = Just <| Edit EditSave
                            , isDisabled =
                                case m.linkEditor of
                                    Nothing ->
                                        True

                                    Just editor ->
                                        List.member
                                            ARNewConstant
                                            m.loading
                                            || EList.find
                                                (.data >> CDU.getEmptyPaths >> List.isEmpty >> not)
                                                editor.trees
                                            /= Nothing
                        }

                Character "a" ->
                    ActionButton
                        { key = key, text = "expand" }
                        { defaultOptions
                            | action = Just <| Edit EditExpand
                            , isDisabled =
                                getSelectedTree_ m
                                    |> Maybe.map
                                        (\tree ->
                                            case tree.selection of
                                                Nothing ->
                                                    True

                                                Just [] ->
                                                    CDU.isExpandable tree.data |> not

                                                Just selection ->
                                                    isNonSchemaData_ selection
                                                        && CDU.isExpandable_
                                                            m.constants.items
                                                            selection
                                                            tree.data
                                                        |> not
                                        )
                                    |> withDefault True
                        }

                Character "s" ->
                    let
                        o =
                            CDU.focusSelectionButtonDefaults
                    in
                    case m.linkEditor of
                        Nothing ->
                            CDU.focusSelectionButton m key o

                        Just editor ->
                            case EList.getAt editor.selectedTreeIdx editor.trees of
                                Nothing ->
                                    CDU.focusSelectionButton m key o

                                Just tree ->
                                    CDU.focusSelectionButton
                                        m
                                        key
                                        { o
                                            | path = tree.selection
                                            , val = Just tree.data
                                            , focusId =
                                                Definitions.selectionId
                                                    ++ String.fromInt editor.selectedTreeIdx
                                                    |> Just
                                        }

                Character "z" ->
                    ActionButton
                        { key = key, text = "replace" }
                        { defaultOptions
                            | action = Just <| Edit EditReplace
                            , isDisabled = not <| isNonSchemaData m
                        }

                Character "x" ->
                    ActionButton
                        { key = key, text = "add joint" }
                        { defaultOptions
                            | action = Just <| Edit EditAddJoint
                            , isDisabled =
                                case m.linkEditor of
                                    Nothing ->
                                        True

                                    Just editor ->
                                        List.length editor.trees > 0
                        }

                Character "f" ->
                    ActionButton
                        { key = key, text = "delete" }
                        { defaultOptions
                            | action = Just <| Edit EditDelete
                            , isDisabled =
                                if isDeletable m then
                                    False

                                else
                                    case m.linkEditor of
                                        Nothing ->
                                            True

                                        Just editor ->
                                            getSelectedTree_ m
                                                |> andThen
                                                    (\tree ->
                                                        case tree.selection of
                                                            Nothing ->
                                                                Just True

                                                            Just selection ->
                                                                case EList.last selection of
                                                                    Nothing ->
                                                                        Just False

                                                                    Just lastStep ->
                                                                        case lastStep of
                                                                            TT.StepIdx _ ->
                                                                                Just False

                                                                            TT.StepKey _ _ ->
                                                                                Just True
                                                    )
                                                |> withDefault True
                        }

                Character "r" ->
                    ActionButton
                        { key = key, text = "discard" }
                        { defaultOptions
                            | action = Just <| Edit EditDiscard
                        }

                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys


editLinkBaseTypesButtons : Model -> List ActionButton
editLinkBaseTypesButtons m =
    let
        setBtn key =
            case key of
                Character "q" ->
                    ActionButton
                        { key = key, text = "record" }
                        { defaultOptions | action = Just <| Edit <| EditSelectType BTRecord }

                Character "a" ->
                    ActionButton
                        { key = key, text = "list" }
                        { defaultOptions | action = Just <| Edit <| EditSelectType BTList }

                Character "z" ->
                    ActionButton
                        { key = key, text = "ref" }
                        { defaultOptions | action = Just <| Edit <| EditSelectType BTRef }

                Character "w" ->
                    ActionButton
                        { key = key, text = "union" }
                        { defaultOptions | action = Just <| Edit <| EditSelectType BTUnion }

                Character "s" ->
                    ActionButton
                        { key = key, text = "string" }
                        { defaultOptions | action = Just <| Edit <| EditSelectType BTString }

                Character "e" ->
                    ActionButton
                        { key = key, text = "int" }
                        { defaultOptions | action = Just <| Edit <| EditSelectType BTInt }

                Character "d" ->
                    ActionButton
                        { key = key, text = "float" }
                        { defaultOptions | action = Just <| Edit <| EditSelectType BTFloat }

                Character "c" ->
                    ActionButton
                        { key = key, text = "bool" }
                        { defaultOptions | action = Just <| Edit <| EditSelectType BTBool }

                Character "r" ->
                    ActionButton
                        { key = key, text = "json" }
                        { defaultOptions | action = Just <| Edit <| EditSelectType BTJson }

                Character "v" ->
                    ActionButton
                        { key = key, text = "return" }
                        { defaultOptions | action = Just Return }

                _ ->
                    ActionButton
                        { key = key, text = "" }
                        { defaultOptions | action = Just NoAction }
    in
    List.map setBtn availableActionKeys
