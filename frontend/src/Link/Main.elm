module Link.Main exposing
    ( canCreate
    , connectLink
    , create
    , save
    , updateUnsavedLink
    , updateUnsavedLinkTarget
    )

import CommonData.Utils as CDU
import DbTypes exposing (Link)
import Dict
import List.Extra as EList
import Maybe exposing (..)
import Setters
import Types exposing (..)
import Utils


findLinkFrom : Model -> String -> Maybe String
findLinkFrom m from =
    case Dict.get from m.links.byFrom of
        Just links ->
            links
                |> List.head
                |> andThen (.id >> Just)

        Nothing ->
            case Dict.get from m.unsavedLinks.byFrom of
                Just links_ ->
                    links_
                        |> List.head
                        |> andThen (.id >> Just)

                Nothing ->
                    Nothing


canCreate : Model -> Bool
canCreate m =
    case m.boardSelection of
        Nothing ->
            False

        Just ref ->
            case
                Dict.get ref.id m.nodes
                    |> andThen (Dict.get ref.version)
            of
                Just node ->
                    case findLinkFrom m node.id of
                        Nothing ->
                            True

                        Just _ ->
                            False

                Nothing ->
                    case
                        EList.find
                            (.base >> Utils.idVerMatching ref)
                            m.eventGenerators.items
                    of
                        Nothing ->
                            False

                        Just evgn ->
                            case findLinkFrom m evgn.base.id of
                                Nothing ->
                                    True

                                Just _ ->
                                    False


save : Model -> Link -> Model
save m link =
    m
        |> Setters.setLinks
            (\a ->
                { a
                    | items = Dict.insert link.id link a.items
                    , byFrom =
                        Dict.update
                            link.from
                            (\b ->
                                case b of
                                    Nothing ->
                                        Just [ link ]

                                    Just links ->
                                        Just <| link :: links
                            )
                            a.byFrom
                    , byTo =
                        case link.to of
                            Nothing ->
                                a.byTo

                            Just to ->
                                Dict.insert to link a.byTo
                }
            )
        |> (case Dict.get link.from m.unsavedLinks.byFrom of
                Nothing ->
                    identity

                Just [] ->
                    identity

                Just (unsavedLink :: _) ->
                    -- TODO handle multiple links byFrom
                    Setters.setUnsavedLinks
                        (\a ->
                            { a
                                | items = Dict.remove unsavedLink.id a.items
                                , byFrom =
                                    Dict.update
                                        unsavedLink.from
                                        (\b ->
                                            case b of
                                                Nothing ->
                                                    Nothing

                                                Just links ->
                                                    Just <| EList.remove unsavedLink links
                                        )
                                        a.byFrom
                                , byTo =
                                    case unsavedLink.to of
                                        Nothing ->
                                            a.byTo

                                        Just to ->
                                            Dict.remove to a.byTo
                            }
                        )
           )


updateUnsavedLink : Link -> Model -> Model
updateUnsavedLink link m =
    Setters.setUnsavedLinks
        (\a ->
            { a
                | items = Dict.insert link.id link a.items
                , byFrom =
                    Dict.update
                        link.from
                        (\b ->
                            case b of
                                Nothing ->
                                    Just [ link ]

                                Just links ->
                                    Just <| EList.uniqueBy .id <| link :: links
                        )
                        a.byFrom
                , byTo =
                    case link.to of
                        Nothing ->
                            a.byTo

                        Just to ->
                            Dict.insert to link a.byTo
            }
        )
        m


create : Model -> Model
create m =
    case m.boardSelection of
        Nothing ->
            m

        Just ref ->
            case findLinkFrom m ref.id of
                Just _ ->
                    m

                Nothing ->
                    let
                        link =
                            { id = String.fromInt m.counter
                            , version = 1
                            , error = Nothing
                            , from = ref.id
                            , to = Nothing
                            , joints = []
                            , createdAt = ""
                            , deletedAt = Nothing
                            }
                    in
                    Setters.setUnsavedLinks
                        (\a ->
                            { a
                                | items = Dict.insert link.id link a.items
                                , byFrom =
                                    Dict.update
                                        ref.id
                                        (\maybeList ->
                                            case maybeList of
                                                Just list ->
                                                    Just <| link :: list

                                                Nothing ->
                                                    Just [ link ]
                                        )
                                        a.byFrom
                            }
                        )
                        { m
                            | counter = m.counter + 1
                            , boardSelection =
                                Just
                                    { id = String.fromInt m.counter
                                    , version = 1
                                    }
                        }


updateUnsavedLinkTarget : String -> Maybe String -> Model -> Model
updateUnsavedLinkTarget oldTargetId maybeNewTargetId m =
    case Dict.get oldTargetId m.unsavedLinks.byTo of
        Nothing ->
            m

        Just uLink ->
            let
                updatedLink =
                    { uLink | to = maybeNewTargetId }
            in
            Setters.setUnsavedLinks
                (\u ->
                    { u
                        | items = Dict.insert uLink.id updatedLink u.items
                        , byTo =
                            u.byTo
                                |> Dict.remove oldTargetId
                                |> (case maybeNewTargetId of
                                        Nothing ->
                                            identity

                                        Just newId ->
                                            Dict.insert newId updatedLink
                                   )
                        , byFrom =
                            Dict.map
                                (\k uLinks ->
                                    if List.member uLink uLinks then
                                        List.map
                                            (\a ->
                                                if a == uLink then
                                                    updatedLink

                                                else
                                                    a
                                            )
                                            uLinks

                                    else
                                        uLinks
                                )
                                u.byFrom
                    }
                )
                m


connectLink : Model -> String -> String -> Model
connectLink m targetId linkId =
    case Dict.get linkId m.unsavedLinks.items of
        Just uLink ->
            let
                updatedLink =
                    { uLink | to = Just targetId }
            in
            Setters.setUnsavedLinks
                (\u ->
                    { u
                        | byTo =
                            Dict.insert targetId updatedLink u.byTo
                        , items =
                            Dict.update
                                uLink.id
                                (\a -> Just updatedLink)
                                u.items
                        , byFrom =
                            Dict.map
                                (\k links ->
                                    if List.member uLink links then
                                        List.map
                                            (\a ->
                                                if a.id == uLink.id then
                                                    updatedLink

                                                else
                                                    a
                                            )
                                            links

                                    else
                                        links
                                )
                                u.byFrom
                    }
                )
                m

        Nothing ->
            case Dict.get linkId m.links.items of
                Nothing ->
                    m

                Just link ->
                    Setters.setLinks
                        (\l ->
                            { l
                                | byTo =
                                    Dict.insert targetId link l.byTo
                            }
                        )
                        m
