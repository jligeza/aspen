import * as R from './resources'

export function stringToResource(str: string): R.Resources | null {
  if (str in R.resources) {
    return str as R.Resources
  } else {
    return null
  }
}
