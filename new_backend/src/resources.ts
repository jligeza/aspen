export let attributes = [
  'id',
  'foreignId',
  'version',
  'date',
  'type',
  'path',
  'name',
  'joints',
  'value',
] as const

export type AttributeName = typeof attributes[number]

export let attributesOpts = {
  nullable: false,
  protected: false,
}

export type AttributeOpts = typeof attributesOpts

export let baseAttributes: Attributes = {
  id: { $: 'id', protected: true },
  version: { $: 'version', protected: true },
  createdAt: { $: 'date', protected: true },
  deletedAt: { $: 'date', nullable: true, protected: true },
}

export let resources = [
  'type',
  'eventGenerator',
  'nameTag',
  'link',
  'node',
  'const',
  'template',
  'templateWithConfig',
] as const

export type Resources = typeof resources[number]

export interface Resource {
  ownAttributes: Attributes
  readOnly?: boolean
}

export type Attributes = { [k: string]: Attribute }

export type Attribute = AttributeName | AttributeExt | ForeignIdExt | EnumExt

export type EnumExt = {
  $: 'enum'
  vals: string[]
} & {
  [K in keyof AttributeOpts]?: AttributeOpts[K]
}

export type ForeignIdExt = {
  $: 'foreignId'
  for?: Resources[]
  dependsOn?: string
} & {
  [K in keyof AttributeOpts]?: AttributeOpts[K]
}

export type AttributeExt = {
  $: AttributeName
} & {
  [K in keyof AttributeOpts]?: AttributeOpts[K]
}

export type ResourceMap = { [a in Resources]: Resource }

export const resourcesMap: ResourceMap = {
  templateWithConfig: {
    readOnly: true,
    ownAttributes: {
      name: 'name',
      schemaId: { $: 'foreignId', for: ['type'] },
      schemaVersion: 'version',
      configId: { $: 'foreignId', for: ['const'], nullable: true },
      configVersion: 'version',
    },
  },
  template: {
    readOnly: true,
    ownAttributes: {
      name: 'name',
      schemaId: { $: 'foreignId', for: ['type'] },
      schemaVersion: 'version',
    },
  },
  type: {
    ownAttributes: {
      val: 'type',
    },
  },
  eventGenerator: {
    ownAttributes: {
      configId: {
        $: 'foreignId',
        for: ['const'],
        dependsOn: 'templateId',
        nullable: true,
      },
      configVersion: 'version',
      templateId: { $: 'foreignId', for: ['templateWithConfig'] },
      templateVersion: 'version',
    },
  },
  nameTag: {
    ownAttributes: {
      targetId: {
        $: 'foreignId',
        for: ['type', 'eventGenerator', 'node', 'const'],
      },
      targetVersion: 'version',
      path: 'path',
      name: 'name',
    },
  },
  link: {
    ownAttributes: {
      from: { $: 'foreignId', for: ['link'] },
      to: { $: 'foreignId', for: ['link'], nullable: true },
      joints: 'joints',
    },
  },
  node: {
    ownAttributes: {
      targetId: { $: 'foreignId', for: ['template', 'templateWithConfig'] },
      targetVersion: 'version',
    },
  },
  const: {
    ownAttributes: {
      val: 'value',
    },
  },
}

export type ResourcesMapKeys = keyof typeof resourcesMap
