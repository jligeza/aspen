export let types = [
  {
    id: '1e232c8c-8adf-465a-bc99-d767d915b7bd',
    version: 1,
    val: {
      a: {
        $type: 'Function',
        $input: { a: 'Int', b: 'Int' },
        $output: { a: 'Int', b: 'Int' },
      },
      e: {
        f: { $type: 'List', $valsType: { a: 'Int', b: 'Int' } },
        g: [
          'A',
          {
            $type: 'UnionMember',
            $choice: 'B',
            $content: { a: 'Int', b: 'Int' },
          },
          'C',
        ],
      },
      i: 'Int',
    },
    createdAt: '2019-04-02 09:21:50.361 +00:00',
    deletedAt: null,
  },
  {
    id: 'c9ff9aa4-ffd2-46d7-a0fc-0ee1b1b89116',
    version: 1,
    val: ['Nothing', { $type: 'UnionMember', $choice: 'Just', $content: 'a' }],
    createdAt: '2019-04-02 09:21:58.492 +00:00',
    deletedAt: null,
  },
  {
    id: 'bac70445-bd29-4ad0-ade9-4289b82e02e7',
    version: 1,
    val: {
      a: 'Int',
      c: { a: { $input: 'String', $output: { $valsType: 'Int' } }, e: 'Int' },
      w: 'Json',
      wwqweqwe: ['A', 'B', 'C'],
    },
    createdAt: '2019-07-10 10:09:17.526 +00:00',
    deletedAt: null,
  },
  {
    id: '8b58cb1b-31ef-4493-9f0c-9b1cc1c9fd43',
    version: 1,
    val: [
      {
        $type: 'UnionMember',
        $choice: 'a',
        $content: { a: { $valsType: 'Float' } },
      },
      {
        $type: 'UnionMember',
        $choice: 'b',
        $content: {
          $input: { $id: 'bac70445-bd29-4ad0-ade9-4289b82e02e7', $version: 1 },
          $output: 'Json',
        },
      },
    ],
    createdAt: '2019-07-10 10:42:53.548 +00:00',
    deletedAt: null,
  },
  {
    id: '4e503d2d-b4c7-4d50-900b-1997a6f4779c',
    version: 1,
    val: { a: { $input: 'Int', $output: { w: { $valsType: 'Float' } } } },
    createdAt: '2019-07-10 10:58:40.626 +00:00',
    deletedAt: null,
  },
  {
    id: '2a276d87-2733-464b-807c-305ad26a8b3a',
    version: 1,
    val: {
      a: {
        $input: 'Int',
        $output: {
          w: {
            $valsType: {
              $id: 'c9ff9aa4-ffd2-46d7-a0fc-0ee1b1b89116',
              $version: 1,
            },
          },
        },
      },
    },
    createdAt: '2019-07-10 11:14:13.330 +00:00',
    deletedAt: null,
  },
  {
    id: 'fcfb3859-a0d1-4e7e-904e-45ea9545b42d',
    version: 1,
    val: { $id: 'c9ff9aa4-ffd2-46d7-a0fc-0ee1b1b89116', $version: 1 },
    createdAt: '2019-07-10 11:15:01.500 +00:00',
    deletedAt: null,
  },
  {
    id: '535f12ed-c549-420a-9c90-d1d19e663a29',
    version: 1,
    val: {
      $id: 'c9ff9aa4-ffd2-46d7-a0fc-0ee1b1b89116',
      $version: 1,
      $generics: { a: 'String' },
    },
    createdAt: '2019-07-10 11:22:35.864 +00:00',
    deletedAt: null,
  },
  {
    id: '900f9aab-eb42-42c2-8a21-70509c944230',
    version: 1,
    val: {
      config: {
        routes: {
          $type: 'List',
          $valsType: {
            url: 'String',
            method: ['Get', 'Post', 'Put', 'Delete'],
          },
        },
        port: 'Int',
      },
      emits: { payload: 'Json', path: 'String' },
      callback: { status: ['Success', 'Error'], payload: 'Json' },
    },
    createdAt: '2020-03-15 13:32:35.864 +00:00',
    deletedAt: null,
  },
] as const

export let nodes = [
  {
    id: 'ff88cf7c-1824-4897-b8e8-e0abdd4c3473',
    version: 1,
    targetId: '19d2ce7f-2a65-4f64-98fd-7550af6850a2',
    targetVersion: 1,
    createdAt: '2019-05-13 12:39:52.559 +00:00',
    deletedAt: null,
  },
  {
    id: '02ac1caf-dac6-48ef-beda-2ee6f9a74a9e',
    version: 1,
    targetId: '19d2ce7f-2a65-4f64-98fd-7550af6850a2',
    targetVersion: 1,
    createdAt: '2019-08-07 11:44:18.045 +00:00',
    deletedAt: null,
  },
] as const

export let nameTags = [
  {
    id: '88d611da-fa76-4e02-8e63-28651f3d3bc0',
    version: 1,
    targetId: 'c9ff9aa4-ffd2-46d7-a0fc-0ee1b1b89116',
    targetVersion: 1,
    path: [],
    name: 'Maybe',
    createdAt: '2019-04-15 07:09:43.431 +00:00',
    deletedAt: null,
  },
  {
    id: 'aaa2b8e3-9a41-49b3-9624-2f098dc9fa6b',
    version: 1,
    targetId: 'bac70445-bd29-4ad0-ade9-4289b82e02e7',
    targetVersion: 1,
    path: [],
    name: 'aaaa',
    createdAt: '2019-07-10 10:09:17.566 +00:00',
    deletedAt: null,
  },
  {
    id: 'eb46582a-a103-449e-976e-1a00ce748797',
    version: 1,
    targetId: '4e503d2d-b4c7-4d50-900b-1997a6f4779c',
    targetVersion: 1,
    path: [],
    name: 'test 2',
    createdAt: '2019-07-10 10:58:40.651 +00:00',
    deletedAt: null,
  },
  {
    id: '2fe73ccc-d9da-42f5-88e7-eaa6843cd92e',
    version: 1,
    targetId: '2a276d87-2733-464b-807c-305ad26a8b3a',
    targetVersion: 1,
    path: [],
    name: 'test 2',
    createdAt: '2019-07-10 11:14:13.362 +00:00',
    deletedAt: null,
  },
  {
    id: 'bed6c4a0-ac30-4972-bf19-5559983cdc2a',
    version: 1,
    targetId: 'fcfb3859-a0d1-4e7e-904e-45ea9545b42d',
    targetVersion: 1,
    path: [],
    name: 'test 3',
    createdAt: '2019-07-10 11:15:01.534 +00:00',
    deletedAt: null,
  },
  {
    id: '84dd502a-3fe3-4777-8615-9bc34788f4d0',
    version: 1,
    targetId: '535f12ed-c549-420a-9c90-d1d19e663a29',
    targetVersion: 1,
    path: [],
    name: 'test 4',
    createdAt: '2019-07-10 11:22:35.898 +00:00',
    deletedAt: null,
  },
] as const

export let links = [
  {
    id: '5b9ac69a-9d65-4f64-96ea-b5061c6c733f',
    version: 1,
    from: '7a0c0648-9077-4b42-9ee2-24444fda0fe4',
    to: 'ff88cf7c-1824-4897-b8e8-e0abdd4c3473',
    joints: [],
    createdAt: '2019-05-13 12:41:19.389 +00:00',
    deletedAt: null,
  },
  {
    id: '130264c7-d376-40c7-affb-b0fa5d0d53fb',
    version: 1,
    from: '98825189-319b-42c6-ae7a-7c83e404512d',
    to: '02ac1caf-dac6-48ef-beda-2ee6f9a74a9e',
    joints: [
      [
        { $id: '3b4139a9-37b2-4bce-9429-bd5caabd6180', $version: 1 },
        ['return'],
      ],
    ],
    createdAt: '2019-08-07 11:44:19.353 +00:00',
    deletedAt: null,
  },
] as const

export let eventGenerators = [
  {
    id: '98825189-319b-42c6-ae7a-7c83e404512d',
    version: 1,
    configId: '7b4139a9-37b2-4bce-9429-bd5caabd6180',
    configVersion: 1,
    templateId: '8df03633-ac56-4057-ab18-9354236c92f2',
    templateVersion: 1,
    createdAt: '2019-04-02 09:23:06.018 +00:00',
    deletedAt: null,
  },
  {
    id: '7a0c0648-9077-4b42-9ee2-24444fda0fe4',
    version: 1,
    configId: '6b4139a9-37b2-4bce-9429-bd5caabd6180',
    configVersion: 1,
    templateId: '8df03633-ac56-4057-ab18-9354236c92f2',
    templateVersion: 1,
    createdAt: '2019-04-30 05:47:30.815 +00:00',
    deletedAt: null,
  },
] as const

export let consts = [
  {
    id: 'a6037744-e383-4d24-a6ab-ee2f9f9484f7',
    version: 1,
    val: {
      status: { $type: 'UnionMember', $choice: 'Success' },
      payload: 'some payload',
    },
    createdAt: '2019-04-08 07:48:11.691 +00:00',
    deletedAt: null,
  },
  {
    id: '0042b46c-0d75-4517-b8cc-5f4a07d8482e',
    version: 1,
    val: {
      a: 'zasdas',
      b: { $type: 'UnionMember', $choice: 'Something' },
      c: [
        { w: '1sdsad', z: { $type: 'UnionMember', $choice: 'Oasdasd' } },
        { w: '123', z: { $type: 'UnionMember', $choice: 'Kjdasjda' } },
      ],
    },
    createdAt: '2019-07-02 10:39:22.622 +00:00',
    deletedAt: null,
  },
  {
    id: 'a02ac38c-26e6-4e1d-9397-273c3b7acf18',
    version: 1,
    val: {
      a: '2223',
      b: { $type: 'UnionMember', $choice: 'Asss' },
      z: [{ a: 'avc' }, { a: '21' }, { a: 'ddd' }],
    },
    createdAt: '2019-07-11 12:17:39.568 +00:00',
    deletedAt: null,
  },
  {
    id: 'f2667867-a5d6-420c-a15b-5ac7a23a73db',
    version: 1,
    val: { $type: 'UnionMember', $choice: 'Just', $content: 'Some string' },
    createdAt: '2019-07-16 05:54:48.974 +00:00',
    deletedAt: null,
  },
  {
    id: 'ac438a33-0de5-49a3-9167-b926401a35eb',
    version: 1,
    val: {
      a: { $id: '0042b46c-0d75-4517-b8cc-5f4a07d8482e', $version: 1 },
      b: 'abcd',
    },
    createdAt: '2019-07-26 08:11:33.204 +00:00',
    deletedAt: null,
  },
  {
    id: '89fc9c69-9a35-4fb9-b9ab-f4f1a6c1548d',
    version: 1,
    val: {
      payload: 'arrrr',
      status: { $type: 'UnionMember', $choice: 'Success' },
    },
    createdAt: '2019-07-29 10:26:05.570 +00:00',
    deletedAt: null,
  },
  {
    id: '3b4139a9-37b2-4bce-9429-bd5caabd6180',
    version: 1,
    val: {
      payload: 'w000t',
      status: { $type: 'UnionMember', $choice: 'Success' },
    },
    createdAt: '2019-07-29 10:32:24.846 +00:00',
    deletedAt: null,
  },
  {
    id: '6b4139a9-37b2-4bce-9429-bd5caabd6180',
    version: 1,
    val: {
      routes: [{ method: { $type: 'UnionMember', $choice: 'Get' }, url: '/' }],
      port: 6543,
    },
    createdAt: '2019-07-29 10:32:24.846 +00:00',
    deletedAt: null,
  },
  {
    id: '7b4139a9-37b2-4bce-9429-bd5caabd6180',
    version: 1,
    val: {
      routes: [{ url: '/', method: { $type: 'UnionMember', $choice: 'Get' } }],
      port: 7777,
    },
    createdAt: '2019-07-29 10:32:24.846 +00:00',
    deletedAt: null,
  },
  {
    id: '4a6ed30f-30b2-4ccc-8dfa-a81b8ec6c99b',
    version: 1,
    val: {
      payload: 'asdasdasda',
      status: { $type: 'UnionMember', $choice: 'Success' },
    },
    createdAt: '2019-07-29 10:42:19.648 +00:00',
    deletedAt: null,
  },
  {
    id: 'defbbb1d-8dee-4e62-9d85-275e6f2eee22',
    version: 1,
    val: {
      routes: [],
      port: 0,
    },
    createdAt: '2020-03-15 13:32:35.864 +00:00',
    deletedAt: null,
  },
] as const

export let templatesWithConfig = [
  {
    id: '8df03633-ac56-4057-ab18-9354236c92f2',
    version: 1,
    name: 'http',
    schemaId: '900f9aab-eb42-42c2-8a21-70509c944230',
    schemaVersion: 1,
    configId: 'defbbb1d-8dee-4e62-9d85-275e6f2eee22',
    configVersion: 1,
    createdAt: '2019-07-29 10:42:19.648 +00:00',
    deletedAt: null,
  },
] as const
