let t = [
  'int',
  'float',
  'string',
  'bool',
  'json',
  'generic',
  'union',
  'unionMember',
  'function',
  'list',
] as const

export type Path = (string | number)[]

export type TypeSchema = string | object // TODO

export let path = [] as Path

export let typeSchema = '' as TypeSchema

export let errors = [
  {
    $: 'G1',
    desc: 'missing schema',
    path,
  },
  {
    $: 'G2',
    desc: 'missing data',
    path,
  },
  {
    $: 'G3',
    desc: 'type mismatch',
    path,
    given: typeSchema,
    expected: typeSchema,
  },
  {
    $: 'G5',
    desc: 'not found in DB',
    path,
    givenId: '',
    givenVersion: 0,
  },
  {
    $: 'G6',
    desc: 'generic type mismatch',
    path,
    previously: typeSchema,
    now: typeSchema,
  },
  {
    $: 'G7',
    desc: 'no such generics',
    path,
    givenExtra: [] as string[],
    available: [] as string[],
  },
  {
    $: 'C1',
    desc: 'damaged schema - ID not found in DB',
    path,
    schemaId: '',
    schemaVersion: 0,
  },
  {
    $: 'R1',
    desc: 'missing keys in record',
    path,
    keys: [] as string[],
    allkeys: [] as string[],
  },
  {
    $: 'U1',
    desc: 'no such type in union',
    path,
    given: typeSchema,
    available: typeSchema,
  },
  {
    $: 'U2',
    desc: 'missing content',
    path,
    expected: typeSchema,
  },
] as const

export type Error = typeof errors[number]
