import * as R from './resources'
import _knex from 'knex'
import { baseAttributes } from './resources'

let knex = _knex({
  client: 'sqlite',
  useNullAsDefault: true,
  connection: {
    filename: process.env.DB_NAME === 'test' ? 'test_db.sqlite' : 'db.sqlite',
  },
})

function parseDbResult(result: any | any[]) {
  function job(result: any) {
    for (let key in result) {
      if (!baseAttributes[key] && typeof result[key] === 'string') {
        try {
          result[key] = JSON.parse(result[key])
        } catch (e) {}
      }
    }
  }

  if (Array.isArray(result)) {
    result.forEach(job)
  } else {
    job(result)
  }

  return result
}

export function list(resource: R.Resources) {
  return knex.select().from(resource).then(parseDbResult)
}

export function getByVersion(
  resource: R.Resources,
  id: string,
  version: number
) {
  return knex
    .select()
    .from(resource)
    .where({ id, version })
    .limit(1)
    .then(a => a[0])
    .then(parseDbResult)
}

export async function getByVersion_(
  resources: R.Resources[],
  id: string,
  version: number
) {
  for (let resource of resources) {
    let result = await getByVersion(resource, id, version)
    if (result) return result
  }
  return null
}

export function getAll(resource: R.Resources, id: string) {
  return knex
    .select()
    .from(resource)
    .orderBy('version', 'desc')
    .then(parseDbResult)
}

export async function getAll_(resources: R.Resources[], id: string) {
  for (let resource of resources) {
    let result = await getAll(resource, id)
    if (result) return result
  }
  return []
}

export async function get(id: string, version?: number) {
  for (let resource of R.resources) {
    let result = await knex
      .select()
      .from(resource)
      .where({ id, version })
      .orderBy('version', 'desc')
      .limit(1)

    if (result) return parseDbResult(result)
  }
}
