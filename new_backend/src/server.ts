import Koa from 'koa'
import Router from '@koa/router'
import * as db from './db_queries'
import * as R from './resources'
import { stringToResource } from './resources_utils'

let onDefault = async ctx => {
  ctx.body = Object.entries(R.resourcesMap).reduce(
    (acc, [resourceName, attrs]) => {
      if (attrs) {
        acc.resources[resourceName] = prettyAttributes(attrs.ownAttributes)
      }
      return acc
    },
    {
      resources: {},
      baseAttrs: prettyAttributes(R.baseAttributes),
    }
  )
}

let onList = (resource: R.Resources) => async ctx => {
  ctx.body = await db.list(resource)
}

let onGet = (resource: R.Resources) => async ctx => {
  ctx.body = await db.getAll(resource, ctx.params.id)
}

let onGetVersion = (resource: R.Resources) => async ctx => {
  let result = await db.getByVersion(
    resource,
    ctx.params.id,
    parseInt(ctx.params.version)
  )

  if (result) {
    ctx.body = result
  } else {
    ctx.status = 404
  }
}

let onPost = (resource: string) => async ctx => {
  ctx.status = 501
}

function prettyAttributes(attrs: R.Attributes): { [k: string]: string } {
  return Object.entries(attrs).reduce((acc, [k, v]) => {
    acc[k] = prettyAttribute(v)
    return acc
  }, {})
}

function prettyAttribute(attr: R.Attribute): string {
  if (typeof attr === 'string') {
    return attr
  } else {
    let meta = [] as string[]
    if (attr.nullable) {
      meta.push('nullable')
    }
    if (attr.protected) {
      meta.push('readOnly')
    }
    let metaString = meta.length ? `(${meta.join(', ')})` : ''

    return `${attr.$}${metaString ? ' ' + metaString : ''}`
  }
}

export function init(): Koa {
  let app = new Koa()
  let router = new Router()

  router.get('/', onDefault)

  Object.entries(R.resourcesMap).forEach(([k, v]) => {
    if (v.readOnly) return

    let resource = stringToResource(k)
    if (!resource) return

    router.get(`/${k}`, onList(resource))
    router.get(`/${k}/:id`, onGet(resource))
    router.get(`/${k}/:id/:version`, onGetVersion(resource))
    router.post(`/${k}`, onPost(resource))
  })

  app.use(router.routes()).use(router.allowedMethods())

  return app
}
