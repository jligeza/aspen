export type NotUndefined = string | number | boolean | null | object

export function isNil(a) {
  return a === null || a === undefined
}
