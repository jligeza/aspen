import * as v from './resources_validation'
import * as r from './resources'
import * as dbq from './db_queries'
import * as testData from './test_data'

let unknownId = 'x'

interface Case {
  desc: string
  resource: r.Resources
  payload: any
  result: v.Result
}

let cases: Case[] = [
  {
    desc: 'simple type ok',
    resource: 'type',
    payload: {
      val: 'int',
    },
    result: null,
  },
  // TODO uncomment when working on type validation
  // {
  //   desc: 'simple type error',
  //   resource: 'type',
  //   payload: {
  //     val: null
  //   },
  //   result: {
  //     $: 'typeError',
  //     err: { $: 'G1', desc: 'missing schema', path: [] }
  //   }
  // },
  // {
  //   desc: 'nested type error',
  //   resource: 'type',
  //   payload: {
  //     val: {
  //       x: null
  //     }
  //   },
  //   result: {
  //     $: 'typeError',
  //     err: { $: 'G1', desc: 'missing schema', path: ['x'] }
  //   }
  // },
  {
    desc: 'extra attr',
    resource: 'type',
    payload: {
      val: 'int',
      x: null,
    },
    result: { $: 'unsupportedAttr', key: 'x' },
  },
  {
    desc: 'missing attr',
    resource: 'type',
    payload: {},
    result: { $: 'missingAttr', key: 'val' },
  },
  {
    desc: 'event generator ok',
    resource: 'eventGenerator',
    payload: {
      configId: testData.consts[0].id,
      configVersion: testData.consts[0].version,
      templateId: testData.templatesWithConfig[0].id,
      templateVersion: testData.templatesWithConfig[0].version,
    },
    result: null,
  },
  {
    desc: 'event generator wrong template',
    resource: 'eventGenerator',
    payload: {
      configId: testData.consts[0].id,
      configVersion: testData.consts[0].version,
      templateId: testData.consts[0].id,
      templateVersion: testData.consts[0].version,
    },
    result: {
      $: 'idNotFound',
      path: ['templateId'],
    },
  },
  {
    desc: 'event generator bad id',
    resource: 'eventGenerator',
    payload: {
      configId: unknownId,
      configVersion: 1,
      templateId: unknownId,
      templateVersion: 1,
    },
    result: {
      $: 'idNotFound',
      path: ['templateId'],
    },
  },
  {
    desc: 'const ok',
    resource: 'const',
    payload: {
      val: 'int',
    },
    result: null,
  },
  {
    desc: 'link ok',
    resource: 'link',
    payload: {
      joints: [],
      to: null,
      from: '',
    },
    result: null,
  },
  {
    desc: 'name tag ok',
    resource: 'nameTag',
    payload: {
      name: 'x',
      path: [],
      targetVersion: testData.consts[0].version,
      targetId: testData.consts[0].id,
    },
    result: null,
  },
  {
    desc: 'name tag bad name',
    resource: 'nameTag',
    payload: {
      name: '',
      path: [],
      targetVersion: testData.consts[0].version,
      targetId: testData.consts[0].id,
    },
    result: {
      $: 'badAttr',
      path: ['name'],
      expected: 'name',
    },
  },
  {
    desc: 'node ok',
    resource: 'node',
    payload: {
      targetId: testData.templatesWithConfig[0].id,
      targetVersion: testData.templatesWithConfig[0].version,
    },
    result: null,
  },
]

describe('resource validation', () => {
  test(`RV - resource validation: check all resources tested`, () => {
    let givenResources = cases.reduce((acc, c) => {
      acc[c.resource] = true
      return acc
    }, {})

    let allResources = r.resources.reduce((acc, resource) => {
      if (r.resourcesMap[resource].readOnly) return acc
      acc[resource] = true
      return acc
    }, {})

    expect(allResources).toEqual(givenResources)
  })

  cases.forEach((c, idx) => {
    test(`RV ${idx} resource validation: ${c.desc}`, async () => {
      expect(await v.validate(c.resource, c.payload)).toEqual(c.result)
    })
  })
})
