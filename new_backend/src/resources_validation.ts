import * as R from './resources'
import { Error as TypeError, Path } from './types'
import * as dbq from './db_queries'
import * as _ from './utils'

export let error = [
  {
    $: 'unsupportedAttr',
    key: '' as string,
  },
  {
    $: 'missingAttr',
    key: '' as string,
  },
  {
    $: 'badAttr',
    path: [] as Path,
    expected: '' as R.Attribute,
  },
  {
    $: 'idNotFound',
    path: [] as Path,
  },
  {
    $: 'typeError',
    path: [] as Path,
    err: {} as TypeError,
  },
] as const

export type Error = typeof error[number]

export type Result = Error | null

export type IdChecker = (name: R.Resources, id: string) => boolean

export function getMatchingVersion(
  key: string,
  payload: object
): number | undefined {
  let expectedKey = key.split('Id')[0] + 'Version'

  return payload[expectedKey]
}

export function validateAttr_(
  attrs_: R.Attributes,
  attrs: [string, R.Attribute][],
  payload: object
) {
  return validateAttr(attrs_, attrs, payload, [])
}

export function validateName(payloadVal: any, path: Path): Result {
  if (typeof payloadVal === 'string') {
    if (payloadVal.trim().length === 0) {
      return { $: 'badAttr', expected: 'name', path }
    } else {
      return null
    }
  } else {
    return { $: 'badAttr', expected: 'name', path }
  }
}

export async function validateAttr(
  attrs_: R.Attributes,
  attrs: [string, R.Attribute][],
  payload: object,
  path: Path
): Promise<Result> {
  let attr = attrs.pop()
  if (!attr) {
    return null
  }

  let [attrKey, attrVal] = attr
  path = (path || []).concat(attrKey)

  if (attrKey in payload) {
    let payloadVal = payload[attrKey]

    // TODO uncomment when working on 'val' validation
    // if (payloadVal === null || payloadVal === undefined) {
    //   return {
    //     $: 'typeError',
    //     err: {
    //       $: 'G1',
    //       desc: 'missing schema',
    //       path
    //     }
    //   }
    // }

    // TODO validate all things here
    if (typeof attrVal === 'string') {
      if (attrVal === 'foreignId') {
        // TODO validate foreignId
      }
      if (attrVal === 'name') {
        return (
          validateName(payloadVal, path) ||
          validateAttr(attrs_, attrs, payload, path)
        )
      }
    } else {
      if (attrVal.$ === 'name') {
        return (
          validateName(payloadVal, path) ||
          validateAttr(attrs_, attrs, payload, path)
        )
      }
      if (attrVal.$ === 'foreignId') {
        if (_.isNil(payloadVal)) {
          if (attrVal.nullable) {
            return validateAttr(attrs_, attrs, payload, path)
          } else {
            return {
              $: 'badAttr',
              expected: attrVal,
              path,
            }
          }
        }

        if ('for' in attrVal && attrVal.for !== undefined) {
          let version = getMatchingVersion(attrKey, payload)
          let id = payload[attrKey]

          if (id === undefined || typeof id !== 'string') {
            return { $: 'badAttr', path: [attrKey], expected: 'id' }
          }

          if (version === undefined) {
            if ((await dbq.getAll_(attrVal.for, id)).length === 0) {
              return {
                $: 'idNotFound',
                path: [attrKey],
              }
            } else {
              return validateAttr(attrs_, attrs, payload, path)
            }
          } else {
            if (!(await dbq.getByVersion_(attrVal.for, id, version))) {
              return {
                $: 'idNotFound',
                path: [attrKey],
              }
            } else {
              return validateAttr(attrs_, attrs, payload, path)
            }
          }
        } else {
          // TODO validate foreignId
        }
      }
    }
    return validateAttr(attrs_, attrs, payload, path)
  } else {
    return { $: 'missingAttr', key: attrKey }
  }
}

export async function validateAttributes(
  attrs: R.Attributes,
  payload: object
): Promise<Result> {
  let unsupportedAttr = Object.keys(payload).find(k => !attrs[k])

  if (unsupportedAttr !== undefined) {
    return { $: 'unsupportedAttr', key: unsupportedAttr }
  }

  return validateAttr_(attrs, Object.entries(attrs), payload)
}

export function validate(type: R.Resources, payload: object): Promise<Result> {
  return validateAttributes(R.resourcesMap[type].ownAttributes, payload)
}
