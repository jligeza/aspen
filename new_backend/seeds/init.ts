import * as Knex from 'knex'
import * as td from '../src/test_data'

let stringify = a => JSON.stringify(a)

export async function seed(knex: Knex) {
  await knex('type').del()
  await knex('type').insert(
    td.types.map(a => ({ ...a, val: stringify(a.val) }))
  )

  await knex('node').del()
  await knex('node').insert(td.nodes)

  await knex('nameTag').del()
  await knex('nameTag').insert(
    td.nameTags.map(a => ({ ...a, path: stringify(a.path) }))
  )

  await knex('link').del()
  await knex('link').insert(
    td.links.map(a => ({ ...a, joints: stringify(a.joints) }))
  )

  await knex('eventGenerator').del()
  await knex('eventGenerator').insert(td.eventGenerators)

  await knex('const').del()
  await knex('const').insert(
    td.consts.map(a => ({ ...a, val: stringify(a.val) }))
  )

  await knex('templateWithConfig').del()
  await knex('templateWithConfig').insert(td.templatesWithConfig)
}
