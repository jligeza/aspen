import * as Knex from 'knex'

function setIdVersionPrimaryKey(table: Knex.TableBuilder) {
  setId(table, 'id').notNullable()
  setVersion(table, 'version').notNullable()
  table.primary(['id', 'version'])
  table.index('id')
}

function setTimestamps(table: Knex.TableBuilder) {
  table.dateTime('createdAt').notNullable()
  table.dateTime('deletedAt')
}

function setBaseColumns(table: Knex.TableBuilder) {
  setIdVersionPrimaryKey(table)
  setTimestamps(table)
}

function setVersion(table: Knex.TableBuilder, name: string) {
  return table.integer(name)
}

function setId(table: Knex.TableBuilder, name: string) {
  return table.uuid(name)
}

export async function up(knex: Knex) {
  return knex.schema
    .createTable('type', table => {
      setBaseColumns(table)

      table.json('val').notNullable()
    })
    .createTable('node', table => {
      setBaseColumns(table)

      setId(table, 'targetId').notNullable()
      setVersion(table, 'targetVersion').notNullable()

      table.index('targetId')
    })
    .createTable('nameTag', table => {
      setBaseColumns(table)

      setId(table, 'targetId').notNullable()
      setVersion(table, 'targetVersion').notNullable()
      table.json('path').notNullable()
      table.string('name').notNullable()

      table.index('targetId')
    })
    .createTable('link', table => {
      setBaseColumns(table)

      setId(table, 'from')
      setId(table, 'to')
      table.json('joints').notNullable()

      table.index(['from', 'to'])
    })
    .createTable('eventGenerator', table => {
      setBaseColumns(table)

      setId(table, 'configId')
      setVersion(table, 'configVersion')
      setId(table, 'templateId').notNullable()
      setVersion(table, 'templateVersion').notNullable()

      table.index(['configId', 'configVersion'])
    })
    .createTable('const', table => {
      setBaseColumns(table)

      table.json('val').notNullable()
    })
    .createTable('template', table => {
      setBaseColumns(table)

      table.string('name')
      setId(table, 'schemaId').notNullable()
      setVersion(table, 'schemaVersion').notNullable()
    })
    .createTable('templateWithConfig', table => {
      setBaseColumns(table)

      table.string('name')
      setId(table, 'schemaId').notNullable()
      setVersion(table, 'schemaVersion').notNullable()
      setId(table, 'configId')
      setVersion(table, 'configVersion')
    })
}

export async function down(knex: Knex) {}
