let errors = {
  'G1': 'missing schema',
  'G2': {
    desc: 'missing data',
    info: ['path']
  },
  'G3': {
    desc: 'type mismatch',
    info: ['path', 'given', 'expected']
  },
  'G5': {
    desc: 'not found in DB',
    info: ['path', 'givenId', 'givenVersion']
  },
  'G6': {
    desc: 'generic type mismatch',
    info: ['path', 'previously', 'now']
  },
  'G7': {
    desc: 'no such generics',
    info: ['path', 'givenExtra', 'available']
  },
  'C1': {
    desc: 'damaged schema - ID not found in DB',
    info: ['path', 'schemaId', 'schemaVersion']
  },
  'R1': {
    desc: 'missing keys in record',
    info: ['path', 'keys', 'allKeys']
  },
  'U1': {
    desc: 'no such type in union',
    info: ['path', 'given', 'available']
  },
  'U2': {
    desc: 'missing content',
    info: ['path', 'expected']
  }
}

module.exports = {errors}
