let _ = require('generic_utils')
let {findById} = require('database/find')

let primitives = [
  ['Bool', _.is(Boolean)],
  ['List', _.is(Array)],
  ['Int', _.isInteger],
  ['Float', _.isTrueNumber],
  ['Record', _.isRecord],
  ['String', _.is(String)]
]

let baseTypesDict = {
  Bool: true,
  List: true,
  Int: true,
  Float: true,
  Record: true,
  String: true,
  Union: true,
  UnionMember: true,
  Json: true,
  Function: true
}

let isBaseType = _.has(_.__, baseTypesDict)

let reqPropsOfTypes = [
  ['UnionMember', ['$choice']],
  ['List', ['$valsType']],
  ['Function', ['$input', '$output']],
  ['Json', ['$vals']]
]

let getTypeByVal = gVal => {
  let gProps = _.keys(gVal)

  let matches = props => _.isEmpty(_.without(gProps, props))

  return _.prop(0, _.find(([x, props]) => matches(props), reqPropsOfTypes))
}

let getShortSchemaType =
  _.cond([
    [_.is(String), _.I],
    [_.is(Array), () => 'Union'],
    [_.isRecord, () => 'Record']
  ])

let result = (val, meta = null) => ({val, meta})

let hasId = data => data && _.has('$id', data)

let getIdResult = async query => {
  let dbFinding = await findById({id: query.$id, version: query.$version})

  if (!dbFinding) return result(null, dbFinding)

  let type = (await getSchemaType(dbFinding.data.schema)).val

  return result(type, dbFinding.data)
}

let getSchemaType = async data => {
  if (hasId(data)) return getIdResult(data)

  return result(
    _.prop('$type', data) ||
    (_.isRecord(data) && getTypeByVal(data)) ||
    getShortSchemaType(data),
    null)
}

let getPerceivedGivenType = async data => {
  if (hasId(data)) return getIdResult(data)

  let typeByVal = getTypeByVal(data)

  return result(
    typeByVal ||
    _.prop(0, _.find(([x, check]) => check(data), primitives))
  )
}

let getGivenType = async (schemaType, given, path) => {
  // TODO simplify
  return (await getPerceivedGivenType(_.path(path, given))).val
}

let getTypes = async (schema, given, path) => {
  let schemaType = await getSchemaType(_.path(path, schema))

  let givenType = result(await getGivenType(schemaType.val, given, path))

  return {schemaType, givenType}
}

let isUnionMember = vd =>
  _.is(String, vd.given) || _.propEq('$type', 'UnionMember', vd.given || {})

module.exports = {
  getSchemaType,
  getGivenType,
  getTypes,
  isUnionMember,
  isBaseType
}
