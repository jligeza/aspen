jest.mock('fs-extra')
jest.mock('database/db_init')

let u = require('test_utils')
let {validateData} = require('./index')
let {errors} = require('./errors')

let cases = [{
  desc: 'missing schema',
  input: [
    1,
    {}
  ],
  output: {id: 'G1'}
}, {
  desc: 'no data at root level',
  input: [
    null,
    'Int'
  ],
  output: {id: 'G2', path: []}
}, {
  desc: 'bool ok',
  input: [
    true,
    'Bool'
  ],
  output: null
}, {
  desc: 'integer ok',
  input: [
    1,
    'Int'
  ],
  output: null
}, {
  desc: 'integer error',
  input: [
    '1',
    'Int'
  ],
  output: {id: 'G3', path: [], given: 'String', expected: 'Int'}
}, {
  desc: 'integer error - given float',
  input: [
    1.1,
    'Int'
  ],
  output: {id: 'G3', path: [], given: 'Float', expected: 'Int'}
}, {
  desc: 'float ok',
  input: [
    1.1,
    'Float'
  ],
  output: null
}, {
  desc: 'float ok - given integer',
  input: [
    1,
    'Float'
  ],
  output: null
}, {
  desc: 'float error',
  input: [
    '1',
    'Float'
  ],
  output: {id: 'G3', path: [], given: 'String', expected: 'Float'}
}, {
  desc: 'string ok',
  input: [
    'a',
    'String'
  ],
  output: null
}, {
  desc: 'string error',
  input: [
    1,
    'String'
  ],
  output: {id: 'G3', path: [], given: 'Int', expected: 'String'}
}, {
  desc: 'union ok',
  input: [
    u.UnionMember('B'),
    ['B']
  ],
  output: null
}, {
  desc: 'union error',
  input: [
    u.UnionMember('C'),
    ['B']
  ],
  output: {id: 'U1', path: [], given: u.UnionMember('C'), available: ['B']}
}, {
  desc: 'union error - type mismatch',
  input: [
    1,
    ['B']
  ],
  output: {id: 'G3', path: [], given: 'Int', expected: 'UnionMember'}
}, {
  desc: 'union ok - args',
  input: [
    u.UnionMember('B', 'test'),
    [u.UnionMember('B', 'String')]
  ],
  output: null
}, {
  desc: 'union ok - long args',
  input: [
    u.UnionMember('B', [1, 2, 3]),
    [u.UnionMember('B', {$type: 'List', $valsType: 'Int'})]
  ],
  output: null
}, {
  desc: 'union error - long args',
  input: [
    u.UnionMember('B', [1, '2', 3]),
    ['Y', u.UnionMember('B', {$type: 'List', $valsType: 'Int'})]
  ],
  output: {
    id: 'G3',
    path: ['$content', 1],
    given: 'String',
    expected: 'Int' }
}, {
  desc: 'union error - args type mismatch',
  input: [
    u.UnionMember('B', 1),
    [u.UnionMember('B', 'String')]
  ],
  output: {
    id: 'G3',
    path: ['$content'],
    given: 'Int',
    expected: 'String'}
}, {
  desc: 'union ok - args custom type',
  input: [
    u.UnionMember('B', {flag: true, num: 1}),
    [u.UnionMember('B', {flag: 'Bool', num: 'Int'})]
  ],
  output: null
}, {
  desc: 'union error - args custom type',
  input: [
    u.UnionMember('B', {flag: true, text: 1}),
    [u.UnionMember('Y', 'Bool'), u.UnionMember('B', {flag: 'Bool', text: 'String'})]
  ],
  output: {
    id: 'G3',
    path: ['$content', 'text'],
    given: 'Int',
    expected: 'String' }
}, {
  desc: 'union ok - mixed schema',
  input: [
    u.UnionMember('C', 1),
    ['A', u.UnionMember('B', 'Bool'), u.UnionMember('C', 'Int')]
  ],
  output: null
}, {
  desc: 'union ok - deep mixed schema',
  input: [
    u.UnionMember('A'),
    ['A', u.UnionMember('B', {q: 'String', w: {z: 'Bool'}})]
  ],
  output: null
}, {
  desc: 'union error - missing required content',
  input: [
    u.UnionMember('A'),
    [u.UnionMember('A', 'String')]
  ],
  output: {id: 'U2', path: ['$content'], expected: 'String'}
}, {
  desc: 'list ok',
  input: [
    [1, 2],
    {$type: 'List', $valsType: 'Int'}
  ],
  output: null
}, {
  desc: 'list error - bad record',
  input: [
    [{a: '1'}],
    {$type: 'List', $valsType: {a: 'Int'}}
  ],
  output: {id: 'G3', path: [0, 'a'], given: 'String', expected: 'Int'}
}, {
  desc: 'list error - custom types',
  input: [
    [{q: '1', w: 2}, {q: 1, w: '1'}],
    {$type: 'List', $valsType: {q: 'Int', w: 'Int'}}
  ],
  output: {id: 'G3', path: [0, 'q'], given: 'String', expected: 'Int'}
}, {
  desc: 'list error - union error deep',
  input: [
    [{q: 1, w: u.UnionMember('B')}],
    {$type: 'List', $valsType: {q: 'Int', w: ['A']}}
  ],
  output: {
    id: 'U1',
    path: [0, 'w'],
    given: {$choice: 'B', $type: 'UnionMember'},
    available: ['A'] }
}, {
  desc: 'list error - union',
  input: [
    [u.UnionMember('B')],
    {$type: 'List', $valsType: ['A']}
  ],
  output: {
    id: 'U1',
    path: [0],
    given: {$choice: 'B', $type: 'UnionMember'},
    available: ['A'] }
}, {
  desc: 'list error',
  input: [
    ['1', 2],
    {$type: 'List', $valsType: 'String'}
  ],
  output: {id: 'G3', path: [1], given: 'Int', expected: 'String'}
}, {
  desc: 'record ok',
  input: [
    {route: {url: 'a', method: u.UnionMember('Get')}},
    {route: {url: 'String', method: ['Get', 'Post']}}
  ],
  output: null
}, {
  desc: 'record error - missing key',
  input: [
    {a: {b: 1}},
    {a: {b: 'Int', c: 'Int'}}
  ],
  output: {id: 'R1', path: ['a'], keys: ['c'], allKeys: ['b', 'c']}
}, {
  desc: 'record ok - extra key',
  input: [
    {a: 1, b: 2},
    {a: 'Int'}
  ],
  output: null
}, {
  desc: 'json ok',
  input: [
    {$vals: {x: 1}},
    {$type: 'Json'}
  ],
  output: null
}, {
  desc: 'json ok - given number',
  input: [
    {$vals: 2},
    {$type: 'Json'}
  ],
  output: null
}, {
  desc: 'generics ok - simple',
  input: [
    1,
    'a'
  ],
  output: null
}, {
  desc: 'generics ok - nested',
  input: [
    [1],
    {$type: 'List', $valsType: 'a'}
  ],
  output: null
}, {
  desc: 'generics error - override',
  input: [
    {a: 'q', b: 1},
    {a: 'x', b: 'x'}
  ],
  output: {id: 'G6', path: ['b'], previously: 'String', now: 'Int'}
}, {
  desc: 'generics error - list',
  input: [
    [1, 'a'],
    {$type: 'List', $valsType: 'a'}
  ],
  output: {id: 'G6', path: [1], previously: 'Int', now: 'String'}
}, {
  desc: 'generics ok - separation',
  input: [
    {a: 'q', b: 1},
    {a: 'a', b: u.refs.generic}
  ],
  output: null
}, {
  desc: 'generics ok - separation reverse',
  input: [
    {a: 'q', b: 1},
    {a: u.refs.generic, b: 'a'}
  ],
  output: null
}, {
  desc: 'generics ok - separation 2x',
  input: [
    {a: 'q', b: 1},
    {a: u.refs.generic, b: u.refs.generic}
  ],
  output: null
}, {
  desc: 'non existent ID',
  input: [
    u.refs.unknownId,
    'x'
  ],
  output: {
    id: 'G5',
    path: [],
    givenId: u.refs.unknownId.$id,
    givenVersion: u.refs.unknownId.$version
  }
}, {
  desc: 'using existing ID in schema',
  input: [
    {a: 1},
    u.refs.int
  ],
  output: null
}, {
  desc: 'using missing ID in schema',
  input: [
    {a: 1},
    u.refs.unknownId
  ],
  output: {
    id: 'C1',
    path: [],
    schemaId: u.refs.unknownId.$id,
    schemaVersion: u.refs.unknownId.$version
  }
}, {
  desc: 'ok - maybe just txt',
  input: [
    u.UnionMember('Just', 'some txt'),
    u.refs.maybeString
  ],
  output: null
}, {
  desc: 'error - maybe bad type',
  input: [
    u.UnionMember('Just', 1),
    u.refs.maybeString
  ],
  output: {id: 'G3', path: ['$content'], given: 'Int', expected: 'String'}
}, {
  desc: 'ok - resolved generic record',
  input: [
    {x: 'txt'},
    u.refs.resolvedGenericRecord
  ],
  output: null
}, {
  desc: 'error - resolved generic record',
  input: [
    {x: 1},
    u.refs.resolvedGenericRecord
  ],
  output: {id: 'G3', path: ['x'], given: 'Int', expected: 'String'}
}]

u.testErrorsCorrectness('main', errors, cases)

u.simpleTests('main', validateData, cases)
