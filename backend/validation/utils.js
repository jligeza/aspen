let _ = require('generic_utils')

let withoutKeys = _.pipe(
  _.unapply(_.map(_.keys)),
  _.apply(_.without))

let isGenericArg = a =>
  typeof a === 'string' &&
  _.isFilled(_.match(/^[a-z]/, a))

let getFullPath = ({prevPath, path}) =>
  _.concat(prevPath, path)

let arePathsMatching = _.curry((a, b) =>
  _.all(_.apply(_.eq), _.zip(a || [], b || [])))

let concatPath = (path, key) =>
  key
    ? path.concat(key)
    : path

let findGenerics = _schema => {
  let job = (path = [], key, schema) => {
    let acc = []

    if (isGenericArg(schema)) {
      if (!key || key === '$valsType' || key === '$content' || key[0] !== '$') {
        acc.push({key: schema, path})
      }
      return acc
    } else if (_.isRecord(schema)) {
      let results = _.toPairs(schema).map(([k, v]) => job(concatPath(path, k), k, v))

      return _.flatten(acc.concat(results))
    } else if (_.is(Array, schema)) {
      let results = schema.map((a, idx) => job(concatPath(path, idx), idx, a))

      return _.flatten(acc.concat(results))
    }
    return acc
  }

  return job(undefined, undefined, _schema)
}

module.exports = {
  arePathsMatching,
  findGenerics,
  getFullPath,
  isGenericArg,
  withoutKeys
}
