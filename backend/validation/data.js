let _ = require('generic_utils')
let {getErrorReport} = require('./error_report')

let flattenSchema = schema => {
  return schema.$main || schema
}

let validate = async (data, schema) => {
  let flatSchema = flattenSchema(schema)

  return (
    (_.anyPass([_.isNil, _.isEmpty])(flatSchema) && {id: 'G1'}) ||
    _.prop('err', await getErrorReport(undefined, flatSchema, data)))
}

module.exports = {validate, flattenSchema}
