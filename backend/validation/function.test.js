jest.mock('fs-extra')
jest.mock('database/db_init')

let u = require('test_utils')
let {validateData} = require('./index')
let {errors} = require('./errors')

let cases = [{
  desc: 'simple generic signature',
  input: [
    u.refs.identity,
    {$type: 'Function', $input: 'a', $output: 'a'}],
  output: null
}, {
  desc: 'simple specific signature',
  input: [
    u.refs.identity,
    {$type: 'Function', $input: 'Int', $output: 'Int'}],
  output: null
}, {
  desc: 'simple type mismatch in input',
  input: [
    {$input: 'Int', $output: 'Int'},
    {$type: 'Function', $input: 'String', $output: 'Int'}],
  output: {id: 'G3', path: ['$input'], given: 'Int', expected: 'String'}
}, {
  desc: 'simple type mismatch in output',
  input: [
    {$input: 'Int', $output: 'Int'},
    {$type: 'Function', $input: 'Int', $output: 'String'}],
  output: {id: 'G3', path: ['$output'], given: 'Int', expected: 'String'}
}, {
  desc: 'list vals type mismatch',
  input: [{
    $input: {$type: 'List', $valsType: 'String'},
    $output: 'String'
  }, {
    $type: 'Function',
    $input: {$type: 'List', $valsType: 'Int'},
    $output: 'String'
  }],
  output: {
    id: 'G3',
    path: ['$input', '$valsType'],
    given: 'String',
    expected: 'Int'
  }
}, {
  desc: 'nested type mismatch',
  input: [
    {$input: {q: {w: 'Int'}}, $output: 'Int'},
    {$type: 'Function', $input: {q: {w: 'String'}}, $output: 'Int'}],
  output: {
    id: 'G3',
    path: ['$input', 'q', 'w'],
    given: 'Int',
    expected: 'String'
  }
}, {
  desc: 'U1',
  input: [
    {$input: [u.UnionMember('A')], $output: 'String'},
    {$type: 'Function', $input: ['B'], $output: 'String'}],
  output: {
    id: 'U1',
    path: ['$input'],
    given: ['A'],
    available: ['B']
  }
}, {
  desc: 'U2',
  input: [
    {$input: [u.UnionMember('A')], $output: 'String'},
    {$type: 'Function', $input: [u.UnionMember('A', 'String')], $output: 'String'}],
  output: {
    id: 'U2',
    path: ['$input', 0, '$content'],
    expected: 'String'
  }
}, {
  desc: 'nested union',
  input: [{
    $input: [u.UnionMember('A', {a: {b: 'Int'}})],
    $output: 'String'
  }, {
    $type: 'Function',
    $input: [u.UnionMember('A', {a: {b: 'String'}})],
    $output: 'String'
  }],
  output: {
    id: 'G3',
    path: ['$input', 0, '$content', 'a', 'b'],
    given: 'Int',
    expected: 'String'
  }
}, {
  desc: 'nested function, bad input',
  input: [{
    $input: {$input: 'Int', $output: 'Int'},
    $output: 'String'
  }, {
    $type: 'Function',
    $input: {$type: 'Function', $input: 'String', $output: 'Int'},
    $output: 'String'
  }],
  output: {id: 'G3', path: ['$input', '$input'], expected: 'String', given: 'Int'}
}, {
  desc: 'generics mixed with specifics error',
  input: [
    u.refs.identity,
    {$type: 'Function', $input: 'Int', $output: 'String'}],
  output: {id: 'G6', path: ['$output'], previously: 'Int', now: 'String'}
}, {
  desc: 'generics mixed with specifics',
  input: [
    u.refs.identity,
    {$type: 'Function', $input: 'Int', $output: 'Int'}],
  output: null
}, {
  desc: 'differently named generic signatures',
  input: [
    u.refs.identity,
    {$type: 'Function', $input: 'x', $output: 'x'}],
  output: null
}, {
  desc: 'generics separation',
  input: [
    {$type: 'Function', $input: {a: 'Int', b: 'Boolean'}, $output: 'String'},
    u.refs.genericClash
  ],
  output: null
}, {
  desc: 'complex signature',
  input: [
    u.refs.complex,
    {$type: 'Function', $input: {a: {c: 'String'}}, $output: {a: 'Int'}}],
  output: null
}, {
  desc: 'complex signature error',
  input: [
    u.refs.complex,
    {$type: 'Function', $input: {a: {c: 'Int'}}, $output: {a: 'Int'}}],
  output: {id: 'G3', path: ['$input', 'a', 'c'], given: 'String', expected: 'Int'}
}, {
  desc: 'nested schema ID',
  input: [
    {$type: 'Function', $input: {a: 'String'}, $output: 'a'},
    {$type: 'Function', $input: u.refs.int, $output: 'a'}],
  output: {id: 'G3', path: ['$input', 'a'], given: 'String', expected: 'Int'}
}]

u.testErrorsCorrectness('function', errors, cases)

u.simpleTests('function', validateData, cases)
