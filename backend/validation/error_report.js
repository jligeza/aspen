let _ = require('generic_utils')
let {findById} = require('database/find')
let u = require('./utils')
let {getTypes, getSchemaType} = require('./types')

let getSchemaByIdVer = async val => {
  let finding = await findById({id: val.$id, version: val.$version})

  return finding
    ? finding.data.schema
    : null
}

let discoverVal = async (val) => {
  if (_.has('$id', val)) {
    let finding = await findById({id: val.$id, version: val.$version})

    if (_.isNil(finding)) {
      return {choice: 'NotFoundWithId', val}
    } else {
      return {choice: 'WithId', val: finding.data.schema}
    }
  } else {
    return {choice: 'WithoutId', val}
  }
}

let getChoice = _.when(_.has('$choice'), _.prop('$choice'))

let isU1 = vd => !_.contains(getChoice(vd.gVal), _.map(getChoice, vd.sVal))

let u1 = vd => ({
  id: 'U1',
  path: u.getFullPath(vd),
  given: vd.gVal,
  available: vd.sVal
})

let findUnionChoice = vd =>
  _.find(_.propEq('$choice', vd.gVal.$choice), vd.sVal)

let isU2 = vd =>
  _.prop('$content', findUnionChoice(vd)) && _.isNil(vd.gVal.$content)

let u2 = vd => ({
  id: 'U2',
  path: _.append('$content', u.getFullPath(vd)),
  expected: _.prop('$content', findUnionChoice(vd))
})

let checkUnion = async vd => {
  if (isU1(vd)) {
    return u1(vd)
  }
  if (isU2(vd)) {
    return u2(vd)
  }
  if (!_.has('$content', vd.gVal)) {
    return null
  }
  return _.prop('err', await getErrorReport(
    _.merge(vd, {path: ['$content'], prevPath: vd.path}),
    findUnionChoice(vd),
    vd.gVal)
  )
}

let checkList = _vd => {
  let job = async (vd, idx) => {
    let nextIdx = idx + 1

    let {err, meta} = await getErrorReport(
      _.merge(vd, {path: [idx], prevPath: u.getFullPath(vd), arrayMode: true}),
      vd.sVal.$valsType,
      {[idx]: vd.gVal[idx]}
    )

    if (err) return err
    if (nextIdx > vd.gVal.length - 1) return null
    return job(evolveMetas(vd, meta), nextIdx)
  }

  return job(_vd, 0)
}

let checkKeys = async (vd, keys) => {
  let [key, ...restKeys] = keys

  let {err, meta} = await getErrorReport(
    _.merge(vd, {path: [key], prevPath: u.getFullPath(vd)}),
    vd.sVal,
    vd.gVal
  )

  if (err) return err
  if (_.isEmpty(restKeys)) return null
  return checkKeys(evolveMetas(vd, meta), restKeys)
}

let checkRecord = async vd => {
  let givenKeys = _.keys(vd.gVal)
  let expectedKeys = _.keys(vd.sVal)

  let missingKeys = _.without(givenKeys, expectedKeys)
  if (_.isFilled(missingKeys)) {
    return {
      id: 'R1',
      path: u.getFullPath(vd),
      keys: missingKeys,
      allKeys: expectedKeys
    }
  }

  return checkKeys(vd, expectedKeys)
}

let isG2 = (path, given) => _.isNil(_.path(path, given))

let g2 = (path, prevPath) => ({
  id: 'G2',
  path: u.getFullPath({prevPath, path})
})

let schemaG3 = svd => ({
  id: 'G3',
  path: u.getFullPath(svd),
  expected: svd.aType.val,
  given: svd.bType.val
})

let isSchemaG3 = svd => {
  let a = svd.aType.val
  let b = svd.bType.val

  if (_.any(u.isGenericArg, [a, b])) {
    return false
  }
  return a !== b
}

let checkGenerics = ({aType, bType, path, prevPath, gens, ids}) => {
  let a = aType.val
  let b = bType.val
  let fullPath = u.getFullPath({path, prevPath})
  let newAGen, newBGen

  if (!_.any(u.isGenericArg, [a, b])) {
    return {gens, genericsErr: null}
  }

  let findBelongingId = genType =>
    _.find(
      _.where({
        path: u.arePathsMatching(fullPath)
      }),
      ids[genType])

  let findCur = (genType, val) => {
    let boundId = findBelongingId(genType)

    let relevantGens = boundId
      ? _.filter(_.where({idPath: _.eq(boundId.path)}), gens[genType])
      : gens[genType]

    return _.prop(
      'val',
      _.find(
        _.where({
          key: _.eq(val),
          idPath: u.arePathsMatching(fullPath)
        }),
        relevantGens))
  }

  if (u.isGenericArg(a)) {
    let cur = findCur('a', a)
    newAGen = true

    if (cur && cur !== b) {
      return {
        gens,
        genericsErr: {
          id: 'G6',
          path: fullPath,
          previously: cur,
          now: b
        }}
    }
  }
  if (u.isGenericArg(b)) {
    let cur = findCur('b', b)
    newBGen = true

    if (cur && cur !== a) {
      return {
        gens,
        genericsErr: {
          id: 'G6',
          path: fullPath,
          previously: cur,
          now: a
        }}
    }
  }

  let findIdPath = (genType) =>
    _.prop(
      'path',
      _.find(
        _.where({path: u.arePathsMatching(fullPath)}),
        ids[genType]))

  return {
    gens: _.evolve({
      a: !newAGen
        ? _.I
        : _.prepend({key: a, val: b, idPath: findIdPath('a')}),
      b: !newBGen
        ? _.I
        : _.prepend({key: b, val: a, idPath: findIdPath('b')})
    }, gens),
    genericsErr: null
  }
}

let checkSchemaList = async svd =>
  (await compareSchemas(
    _.merge(svd, {path: ['$valsType'], prevPath: u.getFullPath(svd)}),
    svd.aVal,
    svd.bVal
  ))
    .err

let evolveMetas = (a, b) =>
  _.evolve({
    gens: {
      a: _.concat(b.gens.a),
      b: _.concat(b.gens.b)
    },
    ids: {
      a: _.concat(b.ids.a),
      b: _.concat(b.ids.b)
    }
  }, a)

let checkSchemaKeys = async (svd, keys) => {
  let [key, ...restKeys] = keys

  let {err, meta} = await compareSchemas(
    _.merge(svd, {path: [key], prevPath: u.getFullPath(svd)}),
    svd.aVal,
    svd.bVal
  )

  return (
    err ||
    (_.isEmpty(restKeys) ? null : checkSchemaKeys(evolveMetas(svd, meta), restKeys))
  )
}

let checkSchemaRecord = async svd => {
  let expectedKeys = _.keys(svd.aVal)
  let givenKeys = _.keys(svd.bVal)

  let missingKeys = _.without(givenKeys, expectedKeys)
  if (_.isFilled(missingKeys)) {
    return {
      id: 'R1',
      path: u.getFullPath(svd),
      keys: missingKeys,
      allKeys: expectedKeys
    }
  }

  return checkSchemaKeys(svd, expectedKeys)
}

let getUnionChoices =
  _.map(_.unless(_.is(String), _.prop('$choice')))

let checkSchemaUnion = async svd => {
  let expectedChoices = getUnionChoices(svd.aVal)
  let givenChoices = getUnionChoices(svd.bVal)

  let extraChoices = _.without(expectedChoices, givenChoices)
  if (_.isFilled(extraChoices)) {
    return {
      id: 'U1',
      path: u.getFullPath(svd),
      given: extraChoices,
      available: expectedChoices
    }
  }

  let job = async (idx, svd) => {
    let a = svd.aVal[idx]
    let b = svd.bVal[idx]
    let nextIdx = idx + 1

    if (a.$content && !b.$content) {
      return {
        id: 'U2',
        path: _.concat(u.getFullPath(svd), [idx, '$content']),
        expected: (await getSchemaType(a.$content)).val
      }
    }
    let {err, meta} = await compareSchemas(
      _.merge(svd, {path: ['$content'], prevPath: _.append(idx, u.getFullPath(svd))}),
      a,
      b
    )

    if (err) return err
    if (nextIdx > svd.aVal.length - 1) return null
    return job(nextIdx, evolveMetas(svd, meta))
  }

  return job(0, svd)
}

let checkSchemaFunction = async svd => {
  return checkSchemaKeys(svd, ['$input', '$output'])
}

let updateIds = ({a, b, path, prevPath, ids}, aDiscovery, bDiscovery) => {
  let fullPath = u.getFullPath({path, prevPath})

  let shouldAdd = key =>
    _.path([0, key, 'path'], ids) !== fullPath

  let aIds = aDiscovery.choice === 'WithId' && shouldAdd('a')
    ? _.prepend({
      id: _.path(path, a).$id,
      path: fullPath
    }, ids.a)
    : ids.a

  let bIds = bDiscovery.choice === 'WithId' && shouldAdd('b')
    ? _.prepend({
      id: _.path(path, b).$id,
      path: fullPath
    }, ids.b)
    : ids.b

  return {a: aIds, b: bIds}
}

let compareSchemas = async (opts = {}, a, b) => {
  let {
    path = [],
    prevPath = [],
    gens = {a: [], b: []},
    ids = {a: [], b: []}
  } = opts

  let res = (err, meta) => ({err, meta})

  if (isG2(path, b)) {
    return res(g2(path, prevPath), null)
  }

  let aDiscovery = await discoverVal(_.path(path, a))

  if (aDiscovery.choice === 'NotFoundWithId') {
    return {
      id: 'C1',
      path: u.getFullPath({prevPath, path}),
      schemaId: aDiscovery.val.$id,
      schemaVersion: aDiscovery.val.$version
    }
  }

  let bDiscovery = await discoverVal(_.path(path, b))

  if (bDiscovery.choice === 'NotFoundWithId') {
    return {
      id: 'G5',
      path: u.getFullPath({prevPath, path}),
      givenId: bDiscovery.val.$id,
      givenVersion: bDiscovery.val.$version
    }
  }

  let aVal = aDiscovery.val
  let bVal = bDiscovery.val
  let aType = await getSchemaType(aVal)
  let bType = await getSchemaType(bVal)
  let updatedIds = updateIds({path, prevPath, a, b, ids}, aDiscovery, bDiscovery)

  let initSvd = {path, prevPath, aVal, bVal, aType, bType, gens, ids: updatedIds}
  // _.logS('initSvd', initSvd);

  let {genericsErr, gens: updatedGens} = checkGenerics(initSvd)
  if (genericsErr) {
    return res(genericsErr, initSvd)
  }

  let svd = _.assoc('gens', updatedGens, initSvd)
  // _.logS('svd', svd);

  let getErr = () => {
    let t = aType.val

    if (isSchemaG3(svd)) return schemaG3(svd)
    if (t === 'List') return checkSchemaList(svd)
    if (t === 'Record') return checkSchemaRecord(svd)
    if (t === 'Union') return checkSchemaUnion(svd)
    if (t === 'Function') return checkSchemaFunction(svd)
    return null
  }

  return res(await getErr(), svd)
}

let checkFunction = async vd => {
  let {err} = await compareSchemas(
    _.pick(['path', 'prevPath', 'gens', 'ids'], vd),
    vd.sVal,
    vd.gVal
  )

  return err
}

let isG3 = ts => {
  let gt = ts.givenType.val
  let st = ts.schemaType.val

  switch (st) {
    case 'Float':
      return !(gt === 'Int' || gt === 'Float')
    case 'Union':
      return gt !== 'UnionMember'
    default:
      return gt !== st
  }
}

let g3 = vd => ({
  id: 'G3',
  path: u.getFullPath(vd),
  given: vd.ts.givenType.val,
  expected: vd.ts.schemaType.val === 'Union' ? 'UnionMember' : vd.ts.schemaType.val
})

let isRef = vd =>
  _.has('$id', vd.sVal)

let checkRef = async vd => {
  let meta = vd.ts.schemaType.meta
  if (!meta) {
    return {
      id: 'C1',
      path: u.getFullPath(vd),
      schemaId: vd.sVal.$id,
      schemaVersion: vd.sVal.$id
    }
  }

  let refSchema = await getSchemaByIdVer(meta.schema)
  let refGenerics = u.findGenerics(refSchema)

  let replacedSchema = refGenerics.reduce((acc, {key, path}) => {
    let type = vd.sVal.$generics[key]
    return _.assocPath(path, type, acc)
  }, refSchema)

  let {err} = await getErrorReport(vd, replacedSchema, vd.gVal)

  return err
}

let getErrorReport = async (opts = {}, schema, given) => {
  let {
    path = [],
    prevPath = [],
    arrayMode = false,
    gens = {a: [], b: []},
    ids = {a: [], b: []}
  } = opts

  let res = (err, meta) => ({err, meta})

  if (isG2(path, given)) {
    return res(g2(path, prevPath))
  }

  let givenDiscovery = await discoverVal(_.path(path, given))

  if (givenDiscovery.choice === 'NotFoundWithId') {
    return res({
      id: 'G5',
      path: u.getFullPath({prevPath, path}),
      givenId: givenDiscovery.val.$id,
      givenVersion: givenDiscovery.val.$version
    })
  }

  let schemaDiscovery = await discoverVal(arrayMode ? schema : _.path(path, schema))

  if (schemaDiscovery.choice === 'NotFoundWithId') {
    return res({
      id: 'C1',
      path: u.getFullPath({prevPath, path}),
      schemaId: schemaDiscovery.val.$id,
      schemaVersion: schemaDiscovery.val.$version
    })
  }

  let ts = await getTypes(arrayMode ? {[path]: schema} : schema, given, path)

  let updatedIds = updateIds(
    {path, prevPath, a: schema, b: given, ids},
    schemaDiscovery,
    givenDiscovery
  )

  // "validation data"
  let initVd = {
    ts,
    path,
    prevPath,
    gVal: givenDiscovery.val,
    sVal: schemaDiscovery.val,
    gens,
    ids: updatedIds
  }
  // _.logS('initVd', initVd);

  let {genericsErr, gens: updatedGens} = checkGenerics({
    aType: ts.schemaType,
    bType: ts.givenType,
    path,
    prevPath,
    gens,
    ids: updatedIds
  })
  if (genericsErr) {
    return res(genericsErr, initVd)
  }

  let vd = _.assoc('gens', updatedGens, initVd)
  // _.logS('vd', vd);

  let getErr = () => {
    let t = ts.schemaType.val

    if (u.isGenericArg(vd.sVal || '')) return null
    if (isG3(ts)) return g3(vd)
    if (isRef(vd)) return checkRef(vd)
    if (t === 'Union') return checkUnion(vd)
    if (t === 'List') return checkList(vd)
    if (t === 'Record') return checkRecord(vd)
    if (t === 'Function') return checkFunction(vd)
    return null
  }

  return res(await getErr(), vd)
}

module.exports = {getErrorReport}
