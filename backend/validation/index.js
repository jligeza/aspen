module.exports = {
  /*
   * Validates data against schema - core functonality of the system.
   */
  validateData: require('./data').validate
}
