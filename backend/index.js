let express = require('express')
let expressWs = require('express-ws')
let bodyParser = require('body-parser')
let core = require('core')

let app = express()
expressWs(app)

app.use(bodyParser.json())

let sendError = res => error => {
  console.log('******* error ********* \n', error)
  res.status(400).json(error || {})
}

app.post('/api', (req, res) => {
  core.executeTask(req.body)
    .then(result => res.json(result))
    .catch(sendError(res))
})

app.ws('/ws', (ws, req) => {
  ws.on('message', (msg) => {
    ws.send('echo: ' + msg)
  })
})

let startServer = () => {
  let port = 4000
  console.log('Listening on port ' + port)
  app.listen(port)
}

core.initialize().then(startServer)
