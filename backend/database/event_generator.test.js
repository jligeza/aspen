jest.mock('fs-extra')
jest.mock('database/db_init')

let _ = require('generic_utils')
let u = require('test_utils')
let fakeDate = require('test_utils_mocks').time.fakedDateStr
let {saveEventGenerator} = require('./event_generator')

let mergeTemplate = (templateKey, opts) => {
  let t = u.refs[templateKey]

  return _.merge({templateId: t.$id, version: t.$version}, opts)
}

let cases = [{
  desc: 'simple ok',
  input: mergeTemplate('http', {
    $config: {
      routes: [{url: '/', method: 'Get'}],
      port: 7777}
  }),
  output: {
    ok: true,
    val: {
      id: '0',
      version: 1,
      createdAt: fakeDate,
      $config: {
        routes: [{url: '/', method: 'Get'}],
        port: 7777}}}
}, {
  desc: 'no config',
  input: mergeTemplate('http', {
    $config: {}
  }),
  output: {
    ok: true,
    val: {
      id: '0',
      version: 1,
      createdAt: fakeDate,
      $config: {
        routes: [],
        port: 0}}}
}, {
  desc: 'partial config',
  input: mergeTemplate('http', {
    $config: {
      routes: [{url: '/', method: 'Get'}]}
  }),
  output: {
    ok: true,
    val: {
      id: '0',
      version: 1,
      createdAt: fakeDate,
      $config: {
        routes: [{url: '/', method: 'Get'}],
        port: 0}}}
}]

u.simpleTests('event generator', saveEventGenerator, cases)
