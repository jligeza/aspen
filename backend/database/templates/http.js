let _ = require('generic_utils')
let express = require('express')
let bodyParser = require('body-parser')
let isPortAvailable = require('is-port-available')

/*
 * Data and emittent info should be dispatched.
 */

let code = ({config, dispatch}) => {
  let statusMap = {
    Success: 200,
    Error: 500
  }

  let port = config.port
  let routes = config.routes

  let server = express()
  server.use(bodyParser.json())

  routes.forEach(route =>
    server[route.method.$choice.toLowerCase()](route.url, (req, res) =>
      dispatch({
        result: {
          payload: req.body,
          path: req.path
        },
        fallback: () => {
          res.status(statusMap.Error).send('fallback')
        },
        callback: ({status, payload}) => {
          try {
            res.status(statusMap[status.$choice || status]).send(payload)
          } catch (e) {
            res.status(statusMap.Error).send('fallback 2')
          }
        }
      })
    ))

  return {
    run: () => new Promise(resolve => {
      try {
        isPortAvailable(port)
          .then(isIt => {
            if (isIt) {
              server.listen(port, httpServer =>
                resolve(_.Right(() => httpServer.close())))
            } else {
              resolve(_.Left(isPortAvailable.lastError))
            }
          })
      } catch (e) {
        resolve(_.Left(e))
      }
    }),
    data: {server, port}
  }
}

module.exports = {code}
