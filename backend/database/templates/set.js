let _ = require('generic_utils')
let {findById} = require('database/find')

async function code ({msg, joints}) {
  if (joints.length) {
    return joints.reduce(async (acc, [from, to]) => {
      let val = _.is(Array, from)
        ? _.path(from, await acc)
        : _.path(['data', 'value'], await findById({id: from.$id, version: from.$version}))

      return _.assocPath(to, val, await acc)
    }, msg)
  } else {
    return _.assoc('return', msg.result, msg)
  }
}

module.exports = { code }
