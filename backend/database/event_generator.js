let _ = require('generic_utils')
let {resolveTypeDepsInSchema} = require('database/type')
let s = require('./shared')
let {validateConfig} = require('./config')
let {findTemplate} = require('./template')
let {findLatestVersions} = require('./db_queries')

/*
 * READ
 */

let readEventGenerator = (id, version) =>
  s.readEntity({type: 'EventGenerator', id, version})

let fetchEventGenerators = () => {
  return findLatestVersions('eventGenerator')
}

/*
 * SAVE
 */

let saveReqKeys = ['templateId', 'version', '$config']
let saveOptKeys = ['version', 'id']
let saveKeys = _.concat(saveReqKeys, saveOptKeys)

let keysError = query => {
  let keys = _.keys(query)

  let extra = _.without(saveKeys, keys)
  if (_.isFilled(extra)) return `unknown keys: ${extra.join(',')}`

  let missing = _.without(keys, saveReqKeys)
  if (_.isFilled(missing)) return `missing required keys: ${missing.join(',')}`
}

// If some key in the config is missing,
// pick a default value from the event generator schema.
let addDefaultsToConfig = _.curry((defaults, config) =>
  _.pipe(
    _.toPairs,
    _.map(([key, val]) => [key, _.isNil(config[key]) ? val : config[key]]),
    _.fromPairs
  )(defaults))

let updateEventGenerator = entity =>
  s.initUpdateEntity({
    readEntity: readEventGenerator,
    id: entity.id
  })
    .then(maybeFetchedEntity => {
      return !maybeFetchedEntity.ok
        ? maybeFetchedEntity
        : s.finishUpdateEntity({
          entity,
          fetchedEntity: maybeFetchedEntity.val,
          type: 'EventGenerator'
        })
    })

let saveNewEventGenerator = entity => {
  let type = 'EventGenerator'

  return s.finishSaveNewEntity({type}, s.initSaveNewEntity({type, entity}))
}

let saveEventGenerator = async query => {
  let keysError_ = keysError(query)

  if (keysError_) return _.Left(keysError_)

  let template = _.prop('data', await findTemplate({id: query.templateId, version: query.version}))
  if (!template) {
    return _.Left(`no event generator template with id ${query.templateId}`)
  }

  let schema = await resolveTypeDepsInSchema(template.schema.$config)

  let {ok, val} = await validateConfig({
    config: addDefaultsToConfig(template.schema.$defaults, query.$config),
    schema
  })

  if (!ok) return _.Left(val)
  let config = val

  let newEvgen = {
    schema: {
      $config: config,
      $template: {
        $id: template.id,
        $version: template.version
      }
    }
  }

  let saveOrUpdate = _.has('id', query)
    ? () => updateEventGenerator(_.assoc('id', query.id, newEvgen))
    : () => saveNewEventGenerator(newEvgen)

  let maybeSaved = await saveOrUpdate()

  return !maybeSaved.ok
    ? maybeSaved
    : _.Right({
      id: maybeSaved.val.id,
      version: maybeSaved.val.version,
      createdAt: maybeSaved.val.createdAt,
      $config: config
    })
}

module.exports = {
  fetchEventGenerators,
  readEventGenerator,
  saveEventGenerator
}
