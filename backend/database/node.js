let _ = require('generic_utils')
let s = require('./shared')
let dbQueries = require('./db_queries')

function createNode (node) {
  return s.finishSaveNewEntity({type: 'Node'}, node)
}

function updateNode (node) {
  return _.Left('TODO update valid node')
}

async function saveNode (node) {
  if (node.id) {
    return updateNode(node)
  } else {
    return createNode(node)
  }
}

function readNode ({id, version}) {
  return s.readEntity({type: 'Node', id, version})
}

function listNodes () {
  return dbQueries.findAll('node')
}

module.exports = {
  listNodes,
  saveNode,
  readNode
}
