let _ = require('generic_utils')
let {findTemplate, findTemplateVersions} = require('./template')
let {getModelsNames} = require('./db_init')
let dbQueries = require('./db_queries')

let getModelsNamesToSearch = () =>
  getModelsNames().filter(a => a !== 'template')

let findInStorage = ({id, version}) => {
  let job = async (idx, names) => {
    let name = names[idx]
    let result = await dbQueries.findIdVersion(name, id, version)

    return result
      ? {type: _.upperFirst(name), data: result}
      : idx + 1 < names.length
        ? job(idx + 1, names)
        : null
  }

  return job(0, getModelsNamesToSearch())
}

let findVersionsInStorage = id => {
  let job = async (idx, names) => {
    let a = await dbQueries.findAll(names[idx], {where: {id}})

    return _.isFilled(a)
      ? _.pluck('version', a)
      : idx + 1 < names.length
        ? job(idx + 1, names)
        : null
  }

  return job(0, getModelsNamesToSearch())
}

let findById = async ({id, version}) => {
  let template = _.prop('data', await findTemplate({id, version}))

  if (template) {
    return {type: 'Template', data: template}
  } else {
    return findInStorage({id, version})
  }
}

let findVersionsById = async id => {
  return await findTemplateVersions(id) || findVersionsInStorage(id)
}

let findAllTypes = () =>
  dbQueries.findAll('type', {order: [['createdAt', 'DESC']]})
    .then(_.uniqBy(_.prop('id')))

module.exports = {findById, findVersionsById, findAllTypes}
