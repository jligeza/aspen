let _ = require('generic_utils')
let {getModels} = require('./db_init')
let u = require('./utils')

let findIdVersion = async (modelType, id, version) => {
  let model = getModels()[modelType]

  return _.isNil(version)
    ? model.findOne({where: {id}, order: [['createdAt', 'DESC']]})
    : model.findOne({where: {id, version}})
}

let findAll = (modelType, constraints) => {
  let model = getModels()[modelType]

  return model.findAll(constraints)
}

let findLatestVersions = modelType => {
  return findAll(modelType)
    .then(a =>
      _.map(u.findLatestVersion, _.values(_.groupBy(_.prop('id'), a))))
}

function findLink ({from}) {
  let model = getModels().link

  return model.findOne({where: {from}, order: [['createdAt', 'DESC']]})
}

let update = (modelType, entity) => {
  let model = getModels()[modelType]

  return model.create(entity)
}

let pickCreationPayload = (type, entity) => {
  switch (type) {
    case 'eventGenerator':
      return entity

    case 'type':
      return _.pick(['schema'], entity)

    case 'nameTag':
      return _.pick(['name', 'targetId', 'targetVersion', 'schemaPath'], entity)

    default:
      return entity
  }
}

let create = (modelType, entity) => {
  return update(
    modelType,
    _.assoc('deletedAt', null, pickCreationPayload(modelType, entity))
  )
}

module.exports = {
  create,
  findAll,
  findIdVersion,
  findLatestVersions,
  findLink,
  update
}
