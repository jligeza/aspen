let _ = require('generic_utils')
let {join} = require('path')
let Sequelize = require('sequelize')
let S = Sequelize
let sequelize

let models = {}

let types = {
  id: S.UUIDV4,
  version: S.INTEGER,
  timestamp: S.DATE,
  schema: S.JSON,
  error: S.JSON
}

let attrTemplates = {
  id: () => ({
    type: types.id,
    defaultValue: S.DataTypes.UUIDV4,
    primaryKey: true
  }),
  version: () => ({
    type: types.version,
    defaultValue: 1,
    primaryKey: true
  }),
  schema: () => ({
    type: types.schema,
    allowNull: false
  }),
  createdAt: () => ({
    type: types.timestamp,
    defaultValue: S.DataTypes.NOW,
    allowNull: false
  }),
  deletedAt: () => ({
    type: types.timestamp
  })
}

let defaultAttrs = () => ({
  id: attrTemplates.id(),
  version: attrTemplates.version(),
  schema: attrTemplates.schema(),
  createdAt: attrTemplates.createdAt(),
  deletedAt: attrTemplates.deletedAt()
})

let defaultOpts = () => ({
  timestamps: false
})

let defineModels = s => {
  models.type = s.define('type', defaultAttrs(), defaultOpts())

  models.eventGenerator = s.define('eventGenerator', defaultAttrs(), defaultOpts())

  models.nameTag = s.define('nameTag', {
    id: attrTemplates.id(),
    version: attrTemplates.version(),
    targetId: {
      type: types.id,
      allowNull: false,
      unique: 'target'
    },
    targetVersion: {
      type: types.version,
      allowNull: false,
      unique: 'target'
    },
    schemaPath: {
      type: S.JSON,
      allowNull: false,
      unique: 'target'
    },
    name: {
      type: S.STRING,
      allowNull: false
    },
    createdAt: attrTemplates.createdAt(),
    deletedAt: attrTemplates.deletedAt()
  }, {
    timestamps: false
  })

  models.link = s.define('link', {
    id: attrTemplates.id(),
    version: attrTemplates.version(),
    error: types.error,
    from: {
      type: types.id,
      allowNull: false
    },
    to: types.id,
    joints: attrTemplates.schema(),
    createdAt: attrTemplates.createdAt(),
    deletedAt: attrTemplates.deletedAt()
  }, {
    timestamps: false
  })

  models.node = s.define('node', {
    id: attrTemplates.id(),
    version: attrTemplates.version(),
    targetId: {
      type: types.id,
      allowNull: false
    },
    targetVersion: {
      type: types.version,
      allowNull: false
    },
    createdAt: attrTemplates.createdAt(),
    deletedAt: attrTemplates.deletedAt()
  }, defaultOpts())

  models.const = s.define('const', {
    id: attrTemplates.id(),
    version: attrTemplates.version(),
    value: attrTemplates.schema(),
    createdAt: attrTemplates.createdAt(),
    deletedAt: attrTemplates.deletedAt()
  }, defaultOpts())
}

let init = async () => {
  sequelize = new S('aspen', null, null, {
    dialect: 'sqlite',
    storage: join('database', 'db.sqlite'),
    operatorsAliases: S.Op
  })
  let s = sequelize

  await s.authenticate()

  defineModels(s)

  // await s.sync({force: true})
  await s.sync()

  return {sequelize, models}
}

let getModels = () => models

let getModelsNames = () =>
  _.keys(models)

module.exports = {init, getModels, getModelsNames}
