let _ = require('generic_utils')
let {findLink} = require('./link')
let {readNode} = require('./node')
let coreNodes = require('./core_nodes')
let {findTemplate} = require('./template')

async function getNewMsg (node, link, msg) {
  switch (node.targetId) {
    case coreNodes.byName.set:
      let template = await findTemplate({id: node.targetId})

      return template && template.code({msg, joints: link.joints})

    default:
      // TODO handle more nodes
      return msg
  }
}

async function findConnection (from, msg) {
  let link = await findLink({from})

  if (!link) return

  let node = await readNode({id: link.to})

  if (!node) return

  let newMsg = await getNewMsg(node, link, msg)

  return {
    newMsg,
    next: () => findConnection(node.id, newMsg)
  }
}

module.exports = {
  findConnection
}
