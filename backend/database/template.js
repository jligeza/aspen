let _ = require('generic_utils')
let fs = require('fs-extra')
let {join} = require('path')

/*
 * Note: templates are read from disk only once, then served from cache.
 */

let _templates = {}

// This weird function replaces "require", which cannot be mocked
// for non-existing files (tests break).
let readJs = async fileName =>
  _.prop(
    'code',
    eval( // eslint-disable-line
      _.toString(
        await fs.readFile(join(__dirname, 'templates', fileName + '.js')))))

let resolveTemplatePaths = async paths => {
  let templates = await Promise.all(_.map(
    p => fs.readJson(join(__dirname, 'templates', p)),
    paths
  ))

  await Promise.all(templates.map(data =>
    Promise.all(data.map(async ({name, id, version}) => {
      if (!_templates[id]) {
        _templates[id] = {}
      }
      _templates[id][version] = {
        data: _.find(_.whereEq({version}), data),
        code: await readJs(name)
      }
    }))))

  return _templates
}

let fetchTemplates = async () =>
  _.isFilled(_templates)
    ? _templates
    : fs.readdir(join(__dirname, 'templates'))
      .then(_.pipe(_.filter(_.endsWith('.json')), resolveTemplatePaths))
      .catch(_.Left)

let findTemplate = async ({id, version}) => {
  let searchFn = version
    ? _.path([id, version])
    : _.pipe(_.prop(id), _.values, _.last)

  return searchFn(await fetchTemplates())
}

let findTemplateVersions = async id => {
  let versions = _.keys((await fetchTemplates())[id] || {})
  return _.isEmpty(versions) ? null : versions
}

function listTemplates () {
  return fetchTemplates()
    .then(_.map(_.pluck('data')))
}

module.exports = {
  fetchTemplates,
  findTemplate,
  findTemplateVersions,
  listTemplates
}
