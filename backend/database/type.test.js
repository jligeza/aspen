jest.mock('fs-extra')
jest.mock('database/db_init')

let _ = require('generic_utils')
let u = require('test_utils')
let {listTypes, saveType, readTypeOpt, resolveTypeDepsInSchema} = require('database/type')
let {simpleTests} = require('test_utils')

let Type = args =>
  _.merge(_.dissoc('type', u.Type(args)), {id: '0'})

let typeSaveCases = [{
  desc: 'simple ok',
  data: {schema: 'Int'},
  out: _.Right(Type({schema: 'Int'}))
}, {
  desc: 'error - no such id',
  data: {
    schema: u.refs.unknownId
  },
  out: _.Left({validationErrors: [{id: 'S2', path: []}]})
}, {
  desc: 'error - no such version',
  data: {
    schema: u.refs.unknownVersion
  },
  out: _.Left({validationErrors: [{id: 'S2', path: []}]})
}]

test('readType - resolve ids', () => readTypeOpt({}, u.refs.multi.$id)
  .then(val => {
    expect(val).toBeTruthy()
    expect(_.keys(val)).toEqual([u.joinIdVersion('multi'), u.joinIdVersion('string')])
  })
)

test('listTypes', async () => {
  let types = await listTypes()
  expect(_.isFilled(_.keys(types))).toBe(true)
})

simpleTests('readType - resolveTypeDepsInSchema', resolveTypeDepsInSchema, [{
  desc: 'simple',
  input: {a: u.refs.int},
  output: {
    $main: {a: u.refs.int},
    $rest: {[u.joinIdVersion('int')]: u.getMockSchema('int')}
  }
}])

let testTypeSaveCase = (c, index) =>
  test(u.prettyTestName(index, 'database - type save', c.desc), () =>
    saveType(c.data)
      .then(result => expect(result).toEqual(c.out)))

describe('database', () => {
  beforeAll(u.time.fake)
  afterAll(u.time.restore)

  _.forEachIndexed(testTypeSaveCase, typeSaveCases)
})
