let s = require('./shared')
let dbQueries = require('./db_queries')

function readConst ({id, version}) {
  return s.readEntity({
    type: 'Const',
    id,
    version
  })
}

function saveConst (data) {
  if (data.id) {
    return s.updateEntity({
      readEntity: readConst,
      id: data.id,
      version: data.version,
      type: 'Const',
      entity: data
    })
  } else {
    return s.finishSaveNewEntity({ type: 'Const' }, data)
  }
}

function listConsts () {
  return dbQueries.findAll('const')
}

module.exports = {
  readConst,
  saveConst,
  listConsts
}
