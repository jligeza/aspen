let _ = require('generic_utils')
let {fetchEventGenerators, readEventGenerator} = require('./event_generator')
let {findTemplate} = require('./template')

let fetchRoots = () =>
  fetchEventGenerators()
    .then(_.map(matchTemplate))
    .then(_.promiseAll)

async function matchTemplate (instanceData) {
  let rawTemp = instanceData.schema.$template
  let template = await findTemplate({id: rawTemp.$id, version: rawTemp.$version})

  return {template, instanceData}
}

async function findRoot (id, version) {
  let evgen = await readEventGenerator(id, version)

  if (!evgen) return evgen

  return matchTemplate(evgen)
}

module.exports = {fetchRoots, findRoot}
