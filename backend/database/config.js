let _ = require('generic_utils')
let {validateData} = require('validation')

/*
 * config: data.
 * completeSchema: schema with resolved types of a template's config ({$main, $rest}).
 *
 * returns: {configPropA: [...errors], configPropB: [...errors]}
 */
let validateConfigProps = ({config, schema}) =>
  _.reduce(async (acc, [k, v]) => {
    let propSchema = _.evolve({$main: _.pick([k])}, schema)

    acc[k] = await validateData({[k]: v}, propSchema)

    return acc
  }, {}, _.toPairs(config))

const allValsEmpty = _.pipe(_.values, _.promiseAll, _.all(_.isEmpty))

let validateConfig = async ({config, schema}) => {
  const validationErrors = validateConfigProps({config, schema})

  return await allValsEmpty(validationErrors)
    ? _.Right(config)
    : _.Left(validationErrors)
}

module.exports = {validateConfig}
