let _ = require('generic_utils')
let s = require('./shared')
let {findById} = require('./find')
let dbQueries = require('./db_queries')

function readLink ({id, version}) {
  return s.readEntity({type: 'Link', id, version})
}

function findLink ({from}) {
  return dbQueries.findLink({from})
}

function createValidLink (link) {
  return s.finishSaveNewEntity({type: 'Link'}, link)
}

function updateValidLink (link) {
  return _.Left('TODO updateValidLink')
}

async function saveLink (link) {
  const source = await findById({id: link.from})

  if (!source) return _.Left('source not found')

  const target = await findById({id: link.to})

  if (!target) return _.Left('target not found')

  if (link.id) {
    return updateValidLink(link)
  } else {
    return createValidLink(link)
  }
}

function listLinks () {
  return dbQueries.findAll('link')
}

module.exports = {
  listLinks,
  readLink,
  saveLink,
  findLink
}
