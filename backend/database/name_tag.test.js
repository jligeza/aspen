let {readSchemaPath} = require('./name_tag')
let {simpleTests, UnionMember} = require('test_utils')

let pathCases = [{
  desc: 'single level',
  input: [['a'], {a: 'Int'}],
  output: 'Int'
}, {
  desc: 'multi level',
  input: [['a', 'b', 'c'], {a: {b: {c: 'Int'}}}],
  output: 'Int'
}, {
  desc: 'union members addressed by uIds rather than indices',
  input: [['B'], ['A', 'B']],
  output: 'B'
}, {
  desc: 'union member content',
  input: [['B', '$content', 'a'], ['A', UnionMember('B', {a: 'Int'})]],
  output: 'Int'
}, {
  desc: 'bad single level',
  input: [['a'], {b: 'Int'}],
  output: undefined
}, {
  desc: 'bad multi level',
  input: [['a', 'b'], {a: {c: 'Int'}}],
  output: undefined
}, {
  desc: 'no schema',
  input: [['a', 'b'], undefined],
  output: undefined
}, {
  desc: 'no path',
  input: [undefined, {a: 'Int'}],
  output: {a: 'Int'}
}]

simpleTests('name tag - read schema path', readSchemaPath, pathCases)
