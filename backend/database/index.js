let _ = require('generic_utils')
let {readTypeOpt} = require('./type')
let {saveType, listTypes} = require('./type')
let {saveEventGenerator} = require('./event_generator')
let {saveNameTag, listNameTags} = require('./name_tag')
let {listTemplates} = require('./template')
let {fetchRoots, findRoot} = require('./roots')
let {saveNode, listNodes} = require('./node')
let {saveLink, listLinks} = require('./link')
let {listActiveNodes} = require('./active_nodes')
let {findConnection} = require('./connection')
let {saveConst, listConsts} = require('./const')
let dbInit = require('./db_init')

let writeCallbacks = []

function subscribeWrite (cb) {
  writeCallbacks.push(cb)
}

function unsubscribeWrite (cb) {
  writeCallbacks = writeCallbacks.filter(a => a !== cb)
}

let querySchema = {
  requiredKeys: ['type', 'payload']
}

let validateQuery = query => {
  let keyErrors = _.checkKeys(querySchema.requiredKeys, _.keys(query))

  let err = _.head(keyErrors)
  return err
    ? _.Left(`${err.type} key: ${err.key}`)
    : _.Right(null)
}

let unknownType = type => _.Left(`unknown type: ${type}`)

function write (query) {
  return _write(query)
    .then(result => {
      writeCallbacks.forEach(cb => cb(query, result))

      return result
    })
}

let _write = async query => {
  let valid = validateQuery(query)
  if (!valid.ok) {
    return valid
  }

  let {type, payload} = query

  switch (type) {
    case 'node':
      return saveNode(payload)

    case 'link':
      return saveLink(payload)

    case 'eventGenerator':
      return saveEventGenerator(payload)

    case 'type':
      return saveType(payload)

    case 'nameTag':
      return saveNameTag(payload)

    case 'const':
      return saveConst(payload)

    default:
      return unknownType(type)
  }
}

let read = async query => {
  let valid = validateQuery(query)
  if (!valid.ok) {
    return valid
  }

  let {type, payload} = query

  switch (type) {
    case 'type':
      return readTypeOpt(_.pick(['version'], payload), payload.id)

    default:
      return unknownType(type)
  }
}

let list = async query => {
  let valid = validateQuery(query)
  if (!valid.ok) {
    return valid
  }

  let {type, payload} = query

  switch (type) {
    case 'link':
      return listLinks(payload)

    case 'node':
      return listNodes(payload)

    case 'const':
      return listConsts(payload)

    case 'activeNode':
      return listActiveNodes(payload)

    case 'type':
      return listTypes(payload)

    case 'nameTag':
      return listNameTags(payload)

    case 'template':
      return listTemplates(payload)

    default:
      return unknownType(type)
  }
}

let init = () => dbInit.init()

module.exports = {
  init,
  fetchRoots,
  findRoot,
  findConnection,
  list,
  read,
  write,
  subscribeWrite,
  unsubscribeWrite
}
