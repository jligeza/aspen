let _ = require('generic_utils')
let {validate} = require('db_validation')
let dbQueries = require('./db_queries')
let lowerFirst = require('lodash.lowerfirst')

let typeToStorageKey = lowerFirst

let readEntity = async ({type, id, version}) =>
  dbQueries.findIdVersion(typeToStorageKey(type), id, version)

let readType = ({id, version}) => readEntity({type: 'Type', id, version})

let initUpdateEntity = ({readEntity, id, version}) =>
  readEntity({id, version})
    .then(a =>
      _.isNil(a)
        ? _.Left(`no entity with id ${id} to update`)
        : _.Right(a))

let omitBaseProps = _.omit(['id', 'version', 'createdAt', 'deletedAt'])

let finishUpdateEntity = ({entity, fetchedEntity, type}) => {
  let nonBaseProps = _.map(a => () => a, omitBaseProps(entity))

  let toSave = _.evolve(_.merge(nonBaseProps, {
    version: _.inc,
    createdAt: () => new Date()
  }), fetchedEntity.dataValues)

  return dbQueries.update(typeToStorageKey(type), toSave).then(_.Right)
}

let updateEntity = ({id, version, readEntity, entity, type}) =>
  initUpdateEntity({readEntity, id, version})
    .then(maybeFetchedEntity => {
      if (!maybeFetchedEntity.ok) return maybeFetchedEntity

      return finishUpdateEntity({
        entity,
        fetchedEntity: maybeFetchedEntity.val,
        type
      })
    })

let initSaveNewEntity = ({type, entity}) => ({
  id: undefined,
  type,
  version: 1,
  schema: entity.schema
})

let finishSaveNewEntity = _.curry(({type}, toSave) =>
  dbQueries.create(typeToStorageKey(type), toSave).then(_.Right))

let saveNewEntity = ({entity, type}) =>
  finishSaveNewEntity({type}, initSaveNewEntity({type, entity}))

let trySaveOrUpdate = ({id, entity, validationErrors, save, update}) =>
  !_.isEmpty(validationErrors)
    ? _.Left({validationErrors})
    : _.isNil(id)
      ? save()
      : update()

let saveEntity = ({entity, type, readEntity}) => {
  let {id, version} = entity

  return validate(type, entity)
    .then(validationErrors =>
      trySaveOrUpdate({
        id,
        entity,
        validationErrors,
        save: () =>
          saveNewEntity({entity, type}),
        update: () =>
          updateEntity({id, version, entity, type, readEntity})
      }))
}

module.exports = {
  finishSaveNewEntity,
  finishUpdateEntity,
  initSaveNewEntity,
  initUpdateEntity,
  readEntity,
  readType,
  saveEntity,
  saveNewEntity,
  trySaveOrUpdate,
  updateEntity
}
