let {fetchEventGenerators} = require('./event_generator')
let {findLink} = require('./link')
let {readNode} = require('./node')

let getInitPack = () => ({
  eventGenerators: [],
  links: [],
  nodes: []
})

async function listActiveNodes () {
  let eventGenerators = await fetchEventGenerators()

  async function linkChain (pack, idx, from) {
    let link = await findLink({ from })

    if (!link) return createPackByEventGenerators(pack, idx + 1)
    pack.links.push(link)

    let node = await readNode({ id: link.to })

    if (!node) return createPackByEventGenerators(pack, idx + 1)
    pack.nodes.push(node)

    return linkChain(pack, idx, node.id)
  }

  async function createPackByEventGenerators (pack, idx = 0) {
    pack = pack || getInitPack()

    let evgn = eventGenerators[idx]

    if (!evgn) return pack
    pack.eventGenerators.push(evgn)

    return linkChain(pack, idx, evgn.id)
  }

  return createPackByEventGenerators()
}

module.exports = {listActiveNodes}
