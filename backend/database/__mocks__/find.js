let _ = require('generic_utils')
let {types, templates} = require('test_utils_mocks')

let allItems = _.merge(types, templates)

let file = (regex, path, cb) => {
  let findings = _.filter(({id}) => regex.test(id), _.values(allItems))

  let paths = findings.map(a => `__mock__/${a.type}/${a.id}`)

  cb(paths)
}

module.exports = {file}
