let _ = require('generic_utils')
let {types, templates} = require('test_utils_mocks')

let resolve = a => Promise.resolve(a)
let reject = a => Promise.reject(a)

let getMockItem = items => _.pipe(
  _.split('/'),
  _.last,
  _.propOr({code: 'ENOENT'}, _.__, items))

let pathToType = getMockItem(types)

let pathToTemplate = getMockItem(templates)

let emptyCodeFileBuf = Buffer.from(`module.exports = {code: null}`, 'utf8')

let mockPathToData = p => {
  let [type, id] = _.tail(p.split('/'))

  switch (type) {
    case 'Type':
      return types[id]
    default:
      return null
  }
}

let pathToData = _.cond([
  [_.startsWith('__mock__'), mockPathToData],

  [_.endsWith('id_counter'), () => Buffer.from('0')],
  [_.endsWith('.js'), () => emptyCodeFileBuf],

  [_.contains('templates/'), pathToTemplate],
  [_.contains('types/'), pathToType],

  [_.true, () => null]
])

let noSuchFile = _.pipe(_.prop('code'), _.eq('ENOENT'))

let onReaddir = async path => {
  let ew = _.endsWith(_.__, path)

  if (ew('templates')) return _.keys(templates)
  if (ew('types')) return _.keys(types)
  return []
}

module.exports = {
  types,
  templates,

  outputJson: () => resolve(null),
  outputFile: () => resolve(null),
  writeFile: () => resolve(null),

  readdir: onReaddir,

  readJson: _.pipe(pathToData, _.ifElse(noSuchFile, reject, resolve)),
  readFile: _.pipe(pathToData, resolve)
}
