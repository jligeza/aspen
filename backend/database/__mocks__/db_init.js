let {time} = require('test_utils')
let mocks = require('test_utils_mocks')
let _ = require('generic_utils')

let getEntity = (id, ver, source) => {
  return ver
    ? (source[id] || []).find(a => a.version === ver)
    : _.last(source[id] || [])
}

let modelProxy = type => ({
  create: async entity => {
    return _.merge(entity, {
      id: '0',
      createdAt: time.fakedDateStr,
      deletedAt: undefined,
      version: 1
    })
  },
  findAll: async ({where} = {}) => {
    let id = where && where.id

    let data = !id
      ? mocks.types
      : (
        mocks.types[id] ||
        _.pickBy((v, k) => v[0].id === where.id, mocks.templates)
      )

    return _.flatten(_.values(data))
  },
  findOne: async ({where: {id, version}}) => {
    return (
      getEntity(id, version, mocks.types) ||
      getEntity(id, version, mocks.templates)
    )
  }
})

let getModels = () => ({
  type: modelProxy('type'),
  eventGenerator: modelProxy('eventGenerator'),
  nameTag: modelProxy('nameTag')
})

let getModelsNames = () =>
  _.keys(getModels())

module.exports = {getModels, getModelsNames}
