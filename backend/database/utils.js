let _ = require('generic_utils')

let findLatestVersion = list =>
  _.reduce((a, b) => a && a.version > b.version ? a : b, null, list)

module.exports = {findLatestVersion}
