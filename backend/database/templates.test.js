jest.unmock('fs-extra')
jest.unmock('database/db_init')

let _ = require('generic_utils')
let {readdir} = require('fs-extra')
let {join} = require('path')

test('templates ids unique', () =>
  readdir(join(__dirname, 'templates'))
    .then(_.pipe(
      _.filter(_.endsWith('.json')),
      _.map(p => require(join(__dirname, 'templates', p)))))
    .then(_templates => {
      let templates = _.map(_.head, _templates)

      _.forEach(
        template => expect(template.id).toBeDefined(), // missing ID
        templates)

      _.forEach(
        items => expect(items.length).toEqual(1), // duplicate IDs',
        _.values(_.groupBy(_.prop('id'), templates))
      )
    })
)
