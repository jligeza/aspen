let _ = require('generic_utils')

let byName = {
  set: '19d2ce7f-2a65-4f64-98fd-7550af6850a2'
}

let byId = _.invertObj(byName)

module.exports = {byName, byId}
