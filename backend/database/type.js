let _ = require('generic_utils')
let {saveEntity, readType} = require('./shared')
let {findAllTypes} = require('./find')

let findTypeDeps = _schema => {
  let job = ({path = [], findings = []} = {}, schema) => {
    let val = _.path(path, schema)

    if (_.has('$id', val)) {
      return _.append(_.assoc('$idVersion', `${val.$id}/${val.$version}`, val), findings)
    }
    if (_.isRecord(val)) {
      return _.chain(k => job({path: [k], findings}, val), _.keys(val))
    }
    if (_.is(Array, val)) {
      return _.chain(a => job({path: [], findings}, a), val)
    }
    return findings
  }

  return job(undefined, _schema)
}

let resolveDeps = _.curry((_resolvedTypes, toResolve) =>
  Promise.all(_.map(
    dep => readTypeOpt({version: dep.$version, _resolvedTypes}, dep.$id),
    toResolve
  ))
    .then(results =>
      results.reduce((acc, type) => {
        return type ? _.merge(type, acc) : acc
      }, {})
    ))

// Just take "schema" and ignore all metadata.
let flattenDeps = ({key = 'schema', deps}) =>
  _.reduce((acc, [k, v]) => {
    acc[k] = v.schema
    return acc
  }, {}, _.toPairs(deps))

/*
 * Example:
 *
 * input: {a: {$id: 123, $version: 1}},
 * output: {
 *   $main: {a: {$id: 123, $version: 1}},
 *   $rest: {'123/1': {a: 'Int'}}
 * }
 */
let resolveTypeDepsInSchema = schema =>
  resolveDeps({}, findTypeDeps(schema))
    .then(deps => ({
      $main: schema,
      $rest: flattenDeps({deps})
    }))

let readTypeOpt = _.curry(({ version, _resolvedTypes = {} } = {}, id) =>
  readType({id, version})
    .then(data => {
      if (!data) return data

      let schema = _.prop('schema', data)
      let foundTypeDeps = findTypeDeps(schema)

      let dataKey = `${id}/${data.version}`
      let types = _.merge(_resolvedTypes, {[dataKey]: data})

      let depsToResolve = _.reject(
        _.pipe(_.prop('$idVersion'), _.has(_.__, _resolvedTypes)),
        foundTypeDeps)

      return resolveDeps(types, depsToResolve)
        .then(_.merge(types))
    }))

let saveType = type =>
  saveEntity({
    entity: type,
    type: 'Type',
    readEntity: readType
  })

let listTypes = () =>
  findAllTypes()

module.exports = {
  listTypes,
  readType,
  readTypeOpt,
  resolveTypeDepsInSchema,
  saveType
}
