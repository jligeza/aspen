let _ = require('generic_utils')
let s = require('./shared')
let {findById} = require('./find')
let dbQueries = require('./db_queries')

let nameTagSchema = {
  updateKeys: [
    {name: 'id', type: 'id'}
  ],
  keys: [
    {name: 'targetId', type: 'id'},
    {name: 'targetVersion', type: 'number', required: false},
    {name: 'schemaPath', type: 'list', extraReqs: ['validSchemaPath']},
    {name: 'name', type: 'string'}
  ]
}

let getTypeErrorMsg = reqs =>
  `${reqs.name}: expected ${reqs.type}`

function findSchemaMemberByUid (uId, schema) {
  if (!_.is(Array, schema)) return null

  return schema.find(member => {
    if (_.is(String, member)) {
      return member === uId
    } else {
      return member.$choice === uId
    }
  })
}

function readSchemaStep (step, schema) {
  return _.is(Array, schema)
    ? findSchemaMemberByUid(step, schema)
    : schema[step]
}

function readSchemaPath (path = [], schema) {
  if (path.length === 0 || !schema) {
    return schema
  } else if (path.length === 1) {
    return readSchemaStep(path[0], schema)
  } else {
    return readSchemaPath(path.slice(1), readSchemaStep(path[0], schema))
  }
}

let checkExtraListReqs = (reqs, tSchema, given) =>
  _.findExec(req => {
    switch (req) {
      case 'validSchemaPath':
        return _.path(given, tSchema)
          ? null
          : `${reqs.name}: no such path in schema`

      default:
        return `technical error: checkExtraListReqs "${req}"`
    }
  }, reqs.extraReqs)

let checkKey = (reqs, tSchema, given) => {
  if (!reqs.required && _.isNil(given)) return null

  switch (reqs.type) {
    case 'id':
      return _.is(String, given) && given.length === 36
        ? null
        : getTypeErrorMsg(reqs)

    case 'number':
      return _.is(Number, given)
        ? null
        : getTypeErrorMsg(reqs)

    case 'string':
      return _.is(String, given)
        ? null
        : getTypeErrorMsg(reqs)

    case 'list':
      return _.is(Array, given)
        ? checkExtraListReqs(reqs, tSchema, given)
        : getTypeErrorMsg(reqs)

    default:
      return `technical error: type "${reqs.type}" not supported`
  }
}

let checkAllKeys = (rules, vd) =>
  _.findExec(
    reqs => checkKey(reqs, vd.tSchema, vd.toCheck[reqs.name]),
    rules
  )

let validateCreation = vd =>
  checkAllKeys(nameTagSchema.keys, vd)

let validateUpdate = vd =>
  checkAllKeys(nameTagSchema.updateKeys, vd) ||
  checkAllKeys(nameTagSchema.keys, vd)

let validate = vd =>
  (_.has('id', vd.toCheck) ? validateUpdate : validateCreation)(vd)

let readNameTag = ({id, version}) =>
  s.readEntity({type: 'NameTag', id, version})

let save = (nameTag, target) => {
  let toSave = _.merge(nameTag, {
    targetVersion: target.data.version
  })

  return dbQueries.create('nameTag', toSave).then(_.Right)
}

let update = async (nameTag, target) => {
  let prevVersion = await readNameTag({id: nameTag.id, version: nameTag.version})

  if (!prevVersion) return _.Left('no such name tag')

  let toSave = _.merge(nameTag, {
    targetVersion: target.data.version,
    version: prevVersion.version + 1
  })

  return dbQueries.update('nameTag', toSave).then(_.Right)
}

let saveNameTag = async nameTag => {
  let target = await findById({id: nameTag.targetId, version: nameTag.targetVersion})

  if (!target) {
    return _.Left('target not found')
  } else if (target.type === 'NameTag') {
    return _.Left('cannot add name tag to a name tag')
  } else {
    let vError = validate({tSchema: target.data.schema, toCheck: nameTag})

    return vError
      ? _.Left(vError)
      : _.has('id', nameTag)
        ? update(nameTag, target)
        : save(nameTag, target)
  }
}

let listNameTags = () => {
  return dbQueries.findLatestVersions('nameTag')
    .then(_.groupBy(_.prop('targetId')))
}

module.exports = {
  listNameTags,
  readNameTag,
  saveNameTag,
  readSchemaPath
}
