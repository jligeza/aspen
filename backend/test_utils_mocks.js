let _ = require('generic_utils')

let origDateNow = Date.now
let time = {
  fakedDateStr: '2018-04-01T20:01:26+02:00',
  fake: () => { Date.now = () => 1522605686498 },
  restore: () => { Date.now = origDateNow }
}

let mockId = id =>
  `${id}`.padStart(8, 0).concat(`-0000-4000-0000-000000000000`)

let Entity = p => ({
  id: mockId(p.id),
  type: p.type,
  version: p.version || 1,
  schema: p.schema,
  createdAt: p.createdAt || time.fakedDateStr,
  deletedAt: p.deletedAt
})

let Type = p => Entity(_.merge(p, {type: 'Type'}))
let EventGenerator = p => Entity(_.merge(p, {type: 'EventGenerator'}))
let Fn = p => Entity(_.merge(p, {type: 'Function'}))

let types = [
  Type({id: '1', schema: {a: 'Int'}}),
  Type({id: '2', schema: {a: {$id: mockId('3'), $version: 1}}}),
  Type({id: '3', schema: {c: 'String'}}),
  Type({id: '4', schema: 'a'}),
  Type({id: '5', schema: {$type: 'List', $valsType: 'a'}}),
  Type({id: '6', schema: {x: 'a'}}),
  Type({id: '7', schema: {x: {y: 'a'}}}),
  Type({
    id: '8',
    schema: [
      'Nothing',
      {$type: 'UnionMember', $choice: 'Just', $content: 'a'}]
  }),
  Type({id: '9', schema: {$id: mockId('8'), $version: 1, $generics: {a: 'String'}}}),
  Type({id: '10', schema: {$id: mockId('6'), $version: 1, $generics: {a: 'String'}}})
]
  .reduce((acc, a) => {
    acc[a.id] = _.castArray(a)
    return acc
  }, {})

let templates = {
  'http.json': [EventGenerator({
    id: '1001',
    schema: {
      $config: {
        routes: {
          $type: 'List',
          $valsType: {url: 'String', method: ['Get', 'Post', 'Put', 'Delete']}
        },
        port: 'Int'
      },
      $defaults: {
        routes: [],
        port: 0
      },
      $emits: {payload: 'Json', path: 'String'},
      $callback: {status: ['Success', 'Error'], payload: 'Json'}
    }
  })],
  'identity.json': [Fn({
    id: '1002',
    schema: {
      $input: 'a',
      $output: 'a'
    }
  })],
  'complex.json': [Fn({
    id: '1003',
    schema: {
      $input: {$id: mockId('2'), $version: 1},
      $output: {$id: mockId('1'), $version: 1}
    }
  })],
  'genericClash.json': [Fn({
    id: '1004',
    schema: {
      $input: {
        a: {$id: mockId('4'), $version: 1},
        b: {$id: mockId('4'), $version: 1}
      },
      $output: 'a'
    }
  })]
}

module.exports = {
  types,
  mockId,
  templates,
  time,
  Entity,
  Type,
  EventGenerator,
  Fn
}
