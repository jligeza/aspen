let _ = require('generic_utils')
let mocks = require('test_utils_mocks')

let prettyTestName = (index, mainDesc, caseDesc) =>
  `${(index + 1).toString().padStart(3, '0')} ${mainDesc}: ${caseDesc}`

let simpleTests = (title, testFn, cases) =>
  describe(title, () =>
    _.forEachIndexed(
      ({desc, xdesc, input, output}, index) =>
        (xdesc ? xtest : test)(prettyTestName(index, title, desc), () =>
          Promise.resolve(testFn(..._.castArray(input)))
            .then(result => expect(result).toEqual(output))),
      cases))

let UnionMember = ($choice, $content) => {
  let base = {$type: 'UnionMember', $choice}

  return $content ? _.merge(base, {$content}) : base
}

let testErrorsCorrectness = (prefix, errors, cases) => {
  cases.forEach(c => {
    if (!c.output) return

    test(`${prefix} - correct error output, in "${c.desc}"`, () => {
      let givenKeys = _.reject(_.eq('id'), _.keys(c.output || {}))
      let matchingError = errors[c.output.id]

      expect(matchingError).toBeTruthy()

      let expectedKeys = matchingError.info || []

      expect(givenKeys.sort()).toEqual(expectedKeys.sort())
    })
  })
}

let getRef = (id, version) =>
  ({$id: mocks.mockId(id), $version: version || 1})

let refs = {
  int: getRef('1'),
  multi: getRef('2'),
  string: getRef('3'),
  generic: getRef('4'),
  genericList: getRef('5'),
  genericRecord: getRef('6'),
  genericRecordNested: getRef('7'),
  maybe: getRef('8'),
  maybeString: getRef('9'),
  resolvedGenericRecord: getRef('10'),
  unknownId: getRef('999999'),
  unknownVersion: getRef('1', 9000),
  http: getRef('1001'),
  identity: getRef('1002'),
  complex: getRef('1003'),
  genericClash: getRef('1004')
}

let joinIdVersion = typeKey =>
  `${refs[typeKey].$id}/${refs[typeKey].$version}`

let getMock = typeKey =>
  mocks.types[refs[typeKey].$id][0]

let getMockSchema = typeKey =>
  mocks.types[refs[typeKey].$id][0].schema

module.exports = _.merge(mocks, {
  getMock,
  getMockSchema,
  refs,
  joinIdVersion,
  prettyTestName,
  simpleTests,
  UnionMember,
  testErrorsCorrectness
})
