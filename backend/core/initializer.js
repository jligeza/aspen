let _ = require('generic_utils')
let {init, subscribeWrite, fetchRoots, findRoot} = require('database')
let {dispatch} = require('./dispatcher')
let {addNode} = require('./nodes')

let data = {
  initializedEventGenerators: []
}

subscribeWrite((query, result) => {
  if (query.type === 'eventGenerator' && result.ok) {
    findRoot(result.val.id).then(query.payload.id ? onRootUpdate : onRootCreate)
  }
})

function onRootCreate (data) {
  data && initializeRoot(data)
}

function onRootUpdate (data) {
  if (!data) return

  data.initializedEventGenerators = data.initializedEventGenerators
    .filter(a => a.id !== data.instanceData.id)

  initializeRoot(data)
}

let initializeRoot = ({template, instanceData}) => {
  if (template.data.type !== 'EventGenerator') {
    throw new Error('not implemented')
  }

  let codeResult = template.code({
    config: instanceData.schema.$config,
    dispatch: msg => dispatch({
      msg,
      meta: {template, instanceData: instanceData.dataValues}
    })
  })

  return codeResult.run()
    .then(closeFnOrErr => {
      let shared = {dbEntry: instanceData, misc: codeResult.data}

      let node = {
        id: instanceData.id,
        version: instanceData.version,
        type: template.data.type,
        data: closeFnOrErr.ok
          ? _.Right(_.merge({close: closeFnOrErr.val}, shared))
          : _.Left(_.merge({error: closeFnOrErr.val}, shared))
      }

      data.initializedEventGenerators.push(node)

      addNode(node)
    })
}

let initializeEventGenerators = () => {
  data.initializedEventGenerators = []

  return fetchRoots()
    .then(_.map(initializeRoot))
    .then(_.promiseAll)
}

let initialize = () =>
  init()
    .then(initializeEventGenerators)

let getInitializedEventGenerators = () =>
  data.initializedEventGenerators

module.exports = {
  getInitializedEventGenerators,
  initialize
}
