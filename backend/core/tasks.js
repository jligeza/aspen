let _ = require('generic_utils')
let db = require('database')
let initializer = require('core/initializer')

let readType = query =>
  db.read({type: 'type', payload: query.payload})
    .then(_.Right)
    .catch(e => {
      console.error(e)
      return _.Left('readType: unknown technical error')
    })

let saveType = query =>
  db.write({type: 'type', payload: query.payload})

let saveEventGenerator = query =>
  db.write({type: 'eventGenerator', payload: query.payload})

let saveNode = query =>
  db.write({type: 'node', payload: query.payload})

let saveLink = query =>
  db.write({type: 'link', payload: query.payload})

let saveConst = query =>
  db.write({type: 'const', payload: query.payload})

let listTypes = query =>
  db.list({type: 'type', payload: query.payload})

let listNameTags = query =>
  db.list({type: 'nameTag', payload: query.payload})

let listConsts = query =>
  db.list({type: 'const', payload: query.payload})

let listLinks = query =>
  db.list({type: 'link', payload: query.payload})

let listNodes = query =>
  db.list({type: 'node', payload: query.payload})

let listInitializedEventGenerators = () =>
  initializer.getInitializedEventGenerators().map(a => {
    let vals = a.data.val.dbEntry.dataValues

    return _.dissoc('schema', _.merge(vals, {
      config: vals.schema.$config,
      template: vals.schema.$template,
      error: a.data.val.error || null
    }))
  })

let listActiveNodes = async query =>
  _.assoc(
    'eventGenerators',
    listInitializedEventGenerators(),
    await db.list({type: 'activeNode', payload: query.payload})
  )

let listTemplates = query =>
  db.list({type: 'template', payload: query.payload})

let saveNameTag = query =>
  db.write({type: 'nameTag', payload: query.payload})

let taskMap = {
  listInitializedEventGenerators,
  listActiveNodes,
  listNameTags,
  listTypes,
  listTemplates,
  listConsts,
  listLinks,
  listNodes,
  readType,
  saveEventGenerator,
  saveNode,
  saveLink,
  saveNameTag,
  saveType,
  saveConst
}

let executeTask = async task => {
  let type = task.taskType

  let taskHandler = taskMap[type]
  if (taskHandler) {
    return Promise.resolve(taskHandler(task))
      .then(result => {
        if (!_.has('ok', result) || result.ok) {
          return result
        } else {
          throw result
        }
      })
  } else {
    return _.throw('unknown task type: ' + type)
  }
}

module.exports = {executeTask}
