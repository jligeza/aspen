let {findConnection} = require('database')

async function callNode (conn) {
  let nextConn = await conn.next()
  if (nextConn) {
    return callNode(nextConn)
  }
  return conn.newMsg
}

async function dispatch ({meta, msg}) {
  console.log('instance', meta.instanceData)
  console.log('msg', msg)

  let conn = await findConnection(meta.instanceData.id, msg)
  console.log('conn', conn)

  if (conn) {
    let newMsg = await callNode(conn)

    console.log('newMsg', newMsg)

    if (newMsg && newMsg.return && newMsg.callback) {
      newMsg.callback(newMsg.return)
    } else {
      newMsg.fallback && newMsg.fallback()
    }
  } else {
    msg.fallback && msg.fallback()
  }
}

module.exports = {dispatch}
