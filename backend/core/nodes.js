let _ = require('generic_utils')

let nodes = {}

let reqKeys = ['id', 'version', 'type', 'data']

let nodeAction = _.curry((action, node) => {
  let extraKeys = _.without(reqKeys, _.keys(node))
  let missingKeys = _.without(_.keys(node), reqKeys)

  if (_.isFilled(missingKeys)) {
    _.throw(`missing keys: ${missingKeys.join(', ')}`)
  }

  if (_.isFilled(extraKeys)) {
    _.throw(`extra keys: ${missingKeys.join(', ')}`)
  }

  return action(node)
})

let addNode = nodeAction(node => {
  nodes[node.id] = node
})

let deleteNode = nodeAction(node => {
  delete nodes[node.id]
})

let getNode = id => nodes[id]

module.exports = {addNode, deleteNode, getNode}
