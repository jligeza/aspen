let R = require('ramda')

let isRecord = a => a instanceof Object && a.constructor === Object

let isFilled = R.complement(R.isEmpty)

let same = xs =>
  xs.length > 1
    ? R.uniq(xs).length < xs.length
    : null

let Left = val => ({ok: false, val})
let Right = val => ({ok: true, val})

let checkKeys = R.curry((template, given) => {
  let missing = R.map(key => ({type: 'missing', key}), R.without(given, template))
  let unknown = R.map(key => ({type: 'unknown', key}), R.without(template, given))

  return R.concat(missing, unknown)
})

let stringifyWithUndefined = (obj, space) =>
  JSON.stringify(
    obj,
    (key, value) =>
      value === undefined
        ? null
        : typeof value === 'function'
          ? '<Function>'
          : value,
    space)

let logS = R.curry((key, val) => {
  console.log(`${key}: ${stringifyWithUndefined(val, 2)}`)
  return val
})

let log = R.curry((key, val) => {
  console.log(`${key}: ${val}`)
  return val
})

let findExec = R.curry((fn, list) => {
  let job = (idx = 0) => {
    let res = fn(list[idx])

    return res || (idx + 1 > list.length - 1
      ? null
      : job(idx + 1))
  }

  return job()
})

let extraFns = {
  Either: R.curry((ok, val) => ok ? Right(val) : Left(val)),
  Left,
  Right,
  isLeft: R.propEq('ok', false),
  isRight: R.propEq('ok, true'),

  I: a => a,
  K: a => b => a,

  castArray: a => R.is(Array, a) ? a : [a],
  checkKeys,
  compact: R.reject(R.isNil),
  eq: R.equals,
  false: R.F,
  findExec,
  forEachIndexed: R.addIndex(R.forEach),
  gt_: R.flip(R.gt),
  gte_: R.flip(R.gte),
  isFilled,
  isFilledRecord: R.allPass([isRecord, isFilled]),
  isInteger: a => Number.isInteger(a),
  isRecord,
  isTrueNumber: R.both(R.is(Number), R.complement(R.equals(NaN))),
  isTruthy: R.complement(R.isNil),
  isUndefined: a => a === undefined,
  log,
  logS,
  lt_: R.flip(R.lt),
  lte_: R.flip(R.lte),
  neq: R.complement(R.equals),
  parseInt: a => parseInt(a),
  promiseAll: a => Promise.all(a),
  resolve: a => Promise.resolve(a),
  result: a => R.is(Function, a) ? a() : a,
  same,
  throw: msg => { throw new Error(msg) },
  true: R.T,
  upperFirst: R.replace(/^(\w)/, R.toUpper)
}

module.exports = R.merge(R, extraFns)
