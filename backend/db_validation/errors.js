let _ = require('generic_utils')
let {mainEntitiesSchemas} = require('./schemas')

module.exports = {
  expectedInputRecord: () =>
    'expected input to be a record',

  unknownType: type =>
    `unknown type: ${type}, available: ${_.keys(mainEntitiesSchemas).join(', ')}`,

  missingKeys: keys =>
    `missing keys: ${keys.join(', ')}`,

  unknownKeys: keys =>
    `unknown keys: ${keys.join(', ')}`,

  typeNotImplemented: type =>
    `validation of type "${type}" not implemented`
}
