let _ = require('generic_utils')

let reqs = {
  isString:
    ['expected a string', _.is(String)],

  isId: [
    'expected UUID',
    _.both(_.is(String), _.pipe(_.length, _.eq(36)))
  ],

  notEmpty: [
    'must not be empty',
    _.ifElse(_.either(_.is(Array), _.isRecord), _.isFilled, _.isTruthy)
  ],

  filledRecord:
    ['expected a filled record', _.isFilledRecord],

  record:
    ['expected a record', _.isRecord]
}
let r = reqs

let mainEntitiesSchemas = {
  Type: {
    requiredKeys: ['schema'],

    attrs: {
      id: [
        r.isId
      ],
      schema: [
        r.notEmpty
      ]
    }
  }
}

module.exports = {mainEntitiesSchemas}
