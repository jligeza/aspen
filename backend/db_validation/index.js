let _ = require('generic_utils')
let {flattenSchema} = require('validation/data')
let {getSchemaType, isBaseType} = require('validation/types')
let err = require('./errors')
let {mainEntitiesSchemas} = require('./schemas')
let {isGenericArg, getFullPath, findGenerics} = require('validation/utils')

let errors = {
  'S1': {
    desc: 'unknown type',
    info: ['path']
  },
  'S2': {
    desc: 'type definition not found',
    info: ['path']
  },
  'S3': {
    desc: 'no such version available',
    info: ['path', 'givenId', 'givenVersion', 'availableVersions']
  },
  'S4': {
    desc: 'no such generics',
    info: ['path', 'givenExtra', 'available']
  },
  'S5': {
    desc: 'no generics available',
    info: ['path']
  }
}

let checkRef = async vd => {
  let val = vd.val

  if (!val.$generics) {
    return []
  } else {
    let refGenerics = _.uniq(findGenerics(vd.meta.schema).map(a => a.key))
    let valGenerics = _.keys(val.$generics)

    let givenExtra = _.without(refGenerics, valGenerics)

    if (givenExtra.length) {
      return [{
        id: 'S4',
        path: _.append('$generics', getFullPath(vd)),
        givenExtra,
        available: refGenerics
      }]
    } else {
      let prevPath = getFullPath(vd).concat('$generics')
      let errs = _.flatten(await Promise.all(valGenerics.map(path => {
        return schemaErrorReports({path: [path], prevPath}, _.path(prevPath, vd.schema))
      })))
      return _.compact(_.castArray(_.head(errs)))
    }
  }
}

let checkList = vd => {
  return schemaErrorReports(
    {path: ['$valsType'], prevPath: getFullPath(vd)},
    vd.val
  )
}

let checkFunction = vd => {
  return []
}

let isNormalRecord = val =>
  _.isRecord(val) && _.any(_.complement(_.startsWith('$')), _.keys(val))

let isS1 = ({sType}) =>
  !isGenericArg(sType) && !isBaseType(sType)

let isS2 = vd =>
  _.has('$id', vd.val) && _.isNil(vd.sType) && _.isNil(vd.meta)

let isS3 = vd =>
  _.has('$id', vd.val) && _.isNil(vd.sType) && vd.meta

let s3 = vd => [{
  id: 'S3',
  path: getFullPath(vd),
  givenId: vd.val.$id,
  givenVersion: vd.val.$version,
  availableVersions: _.map(_.parseInt, _.pluck('version', vd.meta))
}]

let schemaErrorReports = async ({path = [], prevPath = []} = {}, schema) => {
  let val = _.path(path, schema)
  if (isNormalRecord(val)) {
    let errs = _.flatten(await Promise.all(
      _.map(
        k => schemaErrorReports(
          {path: [k], prevPath: getFullPath({path, prevPath})},
          val
        ),
        _.keys(val))))

    return _.compact(_.castArray(_.head(errs)))
  }

  let {val: sType, meta} = await getSchemaType(_.has('$id', schema) ? schema : val)

  let vd = {path, prevPath, schema, sType, meta, val}
  // _.logS('vd', vd)

  return (
    (isS3(vd) && s3(vd)) ||
    (isS2(vd) && [{id: 'S2', path: getFullPath(vd)}]) ||
    (isS1(vd) && [{id: 'S1', path: getFullPath(vd)}]) ||
    (meta && await checkRef(vd)) ||
    (sType === 'List' && checkList(vd)) ||
    (sType === 'Function' && checkFunction(vd)) ||
    []
  )
}

let validateStructure = ({type, input}) => {
  let structureReqs = mainEntitiesSchemas[type]

  if (!structureReqs) {
    return [err.unknownType(type)]
  }

  let inputKeys = _.keys(input)
  let structureReqsKeys = structureReqs.requiredKeys

  let missingKeys = _.without(inputKeys, structureReqsKeys)
  if (!_.isEmpty(missingKeys)) {
    return [err.missingKeys(missingKeys)]
  }

  let unknownKeys = _.without(_.keys(structureReqs.attrs), inputKeys)
  if (!_.isEmpty(unknownKeys)) {
    return [err.unknownKeys(unknownKeys)]
  }

  return _.chain(key => {
    let keyRules = structureReqs.attrs[key]
    let val = input[key]

    let brokenRule = _.find(pack => !pack[1](val), keyRules)

    return brokenRule ? [{msg: brokenRule[0], key}] : []
  }, _.keys(input))
}

let validate = _.curry(async (type, input) => {
  // let structureErrors = validateStructure({type, input})
  // if (_.isFilled(structureErrors)) {
  //   return structureErrors
  // }

  return schemaErrorReports(undefined, flattenSchema(input.schema))
})

module.exports = {validate, validateStructure, schemaErrorReports, errors}
