jest.mock('fs-extra')
jest.mock('find')
jest.mock('database/db_init')

let _ = require('generic_utils')
let u = require('test_utils')
let {validate} = require('./index')

let typeCases = [{
  desc: 'ok - simple',
  input: {
    schema: 'Int'
  },
  output: []
}, {
  desc: 'error - unknown type in definition',
  input: {
    schema: 'X'
  },
  output: [{id: 'S1', path: []}]
}, {
  desc: 'ok - simple ref',
  input: {
    schema: u.refs.int
  },
  output: []
}, {
  desc: 'error - unknown ref by ID',
  input: {
    schema: u.refs.unknownId
  },
  output: [{id: 'S2', path: []}]
}, {
  desc: 'error - list with missing ref ID',
  input: {
    schema: {$type: 'List', $valsType: {x: u.refs.unknownId}}
  },
  output: [{id: 'S2', path: ['$valsType', 'x']}]
}, {
  desc: 'ok - generic list',
  input: {
    schema: {$type: 'List', $valsType: 'x'}
  },
  output: []
}, {
  desc: 'ok - ref with data',
  input: {
    schema: _.merge(u.refs.generic, {$generics: {a: 'String'}})
  },
  output: []
}, {
  desc: 'error - simple ref with data, bad key',
  input: {
    schema: _.merge(u.refs.generic, {$generics: {b: 'String'}})
  },
  output: [{id: 'S4', path: ['$generics'], givenExtra: ['b'], available: ['a']}]
}, {
  desc: 'error - list ref with data, bad key',
  input: {
    schema: _.merge(u.refs.genericList, {$generics: {b: 'String'}})
  },
  output: [{id: 'S4', path: ['$generics'], givenExtra: ['b'], available: ['a']}]
}, {
  desc: 'error - record ref with data, bad key',
  input: {
    schema: _.merge(u.refs.genericRecord, {$generics: {b: 'String'}})
  },
  output: [{id: 'S4', path: ['$generics'], givenExtra: ['b'], available: ['a']}]
}, {
  desc: 'error - union ref with data, bad key',
  input: {
    schema: _.merge(u.refs.maybe, {$generics: {b: 'String'}})
  },
  output: [{id: 'S4', path: ['$generics'], givenExtra: ['b'], available: ['a']}]
}, {
  desc: 'ok - use Maybe',
  input: {
    schema: _.merge(u.refs.maybe, {$generics: {a: 'String'}})
  },
  output: []
}, {
  desc: 'error - unknown type in ref generics',
  input: {
    schema: _.merge(u.refs.maybe, {$generics: {a: 'X'}})
  },
  output: [{id: 'S1', path: ['$generics', 'a']}]
}, {
  desc: 'error - unknown nested type in ref generics',
  input: {
    schema: _.merge(u.refs.maybe, {$generics: {a: {b: 'X'}}})
  },
  output: [{id: 'S1', path: ['$generics', 'a', 'b']}]
}]

u.simpleTests(
  'validate Type creation',
  a => validate('Type', a),
  typeCases
)
